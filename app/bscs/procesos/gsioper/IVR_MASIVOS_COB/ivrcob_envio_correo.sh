#=========================================================================================================#
# PROYECTO            : [10798] GESTI�N DE COBRANZAS V�A IVR Y GESTORES A TRAV�S A CLIENTES CALIFICADOS
# LIDER CONECEL       : SIS Xavier Trivi�o
# LIDER PDS  		  : SUD Richard Rivera
# DESARROLLADOR	      : SUD Dennise Pintado
# FECHA               : 01/11/2016
# COMENTARIO          : Envio de correo con adjunto para el m�dulo de IVR Cobranzas
#=========================================================================================================#

#PROFILE PRODUCCION########################################################################
. /home/gsioper/.profile


# DESARROLLO
###########################################################################################
#. /home/oracle/profile_BSCSD


#---------------------#
# Limpio pantalla
#---------------------#
clear
#---------------------#


#----------------------------------#
# Password Desarrollo
#----------------------------------#
#user="sysadm"
#pass="sysadm"  


#----------------------------------#
# Password Produccion
#----------------------------------#
user="sysadm"
pass=`/home/gsioper/key/pass $user` 


#----------------------------------#
# Rutas Desarrollo / Produccion
#----------------------------------#
ruta_shell="/procesos/gsioper/IVR_MASIVOS_COB"
ruta_logs="/procesos/gsioper/IVR_MASIVOS_COB/logs"
ruta_reporte="/procesos/gsioper/IVR_MASIVOS_COB/reportes"


#----------------------------------#
# Variables:
#----------------------------------#
fecha=`date +'%Y''%m''%d'`	   #--fecha de ejecucion
archivo="ivr_cob_envio_correo"
archivo_log="ivr_cob_envio_correo_"$fecha.log


#----------------------------------#
# Validacion Shell
#----------------------------------#
ontrolFILE=$archivo.pid
shadow=$ruta_shell/$ontrolFILE
if [ -f $shadow ]; then
  cpid=`cat $shadow`
  if [ `ps -edaf | grep -w $cpid | grep $0 | grep -v grep | wc -l` -gt 0 ]
    then
    echo "$0 already running"
    exit 0
  fi
fi
echo $$ > $shadow


echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO ENVIO DE CORREO PARA IVR ==------ \n" > $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== INICIO ENVIO DE CORREO PARA IVR ==------ \n" 
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomara unos minutos ... \n\n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'`          Este proceso tomara unos minutos ... \n\n"


#----------------------------------#
# Envio de correo
#----------------------------------#
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio Generacion de archivo csv de cuentas para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio Generacion de archivo csv de cuentas para IVR... \n"

cat>$ruta_shell/ivrcob_envio_correo.sql<<EOF
SET SERVEROUTPUT ON
declare
Lv_Error varchar2(200);
lv_nombre_archivo varchar2(100);
begin

   mfk_trx_envio_ejecuciones_ivr.mfp_reproceso_ctas(pv_archivo => lv_nombre_archivo,
                                                    pv_error   => lv_error);
   dbms_output.put_line('ARCHIVO:'|| lv_nombre_archivo);
   commit;
exception
 when others then
  rollback;
  Lv_Error:=substr(sqlerrm,1,200);
  Raise_Application_Error(-20101, Lv_Error);
end;
/
exit;
EOF
echo $pass | sqlplus -s $user @$ruta_shell/ivrcob_envio_correo.sql | awk '{ if (NF > 0) print}' > ivrcob_envio_correo.log
nombre_archivo_s=`cat ivrcob_envio_correo.log | egrep -w "ARCHIVO" | awk -F\: '{print $2}'`

if [ -n "$nombre_archivo_s" ]; then
	rm -f $ruta_shell/$archivo".sql";
	rm -f $ruta_shell/$archivo".txt";
cat>$ruta_shell/$archivo".sql"<<END
SET SPACE 0
SET PAGESIZE 0  
SET LINESIZE 400
SET COLSEP
set serveroutput off
set feedback off
set heading off
set echo off

spool $ruta_reporte/$nombre_archivo_s
select 'TELEFONO' from dual;
SELECT valor FROM gv_parametros t WHERE t.id_tipo_parametro = 10798 AND t.nombre = 'TELEFONO_IVR'
UNION
SELECT '0'||t.destinatario FROM mf_mensajes_ivr t WHERE t.estado_archivo = 'P';
spool off

END

	echo $pass | sqlplus -s $user @$ruta_shell/$archivo".sql" > $ruta_shell/$archivo".txt"
	#-------------------------------------------------------------------------
	rm -f $ruta_shell/$archivo".sql";
	rm -f $ruta_shell/$archivo".txt";

	canLineas=`cat $ruta_reporte/$nombre_archivo_s | wc -l`

	if [ $canLineas -eq 1 ]; 
	then
	 rm -f  $ruta_reporte/$nombre_archivo_s
	else
	 compress  $ruta_reporte/$nombre_archivo_s
	 chmod 777 $ruta_reporte/$nombre_archivo_s".Z";
	fi

echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin Generacion de archivo csv de cuentas para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin Generacion de archivo csv de cuentas para IVR... \n"


#----------------------------------#
# Envio de archivo
#----------------------------------#
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio Envio del correo con archivo csv de cuentas para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Inicio Envio del correo con archivo csv de cuentas para IVR... \n"
cat>$ruta_shell/$archivo".sql"<<EOF
SET SERVEROUTPUT ON
declare
CURSOR c_categorias IS
  SELECT DISTINCT t.categoria_cli
	FROM mf_mensajes_ivr t
   WHERE t.estado_archivo = 'P'
   ORDER BY t.categoria_cli;

lv_categorias VARCHAR2(2000);
Lv_Error varchar2(1000);
Lv_Name_File VARCHAR2(2000);
ln_total number;
begin
  FOR i IN c_categorias LOOP
    lv_categorias := lv_categorias || '|' || i.categoria_cli;
  END LOOP;
  lv_categorias := substr(lv_categorias, 2);
  smk_consulta_calificaciones.smp_envia_correo(pv_gestion   => 'IVR',
											   pv_categoria => lv_categorias,
											   pv_archivo	=> '$nombre_archivo_s',
											   pv_tipo      => 0,
											   pv_error     => lv_error);
  IF lv_error IS NOT NULL THEN
	dbms_output.put_line('SALIDA:'|| lv_error);
  ELSE
    SELECT nvl(MAX(to_number(t.mensaje)), 0) + 1 INTO ln_total
      FROM mf_mensajes_ivr t
     WHERE t.estado_archivo = 'F';
    -- actualizco con F los que ya fueron enviados por correo
    UPDATE mf_mensajes_ivr
       SET observacion    = 'Enviado por correo - Fecha: ' || to_char(SYSDATE,'dd/mm/yyyy hh24:mi:ss') || ' - Archivo: ' || '$nombre_archivo_s',
           estado_archivo = 'F',
		   mensaje        = ln_total
     WHERE estado_archivo = 'P';
	COMMIT;
    UPDATE mf_bitacora_envio_ivr
       SET observacion    = 'Enviado por correo - Fecha: ' || to_char(SYSDATE,'dd/mm/yyyy hh24:mi:ss') || ' - Archivo: ' || '$nombre_archivo_s',
           estado_archivo = 'F',
		   mensaje        = ln_total
     WHERE estado_archivo = 'P'; 
    COMMIT;
	mfk_trx_envio_ejecuciones_ivr.mfp_actualiza_ejecuciones(pv_error => Lv_Error);
	COMMIT;
  END IF;
end;
/
exit;
EOF
echo $pass | sqlplus -s $user @$ruta_shell/$archivo".sql" | awk '{ if (NF > 0) print}' > ivrcob_actualiza_envio.log

echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin Envio del correo con archivo csv de cuentas para IVR... \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` --- Fin Envio del correo con archivo csv de cuentas para IVR... \n"

verifica_error_ora=`grep "ORA-" ivrcob_actualiza_envio.log| wc -l`
msj_error=`cat ivrcob_actualiza_envio.log | egrep -w "SALIDA" | awk -F\: '{print $2}'`

if [ $verifica_error_ora -gt 0 ] || [ -n "$msj_error" ]
then
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR envio de reporte... \n" >> $ruta_logs/$archivo_log;
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con ERROR envio de reporte... \n"
	 error=`cat ivrcob_actualiza_envio.log`
     echo "$error" >> $ruta_logs/$archivo_log;
     echo "$error"
	 exit 1
else 
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO envio de reporte... \n" >> $ruta_logs/$archivo_log;
	 echo "`date +'%d/%m/%Y %H:%M:%S'` --- Finalizo con EXITO envio de reporte... \n"
   
     rm -f $ruta_shell/$archivo".sql"
	 rm -f $ruta_shell/ivrcob_actualiza_envio.log
	 rm -f $ruta_shell/ivrcob_envio_correo.log
	 rm -f $ruta_shell/ivrcob_envio_correo.sql
	 rm -f $ruta_shell/$ontrolFILE
fi
else
     rm -f $ruta_shell/ivrcob_envio_correo.log
	 rm -f $ruta_shell/ivrcob_envio_correo.sql
	 rm -f $ruta_shell/$ontrolFILE
fi

echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN ENVIO DE CORREO PARA IVR ==------ \n" >> $ruta_logs/$archivo_log;
echo "`date +'%d/%m/%Y %H:%M:%S'` ------== FIN ENVIO DE CORREO PARA IVR ==------ \n"

exit 0
