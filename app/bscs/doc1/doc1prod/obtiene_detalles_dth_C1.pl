#!/opt/perl_64/bin/perl
#!/usr/bin/perl
#!/usr/local/bin/perl
#==========================================================================================#
#-- Version: 	1.0.0
#==========================================================================================#
#-- Proyecto		:	[8693] - DTH - Venta DTH Postpago
#-- Desarrollado por:   SUD Norman Castro J.
#-- Fecha de creacion:	11/05/2013
#-- Lider Claro		:   Ing. Paola Carvajal
#-- Lider PDS		:	SUD Crishtian Acosta Chambers
#-- Descripcion		:   Creacion de archivos .txt para DTH
#==========================================================================================#
#==========================================================================================#
#-- Proyecto		:	[9587] - Factura Electr�nica DTH
#-- Modificado por  :   Sud Cristhian Acosta Ch.
#-- Modificacion    :	31/07/2014
#-- Lider Claro		:   SIS Xavier Trivinio
#-- Lider PDS		:	SUD Crishtian Acosta Chambers
#-- Descripcion		:   Creacion de archivos .txt para Factura Electr�nica DTH
#==========================================================================================#
# Modificado por    : SUD Vanessa Gutierrez G.
# Proyecto			: [10179] Nuevos ciclos de Facturaci�n DTH
# Lider Conecel     : SIS Paola Carvajal
# Objetivo			: Cambio de rutas de archivos .DAT y .TXT
#--=====================================================================================--
# Modificado por    : SUD Laura Pe�a
# Proyecto			: [10203] Adicionales a la factura
# Lider Conecel     : SIS Julia Rodas
# Objetivo			: Mostrar informacion adicional en la factura de los clientes
#--=====================================================================================--
#-- Version: 	1.0.1
#===============================================================================================#
#-- Proyecto		     :	 [10417] - Multipuntos DTH
#-- Modificado por		 :   SUD Evelyn Gonz�lez
#-- Fecha de modificacion:	 27/01/2016
#-- Lider Claro		     :   Ing. Oscar Apolinario
#-- Lider PDS		     :	 SUD Silvana Pluas
#-- Descripcion		     :   Control de esquema DTH residencial y Multipunto.
#--				 			 Creaci�n anexos Multipuntos
#===============================================================================================#
# Variables #

%MAPEOGeneral;
%MAPEOSuma;
%MAPEOTotales;
%MAPEOImprime;
%MAPEOImprimeUno;
%banderaUno;
%Graba_co_id;
%VerificaNumber;
%Mdetalle;
%Mdetallefinal;
%Mgrupodth;
%Mgrupodth_lineas; #mapeos lineas 20000 -20200

%MAPEOImprime2; #10179
%anexo_b;  #suma de servicios por sncode #10417
%anexo_c;#suma de servicios por coid y fecha 10417

my %MAPEOmpuzotab;
my %MAPEOlegend;
my %MAPEOmpusntab;
my %MAPEOmpuzntab;
my %MAPEOcall_grouping;
my %MAPEOpaymenttype_all;
my %MAPEOservice_label;
my %MAPEOservice_label2;
my %MAPEOmpulktmb;
my %MAPEOmpulktmbANT;
my %MAPEOrateoplan;
my %MAPEOServicesExt;
my %MAPEObgh_tariff_plan;
my %MAPEOImprimeDetLlamadas;
my %MAPEOImprimeConsumoCelular;
#my %MAPEObgh_tariff_plan; #cambio 03-04-2016
my %MAPEOtax_category;
my %MAPEOinfo_contr_text;
my %MapeoSecuencias;
my %Mapeogrupodth;
my %MAPEOtax_billcycle;#SUD ICE
my %MAPEOtax_codigo_sri;#SUD
my %MAPEOtax_filetax_category_iva;#SUD ICE
my %MAPEOtax_filetax_category_ice;#SUD ICE
my %MAPEOserviciosMtpDTH; #10417


my $procesador=$ARGV[0];
my $ciclo=$ARGV[1];
my $ruta=$ARGV[2]."/";
my $tipo_ejecucion=$ARGV[3]; # Control [G]ruop - [C]ommit
my $cuenta_out_llam=$ARGV[4];
if( $#ARGV < 3 ) {
	  print "NO hay argumentos \n";
	  die "error $!";
}
$filempuzotab=$ruta."resources/mpuzotab.dat";
$filelegend=$ruta."resources/legend_doc1.dat";
$filempusntab=$ruta."resources/mpusntab.dat";
$filempuzntab=$ruta."resources/mpuzntab.dat";
$filecall_grouping=$ruta."resources/call_grouping_doc1.dat";
$filepaymenttype_all=$ruta."resources/paymenttype_all.dat";
$fileservice_label=$ruta."resources/service_label_doc1.dat";
$filempulktmb=$ruta."resources/mpulktmb_dth.dat";
$filempulktmbANT=$ruta."resources/mpulktmbANT.dat";
$filerateplan=$ruta."resources/rateplan.dat";
$filebgh_tariff_plan=$ruta."resources/bgh_tariff_plan_doc1.dat";
$filetax_category=$ruta."resources/tax_category_doc1.dat";
$fileinfo_contr_text=$ruta."resources/info_contr_text.dat";
$fileServices_ext=$ruta."resources/services_ext.dat";
$ruta_adicional=$ruta."adicionales";
$archivo_sec=$ruta."PCE/";
$fecha=`date '+%Y-%m-%d-%H-%M-%S'`;
$fecha=&enceraEspacios($fecha);
$archivo_logs=$ruta."log/DOC1_DTH_".$procesador."_".$ciclo."_".$tipo_ejecucion."-".$fecha.".log";
$filexcento_iva=$ruta."resources/excento_iva.dat";
$fileno_etiquetas=$ruta."resources/etiqueta_no_impresa.dat";
$file_nuevorubro=$ruta."resources/nuevos_rubros.dat";
$file_grupodth=$ruta."resources/grupo_dth.dat";
$indexHashUnico = 0;
$file_abonados_feature=$ruta."resources/abonados_DTH_fechaSVA_".$ciclo.".dat";  ## [10203]
$file_abonados_plan=$ruta."resources/abonados_DTH_CtasPlan_".$ciclo.".dat";     ## [10203]
$file_abonados_FeatCta=$ruta."resources/abonados_DTH_FeatCta_".$ciclo.".dat";     ##  [10203] SUD LPE
#$file_fecha_corte=$ruta."resources/fecha_corte.dat";     ##  [10203] SUD LPE
#SUD ICE
$filetax_billcycle=$ruta."resources/tax_billcycle.dat";
$filetax_category_iva=$ruta."resources/tax_conf_iva.dat";
$filetax_category_ice=$ruta."resources/tax_conf_ice.dat";
$file_serviciosMtpDTH=$ruta."resources/servicios_multipuntoDTH_".$ciclo.".dat";#10417
$filetax_codigo_sri=$ruta."resources/codigo_sri.dat";
#Llamadas
$cont_calculo_mayor=0;
&grabo_informacion;
&llama_tim;

sub grabo_informacion{
	#SUD INI MLU
	$line="";	
	open(FILE_READ_TMP, $filetax_codigo_sri);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$MAPEOtax_codigo_sri{$arreglo_lin[0]}=[$arreglo_lin[1]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filetax_codigo_sri." \n";
		die;
	}
	#SUD FIN MLU
	#SUD INI ICE	
	$line="";	
	open(FILE_READ_TMP, $filetax_billcycle);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $dia_corte=&enceraEspacios($arreglo_lin[2]);
		$MAPEOtax_billcycle{$arreglo_lin[0]}=[$arreglo_lin[1]."|".$dia_corte];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filetax_billcycle." \n";
		die;
	}
	
	$line="";	
	open(FILE_READ_TMP, $filetax_category_iva);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $factor_iva=&enceraEspacios($arreglo_lin[4]);	
		$MAPEOtax_filetax_category_iva{$arreglo_lin[1]."|".$arreglo_lin[3]}=[$arreglo_lin[2],$factor_iva];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filetax_category_iva." \n";
		die;
	}

	$valor_etiqueta_iva=$MAPEOtax_filetax_category_iva{$MAPEOtax_billcycle{$ciclo}[0]}[0]; 
	$valor_iva_conf=$MAPEOtax_filetax_category_iva{$MAPEOtax_billcycle{$ciclo}[0]}[1];	
	$line="";	
	open(FILE_READ_TMP, $filetax_category_ice);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $factor_ice=&enceraEspacios($arreglo_lin[4]);	
		$MAPEOtax_filetax_category_ice{$arreglo_lin[1]."|".$arreglo_lin[3]}=[$arreglo_lin[2],$factor_ice];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filetax_category_ice." \n";
		die;
	}

	$valor_etiqueta_ice=$MAPEOtax_filetax_category_ice{$MAPEOtax_billcycle{$ciclo}[0]}[0]; 
	$valor_ice_conf=$MAPEOtax_filetax_category_ice{$MAPEOtax_billcycle{$ciclo}[0]}[1];
	#SUD FIN ICE
	$line="";
    open(FILE_READ_TMP, $filempuzotab);    
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$MAPEOmpuzotab{$arreglo_lin[2]} =[$arreglo_lin[0],$arreglo_lin[1] ,$arreglo_lin[3]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempuzotab." \n";
		die;
	}
#           0                  1          2                          3              4            5            6
#select label_id||'|'|| servshdes||'|'||nvl(tzcode,1)||'|'||nvl(ttcode,1)||'|'||leg_id||'|'|| calltype||'|'||tipo from call_grouping_doc1
	$line="";
    open(FILE_READ_TMP, $filecall_grouping);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[6]);
#		print $arreglo_lin[1]." ".$arreglo_lin[5]." ".$arreglo_lin[2]." ".$arreglo_lin[3]." ".$val." ".$arreglo_lin[0]." ".$arreglo_lin[4]."\n";
		$MAPEOcall_grouping {$arreglo_lin[1],$arreglo_lin[5],$arreglo_lin[2], $arreglo_lin[3], $val} =[$arreglo_lin[0],$arreglo_lin[4]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filecall_grouping." \n";
		die;
	}

	$line="";
#select leg_id||'|'||sh_name||'|'||name from legend_doc1;
    open(FILE_READ_TMP, $filelegend);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOlegend{$arreglo_lin[0]} =[$arreglo_lin[1],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filelegend." \n";
		die;
	}
	
	$line="";
#select sncode ||'|'|| des ||'|'|| shdes ||'|'|| snind ||'|'|| rec_version ||'|'||billed_surcharge_ind ||'|'|| pde_implicit_ind from sysadm.mpusntab;
    open(FILE_READ_TMP, $filempusntab);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$MAPEOmpusntab{$arreglo_lin[2]} =[$arreglo_lin[0],$arreglo_lin[1] ];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempusntab." \n";
		die;
	}
	
	$line="";
#select zncode||'|'||des ||'|'|| shdes from mpuzntab;
    open(FILE_READ_TMP, $filempuzntab);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[2]);
#		print $val." ".$arreglo_lin[0]." ".$arreglo_lin[1]."\n";
		$MAPEOmpuzntab{$val}=[$arreglo_lin[0],$arreglo_lin[1]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempuzntab." \n";
		die;
	}
	
	$line="";
#select pay.payment_id ||'|'||pay.paymentcode||'|'||pay.paymentname datos from paymenttype_all pay
    open(FILE_READ_TMP, $filepaymenttype_all);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
#		print $arreglo_lin[0]." ".$arreglo_lin[1]." ".$val."\n";
		$MAPEOpaymenttype_all{$arreglo_lin[1]}=[$arreglo_lin[0],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filepaymenttype_all." \n";
		die;
	}
	
	$line="";
    #select label_id||'|'||des||'|'||taxinf||'|'||shdes||'|'||ctaps||'|'||codigo_sri from service_label_doc1
    open(FILE_READ_TMP, $fileservice_label);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[4]); #ECO_CIMA
		#print "Service Label ".$arreglo_lin[0]." ".$arreglo_lin[1]." ".$val." ".$arreglo_lin[2]."\n";
		$MAPEOservice_label{$arreglo_lin[0]}=[$arreglo_lin[1],$arreglo_lin[3],$arreglo_lin[2],$arreglo_lin[5]]; #ECO_CIMA #8504 - Se agrega $arreglo_lin[5] C�digo del Servicio Rubro Llamadas para F.E.
		$MAPEOservice_label2{$arreglo_lin[1]}=[$val]; #ECO_CIMA
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$fileservice_label." \n";
		die;
	}
	
	$line="";
    #select tmcode||'|'||  to_char(vsdate,'yyyymmdd')||'|'|| spcode||'|'||  sncode||'|'|| accserv_catcode from MPULKTMb a where vsdate in(select max(b.vsdate) from MPULKTMb b where b.tmcode=a.tmcode and b.sncode = a.sncode)
    open(FILE_READ_TMP, $filempulktmb);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[4]);
#		print $arreglo_lin[0]." ".$arreglo_lin[3]." ".$arreglo_lin[1]." ".$arreglo_lin[2]." ".$val."\n";
		$MAPEOmpulktmb{$arreglo_lin[0]." ".$arreglo_lin[3]}=[$arreglo_lin[1],$arreglo_lin[2],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempulktmb." \n";
		die;
	}
	
	$line="";
    #select tmcode||'|'||  to_char(vsdate,'yyyymmdd')||'|'|| spcode||'|'||  sncode||'|'|| accserv_catcode from MPULKTMb a where vsdate in(select max(b.vsdate) from MPULKTMb b where b.tmcode=a.tmcode and b.sncode = a.sncode)
    open(FILE_READ_TMP, $filempulktmbANT);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[4]);
		$MAPEOmpulktmbANT{$arreglo_lin[0]." ".$arreglo_lin[3]}=[$arreglo_lin[1],$arreglo_lin[2],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempulktmbANT." \n";
		die;
	}
	
	$line="";
	#select tmcode||'|'|| des||'|'|| shdes from rateplan
    open(FILE_READ_TMP, $filerateplan);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOrateplan{$val}=[$arreglo_lin[0],$arreglo_lin[1]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filerateplan." \n";
		die;
	}
	
#cambio 03-04-2016
#	$line="";
#	#select tmcode||'|'||shdes||'|'||firstname||'|'||secondname||'|'||porder ||'|'||plan_type||'|'||tax_code_iva ||'|'||tax_code_ice from bgh_tariff_plan_doc1 order by tmcode,shdes, plan_type, porder
#    open(FILE_READ_TMP, $filebgh_tariff_plan);
#	while (<FILE_READ_TMP>) {
#		$line=$_;
#	    @arreglo_lin=split(/\|/,$line);
#	    $val=&enceraEspacios($arreglo_lin[7]);
##		print $arreglo_lin[1]." ".$arreglo_lin[4]." ".$arreglo_lin[0]." ".$val." ".$arreglo_lin[2]." ".$arreglo_lin[3]."\n";
#		print $arreglo_lin[1]." ".$arreglo_lin[4]." ".$arreglo_lin[0]." ".$val." ".$arreglo_lin[2]." ".$arreglo_lin[3]."\n";
#		$MAPEObgh_tariff_plan{$arreglo_lin[1],$arreglo_lin[4]}=[$arreglo_lin[0],$arreglo_lin[5],$arreglo_lin[2],$arreglo_lin[3],$arreglo_lin[6],$val];
#    }
#	close FILE_READ_TMP;
#	if ($line eq "") {
#		print "Error no se encuentra configurado ".$filebgh_tariff_plan." \n";
#		die;
#	}
	$line="";
	$MAPEObgh_tariff_plan{"GTC82","3"}=["5585","D","Costo Minutos Claro a Telecsa:","$ 0.100","3","4"];
#cambio 03-04-2016
	
	#print "Termina FILE_READ_TMP -- filebgh_tariff_plan\n";

	$line="";
	#select i.taxcat_id||'|'||i.taxcat_name||'|'|| i.taxrate from tax_category i
	open(FILE_READ_TMP, $filetax_category);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOtax_category{$arreglo_lin[0]}=[$arreglo_lin[1],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filetax_category." \n";
		die;
	}

	$line="";
	#select co_id||'|'||text01||'|'||text02||'|'||text03 from info_contr_text;
    # co_id= # de Contrato co_id, Text01= # de la C�dula o RUC, Text02= Nombre, Text03= #Ciclo
	#10179 cambio 15-03-2016
	#'9999|9999|9999|9999'
	$MAPEOinfo_contr_text{"9999"}=["9999","9999"];
	#open(FILE_READ_TMP, $fileinfo_contr_text);
	#while (<FILE_READ_TMP>) {
	#	$line=$_;
	#   @arreglo_lin=split(/\|/,$line);
	#    $val=&enceraEspacios($arreglo_lin[3]);
    #   	$MAPEOinfo_contr_text{$arreglo_lin[0]}=[$arreglo_lin[1],$arreglo_lin[2]];
	#	#print "Leyendo info_contr_text \n";
    #}
	#close FILE_READ_TMP;
	#if ($line eq "") {
	#	print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$fileinfo_contr_text." \n";
	#	die;
	#}
	#10179 cambio 15-03-2016
	
	#print "Termina FILE_READ_TMP -- fileinfo_contr_text\n";
	
	$line="";
	#Clientes excentos de IVA 
	#select y.custcode from customer_all y, customer_tax_exemption x where y.customer_id = x.customer_id and x.exempt_status = 'A';
	open(FILE_READ_TMP, $filexcento_iva);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$Cuentas_Excentas{$arreglo_lin[0]}="S";
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$filexcento_iva." \n";
		die;
	}
	
	$line="";
	#Etiquetas no presentadas
	open(FILE_READ_TMP, $fileno_etiquetas);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$No_etiquetas{$arreglo_lin[0]}=[$arreglo_lin[0]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Ciclo ".$ciclo." el Archivo ".$fileno_etiquetas." \n";
		die;
	}
	
    $line="";
	#Nuevos Rubros
	open(FILE_READ_TMP, $file_nuevorubro);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$Nuevo_rubro{$arreglo_lin[0]}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$arreglo_lin[3]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Ciclo ".$ciclo." el Archivo ".$file_nuevorubro." \n";
		die;
	}
	
    $line="";
	open(FILE_READ_TMP, $fileServices_ext);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
       	$MAPEOServicesExt{$val}=[$arreglo_lin[0],$arreglo_lin[1],$val];
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$fileServices_ext." \n";
		die;
	}
	
	$line="";
	open(FILE_READ_TMP, $file_grupodth);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
       	$Mapeogrupodth{$val}=[$arreglo_lin[0],$arreglo_lin[1],$val,$arreglo_lin[3]];
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_grupodth." \n";
		die;
	} 

#[10203] - LPE  
#	$line="";
#	open(FILE_READ_TMP, $file_fecha_corte);
#	while (<FILE_READ_TMP>) {
#		$line=$_;
#		@arreglo_lin=split(/\|/,$line);
#	    $clave_cuenta=&enceraEspacios($arreglo_lin[0]);
#	    $MapeoFechaCorte{$clave_cuenta}=[$arreglo_lin[1]];
#	}	
#	close FILE_READ_TMP;
#	if ($line eq "") {
#		print "Error no se encuentra el archivo ".$file_fecha_corte." \n";
#		die;
#	}   
   
	$line="";
	open(FILE_READ_TMP, $file_abonados_feature);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $clave_cuenta=&enceraEspacios($arreglo_lin[0]);
	    $MapeoAbonadosFeat{$clave_cuenta." ".$arreglo_lin[2]}=[$arreglo_lin[1]."|".$arreglo_lin[3]];
	}	
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_abonados_feature." \n";
		die;
	}
	
	##########
	$line="";
	$clave_cuenta="";
	open(FILE_READ_TMP, $file_abonados_plan);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $clave_cuenta=&enceraEspacios($arreglo_lin[0]);
       	$MapeoAbonadosPlan{$clave_cuenta." ".$arreglo_lin[2]}=[$arreglo_lin[1]."|".$arreglo_lin[3]];
	}	
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_abonados_plan." \n";
		die;
	}
	
	####[10203] Obtengo informacion de abonados y feature por cuenta -SUD LPE
    $line="";
    $index_CtaBSCS="";
    $index_Plan="";
    $feature_num="";
	open(FILE_READ_TMP, $file_abonados_FeatCta);  #[10554]
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $clave_cuenta=&enceraEspacios($arreglo_lin[0]);
       	$MAPEOCtasSVAFecha_CTA{$clave_cuenta." ".$arreglo_lin[2]}=[$arreglo_lin[1]."|".$arreglo_lin[3]];
	}	
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_abonados_FeatCta." \n";
#		die;
	}		
	#print "Termina FILE_READ_TMP -- file_grupodth\n";
	#print "Termina graba informacion \n";
		#[10417] - INI SUD EGO 
	$line="";
	$index_CtaBSCS="";
	$index_Plan="";
	open(FILE_READ_TMP, $file_serviciosMtpDTH);
	while (<FILE_READ_TMP>) {	
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
        $index_CtaBSCS=&enceraEspacios($arreglo_lin[0]);
        $index_Coid=&enceraEspacios($arreglo_lin[2]);		
		#print "\n PRUEBA ARCHIVO SERVICIO ".$index_CtaBSCS."\n";
       	$MAPEOserviciosMtpDTH{$index_CtaBSCS." ".$index_Coid}=[$index_CtaBSCS,$arreglo_lin[1],$arreglo_lin[2]];
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_serviciosMtpDTH." \n";
	}
	#[10417] -FIN SUD EGO 
}

sub graba_datos($){
my $fileHandle=$_[0];
#print $MAPEOImprimeUno{"20100"}[0];
#foreach (sort(keys %MAPEOImprimeUno)) {	
#	print $fileHandle $MAPEOImprimeUno{$_}[0]."\n";
#}
$ltipoproducto="";
foreach (sort(keys %MAPEOImprimeUno)) {	
	$linea=$MAPEOImprimeUno{$_}[0];	
	@ar_orden=split(/\|/,$linea);
	# 10417 - INI EGO 
	$linea_mtp=$MAPEOImprimeUno{"10000"}[0];
	@ar_orden_mtp=split(/\|/,$linea_mtp);
	#$ltipoproducto=$ar_orden_mtp[28]; # KBA SE CAMBIA POSICION
	$ltipoproducto=$ar_orden_mtp[27];
	if ($ltipoproducto ne "DTHM"){ 
	# 10417 - FIN EGO 
	if ($ar_orden[0] eq "41000"){
		$MdetalleOrden{$ar_orden[0].$ar_orden[7]."|".$ar_orden[5]}=[$linea];
	}
	else{
		if ($ar_orden[0] ge "42000"){
		$MdetalleOrden{$ar_orden[0].$ar_orden[7]."|".$ar_orden[5]}=[$linea];
		}
		else{
			print $fileHandle $MAPEOImprimeUno{$_}[0]."\n";
		}
	}
	}else{
		print $fileHandle $MAPEOImprimeUno{$_}[0]."\n";
	}
}
undef(%MAPEOImprimeUno);
foreach (sort(keys %MdetalleOrden)) {	
	print $fileHandle $MdetalleOrden{$_}[0]."\n";
}
undef(%MdetalleOrden);
if ($banderaUno eq "1") {
#foreach (sort(keys %Mdetallefinal)) {	
#	print $fileHandle $Mdetallefinal{$_}[0]."\n";
#}
#undef(%Mdetallefinal);
#foreach (sort(keys %Mgrupodth)) {	
#	print $fileHandle $Mgrupodth{$_}[0]."|".$Mgrupodth{$_}[1]."|".$Mgrupodth{$_}[2]."|"."\n";
#}
#undef(%Mgrupodth);

}
}

sub graba_co_id {
my $co_id=$_[0];
my $number=$_[1];
my $tipo=$_[2];
my $plan_co_id=$_[3];
my $des_co_id=$_[4];
$tip=$Graba_co_id{$co_id}[1];
$plan_g=$Graba_co_id{$co_id}[2];
$des_plan_g=$Graba_co_id{$co_id}[3];
if ($plan_co_id eq "") {
	$plan_co_id=$plan_g;
}
if ($des_co_id eq "") {
	$des_co_id=$des_plan_g;
}
$tip=$Graba_co_id{$co_id}[1];
if ($tipo eq "0" && $tip eq "6") {
	$tipo=$tip;
}
  if ($number ne "") {
#	  $Graba_co_id{$co_id}=[$number,$tipo];
      $Graba_co_id{$co_id}=[$number,$tipo,$plan_co_id,$des_co_id];
  }
}

sub cambia_etiqueta {
$descripcion_ser=$_[0];
if ($descripcion_ser =~ /Cargo a Paquete Fac/) {
	$descripcion_ser=~ s/Cargo a Paquete Fac/Cargo a Paquete/;
}
if ($descripcion_ser =~ /Cargo a Paquete Cto/) {
	$descripcion_ser=~ s/Cargo a Paquete Cto/Cargo a Paquete/;
}
if ($descripcion_ser =~ /Detalle de llamadas \- \no cobro/) {
	$descripcion_ser=~ s/Detalle de llamadas \- \no cobro/Factura Detallada Plus/;
}
return($descripcion_ser);
}

# Funcion utilizado para tener la CI y el Nombre del due�o de la cuenta
# en caso de no existir informaci�n en el archivo fileinfo_contr_text.dat
# imprimo el de la Cuenta


$lb_control_plan="0";
$lb_control_sum=0;


sub obtiene_valor_resumen {
my $co_id=$_[0];
my $key_resumen_dth=$_[1];
my $cont_calculo=$_[2];
my $val_resumen;
my $val_dolares=0;

$val_resumen=$MAPEOImprime{$co_id." ".$key_resumen_dth." ".$cont_calculo}[0];
@arreglo_num=split(/\|/,$val_resumen);
$val_dolares=$arreglo_num[3];
$tp_inicio=&enceraEspacios($arreglo_num[5]);
$tp_fin=&enceraEspacios($arreglo_num[6]);
return($val_dolares,$tp_inicio,$tp_fin);
}

sub formatea_seg_minutos {
$vseg=$_[0];
$vseg1=$vseg;
$vseg=$vseg/60;
$var=sprintf('%.2f',$vseg);
($a,$b)=split(/\./,$var);
$b=$vseg1-$a*60;
if ($b == 00) {
	$b="00";
} else { 
if ($b <= 9 ) {
	$b="0".$b;
}
}
if ($a == 0) {
   $des_vseg="(".$b." Seg)";
} else {
   $des_vseg="(".$a." Min ".$b." Seg)";
}

return $des_vseg;
}

sub formatea_seg_minutos_primera_hoja {
$vseg=$_[0];
$vseg_gratis=$_[1];
$vseg1=$vseg - $vseg_gratis;
$vseg2=$vseg1/60;
$var=sprintf('%.2f',$vseg2);
($a,$b)=split(/\./,$var);
$b=$vseg1-$a*60;

if ($b == 00) {
	$b="00";
} else { 
if ($b <= 9 ) {
	$b="0".$b;
}
}

if ($a == 0) {
   $des_vseg="(".$b." Seg)";
} else {
   $des_vseg="(".$a." Min ".$b." Seg)";
}

return $des_vseg;
}


sub obtiene_contador {
my $des=$_[0];
my $desglosa_periodos = $_[1];
my $val="";

if ($des eq $No_etiquetas{$des}[0]){
  print "Debud no etiqueta2".$des."---".$No_etiquetas{$des}[0]."\n";
  next;
}else
{
   if ($desglosa_periodos eq "0") {
	  $val=$Resumen_Celular_Dos{$des}[0];
    }

}

if ($val ne "") {
  $valor_obtiene_contador=$val;
} else {
  if ($des eq "Tarifa Basica ") {
	$valor_obtiene_contador="00000001";
} else	{
	   $cont_calculo=$cont_calculo_mayor;
	   $cont_calculo=$cont_calculo+1;
		if ($cont_calculo > 9) { $valor_obtiene_contador="000000".$cont_calculo;
		} else { $valor_obtiene_contador="0000000".$cont_calculo;}
 	   $cont_calculo_mayor=$cont_calculo_mayor+1;
   }
}

$Resumen_Celular_Dos{$des}=[$valor_obtiene_contador];
return($valor_obtiene_contador);
}

sub formatea_numeros {
    my $var=$_[0];
    my $var11=$_[0];
	my $valor_nuevo;
	($a,$b)=split(/\./,$var);
	if (length($b) == 3 && substr($b,2,1) == 5) {
	  if (substr($b,1,1) == 9) {
  	    if (substr($b,0,1) == 9) {
  	     $var=($a+1).".00";
		} else {
  	     $var=$a.".".(substr($b,0,1)+1).".0";
		}
	  } else {
  	    $var=$a.".".substr($b,0,1).(substr($b,1,1)+1);
	  }
	} 
    $valor_nuevo=sprintf('%.2f',$var);
	return $valor_nuevo;
}


sub impuesto_calcular {
$valor_impuesto_calcula=$_[0];
$servicio_c=$_[1];
$plan_c=$_[2];
$tmp_final=$_[3];
$tm_code=$MAPEOrateplan{$plan_c}[0];
$sn_code=$MAPEOmpusntab{$servicio_c}[0];
$impuesto=$MAPEOmpulktmb{$tm_code." ".$sn_code}[2];
if ($tmp_final le "20071231") {
	$impuesto=$MAPEOmpulktmbANT{$tm_code." ".$sn_code}[2];
}
if ($impuesto eq "IVA-ICE") {
	 $total_iva=$MAPEOTotales{$co_id}[1]+$valor_impuesto_calcula;
	 $total_ice=$MAPEOTotales{$co_id}[2]+$valor_impuesto_calcula;
} 
if ($impuesto eq "IVA" || $impuesto eq "2IVA-ICE") {#cambios por el ice 
	 $total_iva=$MAPEOTotales{$co_id}[1]+$valor_impuesto_calcula;
} 
if ($impuesto eq "ICE") {
	 $total_ice=$MAPEOTotales{$co_id}[2]+$valor_impuesto_calcula;
} 
}

sub obtiene_impuesto {
$des=$_[0];
$seg=$_[1];
$tipo_impuesto=uc($_[2]);
$tot_iva=$_[3];
$tot_ice=$_[4];
$valor_dol=$_[5];
$seg_gratis=$_[6];
$descripcionV=&formatea_seg_minutos($seg);
$total_ivaV=$tot_iva;
$total_iceV=$tot_ice;
if ($tipo_impuesto eq "ICE" || $tipo_impuesto eq "IVA-ICE" || $tipo_impuesto eq "2IVA-ICE") {#cambios por el ice) {
	$total_iceV=$total_iceV+$valor_dol;
} 
if ($tipo_impuesto eq "IVA" || $tipo_impuesto eq "IVA-ICE" || $tipo_impuesto eq "2IVA-ICE") {#cambios por el ice) {
	$total_ivaV=$total_ivaV+$valor_dol;
} 
$etiquetaV=$des;
$val=$Minutos{$etiquetaV}[0];
$gratis=$Minutos{$etiquetaV}[1];
$val=$val+$seg;
$gratis=$gratis+$seg_gratis;
$Minutos{$etiquetaV}=[$val,$gratis];
return($descripcionV,$etiquetaV,$total_ivaV,$total_iceV);
}

sub busca_descripcion_etiqueta{
$servshdes=$_[0];
$tzcode=$_[1];
$calltype=$_[2];
$ttcode=$_[3];
$tmshdes=$_[4];
$clase_llam=$_[5];
my $des;
my $sh_name;
my $sh_des;
my $taxinf;
my $label_id="";
my $legend_id="";
my $tipo="";

$codigo_servicio_cl="";

#$tipo=$MAPEObgh_tariff_plan{$tmshdes,1}[1];
$tipo=""; #cambio 03-04-2016
if ($tipo eq "") {
	$tipo ="D";
}
($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,$tzcode,$ttcode,$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,$tzcode,$ttcode,$tipo}[1]);
 if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,$tzcode,"1",$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,$tzcode,"1",$tipo}[1]);
 }
 if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,"1",$ttcode,$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,"1",$ttcode,$tipo}[1]);
 }
 if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,"1","1",$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,"1","1",$tipo}[1]);
 }
 if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{"1",$calltype,$tzcode,"1",$tipo}[0],$MAPEOcall_grouping{"1",$calltype,$tzcode,"1",$tipo}[1]);
 }
 $sh_name=$MAPEOlegend{$legend_id}[0];
 $sh_name=&enceraEspacios($sh_name);
 $des=$MAPEOservice_label{$label_id}[0];
 $sh_des=$MAPEOservice_label{$label_id}[1];
 $taxinf=$MAPEOservice_label{$label_id}[2];
 $sh_des=&enceraEspacios($sh_des);
 $des=&enceraEspacios($des);
 $codigo_servicio_cl=$MAPEOservice_label{$label_id}[3];
 $codigo_servicio_cl=&enceraEspacios($codigo_servicio_cl);
return($des,$sh_name,$sh_des,$taxinf,$codigo_servicio_cl);
}

sub busca_descripcion_servicio{
$zcode=$_[0];
$descripcion_mpusntab=$MAPEOmpusntab{$zcode}[1];
return($descripcion_mpusntab);
}


sub transforma_fecha{
#20060108022847
#28102006|12:00|
$fecha=$_[0];
$anio=substr($fecha,0,4);
$mes=substr($fecha,4,2);
$dia=substr($fecha,6,2);
$hora=substr($fecha,8,2);
$min=substr($fecha,10,2);
$seg=substr($fecha,12,2);
%arreglo= ('01','Ene','02','Feb','03','Mar','04','Abr','05','May','06','Jun','07','Jul','08','Ago','09','Sep','10','Oct','11','Nov','12','Dic');
$mes_letra=$arreglo{$mes};
$fecha_completa=$dia.$mes.$anio."|".$hora.":".$min.":".$seg;
$fecha_completa1=$dia." ".$mes_letra." ".$hora.":".$min;
return($fecha_completa1,$fecha_completa);
}

sub rango_fecha{
#DTM+901:20060308:102'
#DTM+902:20060407:102'
my $fecha=$_[0];
my $fecha1=$_[1];
$mes=substr($fecha,4,2);
$dia=substr($fecha,6,2);
$mes1=substr($fecha1,4,2);
$dia1=substr($fecha1,6,2);

%arreglo= ('01','Ene','02','Feb','03','Mar','04','Abr','05','May','06','Jun','07','Jul','08','Ago','09','Sep','10','Oct','11','Nov','12','Dic');
$mes_letra=$arreglo{$mes};
$mes_letra1=$arreglo{$mes1};

$fecha_completa=$mes_letra.". ".$dia." al ".$mes_letra1.". ".$dia1;
return($fecha_completa);
}

sub enceraEspacios{
	$valor=$_[0];
	$valor=~s/^\s+//;	
	$valor=~s/\s+$//;
	$valor=~s/\n+//;
	return($valor); 
}

sub verifico_tipo_agrupacion{
$iva=$_[0];
$ice=$_[1];
# Servicio de television #
if ($iva > 0 && $ice > 0) {
#	$tipo="20000";
	$tipo="20200";
	$apl_ice="S";
}
# Otros Servicios #
if ($iva > 0 && $ice == 0) {
	$tipo="20200";
	$apl_ice="N";
}
# Sin Impuestos #
if ($iva == 0 && $ice == 0) {
	$tipo="21100";
	$apl_ice="N";
} 
return($tipo,$apl_ice);
}

sub verifica_servExterno{
 $codigo_servicio=$_[0];
 $tipo=$_[1];
 $existe=exists $MAPEOServicesExt{$codigo_servicio};
 if ( $existe == 1 ) {
	 $tipo="21650";
 }
 return($tipo);
}


sub obtieneTotalMenosServExt{
$Valor=$_[0];
$Valor_Total=$Valor-$total_servicios_Ext;
return $Valor_Total
}


sub verifica_proc{
$cod_in=$_[0];
foreach (keys %Mdetallefinal) {
	$key_p=$_;
	$trama_p=$Mdetallefinal{$key_p}[0];
	@arreglo_p=split(/\|/,$trama_p);
	$cod_serv_p=$arreglo_p[7];
	if ($cod_in eq $cod_serv_p) {
		$band_proc="1";
		return ($band_proc);
	}
	else
	{
		$band_proc="0";
	}
}	# fin for 2
return ($band_proc);
}

sub timDosBuscaValores{
# INICIO TIM DOS BUSCA VALORES
    $val_resumen=0;
	$ban_dos="0";
	$fecha_i="";
#    if ( /(IMD\+\+8\+CT\:\:\:\:U\')\n/ )  {
	#10417 SUD EGO
	if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'/ ) {
		$codigo_servicio=$1;
	 }
	 #print "servicio prueba".$codigo_servicio."\n";
    if ( /(IMD\+\+8\+CT\:\:\:\:U)/ )  {
        $ban_dos="1";
        $ban_acceso="0";
        return;
    }
    if ( /(IMD\+\+8\+CT\:\:\:\:A\')\n/ )  {
        $ban_acceso="1";
    }
#    if (/0\+\+\:SA\:\:\+\+02\'\n/) {		19/03/2013
    if (/0\+\+\:SA\:\:\+\+02\'/) {
        $co_id="";
    }
#    if (  /IMD\+\+8\+CO\:\:\:\:([^\:\']*)\'\n/ ) { #if #1			19/03/2013
    if (  /IMD\+\+8\+CO\:\:\:\:([^\:\']*)\'/ ) { #if #1
        $co_id=$1;
    }
#    if ( /(0\+\+\:SA\:\:\+\+02\'\n)/) {			19/03/2013
    if ( /0\+\+\:SA\:\:\+\+02\'/) {
        if (/IMD\+\+8\+MRKT\:\:\:([^\:]*)\:/) {
            $marca_red=$1;
            if ($marca_red eq "ISD") {
                $key_resumen_dth="99992";
                $key_tot_cargos_cta="99993";
                &graba_co_id($co_id,$fono,"0");
                } else {
                $key_resumen_dth="99991";
            }
        }
        #Obtengo el n�mero de Tel�fono#
#        if (/IMD\+\+8\+DNNUM\:M\:\:([^\:]*)\:([^\:\']*)\'\n/) {		19/03/2013
        if (/IMD\+\+8\+DNNUM\:M\:\:.*\:([^\:\']*)/) {
#            $fono=$2;						19/03/2013
            $fono=$1;
        }
        } else {
#        if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n[^\n]/ ) {			19/03/2013
        if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:/ ) {
            $codigo_servicio=$1;
            $co_id_det=$Graba_co_id{$co_id}[1];
            if (/IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'/) {
                    $plan=$1;
                    $des_plan=$2;
                    $plan1=$plan;
                    $des_plan1=$des_plan;
					$cont_detalle=$cont_detalle + 1;
                    #print "Entrada 2 FONO ".$fono." Codigo Servicio ".$codigo_servicio." Co_id ".$co_id_det." Descripcion plan1: ".$des_plan_tele."T |\n" ;
                    &graba_co_id($co_id,$fono,"0",$plan1,$des_plan1);
                    $co_id_alter1=$co_id;
                    $co_id_alter2=$co_id;
            }
            # Otengo el Valor el 2do MOA #
            if (/MOA.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/) {
                $dolar=$1;
            }
#            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/; #  IMD++8+SP:  ::  SP51   :   GSM ? : Servicios Basicos '		19/03/2013
            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'/; #  IMD++8+SP:  ::  SP51   :   GSM ? : Servicios Basicos '
#            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'\n/; #IMD++8+SP   : : :   SP06  :Servicios OCC'		19/03/2013
            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'/; #IMD++8+SP   : : :   SP06  :Servicios OCC'
            $codigo_servicio_basico=$1;   # Es para obtener si es Servicio de Voz B�sico SP51 SP02
            $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
            $descripcion_ser=cambia_etiqueta($descripcion_ser);
            $desglosa_periodos = "0";
			if ($band_activ_mtp eq "1"){
				$dolar_anterior=$dolar;						#guarda el valor del servicio pa compararlo en la otra vuelta
				$codigo_servicio_a=$codigo_servicio;		# guarda el servicio para compararlo en la otr vuelta
				$plan_anterior=$plan;				# guarda el plan pa comparar en la otra vuelta
				$co_id_anterior=$co_id;
				#
				$cont_calculo=&obtiene_contador($descripcion_ser,$desglosa_periodos);
				($val,$tmp_inicio,$tmp_fin)=&obtiene_valor_resumen($co_id,$key_resumen_dth,$cont_calculo);
				$dolar=&formatea_numeros($dolar);
				$dol=$dolar;
				$dolar=~ tr/\.//d;
				$val_resumen=$dolar + $val;
				if ($ban_dos ne "1" && lc($descripcion_ser) ne "Entrega de factura") {
	#17/04/2013				$Mdetalle{$co_id." ".$key_resumen_dth." ".$cont_detalle}=[$key_resumen_dth."|".$descripcion_ser."||".$dolar."|".$dolar."|||".$codigo_servicio."|"]; 
					$Mdetalle{$co_id." ".$key_resumen_dth." ".$cont_detalle}=[$key_resumen_dth."|".$descripcion_ser."||".$dol."|".$dol."|||".$codigo_servicio."|".$des_plan."|"]; #10417 
					$clave_anterior=$co_id." ".$key_resumen_dth." ".$cont_calculo;
					$clave_det_ant=$co_id." ".$key_resumen_dth." ".$cont_detalle;
				}
				if ($tmp_inicio eq "" && $tmp_fin eq "") { #tengo el servicio pero no las fechas
					$band_sin_fecha="1";
				}
			}else{
				if (lc($descripcion_ser) ne "servicio de voz"   &&  lc($descripcion_ser) ne "Emisi�n y entrega de factura"  ) {
					if ((lc($descripcion_ser) eq "tarifa basica " || lc($descripcion_ser) eq "tarifa b�sica " || lc($descripcion_ser) eq "cargo a paquete" ) && $dolar == 0) {
						$no_imprime="1";
						$clave_anterior="";
						$dolar_anterior="";
						$codigo_servicio_a="";
						$plan_tele_a="";
						$co_id_a="";
						} else {
						$no_imprime="0";
					}
					if ($no_imprime eq "0") {
						$total_iva=$MAPEOTotales{$co_id}[1];
						$total_ice=$MAPEOTotales{$co_id}[2];
						$dolar_anterior=$dolar;						#guarda el valor del servicio pa compararlo en la otra vuelta
						$codigo_servicio_a=$codigo_servicio;		# guarda el servicio para compararlo en la otr vuelta
						$plan_anterior=$plan;				# guarda el plan pa comparar en la otra vuelta
						$co_id_anterior=$co_id;
						if ($desglosa_periodos eq "0") {
							$tiempo_fin_no_imprime="20080101";
							if (/IMD\+\+8\+FE/) {
	#                            if (/IMD\+\+8\+FE[^\n]+\-(\d+MAYCART)\'\n/) {			19/03/2013
								if (/IMD\+\+8\+FE[^\n]+\-(\d+MAYCART)\'/) {
									$tiempo_fin_no_imprime=substr($1,0,8);
								}
								&impuesto_calcular($dolar,$codigo_servicio,$plan_tele,$tiempo_fin_no_imprime);
								$suma_total=$MAPEOTotales{$co_id}[0]+$dolar;
								$MAPEOTotales{$co_id}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
							}
						}
						$cont_calculo=&obtiene_contador($descripcion_ser,$desglosa_periodos);
						($val,$tmp_inicio,$tmp_fin)=&obtiene_valor_resumen($co_id,$key_resumen_dth,$cont_calculo);
						$dolar=&formatea_numeros($dolar);
						$dol=$dolar;
						$dolar=~ tr/\.//d;
						$val_resumen=$dolar + $val;
	#					$MAPEOImprime{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_dth."|".$descripcion_ser."||".$val_resumen."|".$val_resumen."|".$tmp_inicio."|".$tmp_fin."|".$codigo_servicio."|"]; 
						$MAPEOImprime2{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_dth."|".$descripcion_ser."||".$val_resumen."|".$val_resumen."|".$tmp_inicio."|".$tmp_fin."|".$codigo_servicio."|"]; 
						if ($ban_dos ne "1" && lc($descripcion_ser) ne "Entrega de factura") {
	#17/04/2013						$Mdetalle{$co_id." ".$key_resumen_dth." ".$cont_detalle}=[$key_resumen_dth."|".$descripcion_ser."||".$dolar."|".$dolar."|||".$codigo_servicio."|"]; 
							$Mdetalle{$co_id." ".$key_resumen_dth." ".$cont_detalle}=[$key_resumen_dth."|".$descripcion_ser."||".$dol."|".$dol."|||".$codigo_servicio."|".$des_plan."|"]; #10417 
							$clave_anterior=$co_id." ".$key_resumen_dth." ".$cont_calculo;
							$clave_det_ant=$co_id." ".$key_resumen_dth." ".$cont_detalle;
						}
						if ($tmp_inicio eq "" && $tmp_fin eq "") { #tengo el servicio pero no las fechas
							$band_sin_fecha="1";
						}
					}
					}
				  $ban_acceso = "1";
				}#10417  
				} else {
				if ($band_activ_mtp ne "1"){
				# Para sacar las fechas del servicio.
				if ($clave_anterior ne "" && $ban_acceso eq "1" && $no_imprime eq "0") { #para el caso en que haya alguna suspension en medio periodo
					$tiempo_inicio="";
					$tiempo_fin="";
					if (/DTM\+901\:([^\:]*)\:102\'/) {
						$tiempo_inicio=&enceraEspacios($1);
					}
					if (/DTM\+902\:([^\:]*)\:102\'/) {
						$tiempo_fin=&enceraEspacios($1);
					}
					if (/MOA\+60\:([^\:]*)\:USD\:959\:9\'/) {
						$dolar_new=&enceraEspacios($1);
					} else { $dolar_new = $0; }
					if ($plan_mostrar eq "") {
					  if ($tiempo_inicio eq $fecha_corte) {
						$plan_mostrar=$plan;
					  }
					}
					if ( ($tiempo_inicio ne "") ) { 
	#                 @arreglo_num=split(/\|/,$MAPEOImprime{$clave_anterior}[0]);
					 @arreglo_num=split(/\|/,$MAPEOImprime2{$clave_anterior}[0]);
					 $clave=$arreglo_num[0];
					 $descripcion=$arreglo_num[1];
					 $dolar=$arreglo_num[3];
					 $t_inicio=&enceraEspacios($arreglo_num[5]);
					 $t_fin=&enceraEspacios($arreglo_num[6]);
					 &impuesto_calcular($dolar_anterior,$codigo_servicio_a,$plan_tele_a,$tiempo_fin);
					 $suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
					 $MAPEOTotales{$co_id_a}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
					if ($t_inicio eq "") {
						 $t_inicio=$tiempo_inicio;
						 $t_fin=$tiempo_fin;
					   }
					if ($tiempo_inicio gt $t_inicio) {
						$tiempo_inicio=$t_inicio;
					  }
				   if ($tiempo_fin lt $t_fin) {
					   $tiempo_fin=$t_fin;
					}
					$des_rango_fecha=&rango_fecha($tiempo_inicio,$tiempo_fin);
	#                $MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar."|".$dolar."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio]; #ECO_CIMA2
					$MAPEOImprime2{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar."|".$dolar."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio]; #ECO_CIMA2
					 } else
					{
	#					@arreglo_num=split(/\|/,$MAPEOImprime{$clave_anterior}[0]);
						@arreglo_num=split(/\|/,$MAPEOImprime2{$clave_anterior}[0]);
						$clave=$arreglo_num[0];
						$descripcion=$arreglo_num[1];
						$dolar_anterior=$dolar_new;
						&impuesto_calcular($dolar_anterior,$codigo_servicio_a,$plan_tele_a,$tiempo_fin);
						$suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
						$MAPEOTotales{$co_id_a}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
						$des_rango_fecha=&rango_fecha($tiempo_inicio,$tiempo_fin);
						$dolar_anterior=&formatea_numeros($dolar_anterior);
						$dolar_anterior=~ tr/\.//d;
	#                   $MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar_anterior."|".$dolar_anterior."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio]; #ECO_CIMA
					   $MAPEOImprime2{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar_anterior."|".$dolar_anterior."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio]; #ECO_CIMA
					}
				}
				if (lc($descripcion_ser) ne "servicio de voz"  && lc($descripcion_ser) ne "tarifa basica " && lc($descripcion_ser) ne "tarifa b�sica " && lc($descripcion_ser) ne "cargo a paquete" &&  lc($descripcion_ser) ne "Entrega de factura") {
					# saco las fechas para el detalle
					if (/DTM\+901\:([^\:]*)\:102\'/) {	
						$fecha_i=&enceraEspacios($1);
					}
					if (/DTM\+902\:([^\:]*)\:102\'/) {
						$fecha_f=&enceraEspacios($1);
					}
					if (/MOA\+60\:([^\:]*)\:USD\:959\:9\'/) {
						$valor_ser=&enceraEspacios($1);
	# 10/04/2013					$valor_ser=&formatea_numeros($valor_ser);
	# 10/04/2013                    $valor_ser=~ tr/\.//d;
					}
					if ($fecha_i ne "") {
					 @arreglo_num=split(/\|/,$Mdetalle{$clave_det_ant}[0]);
					 $clave=$arreglo_num[0];
					 $descripcion=$arreglo_num[1];
					 $dolar=$arreglo_num[3];
					 $t_inicio=&enceraEspacios($arreglo_num[5]);
					 $t_fin=&enceraEspacios($arreglo_num[6]);
					 if ($t_inicio eq "") {
						$rango_tiempo=&rango_fecha($fecha_i,$fecha_f);
						$Mdetalle{$clave_det_ant}=[$clave."|".$descripcion."|".$rango_tiempo."|".$valor_ser."|".$valor_ser."|".$fecha_i."|".$fecha_f."|".$codigo_servicio."|".$des_plan."|"];  #10417
					 } else
					 { 
						$cont_detalle=$cont_detalle + 1;
						$rango_tiempo=&rango_fecha($fecha_i,$fecha_f);
						$Mdetalle{$co_id." ".$key_resumen_dth." ".$cont_detalle}=[$key_resumen_dth."|".$descripcion."|".$rango_tiempo."|".$valor_ser."|".$valor_ser."|".$fecha_i."|".$fecha_f."|".$codigo_servicio."|".$des_plan."|"];  #10417
					 }
					}
				}
				$clave_anterior="";
				}
			}
    }

}# FIN TIM DOS BUSCA VALROES

#10417 SUD EGO nuevo proceso arma_anexo para detallar valores post-pagados y pre-pagados
sub arma_anexo2{

undef(%Mdetallefinal); 
$band_proc="0";
$cont_det_f="0";
#$archivo_post="servicios_postpago_".$fecha_corte."_".$procesador.".txt";
$archivo_post="postpago_".$fecha_corte."_".$co_id."_".$procesador.".txt";
$archivo_postpago=$carpeta_post."/".$archivo_post;
$nombre_out="output_doc1_dth_".$procesador."_".$ciclo.".txt";
$fecha_ingreso=`date '+%d/%m/%Y %H:%M:%S'`;
$fecha_ingreso=~ s/\n//g;
$dia_c=`expr substr $fecha_corte 7 2`;
$dia_c=~ s/\n//g;
$mes_c=`expr substr $fecha_corte 5 2`;
$mes_c=~ s/\n//g;
$anio_c=`expr substr $fecha_corte 1 4`;
$anio_c=~ s/\n//g;
$fecha_c=$dia_c."/".$mes_c."/".$anio_c;

my %rubros_post; #Para escribr los archivos 
my %fec_max_det; #Para tomar la fecha mayor de un grupo de servicios 
my %negativos;


my %coid_servicios; #servicios que contiene los coid
my %servicios_pre;  #servicios del coid prepagado
my %servicios_post;	#servicios del coid postpagos


$elementos=0;
$fec_aux_max; #Varaible auxiliar para comparar las fechas y tomar la fecha mayor

if ($plan_mostrar eq "") {
	$plan_mostrar=$plan;
}
$proc="";
$proc2="";
$c=0;
foreach  (keys %Mdetalle ) {

  #print  "\n Presentando la linea=> $Mdetalle{$_}[0] \n";

  $key=$_;
  $band_proc="0";
  $band_activacion="1";
  $band_negado="0";
  #10417 INI - SUD EGO
  $marca_serv_tc_cab="";
  $marca_serv_tc_pre="";
  $marca_serv_tc_pos="";
  #10417 FIN 
  $trama=$Mdetalle{$key}[0];
  @arreglo_det=split(/\|/,$trama);
  my @co_id_key=split(' ',$key);


  #print "Arreglo_det @arreglo_det \n";##debug
  #print "Key @co_id_key \n";##debug

  #$clave_det=$arreglo_det[0].$co_id; 10417
  $clave_det=$arreglo_det[0].$co_id_key;

  $serv_det=$arreglo_det[1];
  $det_fecha=$arreglo_det[2];
  $valor_det=$arreglo_det[3];

  $valor_negativo=$arreglo_det[3];
  $valor_original=$arreglo_det[3];
  $fec_i_det=$arreglo_det[5];

  $llave_fec=substr($arreglo_det[5],0,6);

  $fec_f_det=$arreglo_det[6];
  $cod_serv_det=$arreglo_det[7];
  $desc_mtp=$arreglo_det[8];#10417
		
  $llave_anexo=$co_id_key[0]." ".$cod_serv_det." ".$key_resumen_dth." ".$fec_i_det;

  ##$valor_det=($valor_det)+($Mdetallefinal{$llave_anexo}[1]);

  $c=$c+1;
  #$clave_coid_serv=$co_id." ".$cod_serv_det;# Clave Global de servicios C, T. O
  $clave_coid_serv=$co_id_key[0]." ".$cod_serv_det;# Clave Global de servicios C, T. O

  if (($Mapeogrupodth{$cod_serv_det}[3] eq "C" || 
	   $Mapeogrupodth{$cod_serv_det}[3] eq "TC")&& 
	   $arreglo_det[1] ne "Cr�dito a Facturaci�n"){ 
	   #Cabecera
		$tipo_cab="CAB";
		$verifica=0;
		$verifica= exists $coid_servicios{$clave_coid_serv};
			if ($verifica == 1){
				$coid_servicios{$clave_coid_serv}[3]=$tipo_cab;
			}else{
				$coid_servicios{$clave_coid_serv}=[$cod_serv_det,"","",$tipo_cab];
			}
			
				$clave_serv_cab=$clave_coid_serv." ".$tipo_cab;
				$verifica=0;
				$verifica= exists $servicios_cab{$clave_serv_cab};

				if ($verifica == 1){
					#
					$valor_det=($valor_det)+($servicios_cab{$clave_serv_cab}[2]);
					$fec_i_act=$servicios_cab{$clave_serv_cab}[3];
					$fec_f_act=$servicios_cab{$clave_serv_cab}[4];

					#
					if($fec_i_act lt $fec_i_det){
						$fec_i_det=$fec_i_act;
					}
					#
					if($fec_f_act gt $fec_f_det ){
						$fec_f_det=$fec_f_act;
					}
					#
					$rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
					#Se actualizan los valores 
					$servicios_cab{$clave_serv_cab}[2]=$valor_det;
					$servicios_cab{$clave_serv_cab}[3]=$fec_i_det;
					$servicios_cab{$clave_serv_cab}[4]=$fec_f_det;
					$servicios_cab{$clave_serv_cab}[5]=$rango_tiempo;
					$servicios_cab{$clave_serv_cab}[8]=$key_resumen_dth;
					#print "servicios_CAB (POR CUENTA) EGO Serv existe $clave_coid_serv $valor_det | $fec_i_det | $fec_f_det | $rango_tiempo \n";
				}else{
					#
					$marca_serv_tc_cab= $Mapeogrupodth{$cod_serv_det}[3];
					$rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
					$servicios_cab{$clave_serv_cab}=[$cod_serv_det,	
													 $serv_det,
													 $valor_det,
													 $fec_i_det,
													 $fec_f_det,
													 $rango_tiempo,
													 $co_id_key[0],
													 $clave_det,
													 $key_resumen_dth,
													 $marca_serv_tc_cab,
													 $det_fecha];
					#print "servicios_CAB (POR CUENTA) EGO Serv No existe $clave_coid_serv $valor_det | $fec_i_det | $fec_f_det | $rango_tiempo \n";	
				}
	
	    }
   
  if (($Mapeogrupodth{$cod_serv_det}[3] eq "T" ||  
	   $Mapeogrupodth{$cod_serv_det}[3] eq "TD" ||
	   $Mapeogrupodth{$cod_serv_det}[3] eq "O" ||
	   $Mapeogrupodth{$cod_serv_det}[3] eq "TC") && 
	   $arreglo_det[1] ne "Cr�dito a Facturaci�n"){ 

		if ($fec_i_det eq $fecha_corte || $fec_i_det eq "") {
     		#Pre-pagados
			$verifica=0;
			$verifica= exists $coid_servicios{$clave_coid_serv};
			if ($verifica == 1){
				$coid_servicios{$clave_coid_serv}[1]="PRE";
			}else{
				$coid_servicios{$clave_coid_serv}=[$cod_serv_det,"PRE","",""];
			}
			
			$clave_serv_pre=$clave_coid_serv." "."PRE";
			$verifica=0;
			$verifica= exists $servicios_pre{$clave_serv_pre};

			if ($verifica == 1){
				#
				$valor_det=($valor_det)+($servicios_pre{$clave_serv_pre}[2]);
				$fec_i_act=$servicios_pre{$clave_serv_pre}[3];
				$fec_f_act=$servicios_pre{$clave_serv_pre}[4];

				#
				if($fec_i_act lt $fec_i_det){
					$fec_i_det=$fec_i_act;
				}
				#
				if($fec_f_act gt $fec_f_det ){
					$fec_f_det=$fec_f_act;
				}
				#
				$rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
				#Se actualizan los valores 
				$servicios_pre{$clave_serv_pre}[2]=$valor_det;
				$servicios_pre{$clave_serv_pre}[3]=$fec_i_det;
				$servicios_pre{$clave_serv_pre}[4]=$fec_f_det;
				$servicios_pre{$clave_serv_pre}[5]=$rango_tiempo;
				$servicios_pre{$clave_serv_pre}[8]=$key_resumen_dth;
				
										
				
			}else{
			
			$marca_serv_tc_pre= $Mapeogrupodth{$cod_serv_det}[3];
				#
				$rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
				$servicios_pre{$clave_serv_pre}=[$cod_serv_det,	
												 $serv_det,
												 $valor_det,
												 $fec_i_det,
												 $fec_f_det,
												 $rango_tiempo,
												 $co_id_key[0],
												 $clave_det,
												 $key_resumen_dth,
												 $marca_serv_tc_pre,
												 $det_fecha,
												 $desc_mtp];
			}
			#        
		}
		else
		{
		 #print "\n VALOR POSTPAGP FECHA CORTE -->".$fecha_corte." -  FECHA INI - ".$fec_i_det."\n";
		 if ($fec_i_det lt $fecha_corte || $fec_i_det ne "" ) {

				#Post-Pagados
				$tipo_post="POS";
				$verifica=0;
				$verifica= exists $coid_servicios{$clave_coid_serv};
				if ($verifica == 1){
						$coid_servicios{$clave_coid_serv}[2]=$tipo_post;
				}else{					
						$coid_servicios{$clave_coid_serv}=[$cod_serv_det,"",$tipo_post,""];								
		  }
				
				$clave_serv_post=$clave_coid_serv." ".$tipo_post;
				$verifica=0;
				$verifica= exists $servicios_post{$clave_serv_post};

				if ($verifica == 1){
					#
					$valor_det=($valor_det)+($servicios_post{$clave_serv_post}[2]);
					$fec_i_act=$servicios_post{$clave_serv_post}[3];
					$fec_f_act=$servicios_post{$clave_serv_post}[4];

					#
					if($fec_i_act lt $fec_i_det){
						$fec_i_det=$fec_i_act;
		  }
					#
					if($fec_f_act gt $fec_f_det ){
						$fec_f_det=$fec_f_act;
		}
					#
					$rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
					#Se actualizan los valores 
					$servicios_post{$clave_serv_post}[2]=$valor_det;
					$servicios_post{$clave_serv_post}[3]=$fec_i_det;
					$servicios_post{$clave_serv_post}[4]=$fec_f_det;
					$servicios_post{$clave_serv_post}[5]=$rango_tiempo;
					$servicios_post{$clave_serv_post}[8]=$key_resumen_dth;

					#print "servicios_post (PostPago) Serv existe $clave_coid_serv $valor_det | $fec_i_det | $fec_f_det | $rango_tiempo \n";
					
				}else{
					#
					$marca_serv_tc_pos= $Mapeogrupodth{$cod_serv_det}[3];
					$rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
					$servicios_post{$clave_serv_post}=[$cod_serv_det,	
													   $serv_det,
													   $valor_det,
													   $fec_i_det,
													   $fec_f_det,
													   $rango_tiempo,
													   $co_id_key[0],
													   $clave_det,
													   $key_resumen_dth,
													   $marca_serv_tc_pos,
													   $det_fecha,
													   $desc_mtp];
					
					$valor_det2f=&formatea_numeros($valor_det);
					#print "servicios_post (PostPago) Serv No existe $clave_coid_serv $valor_det | $fec_i_det | $fec_f_det | $rango_tiempo \n";	

				}
			}
		 }
	}#fin if (($Mapeogrupodth{$cod_serv_det}[3] eq "T"	 

}	#fin for 1
#Se busca registra todos los valores post-pagados recorriendo el arrays rubros_post
$variable_archivo_post="";
open(ARCHIVO_POSTPAGO, ">> $archivo_postpago")|| die "No se puede abrir el archivo\n";	#archivo donde voy a poner los valores pospago
#[10417] - SUD EGO Sumatorias de servicios en Residencial y Multipunto
if ($ltipoproducto eq "DTHM"){
	foreach  (keys %coid_servicios ) {
				$key=$_;
				$verifica=0;
				$verifica_cab=0;
				$verifica_cpr=0;
				$verifica_cps=0;
				$verifica_dpr=0;
				$verifica_dps=0;
				$valor=0;
				$valor_pre=0;
				$valor_pos=0;
				$valor_cab=0;
				$valor_a=0;
				$valor_b=0;
				$valor_c=0;
				$marca_serv_tc="";
				$marca_serv_tc_pre="";
				$marca_serv_tc_pos="";
				$marca_cab="";
				$marca_anexo="";
				$valor_cab_total=0;
				$det_serv="";
				$sncode="";
				$cod_serv_det_pre="";
				$cod_serv_det_pos="";
				$cod_serv_det_cab="";
				$clave_coid_serv_pos="";
				$clave_coid_serv_cab="";
				$clave_coid_serv_pre="";
				$clave_cta_sncode="";
				$fec_i_det_pre="";
				$fec_i_det_pos="";
				$fec_i_det_cab="";
				$marca_pre="";
				$marca_post="";
				$marca_pred="";
				$marca_postd="";
				$det_fecha_pre="";
				$det_fecha_pos="";
				$det_fecha="";
				$det_serv_pre="";
				$det_serv_pos="";
				$clave_coid_servd="";
				if ($coid_servicios{$_}[1] eq "PRE"){
				$co_id_key_pre="";
					$cod_serv_det_pre=$servicios_pre{$_." "."PRE"}[0];
					$det_serv=$servicios_pre{$_." "."PRE"}[1];
					$det_serv_pre=$servicios_pre{$_." "."PRE"}[1];
					$valor_pre=$servicios_pre{$_." "."PRE"}[2];
					$fec_i_det_pre=$servicios_pre{$_." "."PRE"}[3];
					$co_id_key_pre=$servicios_pre{$_." "."PRE"}[6];
					$sncode=$servicios_pre{$_." "."PRE"}[7];
					$key_resumen_dth_pre=$servicios_pre{$_." "."PRE"}[8];
					$marca_serv_tc_pre=$servicios_pre{$_." "."PRE"}[9];
					$det_fecha_pre=$servicios_pre{$_." "."PRE"}[10];
					$desc_mtp_pre=$servicios_pre{$_." "."PRE"}[11];
					$clave_cta_sncode=$lcustcode." ".$cod_serv_det_pre;
					$clave_coid_serv_pre=$co_id_key_pre." ".$cod_serv_det_pre." ".$fec_i_det_pre;
					$clave_coid_servd=$co_id_key_pre;
					$marca_pre="PRE";
					$marca_pred="PRE";
				}
				if ($coid_servicios{$_}[2] eq "POS"){
				$co_id_key_pos="";
					$cod_serv_det_pos=$servicios_post{$_." "."POS"}[0];
					$det_serv=$servicios_post{$_." "."POS"}[1];
					$det_serv_pos=$servicios_post{$_." "."POS"}[1];
					$valor_pos=$servicios_post{$_." "."POS"}[2];
					$fec_i_det_pos=$servicios_post{$_." "."POS"}[3];
					$co_id_key_pos=$servicios_post{$_." "."POS"}[6];
					$sncode=$servicios_post{$_." "."POS"}[7];
					$key_resumen_dth_pos=$servicios_post{$_." "."POS"}[8];
					$marca_serv_tc_pos=$servicios_post{$_." "."POS"}[9];
					$det_fecha_pos=$servicios_post{$_." "."POS"}[10];
					$desc_mtp_pos=$servicios_post{$_." "."POS"}[11];
					$clave_cta_sncode=$lcustcode." ".$cod_serv_det_pos;
					$clave_coid_serv_pos=$co_id_key_pos." ".$cod_serv_det_pos." ".$fec_i_det_pos;
					$clave_coid_servd=$co_id_key_pos;
					$marca_anexo="PO";
					$marca_post="POS";
					$marca_postd="POS";
				}
				if ($coid_servicios{$_}[3] eq "CAB"){
				$co_id_key_cab="";
					$cod_serv_det_cab=$servicios_cab{$_." "."CAB"}[0];
					$det_serv=$servicios_cab{$_." "."CAB"}[1];
					$valor_cab=$servicios_cab{$_." "."CAB"}[2];
					$fec_i_det_cab=$servicios_cab{$_." "."CAB"}[3];
					$co_id_key_cab=$servicios_cab{$_." "."CAB"}[6];
					$sncode=$servicios_cab{$_." "."CAB"}[7];
					$key_resumen_dth_cab=$servicios_cab{$_." "."CAB"}[8];
					$marca_serv_tc=$servicios_cab{$_." "."CAB"}[9];
					$det_fecha=$servicios_cab{$_." "."CAB"}[10];
					$clave_cta_sncode_cab=$lcustcode." ".$cod_serv_det_cab;
					$clave_coid_serv_cab=$co_id_key_cab." ".$cod_serv_det_cab." ".$fec_i_det_cab;
					$marca_cab="CAB";
				}

				$verifica=exists $anexo_b{$clave_cta_sncode};
				$verifica_cab=exists $anexo_b{$clave_cta_sncode_cab};
					
				if ($marca_cab eq ("CAB")){
					$valor_cab=&formatea_numeros($valor_cab);
					if ($verifica_cab == 1){
						#Cabecera
						$valor_cab_total=$valor_cab+$anexo_b{$clave_cta_sncode_cab}[3];
						$valor_cab_total=&formatea_numeros($valor_cab_total);
						$anexo_b{$clave_cta_sncode_cab}[3]=$valor_cab_total;
						
					}else{				
						$valor_cab_total=$valor_cab;
						$valor_cab_total=&formatea_numeros($valor_cab_total);
						$anexo_b{$clave_cta_sncode_cab}=[$coid_servicios{$_}[0],$det_serv,$valor,$valor_cab_total,$coid_servicios{$_}[1],$coid_servicios{$_}[2],$coid_servicios{$_}[3],$marca_serv_tc,$marca_serv_tc_pre,$marca_serv_tc_pos,$sncode];
						#print "\n VALORES ANEXO B  PRE POS -- > ".$coid_servicios{$_}[0]." ".$det_serv." ".$valor." ".$valor_cab_total." ".$coid_servicios{$_}[1]." ".$coid_servicios{$_}[2]." ".$coid_servicios{$_}[3]." ".$marca_serv_tc." ".$marca_serv_tc_pre." ".$marca_serv_tc_pos." ".$sncode;	
					}
				}else{
					$valor_pre=&formatea_numeros($valor_pre);
					$valor_pos=&formatea_numeros($valor_pos);
					if ($verifica == 1){
						$valor=$valor_pre+$valor_pos+$anexo_b{$clave_cta_sncode}[2];
						$valor=&formatea_numeros($valor);
						$anexo_b{$clave_cta_sncode}[2]=$valor;
						
					}else{				
						$valor=$valor_pre+$valor_pos;
						$valor=&formatea_numeros($valor);
						$anexo_b{$clave_cta_sncode}=[$coid_servicios{$_}[0],$det_serv,$valor,$valor_cab_total,$coid_servicios{$_}[1],$coid_servicios{$_}[2],$coid_servicios{$_}[3],$marca_serv_tc,$marca_serv_tc_pre,$marca_serv_tc_pos,$sncode];
						#print "\n VALORES ANEXO B  PRE POS -- > ".$coid_servicios{$_}[0]." ".$det_serv." ".$valor." ".$valor_cab_total." ".$coid_servicios{$_}[1]." ".$coid_servicios{$_}[2]." ".$coid_servicios{$_}[3]." ".$marca_serv_tc." ".$marca_serv_tc_pre." ".$marca_serv_tc_pos." ".$sncode;	
					}
				}	
				
				if( $valor_pos < 0 && $cod_serv_det_pre eq $cod_serv_det_pos &&  $valor_pos lt $valor_pre){
				  #$tipo_post="PON";
				  $clave_coid_serv_pos=$clave_coid_serv_pre;
				  $marca_anexo="CRE";
				  $marca_pre="";
				  $marca_post="";
				  $verifica_cps=exists $anexo_c{$clave_coid_serv_pos};
					if ($verifica_cps == 1){
					    $valor_b=$valor_pre+$valor_pos+$anexo_c{$clave_coid_serv_pos}[2];
						$anexo_c{$clave_coid_serv_pos}[2]=$valor_b;
						
					}else{				
						$valor_b=$valor_pre+$valor_pos;
						$anexo_c{$clave_coid_serv_pos}=[$coid_servicios{$_}[0],$det_serv_pos,$valor_b,$valor_b,$coid_servicios{$_}[1],$coid_servicios{$_}[2],$coid_servicios{$_}[3],$marca_prueba_pos,$co_id_key_pos,$marca_prueba_pos,$marca_serv_tc_pos,$marca_anexo,$cod_serv_det_pos,$fec_i_det_pos,$det_fecha_pos,$desc_mtp_pre,$desc_mtp_pos];
						#print "\n VALORES ANEXO C -- > ".$coid_servicios{$_}[0]." ".$det_serv." ".$valor_b." ".$valor_b." ".$coid_servicios{$_}[1]." ".$coid_servicios{$_}[2]." ".$coid_servicios{$_}[3]." ".$marca_prueba_pos." ".$co_id_key_pos." ".$marca_prueba_pos." ".$marca_serv_tc_pos." ".$marca_anexo;	
					}
				}
				
				$verifica_cpr=exists $anexo_c{$clave_coid_serv_pre};
				$verifica_cps=exists $anexo_c{$clave_coid_serv_pos};
				
				if ($marca_pre eq ("PRE")){
				$marca_prueba_pre="";
					if ($verifica_cpr == 1){
					    $valor_a=$valor_pre+$anexo_c{$clave_coid_serv_pre}[2];
						$anexo_c{$clave_coid_serv_pre}[2]=$valor_a;
					}else{				
						$valor_a=$valor_pre;
						$anexo_c{$clave_coid_serv_pre}=[$coid_servicios{$_}[0],$det_serv_pre,$valor_a,$valor_a,$coid_servicios{$_}[1],$coid_servicios{$_}[2],$coid_servicios{$_}[3],$marca_prueba_pre,$co_id_key_pre,$marca_serv_tc_pre,$marca_prueba_pre,$marca_prueba_pre,$cod_serv_det_pre,$fec_i_det_pre,$det_fecha_pre,$desc_mtp_pre];
						#print "\n VALORES ANEXO C -- > ".$coid_servicios{$_}[0]." ".$det_serv." ".$valor_a." ".$valor_a." ".$coid_servicios{$_}[1]." ".$coid_servicios{$_}[2]." ".$coid_servicios{$_}[3]." ".$marca_prueba_pre." ".$co_id_key_pre." ".$marca_serv_tc_pre." ".$marca_prueba_pre." ".$cod_serv_det_pre." ".$desc_mtp_pre;	
					}
				
			     }
				 if ($marca_post eq ("POS")){
				 $marca_prueba_pos="";
					if ($verifica_cps == 1){
					    $valor_b=$valor_pos+$anexo_c{$clave_coid_serv_pos}[2];
						$anexo_c{$clave_coid_serv_pos}[2]=$valor_b;
						
					}else{				
						$valor_b=$valor_pos;
						$anexo_c{$clave_coid_serv_pos}=[$coid_servicios{$_}[0],$det_serv_pos,$valor_b,$valor_b,$coid_servicios{$_}[1],$coid_servicios{$_}[2],$coid_servicios{$_}[3],$marca_prueba_pos,$co_id_key_pos,$marca_prueba_pos,$marca_serv_tc_pos,$marca_anexo,$cod_serv_det_pos,$fec_i_det_pos,$det_fecha_pos,$desc_mtp_pos];
						#print "\n VALORES ANEXO C -- > ".$coid_servicios{$_}[0]." ".$det_serv." ".$valor_b." ".$valor_b." ".$coid_servicios{$_}[1]." ".$coid_servicios{$_}[2]." ".$coid_servicios{$_}[3]." ".$marca_prueba_pos." ".$co_id_key_pos." ".$marca_prueba_pos." ".$marca_serv_tc_pos." ".$marca_anexo." ".$desc_mtp_pos;	
						if ($valor_b ne "0.00"){
							$variable_archivo_post=$cus_id."|".$co_id_key_pos."|"."PO".$cod_serv_det_pos."|".$valor_b."|".$fecha_c."|"."A"."|".$procesador."|".$fecha_ingreso."|".$nombre_out."|".$lohrefnum."|".$ciclo."|"."\n";
							print ARCHIVO_POSTPAGO "$variable_archivo_post";	
						}
					}
			     }
				 
}

#Fin arma_anexo2 
#[10417] - SUD EGO Llenar la matriz final para anexos en Residencial y Multipunto
}else{
	foreach  (keys %coid_servicios ) {

				$verifica=0;
				$band_pre="0";
				$band_pos="0";
				$valor=0;
				$valor_temp=0;
				$valor_pre=0;
				$valor_pos=0;
				$det_serv="";
				$post="";
				$clave_cta_sncode="$lcustcode $coid_servicios{$_}[0]";
				#

				if ($coid_servicios{$_}[1] eq "PRE"){
					$band_pre="1";
					$cod_serv_det_pre=$servicios_pre{$_." "."PRE"}[0];
					$det_serv_pre=$servicios_pre{$_." "."PRE"}[1];
					$valor=$servicios_pre{$_." "."PRE"}[2];
					$fec_i_det_pre=$servicios_pre{$_." "."PRE"}[3];
					$fec_f_det_pre=$servicios_pre{$_." "."PRE"}[4];
					$rango_time_pre=$servicios_pre{$_." "."PRE"}[5];
					$co_id_key=$servicios_pre{$_." "."PRE"}[6];
					$clave_det_pre=$servicios_pre{$_." "."PRE"}[7];
					$key_resumen_dth_pre=$servicios_pre{$_." "."PRE"}[8];
					$valor_pre = 0 +$valor;
					
				$llave_anexo_pre=$co_id_key." ".$cod_serv_det_pre." ".$key_resumen_dth_pre." ".$fec_i_det_pre;
				}
				if ($coid_servicios{$_}[2] eq "POS"){
					$band_pos="2";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
					$cod_serv_det_pos=$servicios_post{$_." "."POS"}[0];
					$det_serv_pos=$servicios_post{$_." "."POS"}[1];
					$valor=$servicios_post{$_." "."POS"}[2];
					$fec_i_det_pos=$servicios_post{$_." "."POS"}[3];
					$fec_f_det_pos=$servicios_post{$_." "."POS"}[4];
					$rango_time_pos=$servicios_post{$_." "."POS"}[5];
					$co_id_key=$servicios_post{$_." "."POS"}[6];
					$clave_det_pos=$servicios_post{$_." "."POS"}[7];
					$key_resumen_dth_pos=$servicios_post{$_." "."POS"}[8];
					$post="PO";
					$valor_pos = 0 +$valor;
					
				$llave_anexo_pos=$co_id_key." ".$cod_serv_det_pos." ".$key_resumen_dth_pos." ".$fec_i_det_pos;
					
				}
				#Validacion de Rubros Negativos
				if ($valor_pos lt "0"){
					$valor_temp=$valor_pre+$valor_pos; 
					#print "\n Valor temp --> ".$valor_temp;
					if ($valor_temp gt "0"){
					  $band_pre="1";	
					  $band_pos="0";	
					  $valor_pre=$valor_temp; #Se Inserta en Registro PREPAGADOS
					}else{	
					  $band_pos="2";
					  $band_pre="0";					
					  $valor_pos=$valor_temp; #Se Inserta en Registro POSTPAGADOS
					}
				}
				

				 if ($band_pre eq "1" ) {
					$valor_pre=&formatea_numeros($valor_pre);
					$Mdetallefinal{$llave_anexo_pre}=[$clave_det_pre."|".$det_serv_pre."|".$rango_time_pre."|".
													  $valor_pre."|".$valor_pre."|".$fec_i_det_pre."|".
													  $fec_f_det_pre."|".$cod_serv_det_pre."|",
													  $valor_pre,
													  $fec_i_det_pre,
													  $fec_f_det_pre,
													  $cod_serv_det_pre,
													  $det_serv_pre,
													  $co_id_key];
			
					#print "\nDETALLE PRE FINAL: $clave_det_pre $det_serv_pre $rango_time_pre $valor_pre $valor_pre $fec_i_det_pre $fec_f_det_pre $cod_serv_det_pre , $valor_pre, $fec_i_det_pre, $fec_f_det_pre, $cod_serv_det_pre, $det_serv_pre,  $co_id_key\n";
				}
				if ($band_pos eq "2" ) {
				    
					$valor_pos=&formatea_numeros($valor_pos);
					$Mdetallefinal{$llave_anexo_pos}=[$clave_det_pos."|".$det_serv_pos."|".$rango_time_pos."|".
												  $valor_pos."|".$valor_pos."|".$fec_i_det_pos."|".
												  $fec_f_det_pos."|".$cod_serv_det_pos."|".$post."|",
												  $valor_pos,
												  $fec_i_det_pos,
												  $fec_f_det_pos,
												  $cod_serv_det_pos,
												  $det_serv_pos,
												  $co_id_key,
												  $post];	
																										
					if ($valor_pos ne "0.00"){
						$variable_archivo_post=$cus_id."|".$co_id_key."|".$post.$cod_serv_det_pos."|".$valor_pos."|".$fecha_c."|"."A"."|".$procesador."|".$fecha_ingreso."|".$nombre_out."|".$lohrefnum."|".$ciclo."|"."\n";
						print ARCHIVO_POSTPAGO "$variable_archivo_post";
					}
					#print "\nDETALLE POS FINAL: $clave_det_pos $det_serv_pos $rango_time_pos $valor_pos $valor_pos $fec_i_det_pos $fec_f_det_pos $cod_serv_det_pos $post ,$valor_pos, $fec_i_det_pos, $fec_f_det_pos, $cod_serv_det_pos, $det_serv_pos,  $co_id_key\n";																		
				}


	}

}

undef(%servicios_post);
undef(%servicios_pre);
undef(%coid_servicios);


undef(%rubros_post); 
undef(%negativos);
undef(%Mdetalle);
}
sub valores_iva_ice {
	my $shdes=$_[0];
	print "Codigo shdes ".$shdes; #cambio 03-04-2016
	my $des_shdes=$_[1];
	my $aplica_iva=$_[2];
	my $aplica_ice=$_[3];

	if ($aplica_iva eq "S") {
		#$tipo_iva=$MAPEObgh_tariff_plan{$shdes,1}[4]; #cambio 03-04-2016
		$tipo_iva="3"; #cambio 03-04-2016
		print "Tipo IVA ".$tipo_iva; #cambio 03-04-2016
#		$des_iva=$MAPEOtax_category{$tipo_iva}[0];
#		$por_iva=$MAPEOtax_category{$tipo_iva}[1]/100;
		$des_iva=$valor_etiqueta_iva;
		$por_iva=$valor_iva_conf/100;
		print "tipo- ".$tipo_iva."\n";
		print "descripcion- ".$des_iva."\n";
		print "iva- ".$por_iva."\n";			
		if ($des_iva eq "") {
			$des_iva="I.V.A. Por servicios (14%)";
#			$des_iva="I.V.A.";
			$por_iva=0.14;
		}
	} 
	else {
		$des_iva="I.V.A. Por servicios (0%)";
		$por_iva=0.0;
	}

	if ($aplica_ice eq "S") {
		#$tipo_ice=$MAPEObgh_tariff_plan{$shdes,1}[5]; #cambio 03-04-2016
		$tipo_ice="4"; #cambio 03-04-2016
		print "Tipo ICE ".$tipo_ice; #cambio 03-04-2016
		#$des_ice=$MAPEOtax_category{$tipo_ice}[0];
		#$por_ice=$MAPEOtax_category{$tipo_ice}[1]/100;
		$des_ice=$valor_etiqueta_ice;
		$por_ice=$valor_ice_conf/100;
		print "[tipo_ice:1:".$tipo_ice."][des_ice".$des_ice."][por_ice:".$por_ice."]\n";
		if ($des_ice eq "") {
#			$des_ice="ICE en servicios de Telecomunicacion (15%)";
			$des_ice="I.C.E. (15%) (Solo aplica televisi�n)";
			$por_ice=0.15;
		}
	}
	else {
		$des_ice="ICE en servicios de Telecomunicacion (0%)";
		$por_ice=0.0;
	}
} # fin valores_iva_ice

sub obtiene_cedula_nombre {
# INICIO OBTIENE CEDULA NOMBRE
#if ( /RFF\+SC\:([^\:]*)\'\n/ ) { $ci_ruc=$1; } # C�dula #
if ( /RFF\+SC\:([^\:]*)\'/ ) { $ci_ruc=$1; } # C�dula #
if (/NAD\+IV/) {
$_=~ s/\?\://g;
$_=~ s/\?\?/\?/g;
}
if (/NAD\+IV\+\+\+([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:\']*)/) {
  $customer_name=$2;
  $customer_name=~ tr/\|//d;
}
$MAPEOinfo_contr_text{"999999"}=[$ci_ruc,$customer_name];
}# FIN OBTIENE CEDULA NOMBRE

#10417 CAMBIOS
#sub obtiene_valor_resumen {
#my $co_id=$_[0];
#my $key_resumen_celular=$_[1];
#my $cont_calculo=$_[2];
#my $val_resumen;
#my $val_dolares=0;

##$val_resumen=$MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}[0];
#$val_resumen=$MAPEOImprime2{$co_id." ".$key_resumen_celular." ".$cont_calculo}[0];
#@arreglo_num=split(/\|/,$val_resumen);
#$val_dolares=$arreglo_num[3];
#$tp_inicio=&enceraEspacios($arreglo_num[5]);
#$tp_fin=&enceraEspacios($arreglo_num[6]);
#return($val_dolares,$tp_inicio,$tp_fin);
#} # fin obtiene_valor_resumen

sub tim2_calcula {
# INICIO TIM2 CALCULA
foreach  (keys %MAPEOTotales ) {
  #IVA ICE  #
  @arreglo_num=split(/\|/,$_);
  $co_id=$arreglo_num[0];	
  $marca=$MAPEOTotales{$_}[4];
  $key_resumen_dth="42000";
  $key_resumen_total_dth="42200";
  $cta_ps="";
  if ($marca eq "ISD") {
	$key_resumen_dth="31000";
	$key_resumen_total_dth="31100";
  } 
  $IVAv=$MAPEOTotales{$_}[1]*$por_iva;
  $ICEv=$MAPEOTotales{$_}[2]*$por_ice;
  $ICEv1=$MAPEOTotales{$_}[2]*$por_ice;
  $IVAv=&formatea_numeros($IVAv);
  $ICEv=&formatea_numeros($ICEv);
  $suma_total=$MAPEOTotales{$_}[0]+$IVAv+$ICEv;
  $suma_total=&formatea_numeros($suma_total);
  $suma_total=~ tr/\\\.//d;
  $cont_calculo=&obtiene_contador("IVA","0");
  $IVAv=~ tr/\\\.//d;
#  $MAPEOImprime{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_dth."|".$des_iva."||".$IVAv."|".$IVAv."|"];
  $MAPEOImprime2{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_dth."|".$des_iva."||".$IVAv."|".$IVAv."|"];
  $cont_calculo=&obtiene_contador("ICE","0");;
  $ICEv=~ tr/\\\.//d;
  $cont_calculo=&obtiene_contador("SUMA","0");
#  $MAPEOImprime{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_total_dth."|".$suma_total."|"];
  $MAPEOImprime2{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_total_dth."|".$suma_total."|"];
  $ConsumoCelular{$_}=[$suma_total,$MAPEOTotales{$_}[3], $MAPEOTotales{$_}[4]];
  if ($Graba_co_id{$co_id}[0] != "00000000000") {
 	$MAPEOImprime{$co_id." 40000"}=["40000|".$Graba_co_id{$co_id}[0]."|".$co_id."|"];
  }
  $fono1=$Graba_co_id{$co_id}[0];
  &graba_co_id($co_id,$fono1,"2");
 }
 undef(%MAPEOTotales);
}# FIN TIM2 CALCULA

sub buscaValoresResumen{
# INICIO BUSCA VALORES RESUMEN CELULAR
#LIN+12++TP088.4.SP02.AM001.U:SA::++04'
#PIA+1+BASE.F.I.4.3.NOP.ZP13.1.2++'
#IMD++8+PRO::::'
#IMD++8+TM:::TP088:Especial 6'
#IMD++8+SP:::SP02:AMPS ?: Servicios Basicos '
#IMD++8+SN:::AM001:Servicio de Voz'
#QTY+107:83:UNI'
#MOA+125:4.66:USD:959:9'
#MOA+125:4.66:USD:959:19'

#PIA+1+BASE.F.I.4.3.                  NOP.ZP13.1.2++'

if ( /PIA\+1\+*/ ) {    
#  /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'\n/;			19/03/2013
  /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'/;
  $calltype=$1;
  $ttcode=$2;
  $tzcode=$3;
  if ($calltype eq "IC" || $calltype eq "BASE") {
#	if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n[^\n]/ ) {				19/03/2013
	if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'[^\n]/ ) {
		  $codigo_servicio=$1;
		 if (/IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'/) {
		   if ($1 eq "ROTIN" || $1 eq "ROTOP" || $1 eq "ROTIT"|| $1 eq "RITIM" || $1 eq "RITIA"  || $1 eq "RITIN" || $1 eq "ROTIN" || $1 eq "TPESN" || $1 eq "POCC") {
			  $plan=$1;
			  $des_plan=$2;
			 } else {
  			  $plan=$1;
			  $des_plan=$2;
			  $plan1=$plan;
			  $des_plan1=$des_plan;
			  if ($codigo_servicio eq "PV002") {
                if ($bandera_plan eq "0") {
				  &graba_co_id($co_id,$fono,"0",$plan1,$des_plan1);
				  $bandera_plan = "1";
                  $co_id_alter1=$co_id;
		          $co_id_alter2=$co_id;
                }
		      }        	 			  
		    }
		 }
		 # Otengo el Valor el 2do MOA #
		 if (/MOA.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/) {
			 $dolar=$1;
		 }	
		 ($descripcion_ser,$des,$sh_des)=&busca_descripcion_etiqueta($codigo_servicio,$tzcode,$calltype,$ttcode,$plan_tele,"");
		 if ($des eq "") {
			 $des=$sh_des;
		 }
		 $val_dol=$ResumenConsumos{$co_id,$des}[0];
		 $val_dol=$val_dol + $dolar;
		 $ResumenConsumos{$co_id,$des}=[$val_dol];
	} # fi SN
   } # fi calltype
} # fi PIA
}# FIN BUSCA VALORES RESUMNEN CELULAR

sub tim_type_uno{
# INICIO TIME TYPE UNO
$valor_absoluto=0;		
$aire=0;		
$interconexion=0;		
$impuesto_iva=0;
$impuesto_ice=0;
$valor_descuento=0;
$des_descuento="";
$ban_iva="";
$ban_ice="";
$des_tiempo="";
$valor_descuento_original=0;

# Datos Personales del Cliente #
# *I* ANGEL LLERENA HIDALGO CDLA NUEVA KENEDY CALLE B 108 Y LA CUART A ESTE  028 GUAYAQUIL

#NAD+IT+++*I*:JORGE LAGOS BALSECA:VIA A COLOMBIA 1020 Y JORGE A??AZCO  LOCA:L ?: "SUPER MINI  DE TODO " :LAGO AGRIO  *I*:++ECUADOR'
#NAD+IV+++*I*:JORGE LAGOS BALSECA:VIA A COLOMBIA 1020 Y JORGE A??AZCO  LOCA:L ?: "SUPER MINI  DE TODO " :LAGO AGRIO  *I*:++ECUADOR'
#print " linea ".$line."\n";
if (/NAD\+IV/) {
$_=~ s/\?\://g;
$_=~ s/\?\?/\?/g;
}

if (/NAD\+IV\+\+\+([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:\']*)/) {
$cod_insert=$1;
$customer_name=$2;
$customer_name=~ tr/\|//d;
$customer_name=~ tr/\"//d;
$address1=&enceraEspacios($3);
$address2=&enceraEspacios($4);
$address3=$ladrs;
$address1=~ tr/\"//d;
$address2=~ tr/\"//d;
$address1=~ tr/\|//d;
$address2=~ tr/\|//d;
$address3=~ tr/\"//d;
$address3=~ tr/\|//d;
#-----------------------------------
	if ($address1 eq "") {
		$address1=$address2;
		$address2="";
	}
	$zone_name=$5;
	if (/\*I\*\:[^\:\']*/ && $cod_insert eq "") {
		$cod_insert = "*I*";
	}
	if (substr($zone_name,0,1) =~ /^\d/ ) {
		$zone_name =~ /^([^ ]+) (.*)/;
		$zone_no= $1;
		$zone_name= " ".$2;
	} else {
		$zone_no="0"
	}
}
#if ( /RFF\+SC\:([^\:]*)\'\n/ ) {	# C�dula #		19/03/2013
if ( /RFF\+SC\:([^\:]*)\'/ ) {	# C�dula #
	$ci_ruc=$1;		
	$ban_cedula_no_impresa="1";
} 
#if ( /RFF\+PG\:([^\:]*)\'\n/) {  # Forma de Pago #
if ( /RFF\+PG\:([^\:]*)\'/) {  # Forma de Pago #
	$type_pay=$1; 
} 
if ( /FII\+AO\+.*\:+\:([^\+\:\']*)/ ) {		# Descripcion de la forma de pago #
	$type_pay_description=$1; 
}
if (/RFF\+IT\:(\d\.\d+)[^\d]/) {  # Cuenta del cliente #
	$account_no=$1; 
}
if ( /FII\+RH\+9999\:([^\:]*)/) {	# Ciudad #
	$city=$1; 
} 
if ( /DTM\+168\:([^\:]*)/ ) {   # Fecha Fatura #
	$billing_date=$1; 
} 
if (/DTM\+13\:([^\:]*)/) {
  $due_date=$1;
}
# Detalles #
#if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n/ ) {		19/03/2013
if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'/ ) {
  $codigo_servicio=$1;
#  if ( /IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'\n/ ) {			19/03/2013
  if ( /IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'/ ) {
	$tmshdes=$1;
  }
#  if (/PRI\+CAL\:([^\:]*)\:INV\:USD\:959\'\n/) {			19/03/2013
  if (/PRI\+CAL\:([^\:]*)\:INV\:USD\:959\'/) {
	$valor_absoluto=$1;		
  }
  while  (/ALC\+.+\+([^\+]*)\+\'\n/g)  {
     $des_descuento=$1;
      if ( /MOA\+53\:([^\:]*)\:USD.*\n/g ) {
	     $valor_descuento=$1;
	     $valor_descuento_original=$valor_descuento_original+$valor_descuento;
	     $valor_descuento=sprintf('%.2f',$valor_descuento);
	     $valor_descuento1=($valor_descuento* -1) + $MAPEOGeneral{$des_descuento}[1];
	     $MAPEOGeneral{$des_descuento}=["20400",($valor_descuento1)];
      }

  }	# fin while
  if ($des_descuento ne "") {
	  $valor_descuento=$valor_descuento_original;
  }
  # Otengo el Valor el 2do MOA #
  #MOA+125:5.36:USD:959:9'
  #MOA+125:3.75:USD:959:5'
  /MOA\+125\:([^\n\:]*)\:USD.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/;
  $dolar_original=$1;
  $dolar=$2;

#  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/; #  IMD++8+SP    :  ::  SP51   :   GSM ? : Servicios Basicos '		19/03/2013
  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/; #  IMD++8+SP    :  ::  SP51   :   GSM ? : Servicios Basicos '
#  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'\n/; #IMD++8+SP   : : :   SP06  :Servicios OCC'								19/03/2013
  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'/; #IMD++8+SP   : : :   SP06  :Servicios OCC'
  $codigo_servicio_basico=$1;   # Es para obtener si es Servicio de Voz B�sico SP51 SP02
  #IVA_CTA KBA
  if ( /TAX\+1\+VAT\:\:I\.V\.A\.[^D]*\+*\:([^\n\:]*)\:R.*\n/ ) {
      $imp_iva=$1;
	  
  }
  #IVA_CTA KBA
  # IVA   
  if ( /TAX\+1\+VAT\:\:I\.V\.A.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ){	     
	 $impuesto_iva=$1;
     $total_iva=$total_iva+$impuesto_iva;	 
     $ban_iva=1;
	 
  }   
  # ICE #
#  if ( /TAX\+1\+VAT\:\:ICE.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ) {
  if ( /TAX\+1\+VAT\:\:I.C.E.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ) {
     $impuesto_ice=$1;
     $total_ice=$total_ice+$impuesto_ice;
     $ban_ice=1;
  }

  $codigo_aux_rubro="";
  $aux_8504="";

  if ( /PIA\+1\+*/ ) {    
#     /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'\n/;			19/03/2013
     /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'/;
    $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
  } else {
    $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
  }	# fin PIA+1
  if ($descripcion_ser eq "") {
	 $bandera_type_uno="1";
  }
  ($type_key,$aplica_ice)=&verifico_tipo_agrupacion($ban_iva,$ban_ice);
  if ( $type_key eq "21100" ) {
	 $type_key=&verifica_servExterno($codigo_servicio,$type_key);
	 if ( $type_key eq "21650") {
		$total_servicios_Ext=$total_servicios_Ext+$dolar;
	 }
  }
  $dolar=$dolar+$valor_descuento;
  $dolar=sprintf('%.2f',$dolar);
  if ( $dolar != 0 ){
     $descripcion_ser=cambia_etiqueta($descripcion_ser)."|".$type_key;
     $dolar=$dolar + $MAPEOGeneral{$descripcion_ser}[1];

	 #[9587] -ini - SUD CAC - Factura Electr�nica DTH
	 #Se recoge el rubro del descuento 
	 if ( $band_electronica eq "4" && ($type_key eq "20200" || $type_key eq "21100")){
		if($codigo_aux_rubro eq ""){
			$codigo_aux_rubro="0";
		}
		$sumas_desc_rubro=($valor_descuento)+$MAPEOGeneral{$descripcion_ser}[7];
		$sumas_desc_rubro=sprintf('%.2f',$sumas_desc_rubro);

    	#$MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$codigo_aux_rubro,($sumas_desc_rubro)];
		$MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$aplica_ice,$Mapeogrupodth{$codigo_servicio}[3],$codigo_aux_rubro,($sumas_desc_rubro)]; #Se agrega informaci�n de Factura Electr�nica

	}else{#[9587] -fin - Factura Electr�nica DTH

		#$MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$aplica_ice]; #03/04/2013
       $MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$aplica_ice,$Mapeogrupodth{$codigo_servicio}[3]]; #08/07/2013 a�ado si es rubro de cabecera o detalle
	}##
 }
 $codigo_aux_rubro=""; 
}
# Total Consumo del Mes #
# Total consume del Mes se le resta el valor de Otros servicios. 
if (/MOA.*\nMOA\+77\:([^\n\:]*)\:USD.*\n/) {			
  $total_consumo_mes_primera=sprintf('%.2f',&obtieneTotalMenosServExt($1));
  $ban_timm1_completo="1";
}
}# FIN TIM  TYPE UNO

sub tim_type_cero {
# INICIO TIM TYPE CERO
if ( /MOA\+962\:([^\:]*)\:USD.*\n/ ) {  
   $suma_pagos_rec=$1;
}
if ( /MOA\+76\:([^\:]*)\:USD.*\n/ ) {  
$saldo_consumo=$1;
}
### CLS JCE.06/10/2009:Sumas de sobrepagos.
if ( /MOA\+971\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos=$1;
}
if ( /MOA\+978\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos2=$1;
}
if ( /MOA\+981\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos3=$1;
}
if ( /MOA\+988\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos4=$1;
}
$saldo_tmp=$saldo_consumo - $suma_pagos_rec;
$saldo_tmp_tot=$saldo_tmp - $suma_sobrepagos - $suma_sobrepagos2 - $suma_sobrepagos3 - $suma_sobrepagos4;
$saldo_anterior=sprintf('%.2f',$saldo_tmp_tot);
if ( /MOA\+967\:([^\:]*)\:USD.*\n/ ) { # Consumos del Mes
   $consumo_mes=sprintf('%.2f',&obtieneTotalMenosServExt($1));
   $totalservExt=$total_servicios_Ext;
}
if ( /MOA\+968\:([^\:]*)\:USD.*\n/ ) { # Valor a Pagar
   $dolar=sprintf('%.2f',$1);
   $valorpago = $dolar;#10932
   $valor=$consumo_mes."|".$dolar;
   $MAPEOTotales{"21600"}=[$valor];
   $total_due=$dolar;
    #[10932] obtener el valor de pago
	sub valor_pago{
		return $valorpago;
	}
	#[10932] obtener el valor de pago
}
if ( /MOA\+11\:([^\:]*)\:USD.*\n/ ) { # Pagos Recibidos
   $dolar=sprintf('%.2f',$1);
   #Verifica si cuadra el total de la Factura.
   $sumacomprobacion=sprintf('%.2f',$saldo_anterior + $dolar + $consumo_mes + $totalservExt);
   if ( $sumacomprobacion != $total_due) {
	    $saldo_anterior_tmp = $total_due - $dolar - $consumo_mes - $totalservExt;
		$saldo_anterior=sprintf('%.2f',$saldo_anterior_tmp);
		# Verifica si cuadra el total de la Factura II. 
		$sumacomprobacion=sprintf('%.2f',$saldo_anterior + $dolar + $consumo_mes + $totalservExt);
		if ( $sumacomprobacion != $total_due) {
			$bandera_type_cero="1";
		}else{
			$bandera_type_cero="0";
		}
	}else{
		$bandera_type_cero="0";
	}
    $valor=$saldo_anterior."|".$dolar;
    $MAPEOTotales{"21400"}=[$valor];
 }
}# FIN TIM TYPE CERO


sub tim_type_dos{
# inicio tim dos

open(FILE_READ,$file);

$/="LIN\+";
$key_resumen_dth1="99990";
$key_resumen_dth="99991";
$total_iva=0;
$total_ice=0;
$suma_total=0;
$valor_contratado=0;
$ban="0";
$ban_dos="0";
$valor_descuento=0;
$total_consumo_mes_primera=0;
$total_servicios_Ext=0;
$account_no="";
$ci_ruc=""; 
$cod_insert="";
$customer_name="";
$address3="";
$address1="";
$address2="";
$zone_no="";
$zone_name="";
$type_pay="";
$type_pay_description="";
$city="";
$billing_date="";
$total_due="000";
$bandera_plan = "0";
$bandera_plan1 = "0";
$quiebre_co_id = "-999";
$cont_detalle = 0;
#$fecha_emite="";
$marca_mtp="N";#10417
$band_activ_mtp="0";#10417
$ban_com=0;#[10932] Inicializacion de bandera de compensacion
while(<FILE_READ>){ 
	 $line=$_;
	 if ($. == 1) {
		 $lcustcode="";
 	     $lprgcode="";
 	     $lcostcenter_id="";
 	     $lseq_id="";
 	     $lbillcycle="";
		 $lohrefnum="";
		 $ladrs="";

		if (/CUSTCODE\|([^\n]+)\n/) {
		 #1.10306366|1|2|2|28 
	     @arreglo_num=split(/\|/,$1);
 	     $lcustcode=$arreglo_num[0];
 	     $lprgcode=$arreglo_num[1];
 	     $lcostcenter_id=$arreglo_num[2];
 	     $lseq_id=$arreglo_num[3];
 	     $lbillcycle=$arreglo_num[4];
		 $lohrefnum=$arreglo_num[5];
		 $ltipofactura=$arreglo_num[6]; 
		 $lmail=$arreglo_num[7]; 
		 $ltelefono=$arreglo_num[8]; 
		 $tipo_identificacion=$arreglo_num[9]; 
         $ladrs=$arreglo_num[10]; 
		 $lohrefnum=~ tr/'.//d;
         $ltipoproducto=$arreglo_num[11]; #10417 SUD EGO		  
		}
		#saco la fecha de factura.
		if ( /DTM\+168\:([^\:]*)/ ) {   # Fecha Fatura #
			$fecha_fact=$1; 
		} 
		# saco la fecha de corte
		if ( /DTM\+3\:([^\:]*)/ ) {   # Fecha Corte #
			$fecha_corte=$1; 
		} 
	 } # fin si es 1er bloque
	 if ($ltipofactura eq "4"){
		 $band_electronica="4";
	 }
 	 if (/PAT\+5/) {
	    $banderaUno="1";
		
		if ($Cuentas_Excentas{$lcustcode} eq "S") {
			&valores_iva_ice($plan1,$des_plan1,"N","N");
		}
		else {
			&valores_iva_ice($plan1,$des_plan1,"S","S");
		}
        # Obtengo la Cuenta #
		if (/RFF\+IT\:(\d\.\d+)[^\d]/) {  $account_no_resumen=$1; }#4505
		&obtiene_cedula_nombre;
		&tim2_calcula;
		$clave="10000";
		$num_invoice="000-000-0000000";
		$emission_due_date="06032007";
		$total_due="000";
		$type_key="00000";
		$total_iva=0;
		$total_ice=0;
		$valor_absoluto=0;		
		$impuesto_iva=0;
		$impuesto_ice=0;
        $imp_iva=0; #10417
		$cod_bulk="";#10417
		$valor_descuento=0;
		$des_descuento="";
		$ban_iva="";
		$ban_ice="";
     } # fin path+5
	 #print "valor banderaUno".$banderaUno."\n";	
 	 if ($banderaUno eq "0") {
		 #print "valor ban_dos".$ban_dos."\n";
		  if ($ban_dos eq "0") {			
		    &timDosBuscaValores;
		  } else {
			&buscaValoresResumen;
		  }
		  if (/UNB\+UNOC/) {
		  $ban_dos="0";
		  $ban_acceso="0";
		  $account_no_resumen="0"; 
		  }
	 } else { 		
		if ($ltipoproducto eq "DTHM"){
			 if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'/ ) {
				$codigo_servicio_p=$1;
			 }
			#print "\n MUESTRA CODIGO ".$codigo_servicio_p." - ".$co_id."\n";			  
			if ($codigo_servicio_p eq "DE026" || $codigo_servicio_p eq "DE027" || $codigo_servicio_p eq "DE028" || $codigo_servicio_p eq "EV302") {
				$band_activ_mtp="1";
				&timDosBuscaValores;
				&tim2_calcula;
				$dolar="";
				$ban_dos="0"; 
				$ban_acceso = "0";
			}
		}
	   	&tim_type_uno;
	   	&tim_type_cero;
	 }
	#[10932] - SUD INI MLU Expresion para indicar que es de las zonas afectadas 
	if (/TAX\+3\+VAT\:\:I\.V.*E/){
		$ban_com = 1;#[10932] Encender la bandera de compensacion
	}
	#[10932] - SUD FIN MLU Expresion para indicar que es de las zonas afectadas
	} #while

    close FILE_READ;

	# NCA 23/03/2013
	#&arma_anexo;
	&arma_anexo2;
	# Fin NCA 23/03/2013
   if ($banderaUno eq "1") {
	   $total_iva=sprintf('%.2f',$total_iva);
	   $total_ice=sprintf('%.2f',$total_ice);
	   $type_key_iva="20900";
	   if ($total_iva gt 0) {
		$MAPEOGeneral{$des_iva}=[$type_key_iva,$total_iva,"","","IVA"];#[9587] - SUD CAC se agrega marca para el impuesto
	   }
	   if ($total_ice gt 0) {
		$MAPEOGeneral{$des_ice}=[$type_key_iva,$total_ice,"","","ICE"];#[9587] - SUD CAC se agrega marca para el impuesto
	   }
	   $MAPEOTotales{"21300"}=[$total_consumo_mes_primera];

	   #[9587] - SUD CAC 
	   $base_imp_iva="";
	   $base_imp_ice="";
	   $base_iva_ice="";
	   #[9587] - SUD CAC 

		foreach (keys %MAPEOGeneral) {
			$dolar=sprintf('%.2f',$MAPEOGeneral{$_}[1]);
			$dolar=~ tr/\.//d;
			$descCambiar=$_;
			@arreglo_num=split(/\|/,$descCambiar);
			$descCambiar=$arreglo_num[0];			

				# inicio NCA 10/07/2013 Mapeo con tabla de configuracion
				$dolar_2="";
				$descripcion_dth_2="";
				$codigo_dth_2="";
				$key_dth_1="";
				$key_cab="3";
				if ( $MAPEOGeneral{$_}[0] eq "20200"){
					if ($MAPEOGeneral{$_}[5] eq "T" || $MAPEOGeneral{$_}[5] eq "TC") { # pertenece al grupo de TV
						$descripcion_dth_2="Servicios Televisi�n";

						if ($ltipofactura eq "4"){
							$codigo_dth_2="STDO1";#[9587] - SUD CAC - Se agrega c�digo principal al servicio
						}else{
						$codigo_dth_2="";
						}

						$key_dth_1="20200";
						$key_aux="1";
					}
					else{
						if($MAPEOGeneral{$_}[5] eq "O"){# pertenece al grupo de Otros
								#Los que solo aplican IVA
								$descripcion_dth_2="Otros Servicios";

								if ($ltipofactura eq "4"){
									$codigo_dth_2="STOO1";#[9587] - SUD CAC - Se agrega c�digo principal al servicio
								}else{
								$codigo_dth_2="";
								}

								$key_dth_1=$MAPEOGeneral{$_}[0];
								$key_aux="2";
							}
							else	# son parte de la cabecera sin agrupacion.
							{
								$descripcion_dth_2=$Mapeogrupodth{$MAPEOGeneral{$_}[3]}[1];
								$codigo_dth_2=$MAPEOGeneral{$_}[3];
								$key_dth_1="20200";
								if ($MAPEOGeneral{$_}[3] eq "DE003" || $MAPEOGeneral{$_}[3] eq "EV302") {#9587 - Factura Electr�nica DTH
									$key_aux="3";
								}
								else
								{
									$key_cab=$key_cab + 1;
									$key_aux=$key_cab;
								}
							}
					}
					$key_dth_2=$key_dth_1."|".$descripcion_dth_2;
					$dolar_2=sprintf('%.2f',$Mgrupodth_lineas{$key_dth_2}[2]);
					$dolar_2= $dolar_2 + $dolar;
					
					#[9587] - INI SUD CAC 
				    if ($ltipofactura eq "4"){

						$valor_desc_rubro=sprintf('%.2f',$MAPEOGeneral{$_}[7]);
						$valor_desc_rubro=~ tr/\.//d;
						
						#bases imponible de impuesto IVA - ICE
						if ( $MAPEOGeneral{$_}[5] eq "T") {
							#pertenece al grupo de TV
   						    $base_imp_ice=$base_imp_ice+$dolar;
						}else{
							$base_imp_iva=$base_imp_iva+$dolar;
						}

						$Mgrupodth_lineas{$key_dth_2}=[$key_dth_1,$descripcion_dth_2,$dolar_2,$codigo_dth_2,$key_aux,$MAPEOGeneral{$_}[6],$valor_desc_rubro];#codigo axuxiliar y descuento

					}else{ #[9587] - FIN SUD CAC
						
					$Mgrupodth_lineas{$key_dth_2}=[$key_dth_1,$descripcion_dth_2,$dolar_2,$codigo_dth_2,$key_aux];
					}

				}else{#Guarda los impuestos

				   #[9587] - INI SUD CAC 
				   if ($ltipofactura eq "4"){
					   #$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"];
					   $Mgrupodth_lineas{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0],$descCambiar,$dolar,$MAPEOGeneral{$_}[2],$MAPEOGeneral{$_}[3],$MAPEOGeneral{$_}[4]];
					   #print "Imprime Impuestos |".$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|".$MAPEOGeneral{$_}[4]."|\n";

   				       if ($MAPEOGeneral{$_}[4] eq "ICE"){						    
					       $base_iva_ice=$dolar;
					   }

				   }else{#[9587] - INI SUD CAC 

					$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"];
				   }
				}
				# fin NCA 10/07/2013 Mapeo con tabla de configuracion.		


			$dolar=$MAPEOSuma{$MAPEOGeneral{$_}[0]}[0] + $MAPEOGeneral{$_}[1];
			$MAPEOSuma{$MAPEOGeneral{$_}[0]}=[$dolar,$_];
		}

##[9587] - INI SUD CAC 
#print "|base ICE".$base_imp_ice."|Base IVA".$base_imp_iva."|Base IVA-ICE".$base_iva_ice."|\n";
$base_imp_iva_final=$base_imp_ice+$base_imp_iva+$base_iva_ice;
#print "|base ICE".$base_imp_iva_final;#KBA
##[9587] - INI SUD CAC 

foreach (keys %Mgrupodth_lineas) {


#[9587] -ini - SUD CAC - Factura Electr�nica DTH
if ($ltipofactura eq "4"){

print "$Mgrupodth_lineas{$_}[0]|\n";

 	if (($Mgrupodth_lineas{$_}[0] eq "20200")) {	 
	     $MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[4].$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[5]."|".$Mgrupodth_lineas{$_}[6]."|"];
	     #print "\n Electr�nica ".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[5]."|".$Mgrupodth_lineas{$_}[6]."|\n";
	}	
	if (($Mgrupodth_lineas{$_}[0] eq "20900")) {

	    #$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"];
    	if (($Mgrupodth_lineas{$_}[5] eq "IVA")) {
			#10932 INI Calculo del iva
			$baseimponibleiva = $base_imp_iva_final;
			$valor_iva_n = $baseimponibleiva * 0.14/100;	
			$valor_iva_n=sprintf('%.2f',$valor_iva_n);
			$valor_iva_n=~ tr/\.//d;	
			#10932 FIN 
			$MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$valor_iva_n."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|".$base_imp_iva_final."|"];
		    #$MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|".$base_imp_iva_final."|"];#10932
		    #print "Imprime Impuestos Mgrupodth_lineas|".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|=|".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|\n";
		}

		if (($Mgrupodth_lineas{$_}[5] eq "ICE")) {
			$valor_ice_n = $Mgrupodth_lineas{$_}[2];#10932
			$MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|".$base_imp_ice."|"];
		    #print "Imprime Impuestos Mgrupodth_lineas|".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|=|".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|\n";
		}
	}
	
	if (($Mgrupodth_lineas{$_}[0] eq "21650")){
		
		$MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[4].$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|"];
	}

}else#[9587] - fin - SUD CAC - Factura Electr�nica DTH
{

$MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[4].$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|"];
#  print "\n Normal ".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[5]."|\n";

}


}#fin Mgrupodth_lineas 

##[9587] - INI SUD CAC 
$base_imp_ice="";
$base_imp_iva="";
$base_imp_iva_final="";
##[9587] - INI SUD CAC 


$MAPEOImprimeUno{"40000"}=["40000"."|".$account_no_resumen."|".$account_no_resumen."|"]; 
$total_tv="0";
$total_ot="0";
$cont_f="0";
#[10417]- INI SUD EGO
$coid_key="0";
$total_tc="0";
$subtotal_tc="0";
$coid_key_marca="0";
$coid_key_cab="0";
#[10417] - INI PRUEBA EGO  
if ($ltipoproducto eq "DTHM"){
	$sncode="";
	$cont_f_cab="0";
	foreach  (keys %anexo_b) {
		$key=$_;
		$valor_anex_cab=0;
		$subtotal_tc=0;
		$subtotal_iva_tcab=0;
		$subtotal_iva_tcabc=0;
		my @co_id_key=split(' ',$key);
		$sncode=$anexo_b{$key}[0];
		$det_serv=$anexo_b{$key}[1];
		#$valor_anexo=$anexo_b{$key}[2];
		#$valor_anex_i=$anexo_b{$key}[2];
		$valor_anex_cab=$anexo_b{$key}[3];
		$valor_anex_cab_i=$anexo_b{$key}[3];
		$valor_anex_to=&formatea_numeros($anexo_b{$key}[2]);
		$valor_anex_to_i=&formatea_numeros($anexo_b{$key}[2]);
		$marca_anex_pre=$anexo_b{$key}[4];
		$marca_anex_pos=$anexo_b{$key}[5];
		$marca_anex_cab=$anexo_b{$key}[6];
		$marca_serv_tc=$anexo_b{$key}[7];
		$marca_serv_pos=$anexo_b{$key}[8];
		$marca_serv_pre=$anexo_b{$key}[9];
		$valor_anex_to=~ tr/\.//d;
		$coid_key_marca=$co_id_key[0];#coid pruebas
		$clave_det_mar="0";
			#$aplica_ice=$MAPEOGeneral{$det_serv."|"."20200"}[4];
			#if (($aplica_ice eq "S")&&($valor_anexo ne "000")) {
			$clave_det_mar="41000";
			$clave_det_mar_ot="42000";
			$clave_des_television="41100";
			$clave_des_otservicio="42100";
			if ($marca_serv_tc eq "TC"){
				$valor_anex_cab_p=&formatea_numeros($anexo_b{$key}[3]);
				$valor_anex_cab_pi=&formatea_numeros($anexo_b{$key}[3]);
				$valor_anex_cab_p=~ tr/\.//d;	
				if ($valor_anex_cab_p ne "000"){
					$total_tv=$total_tv + $valor_anex_cab_p;	
					$MAPEOImprimeUno{$clave_det_mar.$coid_key_marca.$sncode.$cont_f}=[$clave_det_mar."|".$det_serv."|"."|\$".$valor_anex_cab_pi."|".$valor_anex_cab_p."|"."|"."|".$sncode."|"."|"."|"];  
				}		
			}
			if ($marca_serv_pos eq "T" || $marca_serv_pre eq "T"){	
				$valor_anex_tv_mtp=&formatea_numeros($anexo_b{$key}[2]);
				$valor_anex_tv_mtpi=&formatea_numeros($anexo_b{$key}[2]);
				$valor_anex_tv_mtp=~ tr/\.//d;
				if ($valor_anex_tv_mtp ne "000"){
					$total_tv=$total_tv + $valor_anex_tv_mtp;	
					$MAPEOImprimeUno{$clave_det_mar.$coid_key_marca.$sncode.$cont_f}=[$clave_det_mar."|".$det_serv."|"."|\$".$valor_anex_tv_mtpi."|".$valor_anex_tv_mtp."|"."|"."|".$sncode."|"."|"."|"];  
				}
			}
			if (($marca_serv_pos eq "O")||($marca_serv_pre eq "O")){
				$total_ot=$total_ot + $valor_anex_to;	
				$MAPEOImprimeUno{$clave_det_mar_ot.$coid_key_marca.$sncode.$cont_f}=[$clave_det_mar_ot."|".$det_serv."|"."|".$valor_anex_to."|\$".$valor_anex_to_i."|"."|"."|".$sncode."|"."|"."CRE|"];  
			}
			#DESCUENTO SERVICIOS
			if (($marca_serv_pos eq "TD")||($marca_serv_pre eq "TD")){
				$total_des_stv=$total_des_stv + $valor_anex_to;	
				$MAPEOImprimeUno{$clave_des_television.$coid_key_marca.$sncode.$cont_f}=[$clave_des_television."|".$det_serv."|"."|".$valor_anex_to."|\$".$valor_anex_to_i."|"."|"."|".$sncode."|"."|"."|"];  
			}
			#DESCUENTO OTROS SERVICIOS
			if (($marca_serv_pos eq "TO")||($marca_serv_pre eq "TO")){
				$total_des_sot=$total_des_sot + $valor_anex_to;	
				$MAPEOImprimeUno{$clave_des_otservicio.$coid_key_marca.$sncode.$cont_f}=[$clave_des_otservicio."|".$det_serv."|"."|".$valor_anex_to."|\$".$valor_anex_to_i."|"."|"."|".$sncode."|"."|"."|"];  
			}
			$cont_f=$cont_f + 1;
				#}
		
        if ($marca_anex_cab eq "CAB"){
			$coid_key_cab=$co_id_key[0];#coid pruebas
			$clave_det_cab="0";
			$clave_det_anexC="0";
			#$valor_anex_cab=~ tr/\.//d;
			if ($valor_anex_cab ne "000") {	
				$clave_det_cab="42150";
				$clave_det_anexC="43400";
					if (($marca_serv_tc eq "TC") || ($marca_serv_tc eq "C")){	
						$aplica_ice=$MAPEOGeneral{$det_serv."|"."20200"}[4];
						if ($aplica_ice eq "S") {
							$subtotal_tc=$valor_anex_cab;
							$ice=1.15;
							$subtotal_tc= $subtotal_tc * $ice;
							#$subtotal_iva_tc = $subtotal_tc * 1.12; #KBA POR CAMBIOS IVA
							$subtotal_iva_tc = $subtotal_tc * 1.14;  #KBA POR CAMBIOS IVA
							$total_tc=$total_tc + $subtotal_iva_tc;	
							$subtotal_iva_PRUEBA=$subtotal_iva_tc ;
							$subtotal_iva_tcab=&formatea_numeros($subtotal_iva_PRUEBA);
							$subtotal_iva_tc_i=&formatea_numeros($subtotal_iva_PRUEBA);
							$subtotal_iva_tcab=~ tr/\.//d;
							$MAPEOImprimeUno{$clave_det_anexC.$coid_key_cab.$cont_f_cab}=[$clave_det_anexC."|".$det_serv."|"."|\$".$subtotal_iva_tc_i."|".$subtotal_iva_tcab."|"."|"."|".$sncode."|"."|"."|"];  
						}else{
							$subtotal_c=$valor_anex_cab;
							#$subtotal_c=$subtotal_c * 1.12; #KBA POR CAMBIOS IVA
							#$subtotal_c=$subtotal_c * 1.14;  #KBA POR CAMBIOS IVA
							$valor_iva_c=$subtotal_c * ($imp_iva/100); #KBA POR CAMBIOS IVA
                            $subtotal_c=$subtotal_c+$valor_iva_c ; #KBA POR CAMBIOS IVA
							$total_tc=$total_tc + $subtotal_c; 
							$subtotal_iva_tcabc=&formatea_numeros($subtotal_c);
							$subtotal_c_i=&formatea_numeros($subtotal_c);
							$subtotal_iva_tcabc=~ tr/\.//d;
							$MAPEOImprimeUno{$clave_det_anexC.$coid_key_cab.$cont_f_cab}=[$clave_det_anexC."|".$det_serv."|"."|\$".$subtotal_c_i."|".$subtotal_iva_tcabc."|"."|"."|".$sncode."|"."|"."|"];  							
						}							

					}
					if ($marca_serv_tc eq "C"){	
						$subtotal_c=$valor_anex_cab;
						#$subtotal_c=$subtotal_c * 1.12;
						$subtotal_iva_tcabc=&formatea_numeros($subtotal_c);
						$subtotal_c_i=&formatea_numeros($subtotal_c);
						$subtotal_iva_tcabc=~ tr/\.//d;
						$MAPEOImprimeUno{$clave_det_cab.$coid_key_cab.$cont_f_cab}=[$clave_det_cab."|".$det_serv."|"."|\$".$subtotal_c_i."|".$subtotal_iva_tcabc."|"."|"."|".$sncode."|"."|"."|"];  
					}#print "\n key CADENA 42150 = >  ".$coid_key_cab.$clave_det_cab.$cont_f_cab."\n ";
			}
			$cont_f_cab=$cont_f_cab + 1;			
		}
	
	}
}else{
	#FIN SUD EGO
foreach  (keys %Mdetallefinal) {
  $key=$_;
  $trama=$Mdetallefinal{$key}[0];
  @arreglo_det=split(/\|/,$trama);
  $serv_det=$arreglo_det[1];
  $valor_det=&formatea_numeros($arreglo_det[3]);
  $valor_tv_sum=&formatea_numeros($arreglo_det[3]);
  $valor_det=~ tr/\.//d;
  if ($valor_det ne "000") {	# if para que no pinte los valores en cero  
	$valor_for=&formatea_numeros($arreglo_det[3]);
	$aplica_ice=$MAPEOGeneral{$serv_det."|"."20200"}[4];
	if ($aplica_ice eq "S") {
	  $clave_det="41000" ;
#	  $total_tv=$total_tv + $arreglo_det[3];
	  $total_tv=$total_tv + $valor_det;
	  $clave_impr=$arreglo_det[0].$co_id;
	  
	  #10203 Se a�ade fecha de contratacion y activacion de feature y planes DTH
	  $fechas_ActiCont="|";	
	  $fechas_ActiCont=$MAPEOCtasSVAFecha_CTA{$account_no_resumen." ".$arreglo_det[7]}[0];
	  #print "fechas_ActiCont-->".$fechas_ActiCont."\n";
	  
 	  if ( $fechas_ActiCont eq "" ){  ## 10554
	      $fechas_ActiCont=$MapeoAbonadosFeat{$account_no_resumen." ".$arreglo_det[7]}[0];
   	      if ( $fechas_ActiCont eq "" ){
			   ##$fechas_ActiCont=$MapeoAbonadosPlan{$account_no_resumen." ".$arreglo_det[7]}[0];	
			   $fechas_ActiCont=$MapeoAbonadosPlan{$account_no_resumen." ".$plan_mostrar}[0];			   
			   if ( $fechas_ActiCont eq "" ){
					$fechas_ActiCont="|";					   
			   }   	  
		  }
	  }

#	  print "feature_cta-->".$arreglo_det[7]."\n";
	  #10203
#	  print "Mdetallefinal ".$clave_det."|".$arreglo_det[1]."|".$arreglo_det[2]."|\$".$valor_for."|".$valor_det."|".$arreglo_det[5]."|".$arreglo_det[6]."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|".$fecha_contrato."|".$fecha_activacion."|"."\n";  #10203
	  
#	  $MAPEOImprimeUno{$clave_impr.$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|".$arreglo_det[2]."|\$".$valor_for."|".$valor_det."|".$arreglo_det[5]."|".$arreglo_det[6]."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"]; 
	  $MAPEOImprimeUno{$clave_impr.$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|".$arreglo_det[2]."|\$".$valor_for."|".$valor_det."|".$arreglo_det[5]."|".$arreglo_det[6]."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|".$fechas_ActiCont."|"]; 
	
	}
	$cont_f=$cont_f + 1;
  } # fin if valores en cero

	}#fin for
}
#10417 - INI SUD EGO
$cont_fi="0";	
$clave_det_43450="0";
if ($ltipoproducto eq "DTHM" && $coid_key_marca ne "0"){
	$cont_fi=$cont_f + 1;
	#Linea 
	$clave_det_43450="43450";
	$total_tc=&formatea_numeros($total_tc);
	$total_tc_i=&formatea_numeros($total_tc);
	$total_tc=~ tr/\.//d;
	$MAPEOImprimeUno{$clave_det_43450.$coid_key_marca.$cont_fi}=[$clave_det_43450."|\$".$total_tc_i."|".$total_tc."|"];	# total de otros servicios
}
# 10417 - FIN SUD EGO	   		
$cont_f="0";
$total_tv_fact="000";
$total_ot_fact="000";
$clave_det="";
$aplica_ice="";
$coid_prueba="";
$valor_det="";
#[10417] - INI SUD EGO  
if ($ltipoproducto ne "DTHM"){
	#FIN PRUEBA EGO
	foreach  (keys %Mdetallefinal) {
	  $key=$_;
	  $trama=$Mdetallefinal{$key}[0];
	  @arreglo_det=split(/\|/,$trama);
	  $serv_det=$arreglo_det[1];
	  $valor_det=&formatea_numeros($arreglo_det[3]);
	  $valor_det=~ tr/\.//d;
	  if ($valor_det ne "000") {	# if para que no pinte los valores en cero  
		$valor_for=&formatea_numeros($arreglo_det[3]);
		$aplica_ice=$MAPEOGeneral{$serv_det."|"."20200"}[4];
		if ($aplica_ice eq "N") {
		  $clave_det="42000" ;
	#	  $total_ot=$total_ot + $arreglo_det[3];
		  $total_ot=$total_ot + $valor_det;
		  $clave_impr=$arreglo_det[0].$co_id;
		  if ($arreglo_det[7] ne "DP025") {
			$MAPEOImprimeUno{$clave_impr.$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|"."|".$valor_det."|\$".$valor_for."|"."|"."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"];
		  }
		  else
		  {  
			$MAPEOImprimeUno{$clave_impr.$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|".$arreglo_det[2]."|".$valor_det."|\$".$valor_for."|".$arreglo_det[5]."|".$arreglo_det[6]."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"];
		  }
		}
		$cont_f=$cont_f + 1;
	  } # fin if valores cero
	}
} 

#[10417]- INI SUD EGO Imprime linea 50100
#$total_tv=&formatea_numeros($total_tv);
#$total_ot=&formatea_numeros($total_ot);
$total_tv=~ tr/\.//d;
$total_ot=~ tr/\.//d;
if ($ltipoproducto eq "DTHM"){
	foreach (keys %Mgrupodth_lineas) {
		$clave_det_total="42200" ;
		if (($Mgrupodth_lineas{$_}[0] eq "20900")&&($Mgrupodth_lineas{$_}[5] eq "IVA")) {
		$impuestoIva = $Mgrupodth_lineas{$_}[2];
		
		}
		if (($Mgrupodth_lineas{$_}[0] eq "20900")&&($Mgrupodth_lineas{$_}[5] eq "ICE")) {
		$impuestoIce = $Mgrupodth_lineas{$_}[2];
		
		}
		$MAPEOImprimeUno{$clave_det_total.$arreglo_det[0].$co_id_key[0].$cont_f}=[$clave_det_total."|".$total_tv."|".$total_ot."|".$impuestoIce."|".$impuestoIva."|"];	# total de otros servicios
	}
}else {
#[10417]- FIN EGO 
$MAPEOImprimeUno{$arreglo_det[0].$co_id."42200".$cont_f}=["42200"."|".$total_tv."|".$total_ot."|"];	# total de otros servicios
}
$coid_key="0";
$total_descuento_tv="0";
$total_descuento_ot="0";
$total_tc="0";
$clave_det_mar="";
$coid_key_marca="0";
$coid_key_cab="0";
$cont_fi="0";
$marca_serv_tc="";
$det_fecha="";
#[10417] - INI PRUEBA EGO  
if ($ltipoproducto eq "DTHM"){
	$cont_f=2;
	$cont_fto=1;
	$cont_dtv=0;
	$cont_f_cab="0";
	$valor_anexo="";
	$total_anexo="0";
	$total_anexo_ot="0";
	$coid_key_marca_ant ="0";
	$coid_key_ant_ot="0";
	$total_anexo_d="";
	$total_iva_anexd="";
	$total_ice_53000="";
	$coid_key_ant_53000="0";
	$marca_ttv="";
	$marca_tot="";
	$subtot_iva_anexd="0";
	$subtot_anexo_d="0";
	$subtot_anexo_dtv="0";
	$marca_serv_tc_pre="";
	$marca_serv_tc_pos="";
	$total_fact_anexo_d_fi="";
	$clave_prueba_ant53000="0";
	foreach  (sort(keys %anexo_c)) {
		$key=$_;
		$valor_anexo_mtp="";
		my @co_id_key=split(' ',$key);
		$det_serv=$anexo_c{$key}[1];
		$valor_anexo=&formatea_numeros($anexo_c{$key}[2]);
		$valor_anexo_i=&formatea_numeros($anexo_c{$key}[2]);
		$valor_anexo_ot=&formatea_numeros($anexo_c{$key}[2]);
		$valor_anexo_ot_i=&formatea_numeros($anexo_c{$key}[2]);
		$valor_desc_tv=&formatea_numeros($anexo_c{$key}[2]);#descuento
		$valor_desc_tv_i=&formatea_numeros($anexo_c{$key}[2]);#descuento
		$valor_desc_ot=&formatea_numeros($anexo_c{$key}[2]);#descuento
		$valor_desc_ot_i=&formatea_numeros($anexo_c{$key}[2]);#descuento
		$marca_anex_pre=$anexo_c{$key}[4];
		$marca_anex_pos=$anexo_c{$key}[5];
		$co_id_c=$anexo_c{$key}[8];
		$marca_serv_tc=$anexo_c{$key}[7];
		$marca_serv_tc_pre=$anexo_c{$key}[9];
		$marca_serv_tc_pos=$anexo_c{$key}[10];
		$marca_anexo=$anexo_c{$key}[11];
		$sncode=$anexo_c{$key}[12];
		$fecha_i=$anexo_c{$key}[13];
		$det_fecha=$anexo_c{$key}[14];
		$des_plan_mtp=$anexo_c{$key}[15];
		$valor_desc_tv=~ tr/\.//d;
		$valor_desc_ot=~ tr/\.//d;
	    #COMPARAR CO_ID	
		if (($marca_anex_pre eq "PRE") || ($marca_anex_pos eq "POS")){
			$coid_key_marca=$co_id_key[0];#coid pruebas
			$coid_key_marca_mtp=$co_id_key[0];#coid pruebas
		    $clave_det_mar="0";
			$aplica_ice=$MAPEOGeneral{$det_serv."|"."20200"}[4];
				 if (($marca_serv_tc_pos eq "T")||($marca_serv_tc_pre eq "T")){
					 if (($aplica_ice eq "S")&&($valor_anexo ne "000")) {
						$clave_det_mar="51000";
						$valor_anexo_mtp=$valor_anexo;
						$valor_anexo_mtp=~ tr/\.//d;
						$MAPEOImprimeUno{"45000".$coid_key_marca."9999990".$det_serv.$cont_f}=[$clave_det_mar."|".$det_serv."|".$det_fecha."|\$".$valor_anexo_i."|".$valor_anexo_mtp."|"."|"."|".$sncode."|".$marca_anexo."|".$fecha_i."|"];  
						$cont_f=$cont_f+1;
					}
					#Linea 51100	
					if (($coid_key_marca_ant eq $coid_key_marca )||($coid_key_marca_ant eq "0")){
						$coid_key_marca_ant=$coid_key_marca;
						$total_anexo=$total_anexo + $valor_anexo;	
						if ($total_anexo ne "000"){
							$clave_det_subt="51100";
							$cod_prueba="9999991";
							$total_anexo_P=&formatea_numeros($total_anexo);
							$total_anexo_Pi=&formatea_numeros($total_anexo);
							$total_anexo_P=~ tr/\.//d;
							$MAPEOImprimeUno{"45000".$coid_key_marca.$cod_prueba}=[$clave_det_subt."|\$".$total_anexo_Pi."|".$total_anexo_P."|"];	# total de otros servicios
							$marca_tv ="S";
						}
					}else{
						$total_anexo_P="0";
						$total_anexo_Pi="0";
						$total_anexo_ot_P="0";
						$total_anexo_ot_Pi="0";
						$coid_key_marca_ant=$coid_key_marca;
						$total_anexo=$valor_anexo;
					}
				 }
				 # Linea 52000
				 if (($marca_serv_tc_pos eq "O")||($marca_serv_tc_pre eq "O")){
					if ($valor_anexo_ot ne "000"){
						if ($aplica_ice eq "S") {
							$valor_anexo_ot= $valor_anexo_ot * 1.15;
						}
						$clave_det_mar_ot="52000";
						$valor_anexo_mtp=$valor_anexo_ot;
						$valor_anexo_mtp_i=&formatea_numeros($valor_anexo_mtp);
						$valor_anexo_mtp=~ tr/\.//d;
						#$valor_anexo_ot=~ tr/\.//d;
						$MAPEOImprimeUno{"45000".$coid_key_marca."9999993".$det_serv.$cont_fto}=[$clave_det_mar_ot."|".$det_serv."|".$det_fecha."|".$valor_anexo_mtp."|\$".$valor_anexo_mtp_i."|"."|"."|".$sncode."|".$marca_anexo."|".$fecha_i."|"];  
						$cont_fto=$cont_fto+1;

					}
					#Linea 52100	
					if (($coid_key_ant_ot eq $coid_key_marca )||($coid_key_ant_ot eq "0")){
						$coid_key_ant_ot=$coid_key_marca;
						$total_anexo_ot=$total_anexo_ot + $valor_anexo_ot;	
						if ($total_anexo_ot ne "000"){
							$clave_det_ot="52100";
							$cod_det_ot="9999994";
							$total_anexo_ot_P=&formatea_numeros($total_anexo_ot);
							$total_anexo_ot_Pi=&formatea_numeros($total_anexo_ot);
							$total_anexo_ot_P=~ tr/\.//d;
							$MAPEOImprimeUno{"45000".$coid_key_marca.$cod_det_ot}=[$clave_det_ot."|\$".$total_anexo_ot_Pi."|".$total_anexo_ot_P."|"];	# total de otros servicios
							$marca_ot="S";
						}
					}else{
						$marca_ot="N";
						$total_anexo_P="0";
						$total_anexo_Pi="0";
						$total_anexo_ot_P="0";
						$total_anexo_ot_Pi="0";
						$coid_key_ant_ot=$coid_key_marca;
						$total_anexo_ot=$valor_anexo_ot;
					}					
				  }	
				  #Linea 53000
				  $cod_prueba_ot="9999999"; 
				  $clave_prueba53000="45000".$coid_key_marca.$cod_prueba_ot;
				  if(($clave_prueba_ant53000 eq $clave_prueba53000 )||($clave_prueba_ant53000 eq "0")) {
					  if (($total_anexo_ot_Pi ne "000")||($total_anexo_Pi ne "000")){
						$clave_prueba_ant53000=$clave_prueba53000;	 
						$subtot_anexo_dtv=$total_anexo_Pi;
						$subtot_anexo_d=$total_anexo_Pi+$total_anexo_ot_Pi;
						$total_ice_53000=$subtot_anexo_dtv*0.15; 
						$subtot_iva_anexd=$subtot_anexo_d+$total_ice_53000;
						#$total_iva_anexd=$subtot_iva_anexd*0.12; 
						$total_iva_anexd=$subtot_iva_anexd*0.14; #KBA se modifica por cambio del iva
						$total_iva_anexod=substr($total_iva_anexd,0,index($total_iva_anexd,'.')+3); #kba
						$total_ice_anexo_d=substr($total_ice_53000,0,index($total_ice_53000,'.')+3);#kba						
						$total_anexo_d=$subtot_anexo_d+$total_ice_53000+$total_iva_anexd; #kba						
						$total_fact_anexo_d=substr($total_anexo_d,0,index($total_anexo_d,'.')+3); #kba
						$total_fact_anexo_d=~ tr/\.//d;
						$total_iva_anexod=~ tr/\.//d;#IVA
						$total_ice_anexo_d=~ tr/\.//d;#ICE  				   
						$clave_det_53000="53000";    
			            $MAPEOImprimeUno{$clave_prueba53000}=[$clave_det_53000."|".$total_fact_anexo_d."|".$total_ice_anexo_d."|".$total_iva_anexod."|"];
					  }
					  
				  }else {
						$total_fact_anexo_ant=$total_anexo_d;
						$total_fact_anexo_d_fi=$total_fact_anexo_d_fi+$total_fact_anexo_ant;
						$clave_prueba_ant53000=$clave_prueba53000; 
				  }
				  #Linea 43000
				  if ($total_fact_anexo_d ne "000" ){
					  $aCoid="";
					  foreach (keys %MAPEOserviciosMtpDTH) {	
						$aCustcode=$MAPEOserviciosMtpDTH{$_}[0];
						$aServicio=$MAPEOserviciosMtpDTH{$_}[1];
						$aCoid=$MAPEOserviciosMtpDTH{$_}[2];
						if (($aCustcode eq $lcustcode) && ($aCoid eq $coid_key_marca_mtp)){
							$clave_serv_mtp="43000";
							#$total_fact=$total_fact;
							$MAPEOImprimeUno{$clave_serv_mtp.$aCoid.$cont_f2}=[$clave_serv_mtp."|".$aServicio."|".$total_fact_anexo_d."|"];	# total de otros servicios
							$cont_fi=$cont_f2 + 1;
						}
						#Linea 50000
						if (( $aCoid == $coid_key_marca_mtp ) && ($marca_serv_tc_pos eq "T"|| $marca_serv_tc_pre eq "T") ){
							$clave_det_pri="50000";
							$MAPEOImprimeUno{"45000".$co_id_c.$cont_f_c}=[$clave_det_pri."|".$aServicio."|".$des_plan_mtp."|"];	# total de otros servicios	
						}
					  }
				  }	  
				 
				  #Marca Descuento 51200
				  if (($marca_serv_tc_pos eq "TD")||($marca_serv_tc_pre eq "TD")){
					$total_descuento_tv=$total_descuento_tv + $valor_desc_tv;	
					$clave_det_desc_tv="51200";
					$MAPEOImprimeUno{"45000".$coid_key_marca."9999992".$fecha_i.$cont_dtv}=[$clave_det_desc_tv."|".$det_serv."|".$det_fecha."|".$valor_desc_tv."|\$".$valor_desc_tv_i."|"."|"."|".$sncode."|".$marca_anexo."|".$fecha_i."|"];  
					$cont_dtv=$cont_dtv+1;
				  }	
				  #Marca Descuento 52200
				  if (($marca_serv_tc_pos eq "TO")||($marca_serv_tc_pre eq "TO")){
					$total_descuento_ot=$total_descuento_ot + $valor_desc_ot;	
					$clave_det_desc_ot="52200";
					$MAPEOImprimeUno{"45000".$coid_key_marca."9999995".$fecha_i.$cont_dot}=[$clave_det_desc_ot."|".$det_serv."|".$det_fecha."|".$valor_desc_ot."|\$".$valor_desc_ot_i."|"."|"."|".$sncode."|".$marca_anexo."|".$fecha_i."|"];  
					$cont_dot=$cont_dot+1;
				  }	
		}
	}
	#Linea 43100
	$clave_fact="43100";
	#$total_43100=sprintf('%.3f',($total_fact_anexo_d_fi+$total_anexo_d));
	$total_43100=substr(($total_fact_anexo_d_fi+$total_anexo_d),0,index(($total_fact_anexo_d_fi+$total_anexo_d),'.')+3);
	$total_43100_d=$total_43100;#kba
	$total_43100_di=$total_43100;#kba	
	$total_43100_d=~ tr/\.//d;
	$MAPEOImprimeUno{$clave_fact}=[$clave_fact."|".$total_43100_d."|\$".$total_43100_di."|"];	# total de otros servicios
		  


}
#[10417]- FIN EGO 
undef(%Mdetallefinal);
undef(%anexo_b);#10417
undef(%anexo_c);#10417

		foreach (keys %MAPEOSuma) {
			$key_suma="";
#16042013			if ($_ eq "20000" ) { $key_suma="20100"; }
			if ($_ eq "20200")  { $key_suma="20300"; }
			if ($_ eq "20000" ) { $key_suma="NING"; }
#			if ($_ eq "20200")  { $key_suma="NING"; }
			if ($_ eq "20400")  { $key_suma="20600"; }
			if ($_ eq "20900")  { $key_suma="21000"; }
			if ($_ eq "21100")  { $key_suma="21200"; }
			if ($_ eq "21650")  { $key_suma="NING"; }
		   $dolar=sprintf('%.2f',$MAPEOSuma{$_}[0]);
		   $dolar=~ tr/\.//d;
		   if ( $key_suma ne "NING" ) {
				#[10932] - SUD INI MLU Verifica el codigo de la llave del iva ice
				if ($key_suma eq "21000"){
					$band_imp = 1;
					$key_sumas = "21000";
				}else{#[10932] - SUD FIN MLU 
			   		$MAPEOImprimeUno{$key_suma}=[$key_suma."|".$dolar."|"];
			   	}
			}
		}

# validacion de si cuadran los valores de tv y de otros servicios
		$total_tv_fact=$Mgrupodth_lineas{"20200|Servicios Televisi�n"}[2];
		$total_ot_fact=$Mgrupodth_lineas{"20200|Otros Servicios"}[2];
		if ($total_tv_fact eq "") {
			$total_tv_fact="000";
		}
		if ($total_ot_fact eq "") {
			$total_ot_fact="000";
		}
		if (length($total_tv_fact) == 2)
		{
			$total_tv_fact="0".$total_tv_fact;
		}
		if (length($total_ot_fact) == 2)
		{
			$total_ot_fact="0".$total_ot_fact;
		}
#		print "valor " .$total_tv."|".$total_tv_fact."|".$total_ot."|".$total_ot_fact."|n";
		if ($total_tv ne $total_tv_fact || $total_ot ne $total_ot_fact) {
			$dif_tv = $total_tv - $total_tv_fact;
			$dif_ot = $total_ot - $total_ot_fact;
			if ($dif_tv gt "1" || $dif_tv lt "-1") {
				$band_descuadre="1";
			}else{
				if ($dif_ot gt "1" || $dif_ot lt "-1") {
					$band_descuadre="1";
				}
			}
		}
# fin validacion de si cuadran los valores de tv y de otros servicios
		$dolar=$MAPEOSuma{"20000"}[0] + $MAPEOSuma{"20200"}[0] + $MAPEOSuma{"20400"}[0];
		$dolar=sprintf('%.2f',$dolar);
		$dolar=~ tr/\.//d;
		$MAPEOImprimeUno{"20800"}=["20800|".$dolar."|"];
		
		#[10932] - SUD INI MLU 
		# Calculo de la Compensacion 2% de zonas afectadas
		if ($ban_com eq 1){
			$baseimponible = $dolar + $valor_ice_n;
			($compensacion, $ivanormal, $ivasub) = &calcula_compensacion($baseimponible);
			
			$MAPEOImprimeUno{"21650"}=["21650|(-) Descuento Solidario 2% IVA|".$compensacion."|".$baseimponible."||".$ivanormal."|".$ivasub."|"];
		}

		foreach (keys %MAPEOTotales) {
			$MAPEOTotales{$_}[0]=~ tr/\.//d;
			# sumatoria del consumo del mes
			if($_ eq "21300"){
				#Consumo del mes
				$valorconmes = $dolar + $valor_iva_n + $valor_ice_n;
				$valorconmes = $valorconmes/100;
				$valorconmes=sprintf('%.2f',$valorconmes);
				$valorconmes=~ tr/\.//d;
				if($valorconmes > 0){
					$MAPEOImprimeUno{$_}=[$_."|".$valorconmes."|"];
				}else{
					$MAPEOImprimeUno{$_}=[$_."|".$MAPEOTotales{$_}[0]."|"];
				}
			}else{	
				#Valor a pagar		
				if($_ eq "21600"){
					$valor_pagar = &valor_pago(); 
					$valor_pagar=~ tr/\.//d;
					if($valorconmes > 0){
						$MAPEOImprimeUno{$_}=[$_."|".$valorconmes."|".$valor_pagar."|"];
					}else{
						$MAPEOImprimeUno{$_}=[$_."|".$MAPEOTotales{$_}[0]."|"];
					}
				}else{
					$MAPEOImprimeUno{$_}=[$_."|".$MAPEOTotales{$_}[0]."|"];
				}
			}
		}
		# sumatoria del iva ice
		if($band_imp eq 1){
			$valor_imp = $valor_iva_n + $valor_ice_n;
			$MAPEOImprimeUno{$key_sumas}=[$key_sumas."|".$valor_imp."|"];
		}
		#[10932] - SUD FIN MLU 
		$total_due=~ tr/\.//d;
		$num_invoice="000-000-0000000";
		if ($tipo_ejecucion eq "C" || $lbillcycle eq "34" ) {
			$num_invoice=$lohrefnum;
			$fecha_sec=`date '+%Y-%m-%d-%H-%M-%S'`;
			$fecha_sec=&enceraEspacios($fecha_sec);
			print FILE_SECUENCIAS $account_no."|".$num_invoice."|".$ciclo."|".$fecha_sec."\n";
		}
        if ($lcustcode eq $Nuevo_rubro{$lcustcode}[0]){
			$arreglo_3= $Nuevo_rubro{$lcustcode}[3];
            chop($arreglo_3);
			$MAPEOImprimeUno{"21675"}=["21675|(1) N/C: Cr�dito por  intereses en servicio de \"Factura Detallada\" ". $Nuevo_rubro{$lcustcode}[2] ." a ". $arreglo_3 . ", concepto reclasificado en Mayo 2009" ."|".$Nuevo_rubro{$lcustcode}[2]."|".$arreglo_3."|"];
			$MAPEOImprimeUno{"21676"}=["21675|(2) N/D: D�bito por intereses en servicio \"Distribuci�n de Estado de Cuenta\" ". $Nuevo_rubro{$lcustcode}[2] ." a ". $arreglo_3 . ", concepto reclasificado en Mayo 2009" . "|".$Nuevo_rubro{$lcustcode}[2]."|".$arreglo_3."|"];
		  } 
		# 10417 INI SUD EGO 
		if ($ltipoproducto eq "DTHM"){
			$cod_bulk="2";
		}
		# 10417 FIN SUD EGO
		 #10203 INI obtiene la fecha de emision	
		 $fecha_actual=`date '+%d/%m/%Y`;
		 $fecha_actual=&enceraEspacios($fecha_actual);
		 $fecha_emite=&calcula_fecha($fecha_actual);
         #10203 FIN obtiene la fecha de emision	
         print "fecha corte dat: ".$fecha_corte."\n";
         #$MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1.$address2.$address3."|"."|"."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."|"]; #2589 
		 #$MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1.$address2.$address3."|"."|"."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."|".$fecha_corte."|".$fecha_emite."|"]; #10203
		 #$MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1.$address2.$address3."|"."|"."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."|"."|".$fecha_corte."|".$fecha_emite."|".$ltipoproducto."|"]; #10417 KBA SE ELIMINA EL ESPACIO QUE ESTA DE MAS
		 $MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1.$address2.$address3."|"."|"."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."|".$fecha_corte."|".$fecha_emite."|".$ltipoproducto."|"]; #10417
		if ($ltipofactura eq "4") {		
			if ($ltipofactura eq "0"){$ltipofactura="";}
			if ($lmail eq "0"){$lmail="";}
			if ($ltelefono eq "0"){$ltelefono="";}
			if ($tipo_identificacion eq "0"){$tipo_identificacion="";}
			$MAPEOImprimeUno{"10100"}=["10100|".$ltipofactura."|".$lmail."|".$ltelefono."|".$tipo_identificacion."|"]; 
			$band_electronica="4";
			if ($lmail eq "") {	$band_correo="1"; }
			if ($ltelefono eq "") {	$band_correo="2"; }
			if ($tipo_identificacion eq "") {$band_correo="3"; }
		}
		#10932 SUD MLU se agrega la forma de pago del sri					
		#$codigo_sri=$MAPEOtax_codigo_sri{$account_no}[0]; 
		$codigo_sri=$MAPEOtax_codigo_sri{$type_pay_description}[0];
 
		if ($codigo_sri eq ""){
			$codigo_sri = 20;
		}
		
		$MAPEOImprimeUno{"10200"}=["10200|".$codigo_sri."|"]; 
		#10932 SUD MLU se agrega la forma de pago del sri					
		$MAPEOImprimeUno{"21700"}=["21700| |"];
	}
}# fin tim dos

sub procesa_tim{
# inicio procesa_tim
$file=$file_ls;
$file1=$file_ls;
if ($cuenta_out_llam eq "1") {
   $file1=$ruta_dat."/OUT.llam";
   $file1=$file1;
}
print $file1." ".$cuenta_out_llam." ".$ruta_dat."\n";
#  Informaci�n del Tim II #
undef(%MAPEOImprime);
undef(%MAPEOImprime2); #10179

&tim_type_dos;

if ($band_correo eq "0" && $band_no_imprime_cuenta eq "0" && $ban_timm1_completo eq "1" && $ban_cedula_no_impresa eq "1" && $band_numero_no_existe eq "0" && $bandera_type_uno eq "0" && $num_secuencia ne "000-000-0000000" && $bandera_type_cero ne "1" && $band_descuadre ne "1" ) {
	if ($band_electronica eq "4")
	{ 
      open(FILE_WRITE_E,">> $file_out_e");
	  $fileGlob = \*FILE_WRITE_E;	 
	}
	else
	{
	  open(FILE_WRITE,">> $file_out"); 
      $fileGlob = \*FILE_WRITE;
	}
	&graba_datos($fileGlob);
	$post_final="servicios_postpago_".$fecha_corte."_".$procesador.".txt";
	$archivo_post_final=$carpeta_post."/".$post_final;
	open(ARCHIVO_POST, ">> $archivo_post_final");	#archivo final donde voy a poner los valores pospago
	$llena_post="cat $archivo_postpago >> $archivo_post_final";
	my $llenado=`$llena_post`; 	
} else { 
	if ($banderaUno eq "1") {
		if ($band_no_imprime_cuenta ne "0") {
		 print FILE_WRITE_DET "001|Plan No Encontrado|".$account_no."|".$file_ls."\n";
		}
		if ($ban_timm1_completo ne "1") {
		 print FILE_WRITE_DET "002|TIMM 1 No es Correcto|".$account_no."|".$file_ls."\n";
		}
		if ($ban_cedula_no_impresa ne "1") {
		 print FILE_WRITE_DET "003|C�dula No Impresa|".$account_no."|".$file_ls."\n";
		}
		if ($bandera_type_uno ne "0") {
		 print FILE_WRITE_DET "005|Descripcion en Blanco|".$account_no."|".$file_ls."\n";
		}
		if ($num_secuencia eq "000-000-0000000") {
		 print FILE_WRITE_DET "006|Numero de Secuencia|".$account_no."|".$file_ls."\n";
		}
		if ($bandera_type_cero eq "1") {
		 print FILE_WRITE_DET "007|Saldos de Factura no Cuadran |".$account_no."|".$file_ls."\n";
		}
		if ($band_correo eq "1") {
		 print FILE_WRITE_DET "008|Correo No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		if ($band_correo eq "2") {
		 print FILE_WRITE_DET "009|Telefono del Cliente No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		if ($band_correo eq "3") {
		 print FILE_WRITE_DET "010|Tipo de Identificaci�n del Cliente No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		if ($band_descuadre eq "1") {
		 print FILE_WRITE_DET "011|Los valores de los servicios de Television o de Otros servicios no cuadran|".$account_no."|".$file_ls."\n";
		}
	}	# fin si banderaUno = 1
	my $fileGlob = \*FILE_WRITE_2;
	&graba_datos($fileGlob);
	#10179
	#Guardar la informacion en el archivo postpago_dth
	$post_final="servicios_postpago_".$fecha_corte."_".$procesador.".txt";
	$archivo_post_final=$carpeta_post."/".$post_final;
	open(ARCHIVO_POST, ">> $archivo_post_final");	#archivo final donde voy a poner los valores pospago
	$llena_post="cat $archivo_postpago >> $archivo_post_final";
	my $llenado=`$llena_post`;
	#10179
 }
    # inicio borro el archivo servicios_pospago porque hay error en la ejecucion
	$borra_archivo="rm -f $archivo_postpago";
	my $resul= `$borra_archivo`; 	
	# fin borro archivo
}# fin procesa_tim

sub llama_tim{
#inicio llama tim
#10179 cambio de rutas en carpetas procesados
#$carpeta_ciclo="procesados/CICLO_".$ciclo;
$carpeta_ciclo="DTH/procesados/CICLO_".$ciclo;
#10179 cambio de rutas en carpetas procesados
$file_out=$ruta.$carpeta_ciclo."/output_doc1_dth_".$procesador."_".$ciclo.".txt";
$file_out_e=$ruta.$carpeta_ciclo."/output_doc1_dth_".$procesador."_".$ciclo."_e.txt"; 
$file_out_2=$ruta.$carpeta_ciclo."/output_doc1_dth_".$procesador."_".$ciclo.".out";
$file_out_3=$ruta.$carpeta_ciclo."/output_doc1_dth_".$procesador."_".$ciclo.".det";
#10179 cambio de rutas en carpetas de archivos dat
#$ruta_dat=$ruta."dat/CICLO_".$ciclo;
$ruta_dat=$ruta."DTH/dat/CICLO_".$ciclo;
#10179 cambio de rutas en carpetas de archivos dat
#10179 cambio en nombre de archivo .DAT
#$ruta_ls=$ruta_dat."/20*_".$procesador.".dat";
$ruta_ls=$ruta_dat."/20*_".$procesador."_DTH.dat";
#10179 cambio en nombre de archivo .DAT
$carpeta_post=$ruta."/servicios_postpago_dth/CICLO_".$ciclo;
$crea_carpeta="mkdir $carpeta_post";
#10179 07062016
#respaldar carpeta de servicios postpago dth cada vez que se ejecuta el obtiene
$fecha_carpeta_post=`date '+%Y%m%d_%H%M%S'`;
$carpeta_post_respaldo=$ruta."servicios_postpago_dth/CICLO_".$ciclo."_".$fecha_carpeta_post;
$respalda_carpeta="mv $carpeta_post $carpeta_post_respaldo";
#10179 07062016
if (! -d $carpeta_post) {
	my $resultado= `$crea_carpeta`; 	
#10179 07062016
}else{
  if ($procesador eq "1"){
	my $resultado= `$respalda_carpeta`;
	my $resultado= `$crea_carpeta`;
  }
}
#10179 07062016
#$archivo_postpago=$ruta.$carpeta_ciclo."/servicios_posptpago_".$procesador."_".$ciclo.".txt";

open(FILE_WRITE_2,"> $file_out_2");#archivo destino Cuentas no Procesado por Inconsistencia
open(FILE_WRITE_DET,"> $file_out_3");#Detalle de las Cuentas no Procesado por Inconsistencia
open(FILE_WRITE_LOGS,"> $archivo_logs");#archivo logs
$fecha=&enceraEspacios(`date '+%Y-%m-%d-%H-%M-%S'`);
$archivo_sec=$archivo_sec.$procesador."_".$ciclo."-".$tipo_ejecucion."-".$fecha.".dat";
open(FILE_SECUENCIAS, "> $archivo_sec");#Archivo Secuencias
#open(ARCHIVO_POSTPAGO, "> $archivo_postpago");	#archivo donde voy a poner los valores pospago

$fecha=`date '+%Y/%m/%d %H:%M:%S'`;
print $fecha."\n";
print  FILE_WRITE_LOGS $fecha."\n";
open LS, "ls -1 $ruta_ls|" or die "Error llama_tim No se encuentra archivos ls -1 $ruta_ls  $!";
while (<LS>) {
$file_ls=&enceraEspacios($_);
$file_ls=~ /(.*)\/([^\/]*)$/;
$path=$1;
$archivo=$2;
print $archivo;
@arch=split(/\_/,$archivo);
$cus_id=$arch[1];
$archivo_ctl=$ruta."log/timm_doc1.ctl";
$comando="cat $archivo_ctl";	
my $res_control= `$comando`;
$res_control=&enceraEspacios($res_control);

if ($res_control eq "1") {
	print "Proceso Finalizado Manualmente ".$procesador."\n";
	print FILE_WRITE_LOGS "Proceso Finalizado Manualmente ".$procesador."\n";
	exit;	
}

undef(%MAPEOGeneral);
undef(%MAPEOSuma);
undef(%MAPEOTotales);
undef(%MAPEOImprime);
undef(%MAPEOImprime2);#10179
undef(%MAPEOImprimeUno);
undef(%banderaUno);
undef(%Graba_co_id);
undef(%VerificaNumber);
undef(%Mdetalle);
#undef(%Mdetallefinal);
undef(%Mgrupodth);
undef(%Mgrupodth_lineas);#mapeo lineas 20200 - 20200
#undef(%MAPEOserviciosMtpDTH);#10417

$des_plan="";
$plan1="";
$banderaUno="0";
$cont_calculo=5;
$ban_timm1_completo="0";
$ban_cedula_no_impresa="0";
$band_no_imprime_cuenta="0";
$band_numero_no_existe="0";
$bandera_type_uno="0";
$bandera_type_cero="0";
$band_electronica="0"; 
$band_correo="0";
$band_descuadre="0";
@arreglo=split(/\_/,$archivo);
print $file_ls;
$clave_det="";
$serv_det="";
$rango_tiempo="";
$valor_det="";
$valor_for="";
$fec_i_det="";
$fec_f_det="";
$cod_serv_det="";

$key_resumen_dth="";
$descripcion="";
$rango_tiempo="";
$valor_ser="";
$valor_ser="";
$fecha_i="";
$fecha_f="";
$codigo_servicio="";
$descripcion_ser="";
$dol="";
&procesa_tim($file_ls);

$comando="compress $file";	
my $result= `$comando`; 

$/="\n";

}	# fin while LS	
close FILE_WRITE; # .txt
close FILE_WRITE_E; # _E.txt 
close FILE_WRITE_2; # .out
close FILE_WRITE_DET; # .det
close FILE_SECUENCIAS; # Archivo de Secuencias
$fecha=`date '+%Y/%m/%d %H:%M:%S'`;
print $fecha."\n";
print FILE_WRITE_LOGS $fecha."\n";
close FILE_WRITE_LOGS;
close ARCHIVO_POSTPAGO;
close ARCHIVO_POST;
print "Fin del Proceso ".$procesador."\n";
}#fin llama_tim

#10203 funcion para obtener la fecha de emision
sub calcula_fecha{
my $fecha=$_[0];
my $anio=substr($fecha,6,4);
my $mes=substr($fecha,3,2);
my $dia=substr($fecha,0,2);

%arreglo= ('01','31','02','28','03','31','04','30','05','31','06','30','07','31','08','31','09','30','10','31','11','30','12','31');
$mes_dia=$arreglo{$mes};
if ($mes == 02){
	if (($anio%4)==0 and ($anio%100)!=0 or ($anio%400)==0)
	{
	  $mes_dia="29";
	}
}
if ($dia == $mes_dia) {
       $dia="01";
	   if ($mes != 12 ){
		  $mes=$mes+1;
		  if ($mes < 10){
		    $mes="0".$mes;
		  }
	   }else{
	       $mes="01";
		   $anio=$anio+1;
	   }
}else{	   
   $dia=$dia+1;    
   if ($dia < 10){
   $dia="0".$dia;
   }
}

#$fecha_emision=$dia."/".$mes."/".$anio;
$fecha_emision=$anio.$mes.$dia;

return($fecha_emision);
}
#SUD INI MLU calculo para la compensacion 2% 
sub calcula_compensacion{
	$baseimponible = $_[0];
	$ivasub = $baseimponible * 0.14/100;	
	$ivasub=sprintf('%.2f',$ivasub);
	$ivasub=~ tr/\.//d; 
	$ivanormal = $baseimponible * 0.12/100;	
	$ivanormal=sprintf('%.2f',$ivanormal);
	$ivanormal=~ tr/\.//d; 
	$compensacion = $ivanormal - $ivasub;
	$compensacion=sprintf('%.2f',$compensacion/100);
	$compensacion=~ tr/\.//d; 		
	return ($compensacion,$ivanormal,$ivasub);
}
#SUD FIN MLU calculo para la compensacion 2% 