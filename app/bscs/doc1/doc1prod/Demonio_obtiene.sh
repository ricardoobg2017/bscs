#--=============================================================================
# ===================== Creado por       :  John Cardozo Vill                 ==		 
# ===================== Lider CLS        :  Ing. Sheyla Ramirez		      ==
# ===================== Lider Claro SIS  : Jackeline Gomez                    ==
# ===================== Fecha de creacion: 29/Agosto/2014		      ==
# Proyecto [9797] BCH AUTOMATICOS        : Ejecucion del Obtiene Automatico   ==
#--=============================================================================
# Modificado por    : SUD Vanessa Gutierrez G.
# Proyecto	    : [10179] Nuevos ciclos de Facturación DTH
# Lider Conecel     : SIS Paola Carvajal
# Objetivo	    : Cambio de rutas de archivos .DAT y .TXT
#--=====================================================================================--
#!/usr/bin/sh
. /bscs/bscsprod/.setENV
RUTA=/doc1/doc1prod
cd $RUTA

archivo_configuracion=$RUTA"/configuracion_ultimo.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
ruta_local=$ruta_principal
nombre_archivo_control=$ruta_local"/Demonio_obtiene.ctl"
NOMBRE_FILE_SQL_OBTIENE=$ruta_sql"/Ejecucion_obtiene_comienzo.sql"
NOMBRE_FILE_SQL_BILLCYCLE=$ruta_sql"/Ejecucion_obtiene_billcycle.sql"
NOMBRE_FILE_SQL_CANTIDAD=$ruta_sql"/Ejecucion_cantidad_billcycle.sql"
NOMBRE_FILE_SQL_BAJAR_OBTIENE=$ruta_sql"/Validacion_bajar_obtiene.sql"
NOMBRE_FILE_SQL_VALIDACION_DAT=$ruta_sql"/Validacion_actualizar_obtiene.sql"
NOMBRE_FILE_LOG=$ruta_logs"/Demonio_obtiene.log"
NOMBRE_ERROR_LOG=$ruta_logs"/Demonio_obtiene_error.log" 
Control_alarma=$ruta_local"/Demonio_Control_alarma_obtiene.txt"
cd $ruta_local

# ================== Control para que no este dos procesos a la vez ========================= #

if [ -s $nombre_archivo_control ]
then
	echo "\n    ACTUALMENTE SE ENCUENTRA EN EJECUCION Demonio_Ejecuta_doc1_cuentas.sh"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi

echo $$ > $nombre_archivo_control 
validacion_estados_dat(){
nuevo_estado_txt=$1;
billcycle_dat=$2;
cat > $NOMBRE_FILE_SQL_VALIDACION_DAT <<END
set heading off; 
UPDATE MONITOR_FIN_MES SET ESTADO_TXT = '$nuevo_estado_txt'  WHERE BILLCYCLE = $billcycle_dat;
commit;
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_VALIDACION_DAT  > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_VALIDACION_DAT > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
rm -f $nombre_archivo_control 
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de realizar la conexion a la base para actualizar el campo de verificacion del obtiene ESTADO_TXT .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para actualizar el campo de verificacion del obtiene ESTADO_TXT .......... Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi
}
cat > $NOMBRE_FILE_SQL_OBTIENE <<END
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off 
spool ejecucion_obtiene_verificar.txt
SELECT CANTIDAD_OBTIENE||'|'||TIPO FROM sysadm.PARAMETRO_DAT_AUTOM WHERE INICIO_EXTRACCION = 'S';
spool off
spool verificacion_obtiene_monitor.txt
SELECT COUNT(*) FROM MONITOR_FIN_MES WHERE ESTADO_DAT = 'X';
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_OBTIENE > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_OBTIENE > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
#rm -f $nombre_archivo_control 
rm -f ejecucion_obtiene_verificar.txt
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de realizar la conexion a la base para obtener los parametros de inicializacion del obtiene .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener los parametros de inicializacion del obtiene .......... Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi

var=`cat verificacion_obtiene_monitor.txt`
if [ ! -s ejecucion_obtiene_verificar.txt ]
then
rm -f $nombre_archivo_control
rm -f ejecucion_obtiene_verificar.txt
rm -f verificacion_obtiene_monitor.txt
rm -f $NOMBRE_FILE_SQL_OBTIENE
exit 0;
elif [ $var -eq 0 ]
then
rm -f $nombre_archivo_control
rm -f ejecucion_obtiene_verificar.txt
rm -f verificacion_obtiene_monitor.txt
rm -f $NOMBRE_FILE_SQL_OBTIENE
exit 0;
fi

#-------===================== verifica  espacio en filesystem =====-------------------

espacio=`bdf .|awk ' {print $5}'|grep -v used|head -2`
longitud_sistema=`expr length $espacio`
longitud_nueva_sistema=`expr $longitud_sistema - 1`
longitud_nueva_obtenido=` expr substr $espacio 1 $longitud_nueva_sistema`
echo "$longitud_nueva_obtenido"
if [ $longitud_nueva_obtenido -gt 85 ]
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "ESPACIO EN EL SERVIDOR SOBRE PASA EL 85%, FAVOR VERIFICAR EL ESPACIO EN EL FILESYSTEM .....Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
fi

cantidad_ciclos_obt=`cat  ejecucion_obtiene_verificar.txt| awk -F "|" '{print $1}'`
obtener_tipo_obtiene=`cat  ejecucion_obtiene_verificar.txt| awk -F "|" '{print $2}'`

## ---=======================OBTENER CANTIDAD DE BILLCYCLE PARA GENERAR EL OBTIENE --==========
cantidad_inicio=0
control_obtiene_re=0
while [ $control_obtiene_re -eq 0 ]
do
cat > $NOMBRE_FILE_SQL_BILLCYCLE <<END
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off 
spool obtener_billcycle_obtiene.txt
SELECT BILLCYCLE || '|' || F.CICLO_DEFAULT
FROM MONITOR_FIN_MES M, FA_CICLOS_AXIS_BSCS F
WHERE M.ESTADO_DAT = 'X' 
and M.estado_txt is null
AND F.ID_CICLO_ADMIN = M.BILLCYCLE
and rownum <=$cantidad_ciclos_obt;
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_BILLCYCLE > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_BILLCYCLE > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
#rm -f $nombre_archivo_control 
rm -f ejecucion_obtiene_verificar.txt
rm -f obtener_billcycle_obtiene.txt
rm -f obtener_billcycle_obtiene.txt
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de realizar la conexion a la base para obtener los billcycles para el procesos del obtiene .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener los billcycles para el procesos del obtiene  .......... Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi

cantidad_billlcycle_obtiene=`cat obtener_billcycle_obtiene.txt|wc -l`
if [ $cantidad_billlcycle_obtiene -eq 0 ];
then
echo "No hay ciclos para realizar el Obtiene" >> $NOMBRE_FILE_LOG
rm -f $nombre_archivo_control 
rm -f  obtener_billcycle_obtiene.txt
rm -f ejecucion_obtiene_verificar.txt
rm -f verificacion_obtiene_monitor.txt
rm -f $NOMBRE_FILE_SQL_OBTIENE
exit 0;
else

if [ $cantidad_inicio -eq 0 ]
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "INICIO DEL PROCESO DEMONIO OBTIENE DE TIMM .......... Fecha : $fecha  Hora: $hora" > $Control_alarma 
echo "$Control_alarma " >> $NOMBRE_FILE_LOG
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
fi
cantidad_inicio=`expr  $cantidad_inicio + 1`

#10179
for dato in `cat obtener_billcycle_obtiene.txt`
#10179

do
cd $ruta_local

#10179
line=`echo $dato | awk -F\| '{print $1}'`
tipo_prod=`echo $dato | awk -F\| '{print $2}'`
#10179

validacion_estados_dat "P" "$line" 

#10179
#if [ ! -d $ruta_local"/procesados/CICLO_"$line ]
#then

#mkdir $ruta_local"/procesados/CICLO_"$line 
#fi

if [ "$tipo_prod" = "D" ]
then
	if [ ! -d $ruta_procesados_dth"/CICLO_"$line ]
	then
		mkdir $ruta_procesados_dth"/CICLO_"$line 
	fi
else
	if [ ! -d $ruta_procesados_movil"/CICLO_"$line ]
	then
		mkdir $ruta_procesados_movil"/CICLO_"$line 
	fi
fi
#10179

control_obtiene=1
while [ $control_obtiene -le 8 ]
do

nohup obtiene_detalle_llamada.pl $control_obtiene $line $ruta_local $obtener_tipo_obtiene &
control_obtiene=`expr $control_obtiene + 1`
done
PidsEjecuta2=`ps -eaf | grep obtiene_detalle_llamada.pl |grep -v grep| wc -l`
while [ $PidsEjecuta2 -ne 0 ] 
do
PidsEjecuta2=`ps -eaf | grep obtiene_detalle_llamada.pl |grep -v grep| wc -l`
sleep 5
espacio=`bdf .|awk ' {print $5}'|grep -v used|head -2`
longitud_sistema=`expr length $espacio`
longitud_nueva_sistema=`expr $longitud_sistema - 1`
longitud_nueva_obtenido=` expr substr $espacio 1 $longitud_nueva_sistema`
echo "$longitud_nueva_obtenido"
if [ $longitud_nueva_obtenido -gt 85 ]
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "ESPACIO EN EL SERVIDOR SOBRE PASA EL 85%, FAVOR VERIFICAR EL ESPACIO EN EL FILESYSTEM .....Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
fi
done

cat>$NOMBRE_FILE_SQL_CANTIDAD<<END 
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off
spool obtener_cantidad_doc1_txt.txt
select count(*) from doc1.doc1_cuentas where billcycle in ('$line') and customer_id_high is null  and estado ='P';
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_CANTIDAD > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_CANTIDAD > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
#rm -f $nombre_archivo_control 
rm -f ejecucion_obtiene_verificar.txt
rm -f obtener_billcycle_obtiene.txt
rm -f obtener_billcycle_obtiene.txt
rm -f obtener_cantidad_doc1_txt.txt
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de cuentas del billcycle $line .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de cuentas del billcycle $line  .......... Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi
#10179
#cd $ruta_local"/dat/CICLO_"$line
cd $ruta_datos_movil"/CICLO_"$line
#10179
cantidad_dat_guardado=`ls -1 *".dat"".Z" |wc -l`
#10179
#DTH
cd $ruta_datos_dth"/CICLO_"$line
cantidad_dat_guardado_dth=`ls -1 *".dat"".Z" |wc -l`
cantidad_dat_guardado=`expr $cantidad_dat_guardado + $cantidad_dat_guardado_dth`
#10179
cantidad_dat_doc1_txt=`cat $ruta_local"/obtener_cantidad_doc1_txt.txt`
if [ $cantidad_dat_guardado -eq $cantidad_dat_doc1_txt ];
then
validacion_estados_dat "X" "$line"
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "$line TERMINO DE REALIZAR LA EJECUCION DEL OBTIENE CON EXITO  ... Fecha : $fecha  Hora: $hora " > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
else
validacion_estados_dat "F" "$line"
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "$line NO TERMINO DE REALIZAR LA EJECUCION DEL OBTIENE CON EXITO, POR CUENTAS QUE NO ESTAN COMPLETAS AL GENERAR LOS DAT ,POR FAVOR VERIFICAR EL ARCHIVO TXT  ... Fecha : $fecha  Hora: $hora DE ERROR " > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
fi
done
rm -f obtener_billcycle_obtiene.txt
fi

##---======VERIFICANDO SI EL USUARIO A DECIDIDO BAJAR EL PROCESO ====----------------
cd $ruta_local
cat>$NOMBRE_FILE_SQL_BAJAR_OBTIENE<<END 
set heading off; 
spool bajar_proceso_obtiene.txt
SELECT COUNT(*) FROM sysadm.PARAMETRO_DAT_AUTOM WHERE INICIO_EXTRACCION = 'S';
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_BAJAR_OBTIENE> $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_BAJAR_OBTIENE > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
#rm -f $nombre_archivo_control 
rm -f ejecucion_obtiene_verificar.txt
rm -f obtener_billcycle_obtiene.txt
rm -f obtener_billcycle_obtiene.txt
rm -f obtener_cantidad_doc1_txt.txt
rm -f bajar_proceso_obtiene.txt
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de realizar la conexion a la base para obtener el estado si el usuario a decidico bajar el proceso manualmente .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener el estado si el usuario a decidico bajar el proceso manualmente  .......... Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi
obtener_dato_bajar=`cat bajar_proceso_obtiene.txt`
if [ $obtener_dato_bajar -eq 0 ]
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo " SE A DECIDIDO TERMINAR MANUALMENTE EL PROCESO DEL OBTIENE Fecha : $fecha  Hora: $hora ------" > $Control_alarma 
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
rm -f $nombre_archivo_control
rm -f $NOMBRE_ERROR_LOG 
rm -f billcycle_pendiente.txt
rm -f billcycle_procesando.txt 
rm -f ciclos_extraidos_final_.txt
rm -f billcycles_extraer.txt
rm -f validar_fecha_corte.txt
rm -f obtener_fecha_corte.txt
rm -f verificar_fechas.txt
rm -f extraccion_billcycle.txt
rm -f  total_cantidad_billcycles_extraer.txt
rm -f obtener_fecha_cantidad.txt
rm -f obtener_cantidad_doc1.txt
rm -f verificacion_obtiene_monitor.txt
rm -f bajar_proceso_obtiene.txt
rm -f ejecucion_obtiene_verificar.txt
rm -f obtener_billcycle_obtiene.txt
rm -f $NOMBRE_FILE_SQL_DIRECTORIO_DAT
rm -f $NOMBRE_FILE_SQL2
rm -f $NOMBRE_FILE_SQL3
rm -f $NOMBRE_FILE_SQL4
rm -f $Control_alarma
rm -f $NOMBRE_FILE_SQL_BAJAR_DAT
exit 0;
fi
done

####-============== Eliminar todos los archivos creados ===============------
cd $ruta_local
rm -f $NOMBRE_FILE_SQL_OBTIENE
rm -f $NOMBRE_FILE_SQL_BILLCYCLE
rm -f $NOMBRE_FILE_SQL_CANTIDAD
rm -f $nombre_archivo_control
rm -f $Control_alarma
rm -f $NOMBRE_FILE_SQL_VALIDACION_DAT
rm -f $NOMBRE_FILE_SQL_BAJAR_DAT
rm -f obtener_billcycle_obtiene.txt
rm -f ejecucion_obtiene_verificar.txt
rm -f obtener_cantidad_doc1_txt.txt
rm -f billcycle_pendiente.txt
rm -f billcycle_procesando.txt 
rm -f ciclos_extraidos_final_.txt
rm -f billcycles_extraer.txt
rm -f validar_fecha_corte.txt
rm -f obtener_fecha_corte.txt
rm -f verificar_fechas.txt
rm -f extraccion_billcycle.txt
rm -f  total_cantidad_billcycles_extraer.txt
rm -f obtener_fecha_cantidad.txt
rm -f obtener_cantidad_doc1.txt
rm -f verificacion_obtiene_monitor.txt
rm -f bajar_proceso_obtiene.txt
rm -f $NOMBRE_ERROR_LOG
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "---------- FIN DEL PROCESO TOTAL DEL PROCESO OBTIENE FECHA Y HORA DE FINALIZACION Fecha : $fecha  Hora: $hora ------" > $Control_alarma 
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
rm -f $Control_alarma
