#!/opt/perl_64/bin/perl
#!/usr/bin/perl
#!/usr/local/bin/perl
#===============================================================================================#
#-- Version: 	1.0.0
#===============================================================================================#
#-- Proyecto		     :	[9576] - Analisis_De_Credito_Y_Activacion_Multipunto_DTH 
#--                       Nuevo Esquema Facturación Multipunto-Personal DTH
#-- Desarrollado por :   SUD Luis Zambrano Vera.
#-- Fecha de creacion:	 25/08/2014
#-- Lider Claro		   :   Ing. Oscar Apolinario
#-- Lider PDS		     :	 Ing. Marcela Caicedo
#-- Descripcion		   :   Creacion de archivos .txt para DTH Multipunto y personal nuevo esquema
#===============================================================================================#
#-- Version: 	1.2.0
#===============================================================================================#
#-- Proyecto		     :	[9576] - Analisis_De_Credito_Y_Activacion_Multipunto_DTH 
#-- Modificado por		 :   SUD Luis Zambrano Vera.
#-- Fecha de modificacion:	 10/02/2015
#-- Lider Claro		     :   Ing. Oscar Apolinario
#-- Lider PDS		     :	 SUD. Cristhian Acosta
#-- Descripcion		     :   Nuevo proceso de arma anexo para detallar valores post-pagados y pre-pagados
#--				 			 Con el esquema multipunto y normal.
#--							 En escenarios de cambios de plan con fechas antes y despues del corte
#===============================================================================================#
# Variables #

%MAPEOGeneral;
%MAPEOSuma;
%MAPEOTotales;
%MAPEOImprime;
%MAPEOImprimeUno;
%banderaUno;
%Graba_co_id;
%VerificaNumber;
%Mdetalle;
%Mdetallefinal;
%Mgrupodth;
%Mgrupodth_lineas; #mapeos lineas 20000 -20200

my %MAPEOmpuzotab;
my %MAPEOlegend;
my %MAPEOmpusntab;
my %MAPEOmpuzntab;
my %MAPEOcall_grouping;
my %MAPEOpaymenttype_all;
my %MAPEOservice_label;
my %MAPEOservice_label2;
my %MAPEOmpulktmb;
my %MAPEOmpulktmbANT;
my %MAPEOrateoplan;
my %MAPEOServicesExt;
my %MAPEObgh_tariff_plan;
my %MAPEOImprimeDetLlamadas;
my %MAPEOImprimeConsumoCelular;
my %MAPEObgh_tariff_plan;
my %MAPEOtax_category;
my %MAPEOinfo_contr_text;
my %MapeoSecuencias;
my %Mapeogrupodth;


my $procesador=$ARGV[0];
my $ciclo=$ARGV[1];
my $ruta=$ARGV[2]."/";
my $tipo_ejecucion=$ARGV[3]; # Control [G]ruop - [C]ommit
my $cuenta_out_llam=$ARGV[4];
if( $#ARGV < 3 ) {
	  print "NO hay argumentos \n";
	  die "error $!";
}
$filempuzotab=$ruta."resources/mpuzotab.dat";
$filelegend=$ruta."resources/legend_doc1.dat";
$filempusntab=$ruta."resources/mpusntab.dat";
$filempuzntab=$ruta."resources/mpuzntab.dat";
$filecall_grouping=$ruta."resources/call_grouping_doc1.dat";
$filepaymenttype_all=$ruta."resources/paymenttype_all.dat";
$fileservice_label=$ruta."resources/service_label_doc1.dat";
$filempulktmb=$ruta."resources/mpulktmb.dat";
$filempulktmbANT=$ruta."resources/mpulktmbANT.dat";
$filerateplan=$ruta."resources/rateplan.dat";
$filebgh_tariff_plan=$ruta."resources/bgh_tariff_plan_doc1.dat";
$filetax_category=$ruta."resources/tax_category_doc1.dat";
$fileinfo_contr_text=$ruta."resources/info_contr_text.dat";
$fileServices_ext=$ruta."resources/services_ext.dat";
$ruta_adicional=$ruta."adicionales";
$archivo_sec=$ruta."PCE/";
$fecha=`date '+%Y-%m-%d-%H-%M-%S'`;
$fecha=&enceraEspacios($fecha);
$archivo_logs=$ruta."log/DOC1_DTH_".$procesador."_".$ciclo."_".$tipo_ejecucion."-".$fecha.".log";
$filexcento_iva=$ruta."resources/excento_iva.dat";
$fileno_etiquetas=$ruta."resources/etiqueta_no_impresa.dat";
$file_nuevorubro=$ruta."resources/nuevos_rubros.dat";
$file_grupodth=$ruta."resources/grupo_dth.dat";
$indexHashUnico = 0;

#Llamadas
$cont_calculo_mayor=0;
&grabo_informacion;
&llama_tim;

sub grabo_informacion{
	$line="";
    open(FILE_READ_TMP, $filempuzotab);    
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$MAPEOmpuzotab{$arreglo_lin[2]} =[$arreglo_lin[0],$arreglo_lin[1] ,$arreglo_lin[3]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempuzotab." \n";
		die;
	}
#           0                  1          2                          3              4            5            6
#select label_id||'|'|| servshdes||'|'||nvl(tzcode,1)||'|'||nvl(ttcode,1)||'|'||leg_id||'|'|| calltype||'|'||tipo from call_grouping_doc1
	$line="";
    open(FILE_READ_TMP, $filecall_grouping);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[6]);
#		print $arreglo_lin[1]." ".$arreglo_lin[5]." ".$arreglo_lin[2]." ".$arreglo_lin[3]." ".$val." ".$arreglo_lin[0]." ".$arreglo_lin[4]."\n";
		$MAPEOcall_grouping {$arreglo_lin[1],$arreglo_lin[5],$arreglo_lin[2], $arreglo_lin[3], $val} =[$arreglo_lin[0],$arreglo_lin[4]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filecall_grouping." \n";
		die;
	}

	$line="";
#select leg_id||'|'||sh_name||'|'||name from legend_doc1;
    open(FILE_READ_TMP, $filelegend);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOlegend{$arreglo_lin[0]} =[$arreglo_lin[1],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filelegend." \n";
		die;
	}

	$line="";
#select sncode ||'|'|| des ||'|'|| shdes ||'|'|| snind ||'|'|| rec_version ||'|'||billed_surcharge_ind ||'|'|| pde_implicit_ind from sysadm.mpusntab;
    open(FILE_READ_TMP, $filempusntab);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$MAPEOmpusntab{$arreglo_lin[2]} =[$arreglo_lin[0],$arreglo_lin[1] ];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempusntab." \n";
		die;
	}

	$line="";
#select zncode||'|'||des ||'|'|| shdes from mpuzntab;
    open(FILE_READ_TMP, $filempuzntab);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[2]);
#		print $val." ".$arreglo_lin[0]." ".$arreglo_lin[1]."\n";
		$MAPEOmpuzntab{$val}=[$arreglo_lin[0],$arreglo_lin[1]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempuzntab." \n";
		die;
	}

	$line="";
#select pay.payment_id ||'|'||pay.paymentcode||'|'||pay.paymentname datos from paymenttype_all pay
    open(FILE_READ_TMP, $filepaymenttype_all);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
#		print $arreglo_lin[0]." ".$arreglo_lin[1]." ".$val."\n";
		$MAPEOpaymenttype_all{$arreglo_lin[1]}=[$arreglo_lin[0],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filepaymenttype_all." \n";
		die;
	}

	$line="";
    #select label_id||'|'||des||'|'||taxinf||'|'||shdes||'|'||ctaps||'|'||codigo_sri from service_label_doc1
    open(FILE_READ_TMP, $fileservice_label);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[4]); #ECO_CIMA
		#print "Service Label ".$arreglo_lin[0]." ".$arreglo_lin[1]." ".$val." ".$arreglo_lin[2]."\n";
		$MAPEOservice_label{$arreglo_lin[0]}=[$arreglo_lin[1],$arreglo_lin[3],$arreglo_lin[2],$arreglo_lin[5]]; #ECO_CIMA #8504 - Se agrega $arreglo_lin[5] Código del Servicio Rubro Llamadas para F.E.
		$MAPEOservice_label2{$arreglo_lin[1]}=[$val]; #ECO_CIMA
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$fileservice_label." \n";
		die;
	}

	$line="";
    #select tmcode||'|'||  to_char(vsdate,'yyyymmdd')||'|'|| spcode||'|'||  sncode||'|'|| accserv_catcode from MPULKTMb a where vsdate in(select max(b.vsdate) from MPULKTMb b where b.tmcode=a.tmcode and b.sncode = a.sncode)
    open(FILE_READ_TMP, $filempulktmb);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[4]);
#		print $arreglo_lin[0]." ".$arreglo_lin[3]." ".$arreglo_lin[1]." ".$arreglo_lin[2]." ".$val."\n";
		$MAPEOmpulktmb{$arreglo_lin[0]." ".$arreglo_lin[3]}=[$arreglo_lin[1],$arreglo_lin[2],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempulktmb." \n";
		die;
	}

	$line="";
    #select tmcode||'|'||  to_char(vsdate,'yyyymmdd')||'|'|| spcode||'|'||  sncode||'|'|| accserv_catcode from MPULKTMb a where vsdate in(select max(b.vsdate) from MPULKTMb b where b.tmcode=a.tmcode and b.sncode = a.sncode)
    open(FILE_READ_TMP, $filempulktmbANT);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[4]);
		$MAPEOmpulktmbANT{$arreglo_lin[0]." ".$arreglo_lin[3]}=[$arreglo_lin[1],$arreglo_lin[2],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempulktmbANT." \n";
		die;
	}

	$line="";
	#select tmcode||'|'|| des||'|'|| shdes from rateplan
    open(FILE_READ_TMP, $filerateplan);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOrateplan{$val}=[$arreglo_lin[0],$arreglo_lin[1]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filerateplan." \n";
		die;
	}

	$line="";
	#select tmcode||'|'||shdes||'|'||firstname||'|'||secondname||'|'||porder ||'|'||plan_type||'|'||tax_code_iva ||'|'||tax_code_ice from bgh_tariff_plan_doc1 order by tmcode,shdes, plan_type, porder
    open(FILE_READ_TMP, $filebgh_tariff_plan);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[7]);
#		print $arreglo_lin[1]." ".$arreglo_lin[4]." ".$arreglo_lin[0]." ".$val." ".$arreglo_lin[2]." ".$arreglo_lin[3]."\n";
		$MAPEObgh_tariff_plan{$arreglo_lin[1],$arreglo_lin[4]}=[$arreglo_lin[0],$arreglo_lin[5],$arreglo_lin[2],$arreglo_lin[3],$arreglo_lin[6],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filebgh_tariff_plan." \n";
		die;
	}

	$line="";
	#select i.taxcat_id||'|'||i.taxcat_name||'|'|| i.taxrate from tax_category i
	open(FILE_READ_TMP, $filetax_category);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOtax_category{$arreglo_lin[0]}=[$arreglo_lin[1],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filetax_category." \n";
		die;
	}

	$line="";
	#select co_id||'|'||text01||'|'||text02||'|'||text03 from info_contr_text;
    # co_id= # de Contrato co_id, Text01= # de la Cédula o RUC, Text02= Nombre, Text03= #Ciclo
	open(FILE_READ_TMP, $fileinfo_contr_text);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[3]);
       	$MAPEOinfo_contr_text{$arreglo_lin[0]}=[$arreglo_lin[1],$arreglo_lin[2]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$fileinfo_contr_text." \n";
		die;
	}
	
	$line="";
	#Clientes excentos de IVA 
	#select y.custcode from customer_all y, customer_tax_exemption x where y.customer_id = x.customer_id and x.exempt_status = 'A';
	open(FILE_READ_TMP, $filexcento_iva);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$Cuentas_Excentas{$arreglo_lin[0]}="S";
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$filexcento_iva." \n";
		die;
	}

	$line="";
	#Etiquetas no presentadas
	open(FILE_READ_TMP, $fileno_etiquetas);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$No_etiquetas{$arreglo_lin[0]}=[$arreglo_lin[0]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Ciclo ".$ciclo." el Archivo ".$fileno_etiquetas." \n";
		die;
	}

    $line="";
	#Nuevos Rubros
	open(FILE_READ_TMP, $file_nuevorubro);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$Nuevo_rubro{$arreglo_lin[0]}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$arreglo_lin[3]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Ciclo ".$ciclo." el Archivo ".$file_nuevorubro." \n";
		die;
	}

    $line="";
	open(FILE_READ_TMP, $fileServices_ext);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
       	$MAPEOServicesExt{$val}=[$arreglo_lin[0],$arreglo_lin[1],$val];
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$fileServices_ext." \n";
		die;
	}
	
	$line="";
	open(FILE_READ_TMP, $file_grupodth);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
       	$Mapeogrupodth{$val}=[$arreglo_lin[0],$arreglo_lin[1],$val,$arreglo_lin[3]];
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_grupodth." \n";
		die;
	}
}



sub graba_datos($){
my $fileHandle=$_[0];
#print $MAPEOImprimeUno{"20100"}[0];
#foreach (sort(keys %MAPEOImprimeUno)) {	
#	print $fileHandle $MAPEOImprimeUno{$_}[0]."\n";
#}
foreach (sort(keys %MAPEOImprimeUno)) {	
	$linea=$MAPEOImprimeUno{$_}[0];	
my @co_id_key=split(' ',$_); # lzv 
	@ar_orden=split(/\|/,$linea);
	if ($ar_orden[0] eq "41000"){
		#$MdetalleOrden{$ar_orden[0].$ar_orden[7]."|".$ar_orden[5]}=[$linea];
$MdetalleOrden{$ar_orden[0].$ar_orden[7]."|".$ar_orden[5].$co_id_key[0]}=[$linea]; #lzv
	}
	else{
		if ($ar_orden[0] ge "42000"){
		#$MdetalleOrden{$ar_orden[0].$ar_orden[7]."|".$ar_orden[5]}=[$linea];
$MdetalleOrden{$ar_orden[0].$ar_orden[7]."|".$ar_orden[5].$co_id_key[0]}=[$linea]; #lzv
		}
		else{
			print $fileHandle $MAPEOImprimeUno{$_}[0]."\n";
		}
	}
}
undef(%MAPEOImprimeUno);
foreach (sort(keys %MdetalleOrden)) {	
	print $fileHandle $MdetalleOrden{$_}[0]."\n";
}
undef(%MdetalleOrden);
#if ($banderaUno eq "1") {
#foreach (sort(keys %Mdetallefinal)) {	
#	print $fileHandle $Mdetallefinal{$_}[0]."\n";
#}
#undef(%Mdetallefinal);
#foreach (sort(keys %Mgrupodth)) {	
#	print $fileHandle $Mgrupodth{$_}[0]."|".$Mgrupodth{$_}[1]."|".$Mgrupodth{$_}[2]."|"."\n";
#}
#undef(%Mgrupodth);
#}
}

sub graba_co_id {
my $co_id=$_[0];
my $number=$_[1];
my $tipo=$_[2];
my $plan_co_id=$_[3];
my $des_co_id=$_[4];
$tip=$Graba_co_id{$co_id}[1];
$plan_g=$Graba_co_id{$co_id}[2];
$des_plan_g=$Graba_co_id{$co_id}[3];
if ($plan_co_id eq "") {
	$plan_co_id=$plan_g;
}
if ($des_co_id eq "") {
	$des_co_id=$des_plan_g;
}
$tip=$Graba_co_id{$co_id}[1];
if ($tipo eq "0" && $tip eq "6") {
	$tipo=$tip;
}
  if ($number ne "") {
#	  $Graba_co_id{$co_id}=[$number,$tipo];
      $Graba_co_id{$co_id}=[$number,$tipo,$plan_co_id,$des_co_id];
  }
}

sub cambia_etiqueta {
$descripcion_ser=$_[0];
if ($descripcion_ser =~ /Cargo a Paquete Fac/) {
	$descripcion_ser=~ s/Cargo a Paquete Fac/Cargo a Paquete/;
}
if ($descripcion_ser =~ /Cargo a Paquete Cto/) {
	$descripcion_ser=~ s/Cargo a Paquete Cto/Cargo a Paquete/;
}
if ($descripcion_ser =~ /Detalle de llamadas \- \no cobro/) {
	$descripcion_ser=~ s/Detalle de llamadas \- \no cobro/Factura Detallada Plus/;
}
return($descripcion_ser);
}

# Funcion utilizado para tener la CI y el Nombre del dueño de la cuenta
# en caso de no existir información en el archivo fileinfo_contr_text.dat
# imprimo el de la Cuenta


$lb_control_plan="0";
$lb_control_sum=0;


sub obtiene_valor_resumen {
my $co_id=$_[0];
my $key_resumen_dth=$_[1];
my $cont_calculo=$_[2];
my $val_resumen;
my $val_dolares=0;

$val_resumen=$MAPEOImprime{$co_id." ".$key_resumen_dth." ".$cont_calculo}[0];
@arreglo_num=split(/\|/,$val_resumen);
$val_dolares=$arreglo_num[3];
$tp_inicio=&enceraEspacios($arreglo_num[5]);
$tp_fin=&enceraEspacios($arreglo_num[6]);
return($val_dolares,$tp_inicio,$tp_fin);
}

sub formatea_seg_minutos {
$vseg=$_[0];
$vseg1=$vseg;
$vseg=$vseg/60;
$var=sprintf('%.2f',$vseg);
($a,$b)=split(/\./,$var);
$b=$vseg1-$a*60;
if ($b == 00) {
	$b="00";
} else { 
if ($b <= 9 ) {
	$b="0".$b;
}
}
if ($a == 0) {
   $des_vseg="(".$b." Seg)";
} else {
   $des_vseg="(".$a." Min ".$b." Seg)";
}

return $des_vseg;
}

sub formatea_seg_minutos_primera_hoja {
$vseg=$_[0];
$vseg_gratis=$_[1];
$vseg1=$vseg - $vseg_gratis;
$vseg2=$vseg1/60;
$var=sprintf('%.2f',$vseg2);
($a,$b)=split(/\./,$var);
$b=$vseg1-$a*60;

if ($b == 00) {
	$b="00";
} else { 
if ($b <= 9 ) {
	$b="0".$b;
}
}

if ($a == 0) {
   $des_vseg="(".$b." Seg)";
} else {
   $des_vseg="(".$a." Min ".$b." Seg)";
}

return $des_vseg;
}


sub obtiene_contador {
my $des=$_[0];
my $desglosa_periodos = $_[1];
my $val="";

if ($des eq $No_etiquetas{$des}[0]){
  print "Debud no etiqueta2".$des."---".$No_etiquetas{$des}[0]."\n";
  next;
}else
{
   if ($desglosa_periodos eq "0") {
	  $val=$Resumen_Celular_Dos{$des}[0];
    }

}

if ($val ne "") {
  $valor_obtiene_contador=$val;
} else {
  if ($des eq "Tarifa Basica ") {
	$valor_obtiene_contador="00000001";
} else	{
	   $cont_calculo=$cont_calculo_mayor;
	   $cont_calculo=$cont_calculo+1;
		if ($cont_calculo > 9) { $valor_obtiene_contador="000000".$cont_calculo;
		} else { $valor_obtiene_contador="0000000".$cont_calculo;}
 	   $cont_calculo_mayor=$cont_calculo_mayor+1;
   }
}

$Resumen_Celular_Dos{$des}=[$valor_obtiene_contador];
return($valor_obtiene_contador);
}

sub formatea_numeros {
    my $var=$_[0];
    my $var11=$_[0];
	my $valor_nuevo;
	($a,$b)=split(/\./,$var);
	if (length($b) == 3 && substr($b,2,1) == 5) {
	  if (substr($b,1,1) == 9) {
  	    if (substr($b,0,1) == 9) {
  	     $var=($a+1).".00";
		} else {
  	     $var=$a.".".(substr($b,0,1)+1).".0";
		}
	  } else {
  	    $var=$a.".".substr($b,0,1).(substr($b,1,1)+1);
	  }
	} 
    $valor_nuevo=sprintf('%.2f',$var);
	return $valor_nuevo;
}


sub impuesto_calcular {
$valor_impuesto_calcula=$_[0];
$servicio_c=$_[1];
$plan_c=$_[2];
$tmp_final=$_[3];
$tm_code=$MAPEOrateplan{$plan_c}[0];
$sn_code=$MAPEOmpusntab{$servicio_c}[0];
$impuesto=$MAPEOmpulktmb{$tm_code." ".$sn_code}[2];
if ($tmp_final le "20071231") {
	$impuesto=$MAPEOmpulktmbANT{$tm_code." ".$sn_code}[2];
}
if ($impuesto eq "IVA-ICE") {
	 $total_iva=$MAPEOTotales{$co_id}[1]+$valor_impuesto_calcula;
	 $total_ice=$MAPEOTotales{$co_id}[2]+$valor_impuesto_calcula;
} 
if ($impuesto eq "IVA") {
	 $total_iva=$MAPEOTotales{$co_id}[1]+$valor_impuesto_calcula;
} 
if ($impuesto eq "ICE") {
	 $total_ice=$MAPEOTotales{$co_id}[2]+$valor_impuesto_calcula;
} 
}

sub obtiene_impuesto {
$des=$_[0];
$seg=$_[1];
$tipo_impuesto=uc($_[2]);
$tot_iva=$_[3];
$tot_ice=$_[4];
$valor_dol=$_[5];
$seg_gratis=$_[6];
$descripcionV=&formatea_seg_minutos($seg);
$total_ivaV=$tot_iva;
$total_iceV=$tot_ice;
if ($tipo_impuesto eq "ICE" || $tipo_impuesto eq "IVA-ICE" ) {
	$total_iceV=$total_iceV+$valor_dol;
} 
if ($tipo_impuesto eq "IVA" || $tipo_impuesto eq "IVA-ICE") {
	$total_ivaV=$total_ivaV+$valor_dol;
} 
$etiquetaV=$des;
$val=$Minutos{$etiquetaV}[0];
$gratis=$Minutos{$etiquetaV}[1];
$val=$val+$seg;
$gratis=$gratis+$seg_gratis;
$Minutos{$etiquetaV}=[$val,$gratis];
return($descripcionV,$etiquetaV,$total_ivaV,$total_iceV);
}

sub busca_descripcion_etiqueta{
$servshdes=$_[0];
$tzcode=$_[1];
$calltype=$_[2];
$ttcode=$_[3];
$tmshdes=$_[4];
$clase_llam=$_[5];
my $des;
my $sh_name;
my $sh_des;
my $taxinf;
my $label_id="";
my $legend_id="";
my $tipo="";

$codigo_servicio_cl="";

$tipo=$MAPEObgh_tariff_plan{$tmshdes,1}[1];
if ($tipo eq "") {
	$tipo ="D";
}
($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,$tzcode,$ttcode,$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,$tzcode,$ttcode,$tipo}[1]);
 if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,$tzcode,"1",$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,$tzcode,"1",$tipo}[1]);
 }
 if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,"1",$ttcode,$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,"1",$ttcode,$tipo}[1]);
 }
 if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,"1","1",$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,"1","1",$tipo}[1]);
 }
 if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{"1",$calltype,$tzcode,"1",$tipo}[0],$MAPEOcall_grouping{"1",$calltype,$tzcode,"1",$tipo}[1]);
 }
 $sh_name=$MAPEOlegend{$legend_id}[0];
 $sh_name=&enceraEspacios($sh_name);
 $des=$MAPEOservice_label{$label_id}[0];
 $sh_des=$MAPEOservice_label{$label_id}[1];
 $taxinf=$MAPEOservice_label{$label_id}[2];
 $sh_des=&enceraEspacios($sh_des);
 $des=&enceraEspacios($des);
 $codigo_servicio_cl=$MAPEOservice_label{$label_id}[3];
 $codigo_servicio_cl=&enceraEspacios($codigo_servicio_cl);
return($des,$sh_name,$sh_des,$taxinf,$codigo_servicio_cl);
}

sub busca_descripcion_servicio{
$zcode=$_[0];
$descripcion_mpusntab=$MAPEOmpusntab{$zcode}[1];
return($descripcion_mpusntab);
}


sub transforma_fecha{
#20060108022847
#28102006|12:00|
$fecha=$_[0];
$anio=substr($fecha,0,4);
$mes=substr($fecha,4,2);
$dia=substr($fecha,6,2);
$hora=substr($fecha,8,2);
$min=substr($fecha,10,2);
$seg=substr($fecha,12,2);
%arreglo= ('01','Ene','02','Feb','03','Mar','04','Abr','05','May','06','Jun','07','Jul','08','Ago','09','Sep','10','Oct','11','Nov','12','Dic');
$mes_letra=$arreglo{$mes};
$fecha_completa=$dia.$mes.$anio."|".$hora.":".$min.":".$seg;
$fecha_completa1=$dia." ".$mes_letra." ".$hora.":".$min;
return($fecha_completa1,$fecha_completa);
}

sub rango_fecha{
#DTM+901:20060308:102'
#DTM+902:20060407:102'
my $fecha=$_[0];
my $fecha1=$_[1];
$mes=substr($fecha,4,2);
$dia=substr($fecha,6,2);
$mes1=substr($fecha1,4,2);
$dia1=substr($fecha1,6,2);

%arreglo= ('01','Ene','02','Feb','03','Mar','04','Abr','05','May','06','Jun','07','Jul','08','Ago','09','Sep','10','Oct','11','Nov','12','Dic');
$mes_letra=$arreglo{$mes};
$mes_letra1=$arreglo{$mes1};

$fecha_completa=$mes_letra.". ".$dia." al ".$mes_letra1.". ".$dia1;
return($fecha_completa);
}

sub enceraEspacios{
	$valor=$_[0];
	$valor=~s/^\s+//;	
	$valor=~s/\s+$//;
	$valor=~s/\n+//;
	return($valor); 
}

sub verifico_tipo_agrupacion{
$iva=$_[0];
$ice=$_[1];
# Servicio de television #
if ($iva > 0 && $ice > 0) {
#	$tipo="20000";
	$tipo="20200";
	$apl_ice="S";
}
# Otros Servicios #
if ($iva > 0 && $ice == 0) {
	$tipo="20200";
	$apl_ice="N";
}
# Sin Impuestos #
if ($iva == 0 && $ice == 0) {
	$tipo="21100";
	$apl_ice="N";
} 
return($tipo,$apl_ice);
}

sub verifica_servExterno{
 $codigo_servicio=$_[0];
 $tipo=$_[1];
 $existe=exists $MAPEOServicesExt{$codigo_servicio};
 if ( $existe == 1 ) {
	 $tipo="21650";
 }
 return($tipo);
}


sub obtieneTotalMenosServExt{
$Valor=$_[0];
$Valor_Total=$Valor-$total_servicios_Ext;
return $Valor_Total
}


sub verifica_proc{
$cod_in=$_[0];
foreach (keys %Mdetallefinal) {
	$key_p=$_;
	$trama_p=$Mdetallefinal{$key_p}[0];
	@arreglo_p=split(/\|/,$trama_p);
	$cod_serv_p=$arreglo_p[7];
	if ($cod_in eq $cod_serv_p) {
		$band_proc="1";
		return ($band_proc);
	}
	else
	{
		$band_proc="0";
	}
}	# fin for 2
return ($band_proc);
}

sub timDosBuscaValores{
# INICIO TIM DOS BUSCA VALORES
    $val_resumen=0;
$fecha_i="";#9576 SUD.LZV 250814 Se inicializa la variable
#    if ( /(IMD\+\+8\+CT\:\:\:\:U\')\n/ )  {
    if ( /(IMD\+\+8\+CT\:\:\:\:U)/ )  {
        $ban_dos="1";
        $ban_acceso="0";
        return;
    }
    if ( /(IMD\+\+8\+CT\:\:\:\:A\')\n/ )  {
        $ban_acceso="1";
    }
#    if (/0\+\+\:SA\:\:\+\+02\'\n/) {		19/03/2013
    if (/0\+\+\:SA\:\:\+\+02\'/) {
        $co_id="";
    }
#    if (  /IMD\+\+8\+CO\:\:\:\:([^\:\']*)\'\n/ ) { #if #1			19/03/2013
    if (  /IMD\+\+8\+CO\:\:\:\:([^\:\']*)\'/ ) { #if #1
        $co_id=$1;
    }
#    if ( /(0\+\+\:SA\:\:\+\+02\'\n)/) {			19/03/2013
    if ( /0\+\+\:SA\:\:\+\+02\'/) {
        if (/IMD\+\+8\+MRKT\:\:\:([^\:]*)\:/) {
            $marca_red=$1;
            if ($marca_red eq "ISD") {
                $key_resumen_dth="99992";
                $key_tot_cargos_cta="99993";
                &graba_co_id($co_id,$fono,"0");
                } else {
                $key_resumen_dth="99991";
            }
        }
        #Obtengo el número de Teléfono#
#        if (/IMD\+\+8\+DNNUM\:M\:\:([^\:]*)\:([^\:\']*)\'\n/) {		19/03/2013
        if (/IMD\+\+8\+DNNUM\:M\:\:.*\:([^\:\']*)/) {
#            $fono=$2;						19/03/2013
            $fono=$1;
        }
        } else {
#        if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n[^\n]/ ) {			19/03/2013
        if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:/ ) {
            $codigo_servicio=$1;
            $co_id_det=$Graba_co_id{$co_id}[1];
            if (/IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'/) {
                    $plan=$1;
                    $des_plan=$2;
                    $plan1=$plan;
                    $des_plan1=$des_plan;
					$cont_detalle=$cont_detalle + 1;
                    #print "Entrada 2 FONO ".$fono." Codigo Servicio ".$codigo_servicio." Co_id ".$co_id_det." Descripcion plan1: ".$des_plan_tele."T |\n" ;
                    &graba_co_id($co_id,$fono,"0",$plan1,$des_plan1);
                    $co_id_alter1=$co_id;
                    $co_id_alter2=$co_id;
            }
            # Otengo el Valor el 2do MOA #
            if (/MOA.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/) {
                $dolar=$1;
            }
#            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/; #  IMD++8+SP:  ::  SP51   :   GSM ? : Servicios Basicos '		19/03/2013
            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'/; #  IMD++8+SP:  ::  SP51   :   GSM ? : Servicios Basicos '
#            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'\n/; #IMD++8+SP   : : :   SP06  :Servicios OCC'		19/03/2013
            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'/; #IMD++8+SP   : : :   SP06  :Servicios OCC'
            $codigo_servicio_basico=$1;   # Es para obtener si es Servicio de Voz Básico SP51 SP02
            $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
            $descripcion_ser=cambia_etiqueta($descripcion_ser);
            $desglosa_periodos = "0";
            if (lc($descripcion_ser) ne "servicio de voz"   &&  lc($descripcion_ser) ne "Emisión y entrega de factura"  ) {
                if ((lc($descripcion_ser) eq "tarifa basica " || lc($descripcion_ser) eq "tarifa básica " || lc($descripcion_ser) eq "cargo a paquete" ) && $dolar == 0) {
                    $no_imprime="1";
                    $clave_anterior="";
                    $dolar_anterior="";
                    $codigo_servicio_a="";
                    $plan_tele_a="";
                    $co_id_a="";
                    } else {
                    $no_imprime="0";
                }
                if ($no_imprime eq "0") {
                    $total_iva=$MAPEOTotales{$co_id}[1];
                    $total_ice=$MAPEOTotales{$co_id}[2];
                    $dolar_anterior=$dolar;						#guarda el valor del servicio pa compararlo en la otra vuelta
                    $codigo_servicio_a=$codigo_servicio;		# guarda el servicio para compararlo en la otr vuelta
                    $plan_anterior=$plan;				# guarda el plan pa comparar en la otra vuelta
                    $co_id_anterior=$co_id;
                    if ($desglosa_periodos eq "0") {
                        $tiempo_fin_no_imprime="20080101";
                        if (/IMD\+\+8\+FE/) {
#                            if (/IMD\+\+8\+FE[^\n]+\-(\d+MAYCART)\'\n/) {			19/03/2013
                            if (/IMD\+\+8\+FE[^\n]+\-(\d+MAYCART)\'/) {
                                $tiempo_fin_no_imprime=substr($1,0,8);
                            }
                            &impuesto_calcular($dolar,$codigo_servicio,$plan_tele,$tiempo_fin_no_imprime);
                            $suma_total=$MAPEOTotales{$co_id}[0]+$dolar;
                            $MAPEOTotales{$co_id}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
                        }
                    }
                    $cont_calculo=&obtiene_contador($descripcion_ser,$desglosa_periodos);
                    ($val,$tmp_inicio,$tmp_fin)=&obtiene_valor_resumen($co_id,$key_resumen_dth,$cont_calculo);
                    $dolar=&formatea_numeros($dolar);
					$dol=$dolar;
                    $dolar=~ tr/\.//d;
         			$val_resumen=$dolar + $val;
#					$MAPEOImprime{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_dth."|".$descripcion_ser."||".$val_resumen."|".$val_resumen."|".$tmp_inicio."|".$tmp_fin."|".$codigo_servicio."|"]; 
					$MAPEOImprime2{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_dth."|".$descripcion_ser."||".$val_resumen."|".$val_resumen."|".$tmp_inicio."|".$tmp_fin."|".$codigo_servicio."|"]; 
					if ($ban_dos ne "1" && lc($descripcion_ser) ne "Entrega de factura") {
#17/04/2013						$Mdetalle{$co_id." ".$key_resumen_dth." ".$cont_detalle}=[$key_resumen_dth."|".$descripcion_ser."||".$dolar."|".$dolar."|||".$codigo_servicio."|"]; 
						$Mdetalle{$co_id." ".$key_resumen_dth." ".$cont_detalle}=[$key_resumen_dth."|".$descripcion_ser."||".$dol."|".$dol."|||".$codigo_servicio."|"]; 
                        $clave_anterior=$co_id." ".$key_resumen_dth." ".$cont_calculo;
					    $clave_det_ant=$co_id." ".$key_resumen_dth." ".$cont_detalle;
					}
					if ($tmp_inicio eq "" && $tmp_fin eq "") { #tengo el servicio pero no las fechas
						$band_sin_fecha="1";
					}
                }
                }
			  $ban_acceso = "1";
            } else {	
            # Para sacar las fechas del servicio.
            if ($clave_anterior ne "" && $ban_acceso eq "1" && $no_imprime eq "0") { #para el caso en que haya alguna suspension en medio periodo
                $tiempo_inicio="";
                $tiempo_fin="";
                if (/DTM\+901\:([^\:]*)\:102\'/) {
                    $tiempo_inicio=&enceraEspacios($1);
                }
                if (/DTM\+902\:([^\:]*)\:102\'/) {
                    $tiempo_fin=&enceraEspacios($1);
                }
				if (/MOA\+60\:([^\:]*)\:USD\:959\:9\'/) {
                    $dolar_new=&enceraEspacios($1);
                } else { $dolar_new = $0; }
				if ($plan_mostrar eq "") {
				  if ($tiempo_inicio eq $fecha_corte) {
					$plan_mostrar=$plan;
				  }
				}
				if ( ($tiempo_inicio ne "") ) { 
#                 @arreglo_num=split(/\|/,$MAPEOImprime{$clave_anterior}[0]);
                 @arreglo_num=split(/\|/,$MAPEOImprime2{$clave_anterior}[0]);
                 $clave=$arreglo_num[0];
                 $descripcion=$arreglo_num[1];
                 $dolar=$arreglo_num[3];
                 $t_inicio=&enceraEspacios($arreglo_num[5]);
                 $t_fin=&enceraEspacios($arreglo_num[6]);
                 &impuesto_calcular($dolar_anterior,$codigo_servicio_a,$plan_tele_a,$tiempo_fin);
                 $suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
                 $MAPEOTotales{$co_id_a}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
				if ($t_inicio eq "") {
                     $t_inicio=$tiempo_inicio;
                     $t_fin=$tiempo_fin;
                   }
                if ($tiempo_inicio gt $t_inicio) {
                    $tiempo_inicio=$t_inicio;
                  }
               if ($tiempo_fin lt $t_fin) {
                   $tiempo_fin=$t_fin;
                }
                $des_rango_fecha=&rango_fecha($tiempo_inicio,$tiempo_fin);
#                $MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar."|".$dolar."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio]; #ECO_CIMA2
                $MAPEOImprime2{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar."|".$dolar."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio]; #ECO_CIMA2
                 } else
				{
#					@arreglo_num=split(/\|/,$MAPEOImprime{$clave_anterior}[0]);
					@arreglo_num=split(/\|/,$MAPEOImprime2{$clave_anterior}[0]);
                    $clave=$arreglo_num[0];
                    $descripcion=$arreglo_num[1];
                    $dolar_anterior=$dolar_new;
                    &impuesto_calcular($dolar_anterior,$codigo_servicio_a,$plan_tele_a,$tiempo_fin);
                    $suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
                    $MAPEOTotales{$co_id_a}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
                    $des_rango_fecha=&rango_fecha($tiempo_inicio,$tiempo_fin);
                    $dolar_anterior=&formatea_numeros($dolar_anterior);
                    $dolar_anterior=~ tr/\.//d;
#                   $MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar_anterior."|".$dolar_anterior."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio]; #ECO_CIMA
                   $MAPEOImprime2{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar_anterior."|".$dolar_anterior."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio]; #ECO_CIMA
				}
            }
            if (lc($descripcion_ser) ne "servicio de voz"  && lc($descripcion_ser) ne "tarifa basica " && lc($descripcion_ser) ne "tarifa básica " && lc($descripcion_ser) ne "cargo a paquete" &&  lc($descripcion_ser) ne "Entrega de factura") {
				# saco las fechas para el detalle
                if (/DTM\+901\:([^\:]*)\:102\'/) {	
					$fecha_i=&enceraEspacios($1);
                }
                if (/DTM\+902\:([^\:]*)\:102\'/) {
					$fecha_f=&enceraEspacios($1);
                }
				if (/MOA\+60\:([^\:]*)\:USD\:959\:9\'/) {
					$valor_ser=&enceraEspacios($1);
# 10/04/2013					$valor_ser=&formatea_numeros($valor_ser);
# 10/04/2013                    $valor_ser=~ tr/\.//d;
                }
				if ($fecha_i ne "") {
		         @arreglo_num=split(/\|/,$Mdetalle{$clave_det_ant}[0]);
                 $clave=$arreglo_num[0];
                 $descripcion=$arreglo_num[1];
                 $dolar=$arreglo_num[3];
                 $t_inicio=&enceraEspacios($arreglo_num[5]);
                 $t_fin=&enceraEspacios($arreglo_num[6]);
				 if ($t_inicio eq "") {
					$rango_tiempo=&rango_fecha($fecha_i,$fecha_f);
				    $Mdetalle{$clave_det_ant}=[$clave."|".$descripcion."|".$rango_tiempo."|".$valor_ser."|".$valor_ser."|".$fecha_i."|".$fecha_f."|".$codigo_servicio."|"]; 
                 } else
			     { 
				    $cont_detalle=$cont_detalle + 1;
                    $rango_tiempo=&rango_fecha($fecha_i,$fecha_f);
				    $Mdetalle{$co_id." ".$key_resumen_dth." ".$cont_detalle}=[$key_resumen_dth."|".$descripcion."|".$rango_tiempo."|".$valor_ser."|".$valor_ser."|".$fecha_i."|".$fecha_f."|".$codigo_servicio."|"]; 
			     }
				}
			}
			$clave_anterior="";
        }
    }

}# FIN TIM DOS BUSCA VALROES

sub arma_anexo{
undef(%Mdetallefinal); $band_proc="0";
$cont_det_f="0";
#$archivo_post="servicios_postpago_".$fecha_corte."_".$procesador.".txt";
$archivo_post="postpago_".$fecha_corte."_".$co_id."_".$procesador.".txt";
$archivo_postpago=$carpeta_post."/".$archivo_post;
$nombre_out="output_doc1_dth_".$procesador."_".$ciclo.".txt";
$fecha_ingreso=`date '+%d/%m/%Y %H:%M:%S'`;
$fecha_ingreso=~ s/\n//g;
$dia_c=`expr substr $fecha_corte 7 2`;
$dia_c=~ s/\n//g;
$mes_c=`expr substr $fecha_corte 5 2`;
$mes_c=~ s/\n//g;
$anio_c=`expr substr $fecha_corte 1 4`;
$anio_c=~ s/\n//g;
$fecha_c=$dia_c."/".$mes_c."/".$anio_c;
open(ARCHIVO_POSTPAGO, ">> $archivo_postpago");	#archivo donde voy a poner los valores pospago
if ($plan_mostrar eq "") {
	$plan_mostrar=$plan;
}
$proc="";
$proc2="";
foreach  (keys %Mdetalle ) {
  $key=$_;
  $band_activacion="1";
  $band_negado="0";
  $trama=$Mdetalle{$key}[0];
  @arreglo_det=split(/\|/,$trama);
  $clave_det=$arreglo_det[0].$co_id;
  $serv_det=$arreglo_det[1];
  $valor_det=$arreglo_det[3];
  $valor_original=$arreglo_det[3];
  $fec_i_det=$arreglo_det[5];
  $fec_f_det=$arreglo_det[6];
  $cod_serv_det=$arreglo_det[7];
#  if ($cod_serv_det ne "DE003"){
# NCA 30/07/2013  if ($Mapeogrupodth{$cod_serv_det}[3] ne "C"){ 
  if ($Mapeogrupodth{$cod_serv_det}[3] ne "C" && $arreglo_det[1] ne "Crédito a Facturación"){ 
    if ($fec_i_det eq $fecha_corte) {		# si la fecha de inicio es la misma que la emision de la fatura significa que es el facturado por anticipado
	  for (keys %Mdetalle) {			# recorro el arreglo en busca del valor pagado x anticipado
		$key2=$_;
		$trama2=$Mdetalle{$key2}[0];
		@arreglo_det2=split(/\|/,$trama2);
		$valor_det2=$arreglo_det2[3];
		if ($cod_serv_det eq $arreglo_det2[7]) {	# es el mismo servicio
        	if ($arreglo_det2[3] lt "0") {	# si el valor es negativo entonces encontre el pagado x anticipado
		    	for (keys %Mdetalle) {	#busco el valor utilizado
			    	$key3=$_;
					$trama3=$Mdetalle{$key3}[0];
					@arreglo_det3=split(/\|/,$trama3);
					if ($cod_serv_det eq $arreglo_det3[7]){
						if ($key3 ne $key2 && $key3 ne $key) {
						    $valor_det2=$valor_det2 + $arreglo_det3[3];
							$band_negado = "1";
						}
				  }
				}
				$valor_det=$valor_det + $valor_det2;
			} # fin si es menor que 0
			else	# si es mayor que 0
			{
				$band_activacion = "1";
			}
		} # fin si es el mismo servicio
	  } #fin 2do for
	  if ($band_activacion eq "1" && $band_negado eq "0") {	# si es primera activacion
	    for (keys %Mdetalle) {			# recorro el arreglo en busca del valor postpagado
	      $key2=$_;
	      $trama2=$Mdetalle{$key2}[0];
	      @arreglo_det2=split(/\|/,$trama2);
	      $valor_det2=$arreglo_det2[3];
	      $fec_i_act=$arreglo_det2[5];	
	      $fec_f_act=$arreglo_det2[6];
    	  if ($cod_serv_det eq $arreglo_det2[7]) {	# es el mismo servicio
			if ($key2 ne $key) {	# si el registro es diferente que el que ya estoy analizando
	          # voy a buscar si hay mas
			  if ($key2 ne $proc && $key2 ne $proc2) {
			  foreach (keys %Mdetalle) {			# recorro el arreglo en busca de si hay otro valor mas de cobrar x ahi
				$key3=$_;
				$trama3=$Mdetalle{$key3}[0];
				@arreglo_det3=split(/\|/,$trama3);
				if ($cod_serv_det eq $arreglo_det3[7]) {	# si es el mismo servicio
				  if ($key3 ne $key2 && $key3 ne $key) { # si no es ninguno de los 2 tomados anteriormente
				      $valor_det2=$valor_det2 + $arreglo_det3[3];
					  if ( $fec_i_act gt $arreglo_det3[5] ) {
						$fec_i_act=$arreglo_det3[5];
					  }
					  if ($fec_f_act lt $arreglo_det3[6]) {
					    $fec_f_act=$arreglo_det3[6];
					  }
					  if ($proc eq "") {
					    $proc=$key3;	# guardo el servicio que ya ha sido procesado
					  }
					  else
					  {
						  $proc2=$key3;
					  }
				  } # fin si no es ninguno de los registros anteriores
			    } # fin mismo servicio
			  } # fin 3er for
			  $cont_det_f=$cont_det_f + 1;
			  $valor_det2f=&formatea_numeros($valor_det2);
		      $rango_tiempo=&rango_fecha($fec_i_act,$fec_f_act);		# guardo este registro en un arreglo del detalle final.
           	  $Mdetallefinal{$co_id." ".$key_resumen_dth." ".$cont_det_f}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det2."|".$valor_det2."|".$fec_i_act."|".$fec_f_act."|".$cod_serv_det."|"."PO"."|"]; 
			  print ARCHIVO_POSTPAGO $cus_id."|".$co_id."|"."PO".$cod_serv_det."|".$valor_det2f."|".$fecha_c."|"."A"."|".$procesador."|".$fecha_ingreso."|".$nombre_out."|".$lohrefnum."|".$ciclo."|"."\n";
			  }# fin si es diferente a proc
			} # fin key
		  } # fin el mismo servicio
	    } # fin valor postpago
	  }	# fin si es primera activacion
      $cont_det_f=$cont_det_f + 1;
      $rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);		# guardo este registro en un arreglo del detalle final.
      if ($valor_original ne $valor_det){
	  	$Mdetallefinal{$co_id." ".$key_resumen_dth." ".$cont_det_f}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_det."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|"];
	  }
	  else
	  {
		$Mdetallefinal{$co_id." ".$key_resumen_dth." ".$cont_det_f}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_det."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|"];
	  }
    } # fin si la fecha es igual que la fecha de corte
  } # fin si no es entrega de factura
}	# fin for del detalle
$primera_act="1";
foreach  (keys %Mdetalle ) {	# for para procesar los que no tienen renovacion
  $band_proc="0";
  $key=$_;
  $trama=$Mdetalle{$key}[0];
  @arreglo=split(/\|/,$trama);
  $cod_serv_det=$arreglo[7];
  $serv_det=$arreglo[1];
  $valor_det=$arreglo[3];
#  if ($cod_serv_det ne "DE003"){
# NCA 30/07/2013  if ($Mapeogrupodth{$cod_serv_det}[3] ne "C"){
  if ($Mapeogrupodth{$cod_serv_det}[3] ne "C" && $arreglo[1] ne "Crédito a Facturación"){ 
	$band_proc=&verifica_proc($cod_serv_det);
	if ($band_proc ne "1") {	# no ha sido procesado
    	$fec_i_det=$arreglo[5];
		$fec_f_det=$arreglo[6];
		foreach  (keys %Mdetalle) {
    	    $key2=$_;
		    $trama2=$Mdetalle{$key2}[0];
		    @arreglo_det2=split(/\|/,$trama2);
		    $valor_det2=$arreglo_det2[3];
    		if ($cod_serv_det eq $arreglo_det2[7]) {	# es el mismo servicio
			  if ($key2 ne $key ) { # si no es el mismo registro tomado anteriormente
				  if ($arreglo_det2[3] lt "0" || $arreglo[3] lt "0") {
				    $primera_act="0";
				  }	# fin si el valor es negativo
	              $valor_det=$valor_det + $valor_det2;
				  if ($fec_i_det gt $arreglo_det2[5]) { # si la fecha de inicio que ya tenia es mayor que la del siguiente bloque almaceno la menor
					$fec_i_det=$arreglo_det2[5];
				  }
				  if ($fec_f_det lt $arreglo_det2[6]) { # si la fecha fin que ya tenia es menor que la del siguiente bloque almaceno la mayor
					$fec_f_det=$arreglo_det2[6];
				  }
				}	# fin si no es el mismo registro
			}	# fin si es el mismo servicio
		}	# fin for 2
		$cont_det_f=$cont_det_f + 1;
		$valor_for=$valor_det;
    	$valor_for=~ tr/\.//d;
		$rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);		# guardo este registro en un arreglo del detalle final.
		if ($primera_act eq "0") {
			# NCA 30/07/2013 si el valor es negativo lo guardo como postpagado a pesar de que se que no es asi.
			if ($valor_det lt "0")
			{
			  $valor_detf=&formatea_numeros($valor_det);
   			  $Mdetallefinal{$co_id." ".$key_resumen_dth." ".$cont_det_f}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_for."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|"."PO"."|"];
 		          print ARCHIVO_POSTPAGO $cus_id."|".$co_id."|"."PO".$cod_serv_det."|".$valor_detf."|".$fecha_c."|"."A"."|".$procesador."|".$fecha_ingreso."|".$nombre_out."|".$lohrefnum."|".$ciclo."|"."\n";
			}
		
			else
			{
   			$Mdetallefinal{$co_id." ".$key_resumen_dth." ".$cont_det_f}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_for."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|"];
			}
		}
		else
		{
		  if ($arreglo[5] ne "") {
			$valor_detf=&formatea_numeros($valor_det);
   			$Mdetallefinal{$co_id." ".$key_resumen_dth." ".$cont_det_f}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_for."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|"."PO"."|"];
 		    print ARCHIVO_POSTPAGO $cus_id."|".$co_id."|"."PO".$cod_serv_det."|".$valor_detf."|".$fecha_c."|"."A"."|".$procesador."|".$fecha_ingreso."|".$nombre_out."|".$lohrefnum."|".$ciclo."|"."\n";
		  }
		  else
		  {
   			$Mdetallefinal{$co_id." ".$key_resumen_dth." ".$cont_det_f}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_for."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|"."|"];
		  }
		}
	}	# fin no procesado
   } # fin si no es entrega de factura
}	#fin for 1
undef(%Mdetalle);
}	# fin arma_anexo

#9576 SUD.LZV 18112014 nuevo proceso arma_anexo2 para detallar valores post-pagados y pre-pagados
sub arma_anexo2{
undef(%Mdetallefinal); 
$band_proc="0";
$cont_det_f="0";
#$archivo_post="servicios_postpago_".$fecha_corte."_".$procesador.".txt";
$archivo_post="postpago_".$fecha_corte."_".$co_id."_".$procesador.".txt";
$archivo_postpago=$carpeta_post."/".$archivo_post;
$nombre_out="output_doc1_dth_".$procesador."_".$ciclo.".txt";
$fecha_ingreso=`date '+%d/%m/%Y %H:%M:%S'`;
$fecha_ingreso=~ s/\n//g;
$dia_c=`expr substr $fecha_corte 7 2`;
$dia_c=~ s/\n//g;
$mes_c=`expr substr $fecha_corte 5 2`;
$mes_c=~ s/\n//g;
$anio_c=`expr substr $fecha_corte 1 4`;
$anio_c=~ s/\n//g;
$fecha_c=$dia_c."/".$mes_c."/".$anio_c;
my %rubros_post; #Para escribr los archivos 
my %fec_max_det; #Para tomar la fecha mayor de un grupo de servicios 
my %negativos;
$elementos=0;
$fec_aux_max; #Varaible auxiliar para comparar las fechas y tomar la fecha mayor
open(ARCHIVO_POSTPAGO, ">> $archivo_postpago");	#archivo donde voy a poner los valores pospago
if ($plan_mostrar eq "") {
	$plan_mostrar=$plan;
}
$proc="";
$proc2="";
$c=0;
foreach  (keys %Mdetalle ) {

  $key=$_;
  $band_activacion="1";
  $band_negado="0";
  $trama=$Mdetalle{$key}[0];
  @arreglo_det=split(/\|/,$trama);
  my @co_id_key=split(' ',$key);
  $clave_det=$arreglo_det[0].$co_id;
  $serv_det=$arreglo_det[1];
  $valor_det=$arreglo_det[3];
  $valor_negativo=$arreglo_det[3];
  $valor_original=$arreglo_det[3];
  $fec_i_det=$arreglo_det[5];
  $llave_fec=substr($arreglo_det[5],0,6);
  $fec_f_det=$arreglo_det[6];
  $cod_serv_det=$arreglo_det[7];
  $llave_anexo=$co_id_key[0]." ".$cod_serv_det." ".$key_resumen_dth." ".$fec_i_det;
  $valor_det=($valor_det)+($Mdetallefinal{$llave_anexo}[1]);
  $valor_det=sprintf('%.2f',$valor_det);
  $c=$c+1;
  if (($Mapeogrupodth{$cod_serv_det}[3] eq "T" || $Mapeogrupodth{$cod_serv_det}[3] eq "O") && $arreglo_det[1] ne "Crédito a Facturación"){ 
     if ($fec_i_det eq $fecha_corte) {
        #Pre-pagados
         $rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
		 $fec_f_anex=$Mdetallefinal{$llave_anexo}[3];
		 $Mdetallefinal{$llave_anexo}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_det."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|",$valor_det,$fec_i_det,$fec_f_det,$cod_serv_det,$serv_det,$co_id_key[0]];
		
						  if ($fec_f_anex ne ""){
						  $fec_est=0;
						  $fec_est= exists $Mdetallefinal{$llave_anexo};
							     if ($este == 1 ){
									 if ($Mdetallefinal{$llave_anexo}[3] lt $fec_f_anex){
									     $rango_tiempo=&rango_fecha($fec_i_det,$fec_f_anex);
									     $Mdetallefinal{$llave_anexo}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_det."|".$fec_i_det."|".$fec_f_anex."|".$cod_serv_det."|",$valor_det,$fec_i_det,$fec_f_anex,$cod_serv_det,$serv_det,$co_id_key[0]];
									 }
									 
						         }
					}
		}
        else {
		 if ($fec_i_det lt $fecha_corte) {
				 #Post-Pagados
				 $rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
					if($fec_i_det eq "" || $fec_f_det eq "" ) {
					   $Mdetallefinal{$llave_anexo}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_det."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|",$valor_det];
					}
					else{
					    $post="PO";
						#remplazo la llave anexo para que haga el calculo correcto
						$c=0;
						if ($valor_negativo lt "0"){
							$negativos{$llave_anexo}=[$valor_det];
							$elementos=1;
						}  
						  
						  if ($elementos == 1){
						  $este=0;
						  $este= exists $negativos{$llave_anexo};
							     if ($este == 1 ){
									 $post="";
						         }
						  }
						 if ($negativos{$llave_anexo}[0] lt "0"){
						     $post="PO";
					     }
						 $fec_f_anex=$Mdetallefinal{$llave_anexo}[3]; 
						 $Mdetallefinal{$llave_anexo}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_det."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|".$post."|",$valor_det,$fec_i_det,$fec_f_det,$cod_serv_det,$serv_det,$co_id_key[0],$post];
						 $valor_detf=&formatea_numeros($valor_det);
						 $rubros_post{$llave_anexo}=[$cus_id."|".$co_id_key[0]."|"."PO".$cod_serv_det."|".$valor_detf."|".$fecha_c."|"."A"."|".$procesador."|".$fecha_ingreso."|".$nombre_out."|".$lohrefnum."|".$ciclo."|"];
				
					if ($fec_f_anex ne ""){
						  $fec_est=0;
						  $fec_est= exists $Mdetallefinal{$llave_anexo};
						   if ($fec_est == 1 ){
								    if ($elementos == 1){
										 $este=0;
										 $este= exists $negativos{$llave_anexo};
									    if ($este == 1 ){
										    $post="";
									    }
								    }
									if ($negativos{$llave_anexo}[0] lt "0"){
										$post="PO";
									}
								if ($Mdetallefinal{$llave_anexo}[3] lt $fec_f_anex){
								    $rango_tiempo=&rango_fecha($fec_i_det,$fec_f_anex);
								    $Mdetallefinal{$llave_anexo}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_det."|".$fec_i_det."|".$fec_f_anex."|".$cod_serv_det."|".$post."|",$valor_det,$fec_i_det,$fec_f_anex,$cod_serv_det,$serv_det,$co_id_key[0],$post];
								}
									 
						    }
					}			
     			}
			#}
		  
		}
	 } #fin else
	}

}#fin for 1
#SUD.LZV 181114 Se busca registra todos los valores post-pagados recorriendo el arrays rubros_post

foreach (keys %Mdetallefinal){
$llave_1=$_;
if (($Mapeogrupodth{$Mdetallefinal{$llave_1}[4]}[3] eq "T" || $Mapeogrupodth{$Mdetallefinal{$llave_1}[4]}[3] eq "O") && $Mdetallefinal{$llave_1}[5] ne "Crédito a Facturación"){  
foreach (keys %Mdetallefinal){
$llave_2=$_;
if (($Mapeogrupodth{$Mdetallefinal{$llave_2}[4]}[3] eq "T" || $Mapeogrupodth{$Mdetallefinal{$llave_2}[4]}[3] eq "O") && $Mdetallefinal{$llave_2}[5] ne "Crédito a Facturación"){ 
	if ($Mdetallefinal{$llave_1}[2] ne $Mdetallefinal{$llave_2}[2]){  
	if ((substr($Mdetallefinal{$llave_1}[3],0,6)) eq (substr($Mdetallefinal{$llave_2}[3],0,6))){
	  if($Mdetallefinal{$llave_1}[4] eq $Mdetallefinal{$llave_2}[4]){ 
			if ($llave_1 ne $llave_2){
			if ($Mdetallefinal{$llave_1}[6] eq $Mdetallefinal{$llave_2}[6]){			
			if ($Mdetallefinal{$llave_1}[2] gt $Mdetallefinal{$llave_2}[2]){
			$llave_aux=$llave_1;
			$llave_1=$llave_2;
			$llave_2=$llave_aux;
			}
			$trama_final=$Mdetallefinal{$llave_1}[0];
			@arre_det=split(/\|/,$trama_final);
			$clave_det=$arre_det[0];
			$serv_det=$arre_det[1];
			$rango_tiempo=$arre_det[2];
			$fec_i_det=$arre_det[5];
			$fec_f_det=$arre_det[6];
			if ($fec_f_det lt $Mdetallefinal{$llave_2}[3]){
			$fec_f_det=$Mdetallefinal{$llave_2}[3];
			}
			$cod_serv_det=$arre_det[7];
			$post=$arre_det[8];
				$llave_borra=$llave_2;
				$valor_con=($Mdetallefinal{$llave_1}[1])+($Mdetallefinal{$llave_2}[1]);
				$valor_det=sprintf('%.2f',$valor_con);
				$rango_tiempo=&rango_fecha($fec_i_det,$fec_f_det);
				$Mdetallefinal{$llave_1}=[$clave_det."|".$serv_det."|".$rango_tiempo."|".$valor_det."|".$valor_det."|".$fec_i_det."|".$fec_f_det."|".$cod_serv_det."|".$post."|",$valor_det,$fec_i_det,$fec_f_det,$cod_serv_det,$serv_det,$Mdetallefinal{$llave_1}[6],$post];
				delete($Mdetallefinal{$llave_borra});
		     }
			}
		 }
		}
	   }#poner nueva llave
   }# ya estaba comentada
  }
 }
}
foreach (keys %rubros_post) {
	$key_post=$_;
	if ($elementos == 1){
	$este=0;
	$este= exists $negativos{$key_post};
	   if ($este == 1){
		      $valida=0;
			  $valida= exists $Mdetallefinal{$key_post};
			  if ($valida == 1){
			  $trama_rubros=$rubros_post{$key_post}[0];
			  @arre_ne=split(/\|/,$trama_rubros);
			  $ncus_id=$arre_ne[0];
			  $nco_id_key=$arre_ne[1];
			  $ncod_serv=$arre_ne[2];
			  $nvalor_detf=$arre_ne[3];
			  $nfecha_c=$arre_ne[4];
			  $nestado=$arre_ne[5];
			  $nfecha_ing=$arre_ne[7];
			  $nnombre_out=$arre_ne[8];
			  $nlohrefnum=$arre_ne[9];
			  $nciclo=$arre_ne[10];
			  $nvalor_detf=&formatea_numeros($Mdetallefinal{$key_post}[1]);
			  if (($nvalor_detf ne "0.00" || $nvalor_detf != 0) && ($Mdetallefinal{$key_post}[7] eq "PO" )) {
			  print ARCHIVO_POSTPAGO $ncus_id."|".$nco_id_key."|".$ncod_serv."|".$nvalor_detf."|".$nfecha_c."|".$nestado."|".$procesador."|".$nfecha_ing."|".$nnombre_out."|".$nlohrefnum."|".$nciclo."|"."\n";
		     }
			}
	   }
	}
	else{
	#$rubros_post{$llave_anexo}=[$cus_id."|".$co_id_key[0]."|"."PO".$cod_serv_det."|".$valor_detf."|".$fecha_c."|"."A"."|".$procesador."|".$fecha_ingreso."|".$nombre_out."|".$lohrefnum."|".$ciclo."|"];
		  $trama_rubros=$rubros_post{$key_post}[0];
			  @arre_ne=split(/\|/,$trama_rubros);
			  $pcus_id=$arre_ne[0];
			  $pco_id_key=$arre_ne[1];
			  $pcod_serv=$arre_ne[2];
			  $pvalor_detf=$arre_ne[3];
			  $pfecha_c=$arre_ne[4];
			  $pestado=$arre_ne[5];
			  $pfecha_ing=$arre_ne[7];
			  $pnombre_out=$arre_ne[8];
			  $plohrefnum=$arre_ne[9];
			  $pciclo=$arre_ne[10];
			  $pvalor_detf=&formatea_numeros($Mdetallefinal{$key_post}[1]);
			  if (($pvalor_detf ne "0.00" || $pvalor_detf != 0) && ($Mdetallefinal{$key_post}[7] eq "PO")) {
			  print ARCHIVO_POSTPAGO $pcus_id."|".$pco_id_key."|".$pcod_serv."|".$pvalor_detf."|".$pfecha_c."|".$pestado."|".$procesador."|".$pfecha_ing."|".$pnombre_out."|".$plohrefnum."|".$pciclo."|"."\n";
	         }
}
}
undef(%rubros_post); #SUD.LZV 181114 Se libera el array asociativo rubros_post
undef(%negativos);
undef(%Mdetalle);
}

#SUD.LZV 18112014 Fin arma_anexo2 
sub valores_iva_ice {
	my $shdes=$_[0];
	my $des_shdes=$_[1];
	my $aplica_iva=$_[2];
	my $aplica_ice=$_[3];

	if ($aplica_iva eq "S") {
		$tipo_iva=$MAPEObgh_tariff_plan{$shdes,1}[4];
		$des_iva=$MAPEOtax_category{$tipo_iva}[0];
		$por_iva=$MAPEOtax_category{$tipo_iva}[1]/100;
		if ($des_iva eq "") {
			$des_iva="I.V.A. Por servicios (12%)";
#			$des_iva="I.V.A.";
			$por_iva=0.12;
		}
	} 
	else {
		$des_iva="I.V.A. Por servicios (0%)";
		$por_iva=0.0;
	}

	if ($aplica_ice eq "S") {
		$tipo_ice=$MAPEObgh_tariff_plan{$shdes,1}[5];
		$des_ice=$MAPEOtax_category{$tipo_ice}[0];
		$por_ice=$MAPEOtax_category{$tipo_ice}[1]/100;
		if ($des_ice eq "") {
#			$des_ice="ICE en servicios de Telecomunicacion (15%)";
			$des_ice="I.C.E. (15%) (Solo aplica televisión)";
			$por_ice=0.15;
		}
	}
	else {
		$des_ice="ICE en servicios de Telecomunicacion (0%)";
		$por_ice=0.0;
	}
} # fin valores_iva_ice

sub obtiene_cedula_nombre {
# INICIO OBTIENE CEDULA NOMBRE
#if ( /RFF\+SC\:([^\:]*)\'\n/ ) { $ci_ruc=$1; } # Cédula #
if ( /RFF\+SC\:([^\:]*)\'/ ) { $ci_ruc=$1; } # Cédula #
if (/NAD\+IV/) {
$_=~ s/\?\://g;
$_=~ s/\?\?/\?/g;
}
if (/NAD\+IV\+\+\+([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:\']*)/) {
  $customer_name=$2;
  $customer_name=~ tr/\|//d;
}
$MAPEOinfo_contr_text{"999999"}=[$ci_ruc,$customer_name];
}# FIN OBTIENE CEDULA NOMBRE


sub obtiene_valor_resumen {
my $co_id=$_[0];
my $key_resumen_celular=$_[1];
my $cont_calculo=$_[2];
my $val_resumen;
my $val_dolares=0;

#$val_resumen=$MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}[0];
$val_resumen=$MAPEOImprime2{$co_id." ".$key_resumen_celular." ".$cont_calculo}[0];
@arreglo_num=split(/\|/,$val_resumen);
$val_dolares=$arreglo_num[3];
$tp_inicio=&enceraEspacios($arreglo_num[5]);
$tp_fin=&enceraEspacios($arreglo_num[6]);
return($val_dolares,$tp_inicio,$tp_fin);
} # fin obtiene_valor_resumen

sub tim2_calcula {
# INICIO TIM2 CALCULA
foreach  (keys %MAPEOTotales ) {
  #IVA ICE  #
  @arreglo_num=split(/\|/,$_);
  $co_id=$arreglo_num[0];	
  $marca=$MAPEOTotales{$_}[4];
  $key_resumen_dth="42000";
  $key_resumen_total_dth="42200";
  $cta_ps="";
  if ($marca eq "ISD") {
	$key_resumen_dth="31000";
	$key_resumen_total_dth="31100";
  } 
  $IVAv=$MAPEOTotales{$_}[1]*$por_iva;
  $ICEv=$MAPEOTotales{$_}[2]*$por_ice;
  $ICEv1=$MAPEOTotales{$_}[2]*$por_ice;
  $IVAv=&formatea_numeros($IVAv);
  $ICEv=&formatea_numeros($ICEv);
  $suma_total=$MAPEOTotales{$_}[0]+$IVAv+$ICEv;
  $suma_total=&formatea_numeros($suma_total);
  $suma_total=~ tr/\\\.//d;
  $cont_calculo=&obtiene_contador("IVA","0");
  $IVAv=~ tr/\\\.//d;
#  $MAPEOImprime{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_dth."|".$des_iva."||".$IVAv."|".$IVAv."|"];
  $MAPEOImprime2{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_dth."|".$des_iva."||".$IVAv."|".$IVAv."|"];
  $cont_calculo=&obtiene_contador("ICE","0");;
  $ICEv=~ tr/\\\.//d;
  $cont_calculo=&obtiene_contador("SUMA","0");
#  $MAPEOImprime{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_total_dth."|".$suma_total."|"];
  $MAPEOImprime2{$co_id." ".$key_resumen_dth." ".$cont_calculo}=[$key_resumen_total_dth."|".$suma_total."|"];
  $ConsumoCelular{$_}=[$suma_total,$MAPEOTotales{$_}[3], $MAPEOTotales{$_}[4]];
  if ($Graba_co_id{$co_id}[0] != "00000000000") {
 	$MAPEOImprime{$co_id." 40000"}=["40000|".$Graba_co_id{$co_id}[0]."|".$co_id."|"];
  }
  $fono1=$Graba_co_id{$co_id}[0];
  &graba_co_id($co_id,$fono1,"2");
 }
 undef(%MAPEOTotales);
}# FIN TIM2 CALCULA

sub buscaValoresResumen{
# INICIO BUSCA VALORES RESUMEN CELULAR
#LIN+12++TP088.4.SP02.AM001.U:SA::++04'
#PIA+1+BASE.F.I.4.3.NOP.ZP13.1.2++'
#IMD++8+PRO::::'
#IMD++8+TM:::TP088:Especial 6'
#IMD++8+SP:::SP02:AMPS ?: Servicios Basicos '
#IMD++8+SN:::AM001:Servicio de Voz'
#QTY+107:83:UNI'
#MOA+125:4.66:USD:959:9'
#MOA+125:4.66:USD:959:19'

#PIA+1+BASE.F.I.4.3.                  NOP.ZP13.1.2++'

if ( /PIA\+1\+*/ ) {    
#  /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'\n/;			19/03/2013
  /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'/;
  $calltype=$1;
  $ttcode=$2;
  $tzcode=$3;
  if ($calltype eq "IC" || $calltype eq "BASE") {
#	if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n[^\n]/ ) {				19/03/2013
	if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'[^\n]/ ) {
		  $codigo_servicio=$1;
		 if (/IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'/) {
		   if ($1 eq "ROTIN" || $1 eq "ROTOP" || $1 eq "ROTIT"|| $1 eq "RITIM" || $1 eq "RITIA"  || $1 eq "RITIN" || $1 eq "ROTIN" || $1 eq "TPESN" || $1 eq "POCC") {
			  $plan=$1;
			  $des_plan=$2;
			 } else {
  			  $plan=$1;
			  $des_plan=$2;
			  $plan1=$plan;
			  $des_plan1=$des_plan;
			  if ($codigo_servicio eq "PV002") {
                if ($bandera_plan eq "0") {
				  &graba_co_id($co_id,$fono,"0",$plan1,$des_plan1);
				  $bandera_plan = "1";
                  $co_id_alter1=$co_id;
		          $co_id_alter2=$co_id;
                }
		      }        	 			  
		    }
		 }
		 # Otengo el Valor el 2do MOA #
		 if (/MOA.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/) {
			 $dolar=$1;
		 }	
		 ($descripcion_ser,$des,$sh_des)=&busca_descripcion_etiqueta($codigo_servicio,$tzcode,$calltype,$ttcode,$plan_tele,"");
		 if ($des eq "") {
			 $des=$sh_des;
		 }
		 $val_dol=$ResumenConsumos{$co_id,$des}[0];
		 $val_dol=$val_dol + $dolar;
		 $ResumenConsumos{$co_id,$des}=[$val_dol];
	} # fi SN
   } # fi calltype
} # fi PIA
}# FIN BUSCA VALORES RESUMNEN CELULAR

sub tim_type_uno{
# INICIO TIME TYPE UNO
$valor_absoluto=0;		
$aire=0;		
$interconexion=0;		
$impuesto_iva=0;
$impuesto_ice=0;
$valor_descuento=0;
$des_descuento="";
$ban_iva="";
$ban_ice="";
$des_tiempo="";
$valor_descuento_original=0;

# Datos Personales del Cliente #
# *I* ANGEL LLERENA HIDALGO CDLA NUEVA KENEDY CALLE B 108 Y LA CUART A ESTE  028 GUAYAQUIL

#NAD+IT+++*I*:JORGE LAGOS BALSECA:VIA A COLOMBIA 1020 Y JORGE A??AZCO  LOCA:L ?: "SUPER MINI  DE TODO " :LAGO AGRIO  *I*:++ECUADOR'
#NAD+IV+++*I*:JORGE LAGOS BALSECA:VIA A COLOMBIA 1020 Y JORGE A??AZCO  LOCA:L ?: "SUPER MINI  DE TODO " :LAGO AGRIO  *I*:++ECUADOR'
#print " linea ".$line."\n";
if (/NAD\+IV/) {
$_=~ s/\?\://g;
$_=~ s/\?\?/\?/g;
}

if (/NAD\+IV\+\+\+([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:\']*)/) {
$cod_insert=$1;
$customer_name=$2;
$customer_name=~ tr/\|//d;
$customer_name=~ tr/\"//d;
$address1=&enceraEspacios($3);
$address2=&enceraEspacios($4);
$address3=$ladrs;
$address1=~ tr/\"//d;
$address2=~ tr/\"//d;
$address1=~ tr/\|//d;
$address2=~ tr/\|//d;
$address3=~ tr/\"//d;
$address3=~ tr/\|//d;
#-----------------------------------
	if ($address1 eq "") {
		$address1=$address2;
		$address2="";
	}
	$zone_name=$5;
	if (/\*I\*\:[^\:\']*/ && $cod_insert eq "") {
		$cod_insert = "*I*";
	}
	if (substr($zone_name,0,1) =~ /^\d/ ) {
		$zone_name =~ /^([^ ]+) (.*)/;
		$zone_no= $1;
		$zone_name= " ".$2;
	} else {
		$zone_no="0"
	}
}
#if ( /RFF\+SC\:([^\:]*)\'\n/ ) {	# Cédula #		19/03/2013
if ( /RFF\+SC\:([^\:]*)\'/ ) {	# Cédula #
	$ci_ruc=$1;		
	$ban_cedula_no_impresa="1";
} 
#if ( /RFF\+PG\:([^\:]*)\'\n/) {  # Forma de Pago #
if ( /RFF\+PG\:([^\:]*)\'/) {  # Forma de Pago #
	$type_pay=$1; 
} 
if ( /FII\+AO\+.*\:+\:([^\+\:\']*)/ ) {		# Descripcion de la forma de pago #
	$type_pay_description=$1; 
}
if (/RFF\+IT\:(\d\.\d+)[^\d]/) {  # Cuenta del cliente #
	$account_no=$1; 
}
if ( /FII\+RH\+9999\:([^\:]*)/) {	# Ciudad #
	$city=$1; 
} 
if ( /DTM\+168\:([^\:]*)/ ) {   # Fecha Fatura #
	$billing_date=$1; 
} 
if (/DTM\+13\:([^\:]*)/) {
  $due_date=$1;
}
# Detalles #
#if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n/ ) {		19/03/2013
if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'/ ) {
  $codigo_servicio=$1;
#  if ( /IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'\n/ ) {			19/03/2013
  if ( /IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'/ ) {
	$tmshdes=$1;
  }
#  if (/PRI\+CAL\:([^\:]*)\:INV\:USD\:959\'\n/) {			19/03/2013
  if (/PRI\+CAL\:([^\:]*)\:INV\:USD\:959\'/) {
	$valor_absoluto=$1;		
  }
  while  (/ALC\+.+\+([^\+]*)\+\'\n/g)  {
     $des_descuento=$1;
      if ( /MOA\+53\:([^\:]*)\:USD.*\n/g ) {
	     $valor_descuento=$1;
	     $valor_descuento_original=$valor_descuento_original+$valor_descuento;
	     $valor_descuento=sprintf('%.2f',$valor_descuento);
	     $valor_descuento1=($valor_descuento* -1) + $MAPEOGeneral{$des_descuento}[1];
	     $MAPEOGeneral{$des_descuento}=["20400",($valor_descuento1)];
      }

  }	# fin while
  if ($des_descuento ne "") {
	  $valor_descuento=$valor_descuento_original;
  }
  # Otengo el Valor el 2do MOA #
  #MOA+125:5.36:USD:959:9'
  #MOA+125:3.75:USD:959:5'
  /MOA\+125\:([^\n\:]*)\:USD.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/;
  $dolar_original=$1;
  $dolar=$2;

#  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/; #  IMD++8+SP    :  ::  SP51   :   GSM ? : Servicios Basicos '		19/03/2013
  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/; #  IMD++8+SP    :  ::  SP51   :   GSM ? : Servicios Basicos '
#  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'\n/; #IMD++8+SP   : : :   SP06  :Servicios OCC'								19/03/2013
  /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'/; #IMD++8+SP   : : :   SP06  :Servicios OCC'
  $codigo_servicio_basico=$1;   # Es para obtener si es Servicio de Voz Básico SP51 SP02
  # IVA #
  if ( /TAX\+1\+VAT\:\:I\.V\.A.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ){		
     $impuesto_iva=$1;
     $total_iva=$total_iva+$impuesto_iva;
     $ban_iva=1;
  }
  # ICE #
#  if ( /TAX\+1\+VAT\:\:ICE.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ) {
  if ( /TAX\+1\+VAT\:\:I.C.E.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ) {
     $impuesto_ice=$1;
     $total_ice=$total_ice+$impuesto_ice;
     $ban_ice=1;
  }

  $codigo_aux_rubro="";
  $aux_8504="";

  if ( /PIA\+1\+*/ ) {    
#     /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'\n/;			19/03/2013
     /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'/;
    $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
  } else {
    $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
  }	# fin PIA+1
  if ($descripcion_ser eq "") {
	 $bandera_type_uno="1";
  }
  ($type_key,$aplica_ice)=&verifico_tipo_agrupacion($ban_iva,$ban_ice);
  if ( $type_key eq "21100" ) {
	 $type_key=&verifica_servExterno($codigo_servicio,$type_key);
	 if ( $type_key eq "21650") {
		$total_servicios_Ext=$total_servicios_Ext+$dolar;
	 }
  }
  $dolar=$dolar+$valor_descuento;
  $dolar=sprintf('%.2f',$dolar);
  if ( $dolar != 0 ){
     $descripcion_ser=cambia_etiqueta($descripcion_ser)."|".$type_key;
     $dolar=$dolar + $MAPEOGeneral{$descripcion_ser}[1];

	 #[9587] -ini - SUD CAC - Factura Electrónica DTH
	 #Se recoge el rubro del descuento 
	 if ( $band_electronica eq "4" && ($type_key eq "20200" || $type_key eq "21100")){
		if($codigo_aux_rubro eq ""){
			$codigo_aux_rubro="0";
		}
		$sumas_desc_rubro=($valor_descuento)+$MAPEOGeneral{$descripcion_ser}[7];
		$sumas_desc_rubro=sprintf('%.2f',$sumas_desc_rubro);

    	#$MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$codigo_aux_rubro,($sumas_desc_rubro)];
		$MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$aplica_ice,$Mapeogrupodth{$codigo_servicio}[3],$codigo_aux_rubro,($sumas_desc_rubro)]; #Se agrega información de Factura Electrónica

	}else{#[9587] -fin - Factura Electrónica DTH

		#$MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$aplica_ice]; #03/04/2013
       $MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$aplica_ice,$Mapeogrupodth{$codigo_servicio}[3]]; #08/07/2013 añado si es rubro de cabecera o detalle
	}##
 }
 $codigo_aux_rubro=""; 
}
# Total Consumo del Mes #
# Total consume del Mes se le resta el valor de Otros servicios. 
if (/MOA.*\nMOA\+77\:([^\n\:]*)\:USD.*\n/) {			
  $total_consumo_mes_primera=sprintf('%.2f',&obtieneTotalMenosServExt($1));
  $ban_timm1_completo="1";
}
}# FIN TIM  TYPE UNO

sub tim_type_cero {
# INICIO TIM TYPE CERO
if ( /MOA\+962\:([^\:]*)\:USD.*\n/ ) {  
   $suma_pagos_rec=$1;
}
if ( /MOA\+76\:([^\:]*)\:USD.*\n/ ) {  
$saldo_consumo=$1;
}
### CLS JCE.06/10/2009:Sumas de sobrepagos.
if ( /MOA\+971\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos=$1;
}
if ( /MOA\+978\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos2=$1;
}
if ( /MOA\+981\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos3=$1;
}
if ( /MOA\+988\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos4=$1;
}
$saldo_tmp=$saldo_consumo - $suma_pagos_rec;
$saldo_tmp_tot=$saldo_tmp - $suma_sobrepagos - $suma_sobrepagos2 - $suma_sobrepagos3 - $suma_sobrepagos4;
$saldo_anterior=sprintf('%.2f',$saldo_tmp_tot);
if ( /MOA\+967\:([^\:]*)\:USD.*\n/ ) { # Consumos del Mes
   $consumo_mes=sprintf('%.2f',&obtieneTotalMenosServExt($1));
   $totalservExt=$total_servicios_Ext;
}
if ( /MOA\+968\:([^\:]*)\:USD.*\n/ ) { # Valor a Pagar
   $dolar=sprintf('%.2f',$1);
   $valor=$consumo_mes."|".$dolar;
   $MAPEOTotales{"21600"}=[$valor];
   $total_due=$dolar;
}
if ( /MOA\+11\:([^\:]*)\:USD.*\n/ ) { # Pagos Recibidos
   $dolar=sprintf('%.2f',$1);
   #Verifica si cuadra el total de la Factura.
   $sumacomprobacion=sprintf('%.2f',$saldo_anterior + $dolar + $consumo_mes + $totalservExt);
   if ( $sumacomprobacion != $total_due) {
	    $saldo_anterior_tmp = $total_due - $dolar - $consumo_mes - $totalservExt;
		$saldo_anterior=sprintf('%.2f',$saldo_anterior_tmp);
		# Verifica si cuadra el total de la Factura II. 
		$sumacomprobacion=sprintf('%.2f',$saldo_anterior + $dolar + $consumo_mes + $totalservExt);
		if ( $sumacomprobacion != $total_due) {
			$bandera_type_cero="1";
		}else{
			$bandera_type_cero="0";
		}
	}else{
		$bandera_type_cero="0";
	}
    $valor=$saldo_anterior."|".$dolar;
    $MAPEOTotales{"21400"}=[$valor];
 }
}# FIN TIM TYPE CERO


sub tim_type_dos{
# inicio tim dos

open(FILE_READ,$file);

$/="LIN\+";
$key_resumen_dth1="99990";
$key_resumen_dth="99991";
$total_iva=0;
$total_ice=0;
$suma_total=0;
$valor_contratado=0;
$ban="0";
$ban_dos="0";
$valor_descuento=0;
$total_consumo_mes_primera=0;
$total_servicios_Ext=0;
$account_no="";
$ci_ruc=""; 
$cod_insert="";
$customer_name="";
$address3="";
$address1="";
$address2="";
$zone_no="";
$zone_name="";
$type_pay="";
$type_pay_description="";
$city="";
$billing_date="";
$total_due="000";
$bandera_plan = "0";
$bandera_plan1 = "0";
$quiebre_co_id = "-999";
$cont_detalle = 0;

while(<FILE_READ>){ 
	 $line=$_;
	 if ($. == 1) {
		 $lcustcode="";
 	     $lprgcode="";
 	     $lcostcenter_id="";
 	     $lseq_id="";
 	     $lbillcycle="";
		 $lohrefnum="";
		 $ladrs="";

		if (/CUSTCODE\|([^\n]+)\n/) {
		 #1.10306366|1|2|2|28 
	     @arreglo_num=split(/\|/,$1);
 	     $lcustcode=$arreglo_num[0];
 	     $lprgcode=$arreglo_num[1];
 	     $lcostcenter_id=$arreglo_num[2];
 	     $lseq_id=$arreglo_num[3];
 	     $lbillcycle=$arreglo_num[4];
		 $lohrefnum=$arreglo_num[5];
		 $ltipofactura=$arreglo_num[6]; 
		 $lmail=$arreglo_num[7]; 
		 $ltelefono=$arreglo_num[8]; 
		 $tipo_identificacion=$arreglo_num[9]; 
         $ladrs=$arreglo_num[10]; 
		 #$lohrefnum=~ tr/\'.//d;  #lzv 9576 10022015
		 $lohrefnum=~ tr/\.//d;  #lzv 9576 	10022015	 			
		}
		#saco la fecha de factura.
		if ( /DTM\+168\:([^\:]*)/ ) {   # Fecha Fatura #
			$fecha_fact=$1; 
		} 
		# saco la fecha de corte
		if ( /DTM\+3\:([^\:]*)/ ) {   # Fecha Corte #
			$fecha_corte=$1; 
		} 
	 } # fin si es 1er bloque
	 if ($ltipofactura eq "4"){
		 $band_electronica="4";
	 }
 	 if (/PAT\+5/) {
	    $banderaUno="1";
		if ($Cuentas_Excentas{$lcustcode} eq "S") {
			&valores_iva_ice($plan1,$des_plan1,"N","N");
		}
		else {
			&valores_iva_ice($plan1,$des_plan1,"S","S");
		}
        # Obtengo la Cuenta #
		if (/RFF\+IT\:(\d\.\d+)[^\d]/) {  $account_no_resumen=$1; }#4505
		&obtiene_cedula_nombre;
		&tim2_calcula;
		$clave="10000";
		$num_invoice="000-000-0000000";
		$emission_due_date="06032007";
		$total_due="000";
		$type_key="00000";
		$total_iva=0;
		$total_ice=0;
		$valor_absoluto=0;		
		$impuesto_iva=0;
		$impuesto_ice=0;
		$valor_descuento=0;
		$des_descuento="";
		$ban_iva="";
		$ban_ice="";
     } # fin path+5
 	 if ($banderaUno eq "0") {
		  if ($ban_dos eq "0") {
		    &timDosBuscaValores;
		  } else {
			&buscaValoresResumen;
		  }
		  if (/UNB\+UNOC/) {
		  $ban_dos="0";
		  $ban_acceso="0";
		  $account_no_resumen="0"; 
		  }
	 } else { 		
	   	&tim_type_uno;
	   	&tim_type_cero;
	 }
	} #while

    close FILE_READ;

	# NCA 23/03/2013
	#&arma_anexo;
&arma_anexo2;
	# Fin NCA 23/03/2013
   if ($banderaUno eq "1") {
	   $total_iva=sprintf('%.2f',$total_iva);
	   $total_ice=sprintf('%.2f',$total_ice);
	   $type_key_iva="20900";
	   if ($total_iva gt 0) {
		$MAPEOGeneral{$des_iva}=[$type_key_iva,$total_iva,"","","IVA"];#[9587] - SUD CAC se agrega marca para el impuesto
	   }
	   if ($total_ice gt 0) {
		$MAPEOGeneral{$des_ice}=[$type_key_iva,$total_ice,"","","ICE"];#[9587] - SUD CAC se agrega marca para el impuesto
	   }
	   $MAPEOTotales{"21300"}=[$total_consumo_mes_primera];

	   #[9587] - SUD CAC 
	   $base_imp_iva="";
	   $base_imp_ice="";
	   $base_iva_ice="";
	   #[9587] - SUD CAC 

		foreach (keys %MAPEOGeneral) {
			$dolar=sprintf('%.2f',$MAPEOGeneral{$_}[1]);
			$dolar=~ tr/\.//d;
			$descCambiar=$_;
			@arreglo_num=split(/\|/,$descCambiar);
			$descCambiar=$arreglo_num[0];			

				# inicio NCA 10/07/2013 Mapeo con tabla de configuracion
				$dolar_2="";
				$descripcion_dth_2="";
				$codigo_dth_2="";
				$key_dth_1="";
				$key_cab="3";
				if ( $MAPEOGeneral{$_}[0] eq "20200"){
					if ( $MAPEOGeneral{$_}[5] eq "T") { # pertenece al grupo de TV
						$descripcion_dth_2="Servicios Televisión";

						if ($ltipofactura eq "4"){
							$codigo_dth_2="STDO1";#[9587] - SUD CAC - Se agrega código principal al servicio
						}else{
						$codigo_dth_2="";
						}

						$key_dth_1="20200";
						$key_aux="1";
					}
					else{
						if($MAPEOGeneral{$_}[5] eq "O"){# pertenece al grupo de Otros
								#Los que solo aplican IVA
								$descripcion_dth_2="Otros Servicios";

								if ($ltipofactura eq "4"){
									$codigo_dth_2="STOO1";#[9587] - SUD CAC - Se agrega código principal al servicio
								}else{
								$codigo_dth_2="";
								}

								$key_dth_1=$MAPEOGeneral{$_}[0];
								$key_aux="2";
							}
							else	# son parte de la cabecera sin agrupacion.
							{
								$descripcion_dth_2=$Mapeogrupodth{$MAPEOGeneral{$_}[3]}[1];
								$codigo_dth_2=$MAPEOGeneral{$_}[3];
								$key_dth_1="20200";
								if ($MAPEOGeneral{$_}[3] eq "DE003" || $MAPEOGeneral{$_}[3] eq "EV302") {#9587 - Factura Electrónica DTH
									$key_aux="3";
								}
								else
								{
									$key_cab=$key_cab + 1;
									$key_aux=$key_cab;
								}
							}
					}
					$key_dth_2=$key_dth_1."|".$descripcion_dth_2;
					$dolar_2=sprintf('%.2f',$Mgrupodth_lineas{$key_dth_2}[2]);
					$dolar_2= $dolar_2 + $dolar;
					
					#[9587] - INI SUD CAC 
				    if ($ltipofactura eq "4"){

						$valor_desc_rubro=sprintf('%.2f',$MAPEOGeneral{$_}[7]);
						$valor_desc_rubro=~ tr/\.//d;
						
						#bases imponible de impuesto IVA - ICE
						if ( $MAPEOGeneral{$_}[5] eq "T") {
							#pertenece al grupo de TV
   						    $base_imp_ice=$base_imp_ice+$dolar;
						}else{
							$base_imp_iva=$base_imp_iva+$dolar;
						}

						$Mgrupodth_lineas{$key_dth_2}=[$key_dth_1,$descripcion_dth_2,$dolar_2,$codigo_dth_2,$key_aux,$MAPEOGeneral{$_}[6],$valor_desc_rubro];#codigo axuxiliar y descuento

					}else{ #[9587] - FIN SUD CAC
						
					$Mgrupodth_lineas{$key_dth_2}=[$key_dth_1,$descripcion_dth_2,$dolar_2,$codigo_dth_2,$key_aux];
					}

				}else{#Guarda los impuestos

				   #[9587] - INI SUD CAC 
				   if ($ltipofactura eq "4"){
					   #$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"];
					   $Mgrupodth_lineas{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0],$descCambiar,$dolar,$MAPEOGeneral{$_}[2],$MAPEOGeneral{$_}[3],$MAPEOGeneral{$_}[4]];
					   #print "Imprime Impuestos |".$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|".$MAPEOGeneral{$_}[4]."|\n";

   				       if ($MAPEOGeneral{$_}[4] eq "ICE"){						    
					       $base_iva_ice=$dolar;
					   }

				   }else{#[9587] - INI SUD CAC 

					$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"];
				   }
				}
				# fin NCA 10/07/2013 Mapeo con tabla de configuracion.		


			$dolar=$MAPEOSuma{$MAPEOGeneral{$_}[0]}[0] + $MAPEOGeneral{$_}[1];
			$MAPEOSuma{$MAPEOGeneral{$_}[0]}=[$dolar,$_];
		}

##[9587] - INI SUD CAC 
#print "|base ICE".$base_imp_ice."|Base IVA".$base_imp_iva."|\n";
$base_imp_iva_final=$base_imp_ice+$base_imp_iva+$base_iva_ice;
##[9587] - INI SUD CAC 

foreach (keys %Mgrupodth_lineas) {


#[9587] -ini - SUD CAC - Factura Electrónica DTH
if ($ltipofactura eq "4"){

print "$Mgrupodth_lineas{$_}[0]|\n";

 	if (($Mgrupodth_lineas{$_}[0] eq "20200")) {	 
	     $MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[4].$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[5]."|".$Mgrupodth_lineas{$_}[6]."|"];
	     #print "\n Electrónica ".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[5]."|".$Mgrupodth_lineas{$_}[6]."|\n";
	}	
	if (($Mgrupodth_lineas{$_}[0] eq "20900")) {

	    #$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"];
    	if (($Mgrupodth_lineas{$_}[5] eq "IVA")) {	
		    $MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|".$base_imp_iva_final."|"];
		    #print "Imprime Impuestos Mgrupodth_lineas|".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|=|".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|\n";
		}

		if (($Mgrupodth_lineas{$_}[5] eq "ICE")) {
			$MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|".$base_imp_ice."|"];
		    #print "Imprime Impuestos Mgrupodth_lineas|".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|=|".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[4]."|\n";
		}
	}
	
	if (($Mgrupodth_lineas{$_}[0] eq "21650")){
		
		$MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[4].$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|"];
	}

}else#[9587] - fin - SUD CAC - Factura Electrónica DTH
{

$MAPEOImprimeUno{$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[4].$Mgrupodth_lineas{$_}[1]}=[$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|"];
#  print "\n Normal ".$Mgrupodth_lineas{$_}[0]."|".$Mgrupodth_lineas{$_}[1]."|".$Mgrupodth_lineas{$_}[2]."|".$Mgrupodth_lineas{$_}[3]."|".$Mgrupodth_lineas{$_}[5]."|\n";

}


}#fin Mgrupodth_lineas 

##[9587] - INI SUD CAC 
$base_imp_ice="";
$base_imp_iva="";
$base_imp_iva_final="";
##[9587] - INI SUD CAC 


$MAPEOImprimeUno{"40000"}=["40000"."|".$account_no_resumen."|".$account_no_resumen."|"]; 
$total_tv="0";
$total_ot="0";
$cont_f="0";

foreach  (keys %Mdetallefinal) {
  $key=$_;
  $trama=$Mdetallefinal{$key}[0];
  my @co_id_key=split(' ',$key); # lzv 
  @arreglo_det=split(/\|/,$trama);
  $serv_det=$arreglo_det[1];
  $valor_det=&formatea_numeros($arreglo_det[3]);
  $valor_tv_sum=&formatea_numeros($arreglo_det[3]);
  $valor_det=~ tr/\.//d;
  if ($valor_det ne "000") {	# if para que no pinte los valores en cero  
	$valor_for=&formatea_numeros($arreglo_det[3]);
	$aplica_ice=$MAPEOGeneral{$serv_det."|"."20200"}[4];
	if ($aplica_ice eq "S") {
	  $clave_det="41000" ;
#	  $total_tv=$total_tv + $arreglo_det[3];
	  $total_tv=$total_tv + $valor_det;
	  #$clave_impr=$arreglo_det[0].$co_id;
	  $clave_impr=$arreglo_det[0].$co_id_key[0];
	  #$MAPEOImprimeUno{$clave_impr.$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|".$arreglo_det[2]."|\$".$valor_for."|".$valor_det."|".$arreglo_det[5]."|".$arreglo_det[6]."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"]; 
$MAPEOImprimeUno{$co_id_key[0].$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|".$arreglo_det[2]."|\$".$valor_for."|".$valor_det."|".$arreglo_det[5]."|".$arreglo_det[6]."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"]; 
	}
	$cont_f=$cont_f + 1;
  } # fin if valores en cero
}
#$total_tv=&formatea_numeros($total_tv);
$total_tv=~ tr/\.//d;

$cont_f="0";
$total_tv_fact="000";
$total_ot_fact="000";
foreach  (keys %Mdetallefinal) {
  $key=$_;
  $trama=$Mdetallefinal{$key}[0];
my @co_id_key=split(' ',$key); # lzv 
  @arreglo_det=split(/\|/,$trama);
  $serv_det=$arreglo_det[1];
  $valor_det=&formatea_numeros($arreglo_det[3]);
  $valor_det=~ tr/\.//d;
  if ($valor_det ne "000") {	# if para que no pinte los valores en cero  
	$valor_for=&formatea_numeros($arreglo_det[3]);
	$aplica_ice=$MAPEOGeneral{$serv_det."|"."20200"}[4];
	if ($aplica_ice eq "N") {
	  $clave_det="42000" ;
#	  $total_ot=$total_ot + $arreglo_det[3];
	  $total_ot=$total_ot + $valor_det;
	  #$clave_impr=$arreglo_det[0].$co_id;
$clave_impr=$arreglo_det[0].$co_id_key[0]; #lzv
	  if ($arreglo_det[7] ne "DP025") {
		#$MAPEOImprimeUno{$clave_impr.$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|"."|".$valor_det."|\$".$valor_for."|"."|"."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"];
	  $MAPEOImprimeUno{$co_id_key[0].$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|"."|".$valor_det."|\$".$valor_for."|"."|"."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"];
}
	  else
 	  {  
		#$MAPEOImprimeUno{$clave_impr.$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|".$arreglo_det[2]."|".$valor_det."|\$".$valor_for."|".$arreglo_det[5]."|".$arreglo_det[6]."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"];
	    $MAPEOImprimeUno{$co_id_key[0].$clave_det.$cont_f}=[$clave_det."|".$arreglo_det[1]."|".$arreglo_det[2]."|".$valor_det."|\$".$valor_for."|".$arreglo_det[5]."|".$arreglo_det[6]."|".$arreglo_det[7]."|".$arreglo_det[8]."|".$plan_mostrar."|"];
      }
	}
	$cont_f=$cont_f + 1;
  } # fin if valores cero
}
#$total_ot=&formatea_numeros($total_ot);
$total_ot=~ tr/\.//d;
#$MAPEOImprimeUno{$arreglo_det[0].$co_id."42200".$cont_f}=["42200"."|".$total_tv."|".$total_ot."|"];	# total de otros servicios
$MAPEOImprimeUno{$arreglo_det[0].$co_id_key[0]."42200".$cont_f}=["42200"."|".$total_tv."|".$total_ot."|"];	# total de otros servicios
undef(%Mdetallefinal);
		foreach (keys %MAPEOSuma) {
			$key_suma="";
#16042013			if ($_ eq "20000" ) { $key_suma="20100"; }
			if ($_ eq "20200")  { $key_suma="20300"; }
			if ($_ eq "20000" ) { $key_suma="NING"; }
#			if ($_ eq "20200")  { $key_suma="NING"; }
			if ($_ eq "20400")  { $key_suma="20600"; }
			if ($_ eq "20900")  { $key_suma="21000"; }
			if ($_ eq "21100")  { $key_suma="21200"; }
			if ($_ eq "21650")  { $key_suma="NING"; }
		   $dolar=sprintf('%.2f',$MAPEOSuma{$_}[0]);
		   $dolar=~ tr/\.//d;
		   if ( $key_suma ne "NING" ) {
			   $MAPEOImprimeUno{$key_suma}=[$key_suma."|".$dolar."|"];
		   }
		}

# validacion de si cuadran los valores de tv y de otros servicios
		$total_tv_fact=$Mgrupodth_lineas{"20200|Servicios Televisión"}[2];
		$total_ot_fact=$Mgrupodth_lineas{"20200|Otros Servicios"}[2];
		if ($total_tv_fact eq "") {
			$total_tv_fact="000";
		}
		if ($total_ot_fact eq "") {
			$total_ot_fact="000";
		}
		if (length($total_tv_fact) == 2)
		{
			$total_tv_fact="0".$total_tv_fact;
		}
		if (length($total_ot_fact) == 2)
		{
			$total_ot_fact="0".$total_ot_fact;
		}
#		print "valor " .$total_tv."|".$total_tv_fact."|".$total_ot."|".$total_ot_fact."|n";
		if ($total_tv ne $total_tv_fact || $total_ot ne $total_ot_fact) {
			$dif_tv=$total_tv - $total_tv_fact;
			$dif_ot=$total_ot - $total_ot_fact;
			if ($dif_tv gt "1" || $dif_tv lt "-1") {
				$band_descuadre="1";
			}else{
				if ($dif_ot gt "1" || $dif_ot lt "-1") {
					$band_descuadre="1";
				}
			}
		}
# fin validacion de si cuadran los valores de tv y de otros servicios
		$dolar=$MAPEOSuma{"20000"}[0] + $MAPEOSuma{"20200"}[0] + $MAPEOSuma{"20400"}[0];
		$dolar=sprintf('%.2f',$dolar);
		$dolar=~ tr/\.//d;
		$MAPEOImprimeUno{"20800"}=["20800|".$dolar."|"];
		foreach (keys %MAPEOTotales) {
			$MAPEOTotales{$_}[0]=~ tr/\.//d;
			$MAPEOImprimeUno{$_}=[$_."|".$MAPEOTotales{$_}[0]."|"];
		}

		$total_due=~ tr/\.//d;
		$num_invoice="000-000-0000000";
		if ($tipo_ejecucion eq "C" || $lbillcycle eq "34" ) {
			$num_invoice=$lohrefnum;
			$fecha_sec=`date '+%Y-%m-%d-%H-%M-%S'`;
			$fecha_sec=&enceraEspacios($fecha_sec);
			print FILE_SECUENCIAS $account_no."|".$num_invoice."|".$ciclo."|".$fecha_sec."\n";
		}
        if ($lcustcode eq $Nuevo_rubro{$lcustcode}[0]){
			$arreglo_3= $Nuevo_rubro{$lcustcode}[3];
            chop($arreglo_3);
			$MAPEOImprimeUno{"21675"}=["21675|(1) N/C: Crédito por  intereses en servicio de \"Factura Detallada\" ". $Nuevo_rubro{$lcustcode}[2] ." a ". $arreglo_3 . ", concepto reclasificado en Mayo 2009" ."|".$Nuevo_rubro{$lcustcode}[2]."|".$arreglo_3."|"];
			$MAPEOImprimeUno{"21676"}=["21675|(2) N/D: Débito por intereses en servicio \"Distribución de Estado de Cuenta\" ". $Nuevo_rubro{$lcustcode}[2] ." a ". $arreglo_3 . ", concepto reclasificado en Mayo 2009" . "|".$Nuevo_rubro{$lcustcode}[2]."|".$arreglo_3."|"];
		  } 
   	    $MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1.$address2.$address3."|"."|"."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."|"]; #2589 
		if ($ltipofactura eq "4") {		
			if ($ltipofactura eq "0"){$ltipofactura="";}
			if ($lmail eq "0"){$lmail="";}
			if ($ltelefono eq "0"){$ltelefono="";}
			if ($tipo_identificacion eq "0"){$tipo_identificacion="";}
			$MAPEOImprimeUno{"10100"}=["10100|".$ltipofactura."|".$lmail."|".$ltelefono."|".$tipo_identificacion."|"]; 
			$band_electronica="4";
			if ($lmail eq "") {	$band_correo="1"; }
			if ($ltelefono eq "") {	$band_correo="2"; }
			if ($tipo_identificacion eq "") {$band_correo="3"; }
		}
		$MAPEOImprimeUno{"21700"}=["21700| |"];
	}
}# fin tim dos

sub procesa_tim{
# inicio procesa_tim
$file=$file_ls;
$file1=$file_ls;
if ($cuenta_out_llam eq "1") {
   $file1=$ruta_dat."/OUT.llam";
   $file1=$file1;
}
print $file1." ".$cuenta_out_llam." ".$ruta_dat."\n";
#  Información del Tim II #
undef(%MAPEOImprime);
&tim_type_dos;

if ($band_correo eq "0" && $band_no_imprime_cuenta eq "0" && $ban_timm1_completo eq "1" && $ban_cedula_no_impresa eq "1" && $band_numero_no_existe eq "0" && $bandera_type_uno eq "0" && $num_secuencia ne "000-000-0000000" && $bandera_type_cero ne "1" && $band_descuadre ne "1" ) {
	if ($band_electronica eq "4")
	{ 
      open(FILE_WRITE_E,">> $file_out_e");
	  $fileGlob = \*FILE_WRITE_E;	 
	}
	else
	{
	  open(FILE_WRITE,">> $file_out"); 
      $fileGlob = \*FILE_WRITE;
	}
	&graba_datos($fileGlob);
	$post_final="servicios_postpago_".$fecha_corte."_".$procesador.".txt";
	$archivo_post_final=$carpeta_post."/".$post_final;
	open(ARCHIVO_POST, ">> $archivo_post_final");	#archivo final donde voy a poner los valores pospago
	$llena_post="cat $archivo_postpago >> $archivo_post_final";
	my $llenado=`$llena_post`; 	
} else { 
	if ($banderaUno eq "1") {
		if ($band_no_imprime_cuenta ne "0") {
		 print FILE_WRITE_DET "001|Plan No Encontrado|".$account_no."|".$file_ls."\n";
		}
		if ($ban_timm1_completo ne "1") {
		 print FILE_WRITE_DET "002|TIMM 1 No es Correcto|".$account_no."|".$file_ls."\n";
		}
		if ($ban_cedula_no_impresa ne "1") {
		 print FILE_WRITE_DET "003|Cédula No Impresa|".$account_no."|".$file_ls."\n";
		}
		if ($bandera_type_uno ne "0") {
		 print FILE_WRITE_DET "005|Descripcion en Blanco|".$account_no."|".$file_ls."\n";
		}
		if ($num_secuencia eq "000-000-0000000") {
		 print FILE_WRITE_DET "006|Numero de Secuencia|".$account_no."|".$file_ls."\n";
		}
		if ($bandera_type_cero eq "1") {
		 print FILE_WRITE_DET "007|Saldos de Factura no Cuadran |".$account_no."|".$file_ls."\n";
		}
		if ($band_correo eq "1") {
		 print FILE_WRITE_DET "008|Correo No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		if ($band_correo eq "2") {
		 print FILE_WRITE_DET "009|Telefono del Cliente No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		if ($band_correo eq "3") {
		 print FILE_WRITE_DET "010|Tipo de Identificación del Cliente No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		if ($band_descuadre eq "1") {
		 print FILE_WRITE_DET "011|Los valores de los servicios de Television o de Otros servicios no cuadran|".$account_no."|".$file_ls."\n";
		}
	}	# fin si banderaUno = 1
	my $fileGlob = \*FILE_WRITE_2;
	&graba_datos($fileGlob);
 }
    # inicio borro el archivo servicios_pospago porque hay error en la ejecucion
	$borra_archivo="rm -f $archivo_postpago";
	my $resul= `$borra_archivo`; 	
	# fin borro archivo
}# fin procesa_tim

sub llama_tim{
#inicio llama tim
$carpeta_ciclo="procesados/CICLO_".$ciclo;
$file_out=$ruta.$carpeta_ciclo."/output_doc1_dth_".$procesador."_".$ciclo.".txt";
$file_out_e=$ruta.$carpeta_ciclo."/output_doc1_dth_".$procesador."_".$ciclo."_e.txt"; 
$file_out_2=$ruta.$carpeta_ciclo."/output_doc1_dth_".$procesador."_".$ciclo.".out";
$file_out_3=$ruta.$carpeta_ciclo."/output_doc1_dth_".$procesador."_".$ciclo.".det";
$ruta_dat=$ruta."dat/CICLO_".$ciclo;
$ruta_ls=$ruta_dat."/20*_".$procesador.".dat";
$carpeta_post=$ruta."/servicios_postpago_dth/CICLO_".$ciclo;
$crea_carpeta="mkdir $carpeta_post";
if (! -d $carpeta_post) {
	my $resultado= `$crea_carpeta`; 	
}
#$archivo_postpago=$ruta.$carpeta_ciclo."/servicios_posptpago_".$procesador."_".$ciclo.".txt";

open(FILE_WRITE_2,"> $file_out_2");#archivo destino Cuentas no Procesado por Inconsistencia
open(FILE_WRITE_DET,"> $file_out_3");#Detalle de las Cuentas no Procesado por Inconsistencia
open(FILE_WRITE_LOGS,"> $archivo_logs");#archivo logs
$fecha=&enceraEspacios(`date '+%Y-%m-%d-%H-%M-%S'`);
$archivo_sec=$archivo_sec.$procesador."_".$ciclo."-".$tipo_ejecucion."-".$fecha.".dat";
open(FILE_SECUENCIAS, "> $archivo_sec");#Archivo Secuencias
#open(ARCHIVO_POSTPAGO, "> $archivo_postpago");	#archivo donde voy a poner los valores pospago

$fecha=`date '+%Y/%m/%d %H:%M:%S'`;
print $fecha."\n";
print  FILE_WRITE_LOGS $fecha."\n";
open LS, "ls -1 $ruta_ls|" or die "Error llama_tim No se encuentra archivos ls -1 $ruta_ls  $!";
while (<LS>) {
$file_ls=&enceraEspacios($_);
$file_ls=~ /(.*)\/([^\/]*)$/;
$path=$1;
$archivo=$2;
print $archivo;
@arch=split(/\_/,$archivo);
$cus_id=$arch[1];
$archivo_ctl=$ruta."log/timm_doc1.ctl";
$comando="cat $archivo_ctl";	
my $res_control= `$comando`;
$res_control=&enceraEspacios($res_control);

if ($res_control eq "1") {
	print "Proceso Finalizado Manualmente ".$procesador."\n";
	print FILE_WRITE_LOGS "Proceso Finalizado Manualmente ".$procesador."\n";
	exit;	
}

undef(%MAPEOGeneral);
undef(%MAPEOSuma);
undef(%MAPEOTotales);
undef(%MAPEOImprime);
undef(%MAPEOImprimeUno);
undef(%banderaUno);
undef(%Graba_co_id);
undef(%VerificaNumber);
undef(%Mdetalle);
#undef(%Mdetallefinal);
undef(%Mgrupodth);
undef(%Mgrupodth_lineas);#mapeo lineas 20200 - 20200

$des_plan="";
$plan1="";
$banderaUno="0";
$cont_calculo=5;
$ban_timm1_completo="0";
$ban_cedula_no_impresa="0";
$band_no_imprime_cuenta="0";
$band_numero_no_existe="0";
$bandera_type_uno="0";
$bandera_type_cero="0";
$band_electronica="0"; 
$band_correo="0";
$band_descuadre="0";
@arreglo=split(/\_/,$archivo);
print $file_ls;
$clave_det="";
$serv_det="";
$rango_tiempo="";
$valor_det="";
$valor_for="";
$fec_i_det="";
$fec_f_det="";
$cod_serv_det="";

$key_resumen_dth="";
$descripcion="";
$rango_tiempo="";
$valor_ser="";
$valor_ser="";
$fecha_i="";
$fecha_f="";
$codigo_servicio="";
$descripcion_ser="";
$dol="";
&procesa_tim($file_ls);

$comando="compress $file";	
my $result= `$comando`; 

$/="\n";

}	# fin while LS	
close FILE_WRITE; # .txt
close FILE_WRITE_E; # _E.txt 
close FILE_WRITE_2; # .out
close FILE_WRITE_DET; # .det
close FILE_SECUENCIAS; # Archivo de Secuencias
$fecha=`date '+%Y/%m/%d %H:%M:%S'`;
print $fecha."\n";
print FILE_WRITE_LOGS $fecha."\n";
close FILE_WRITE_LOGS;
close ARCHIVO_POTSPAGO;
close ARCHIVO_POST;
print "Fin del Proceso ".$procesador."\n";
}#fin llama_tim