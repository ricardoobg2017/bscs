#select rowid, g.* from sysadm.monitor_fin_mes g where g.estado = 'T'
#I Pendiente(Inicial)
#P Procesando
#C Cargado
#D Doc1
#T Terminado
#E Error


#--======================================================================================--
#-- Version: 1.0.0
#-- Descripcion: Ejecuta DOC1_INSERT_CUSTOMER
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 27/Abril/2009
#-- Proyecto: [4454] Hora de Mejora - Mejora a Informe de Financiero Fin Mes
#--=====================================================================================--
 
archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

ruta_local=$ruta_principal
usuario_base=$usuario_base
password_base=$password_base
sid_base=$sid_base
# VARIABLES DE FECHA
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_corte=$mes"/"$anio
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
ORACLE_SID=$sid_base
export ORACLE_SID


#Parametros#
cv_billcycle=$1
cv_commit=$2
cn_billseqno=$3

echo "Parametro $cv_billcycle $cv_commit $cn_billseqno"
if [ $# -eq 0 ]
then
	echo "\n\t\tEjecuta_doc1_cuentas.sh  # pv_billcycle pv_commit pn_billseqno\n"
	exit;
fi


NOMBRE_FILE_LOG=$ruta_local"/log/Ejecuta_doc1_cuentas_"$cv_billcycle".log"
NOMBRE_FILE_SQL=$ruta_local"/log/Ejecuta_doc1_cuentas_"$cv_billcycle".sql"

actualiza_monitor_control() {
estado=$1;
cat>$NOMBRE_FILE_SQL<<END
set heading off;
update monitor_fin_mes set doc1 = '$estado', fecha_inicio_doc1 = sysdate where billcycle = $cv_billcycle;
commit;
exit;
END
resultado6=`sqlplus -s $usuario_base/$password_base @$NOMBRE_FILE_SQL 2>/dev/null`
}

echo "\t I N I C I O   D E L   P R O C E S O Ejecuta_doc1_cuentas.sh ">$NOMBRE_FILE_LOG
fecha=`date`
echo $fecha  >>$NOMBRE_FILE_LOG
actualiza_monitor_control "C"
cat > $NOMBRE_FILE_SQL << eof_sql
SET SERVEROUTPUT ON
Declare
	lv_error varchar2(2000);
Begin
  DOC1_EJECUTA_FIN_MES(pv_billcycle => '$cv_billcycle',
                          pv_commit => '$cv_commit',
                          pn_billseqno => $cn_billseqno,
                          pv_error => lv_error);

end;
/
exit;
eof_sql

sqlplus -s $usuario_base/$password_base@$sid_base @$NOMBRE_FILE_SQL >> $NOMBRE_FILE_LOG

ESTADO=`grep "PL/SQL procedure successfully completed" $NOMBRE_FILE_LOG|wc -l`
ERROR=`grep "ORA-" $NOMBRE_FILE_LOG|wc -l`
lv_estado='D'
echo $ESTADO
if [ $ESTADO -lt 1 ] 
then
   echo "Verificar error presentado... $ERROR"  >> $NOMBRE_FILE_LOG
   actualiza_monitor_control "E"
   lv_estado="E"
else
   if [ $ERROR -ge 1 ]
   then
   echo "Verificar error presentado... $ERROR"  >> $NOMBRE_FILE_LOG	
   actualiza_monitor_control "E"
   lv_estado="E"
   fi
fi

fecha=`date`
echo $fecha  >>$NOMBRE_FILE_LOG
echo "\n \t F I N   D E L   P R O C E S O \n">>$NOMBRE_FILE_LOG

echo $NOMBRE_FILE_SQL
rm -f $NOMBRE_FILE_SQL

if [ "$lv_estado" = "D" ]
then
actualiza_monitor_control $lv_estado
nohup ./llama_proceso.sh 1 &
nohup ./llama_proceso.sh 2 &
nohup ./llama_proceso.sh 3 &
nohup ./llama_proceso.sh 4 &
nohup ./llama_proceso.sh 5 &
nohup ./llama_proceso.sh 6 &
nohup ./llama_proceso.sh 7 &
nohup ./llama_proceso.sh 8 &
fi
