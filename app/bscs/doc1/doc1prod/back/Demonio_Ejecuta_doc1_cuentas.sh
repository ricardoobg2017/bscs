#select rowid, g.* from sysadm.monitor_fin_mes g where g.estado = 'T'
#I Pendiente(Inicial)
#P Procesando
#C Cargado
#D Doc1
#T Terminado
#E Error


#--======================================================================================--
#-- Version: 1.0.0
#-- Descripcion: Demonio Ejecuta DOC1_INSERT_CUSTOMER
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal/Mario Aycart
#-- Fecha de creacion: 27/Abril/2009
#-- Proyecto: [4454] Hora de Mejora - Mejora a Informe de Financiero Fin Mes
#--=====================================================================================--
 
archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

ruta_local=$ruta_principal
usuario_base=$usuario_base
password_base=$password_base
sid_base=$sid_base
# VARIABLES DE FECHA
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_corte=$mes"/"$anio
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
ORACLE_SID=$sid_base
export ORACLE_SID
IP="18.14"
nombre_archivo_control=$ruta_logs"Demonio_Ejecuta.ctl"

NOMBRE_FILE_LOG=$ruta_local"/log/Demonio_Ejecuta_doc1_cuentas.log"
NOMBRE_FILE_SQL=$ruta_local"/log/Demonio_Ejecuta_doc1_cuentas.sql"

fecha=`date`
echo "\n \t I N I C I O   D E L   P R O C E S O $fecha \n">$NOMBRE_FILE_LOG

actualiza_ciclo(){
echo "Actualiza Ciclo " $periodo $ciclo $secuencia $numero_despachador $estado_ciclo $estado_ciclo $fecha $fecha_fin
cat >$NOMBRE_FILE_SQL<< eof
execute DOC1_EJECUTA_FIN_MES('$periodo','$ciclo','$secuencia',$numero_despachador,'$estado_ciclo','$estado_ciclo','$fecha','$fecha_fin');                                                                                
commit;
exit;
eof
dato=`sqlplus  $usuario_base/$password_base  @$NOMBRE_FILE_SQL`
sleep 1  
rm $NOMBRE_FILE_SQL                                                                                                                                                                                  
}


# ----------------- Control para que no este dos procesos a la vez --------------------------#
if [ -s $nombre_archivo_control ]
then
	echo "\n    ACTUALMENTE SE ENCUENTRA EN EJECUCION Demonio_Ejecuta_doc1_cuentas.sh"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1
# --------------------------------------------------------------------------------------------#

echo "0" > $nombre_archivo_control


pendiente=10
while [ "$pendiente" -gt 0 ] 
do

#------------- obtiene ciclo que segun orden debe cerrar # -----------------------#
# El BCH deja con estado P, se debe consultar si ya se encuentra listo ciclo para
# procesar DOC1.  Se queda en espera hasta que tenga un BCH a Procesar            #
#---------------------------------------------------------------------------------#
fecha=`date`
ciclo_act=0
echo "\n Busca Ciclo a Procesar $ciclo_act "$fecha >> $NOMBRE_FILE_LOG
echo "\n Busca Ciclo a Procesar $ciclo_act "$fecha 
while [ "$ciclo_act" -eq 0 ] 
do
cat>$NOMBRE_FILE_SQL<<END
set head off
set feedback off
select nvl(billcycle,0) billcycle from monitor_fin_mes where 
doc1 in ('P') and prioridad =
(select min(prioridad) from monitor_fin_mes where doc1 in ('P') ) ;
exit;
END

resultado=`sqlplus -s $usuario_base/$password_base @$NOMBRE_FILE_SQL 2>/dev/null`
ciclo_act=`echo $resultado | awk -F\| '{ print $1}'`
if [ "$ciclo_act" -eq "" ] 
then
ciclo_act=0
sleep 2700
else
#-----------------------------------------------------------------------------#
#                 Procesa los hilos                                          -#
#-----------------------------------------------------------------------------#
fecha=`date`
echo "\n Ciclo a Procesar "$ciclo_act" "$fecha >> $NOMBRE_FILE_LOG

pv_commit="CG"
pv_spec_ctrl_grp=" "
pn_billseqno=0

if [ "$ciclo_act" -gt 0 ]
then
echo "Ejecuta_doc1_cuentas.sh" $ciclo_act $pv_commit $pn_billseqno >> $NOMBRE_FILE_LOG
Ejecuta_doc1_cuentas.sh $ciclo_act $pv_commit $pn_billseqno
fi
fecha=`date`
echo "\n Fin de Envio de Hilos del Ciclo "$ciclo_act" "$fecha >> $NOMBRE_FILE_LOG
# --------------------------------------------------------------- #
# Debe Ciclar hasta que finalice el Ciclo que se envi� a Ejecutar #
# ----------------------------------------------------------------#              

pendiente_ejecutar=99
while [ "pendiente_ejecutar" -gt 0 ] 
do


actualiza_ciclo

cat>$NOMBRE_FILE_SQL<<END
set head off
set feedback off
select nvl(billcycle,0) billcycle from monitor_fin_mes where doc1 in ('C','D');
exit;
END
resultado=`sqlplus -s $usuario_base/$password_base @$NOMBRE_FILE_SQL 2>/dev/null`
echo "Pendiente "$resultado
pendiente_ejecutar=`echo $resultado | awk -F\| '{ print $1}'`
if [ "$pendiente_ejecutar" -eq "" ] 
then
pendiente_ejecutar=0
fi
done
fecha=`date`
echo "\n Finalizacion de los Hilos del Ciclo "$ciclo_act" "$fecha >> $NOMBRE_FILE_LOG
# ----------------------------------------------------------------#              
fi


# ----------------------------------------------------------------#              
# Ciclo Pendientes											      #
# ----------------------------------------------------------------#              
cat>$NOMBRE_FILE_SQL<<END
set head off
select count(billcycle) from monitor_fin_mes where doc1 in ('I');
exit;
END

resultado6=`sqlplus -s $usuario_base/$password_base @$NOMBRE_FILE_SQL 2>/dev/null`
pendiente=`echo $resultado6 | awk -F\| '{ print $1}'`
fecha=`date`
echo "\n Ciclo Pendientes "$pendiente" "$fecha > $NOMBRE_FILE_LOG
#---------------------------------------------------------------------------------#

done

done

fecha=`date`
echo $fecha  >>$NOMBRE_FILE_LOG
##echo "\n \t F I N   D E L   P R O C E S O \n">>$NOMBRE_FILE_LOG

rm -f $NOMBRE_FILE_SQL
rm -f $nombre_archivo_control



