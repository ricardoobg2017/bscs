SESIONES=5

##-------------------------------------------------------------------------------------------------------------------------
cd /bscs/bscsprod/scripts/INDICES
echo "Creando indice Ciclo"
sqlplus sysadm/prta12jul @crea_indice_campo_24.sql

echo "Creando indice Campo_2"
sqlplus sysadm/prta12jul @crea_indice_campo_2.sql

echo "Creando indice Codigo"
sqlplus sysadm/prta12jul @crea_indice_ind_qc1.sql

echo "Creando indice Cuenta"
sqlplus sysadm/prta12jul @crea_indice_ind_qc2.sql
echo "Fin Creacion de Indices"

##-------------------------------------------------------------------------------------------------------------------------
cat >InsertFacturas.sql<<FIN
truncate table qc_proc_facturas
/
insert into qc_proc_facturas
select distinct cuenta, CAMPO_11, CAMPO_13,CAMPO_18,CAMPO_22,CAMPO_24,CAMPO_23, 0,0 from facturas_cargadas_tmp where codigo = 10000;
analyze table FACTURAS_CARGADAS_TMP estimate statistics;
analyze table qc_proc_facturas estimate statistics;
/
quit
FIN

sqlplus sysadm/prta12jul @InsertFacturas.sql

##-------------------------------------------------------------------------------------------------------------------------
##SECIONA DATOS EN 5 SESIONES
cat >Secciona.sql<<FIN
exec det_secciona_fac(5)
quit
FIN

sqlplus sysadm/prta12jul @Secciona.sql

##-------------------------------------------------------------------------------------------------------------------------
##ANALISIS DE CUENTAS CON DIFERENTE PLAN
##SE ENVIA A EJECUTAR EN MODO NOHUP

Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >DetDifPlan.$Contador.sql<<FIN
exec det_cuen_dif_plan($Contador)
quit
FIN

cat >DetDifPlan.$Contador.sh<<FIN
sqlplus sysadm/tle @DetDifPlan.$Contador.sql
FIN

nohup sh DetDifPlan.$Contador.sh & 
done

##-------------------------------------------------------------------------------------------------------------------------
##ANALISIS DEL SERVICO FACTURA DETALLADA
cat >DetFacDet.sql<<FIN
exec det_fact_detallada
quit
FIN

sqlplus sysadm/prta12jul @DetFacDet.sql

##-------------------------------------------------------------------------------------------------------------------------
##ANALISIS DE FORMA DE PAGO
cat >DetFormPag.sql<<FIN
exec det_fecha_frompag($CICLO)
quit
FIN

sqlplus sysadm/prta12jul @DetFormPag.sql

##-------------------------------------------------------------------------------------------------------------------------
##ANALISIS DE CARGOS CREDITOS
cat >DetCargCre.sql<<FIN
exec det_cargos_creditos
quit
FIN

sqlplus sysadm/prta12jul @DetCargCre.sql

##-------------------------------------------------------------------------------------------------------------------------
##ANALISIS DE DESCUADRE PRIMERA HOJA
cat >DetDesPriHoja.sql<<FIN
exec det_descua_prime_hoja
quit
FIN

sqlplus sysadm/prta12jul @DetDesPriHoja.sql

##-------------------------------------------------------------------------------------------------------------------------
##ANALISIS DE CEDULA RUC SE EJECUTA POR SESIONES
Contador=0
while [ $Contador -lt $SESIONES ]
do
Contador=`expr $Contador + 1`
cat >DetValCedRuc.$Contador.sql<<FIN
exec det_valida_ced_ruc($Contador)
quit
FIN

cat >DetValCedRuc.$Contador.sh<<FIN
sqlplus sysadm/tle @DetValCedRuc.$Contador.sql
FIN

nohup sh DetValCedRuc.$Contador.sh & 
done

##-------------------------------------------------------------------------------------------------------------------------