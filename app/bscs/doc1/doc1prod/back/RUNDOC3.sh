CICLO=$1
HILO=$2
MODO=$3

cd /doc1/doc1prod/DLL

#=========================
# Variable de entrada
#=========================

IMPRESORA=`cat impresora.txt`
PERIODO=`cat periodo.txt`
BIN="/doc1/doc1prod/bin/"
DOC1="/doc1/doc1prod/"
PROCESADOS="/doc1/doc1prod/procesados/CICLO_"
ASCII="/doc1/doc1prod/ASCII/"
ARCH="output_doc1_"$HILO"_"$CICLO".txt"
ARCHIVO="Conecel"
LOG_DATE=`date +"%d%m%Y"`
DATE=`date +"%d/%m/%Y %H:%M:%C"`


##================
##ARCHIVO PROCESO
##================
ADD=_$CICLO"_"$HILO
ARC_GEN=Generate_$CICLO"_"$HILO.ops
ARC_CON=Consolidar_$CICLO"_"$HILO.ini
ARC_PCE=Pce_$CICLO"_"$HILO.ini

##============
##ARCHIVO LOG
##============
ARC_GENLOG=Generate_$CICLO"_"$HILO.log
ARC_CONLOG=Consolidar_$CICLO"_"$HILO.log
ARC_PCELOG=Pce_$CICLO"_"$HILO.log
ARCH="output_doc1_"$HILO"_"$CICLO".txt"

##========================
##VALIDACION ARCHIVOS TXT
##========================


##========================
##VALIDACION ARCHIVOS TXT
##========================
genarch ()
{

cd $PROCESADOS$CICLO
pwd
CONT=0
##CREO LA CARPETA DONDE DOC1GEN DEPOSITARA LA SALIDA DE SPOLL DE IMPRESION
if [ -d $SALIDA ]
	then
		echo ""
else
		echo "Creando carpeta SALIDA"
		mkdir $SALIDA
		chmod 777 $SALIDA
		echo ""
fi
##CREO LA CARPETA DONDE ESTARAN LOS ARCHIVOS OPS, INI
if [ -d $ARCHI ]
	then
		echo ""
else
		echo "Creando carpeta DOC1"
		mkdir $ARCHI
		chmod 777 $ARCHI
fi



}


#=========================
# Desarrollo cabecera programa
#=========================
cabecera () 
{
		clear
		echo "\n\n\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $fecha"
		echo "\t\t                              Usuario: $LOGNAME"
		echo "\n\t   ==============================================="
		echo "\t               EJECUCION DE CICLO:"$CICLO
		echo "\t   ==============================================="
}

##===========================
##PARTE PRINCIPAL DEL PROGRAMA
##===========================
principal ()
{

cd /doc1/doc1prod/DLL
cabecera;

if [$MODO = ""] 
	then
	generate;
	consolidar;
	pce;
else
	echo "opcion G"
	if [ $MODO = "G" ]
	then
	generate;
	consolidar;
	else
		if [ $MODO = "P" ]
			then
			pce;
		fi
	fi
fi

}

##===========================
##GENERANDO ARCHIVO GENERATE
##===========================
generate ()
{

cd $PROCESADOS$CICLO

uncompress $ARCH".Z"

echo "===================================================="
echo "GENERACION ARCHIVO GENERATE y ASCII"
echo "===================================================="
DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE
cd $BIN

##VALIDACION DE EJECUCION GENERATE
	doc1gen /doc1/doc1prod/DLL/Conecel_PS.hip ops=$PROCESADOS$CICLO"/DOC1/"$ARC_GEN > $PROCESADOS$CICLO"/DOC1/"$ARC_GENLOG 

DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE

cd $PROCESADOS$CICLO
VALIDA_GENERATE=`grep "GEN0033A Production Job failed" $PROCESADOS$CICLO"/DOC1/"$ARC_GENLOG | wc -l`
	if [ $VALIDA_GENERATE -eq 1 ]
		then
		echo "===================================================="
		echo "Error en procesar el archivo: "$ARCH 
		echo "Presione una tecla para ver archivo de log"
		echo "===================================================="
		read
		cd  $PROCESADOS$CICLO"/DOC1/"
		cat $ARC_GENLOG
		exit
	else
	compress $ARCH
	fi

echo "===================================================="
echo "FIN GENERACION ARCHIVO GENERATE y ASCII"
echo "===================================================="
DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE

}

##===============================
##GENERANDO ARCHIVO CONSOLIDADO
##===============================
consolidar ()
{

cd $PROCESADOS$CICLO

##------------------------------------------------------
##GENERANDO ARCHIVO CONSOLIDADO Y ORDENADO
##------------------------------------------------------
echo "===================================================="
echo "GENERANDO ARCHIVO CONSOLIDADO Y ORDENADO "$ARC_CON
echo "===================================================="
DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE

cd $BIN

doc1pce ini=$PROCESADOS$CICLO"/DOC1/"$ARC_CON > $PROCESADOS$CICLO"/DOC1/"$ARC_CONLOG

DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE

cd $PROCESADOS$CICLO
##VALIDACION DE EJECUCION GENERATE
VALIDA_CONSOLIDAR=`grep "PCE completed with errors" $PROCESADOS$CICLO"/DOC1/"$ARC_CONLOG | wc -l`
	if [ $VALIDA_CONSOLIDAR -eq 1 ]
		then
		echo "===================================================="
		echo "ERROR EN PROCESAR ARCHIVO: "$ARC_CON 
		echo "Presione una tecla para ver archivo de log "
		echo "===================================================="
		read
		cd  $PROCESADOS$CICLO"/DOC1"
		cat $ARC_CONLOG
		exit
	fi
sort -t"|" -k4 $PROCESADOS$CICLO"/SALIDA/Conecel_Unif"$ADD".jrn" > $PROCESADOS$CICLO"/SALIDA/Conecel_Unif"$ADD".jrn2"
rm $PROCESADOS$CICLO"/SALIDA/Conecel_Unif"$ADD".jrn"
mv $PROCESADOS$CICLO"/SALIDA/Conecel_Unif"$ADD".jrn2" $PROCESADOS$CICLO"/SALIDA/Conecel_Unif"$ADD".jrn"

echo "===================================================="
echo "FIN GENERANDO ARCHIVO CONSOLIDADO Y ORDENADO "
echo "===================================================="

DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE

}

##===============================
##GENERANDO ARCHIVO POSCRITS
##===============================
pce ()
{

cd $PROCESADOS$CICLO

##------------------------------------------------------
##GENERANDO ARCHIVOS POSTCRIPT
##------------------------------------------------------
echo "===================================================="
echo "GENERANDO ARCHIVOS POSTCRIPT "$ARC_PCE
echo "===================================================="
DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE

cd $BIN

doc1pce ini=$PROCESADOS$CICLO"/DOC1/"$ARC_PCE > $PROCESADOS$CICLO"/DOC1/"$ARC_PCELOG

DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE

cd $PROCESADOS$CICLO
VALIDA_PCE=`grep "PCE completed with errors" $PROCESADOS$CICLO"/DOC1/"$ARC_PCELOG | wc -l`
	if [ $VALIDA_PCE -eq 1 ]
		then
		echo "===================================================="
		echo "ERROR EN PROCESAR ARCHIVO: "$ARC_PCE 
		echo "Presione una tecla para ver archivo de log "
		echo "===================================================="
		read
		cd  $PROCESADOS$CICLO"/DOC1"
		cat $ARC_PCELOG
		exit
	fi

DATE=`date +"%d/%m/%Y %H:%M:%C"`
echo $DATE

}

#================================
#Ejecución de Procesos.
#================================
fecha=`date +%Y-%m-%d`
principal