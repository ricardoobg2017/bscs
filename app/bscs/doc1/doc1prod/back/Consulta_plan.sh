#--======================================================================================--
#-- Descripcion: Realiza consulta a la tablas de BSCS para sacar el plan actual 
#--=====================================================================================--
#-- Desarrollado por:  Miguel Garc�a
#-- Fecha de creacion: 20/Ene/2009
#-- Proyecto: [4044] AAI-VFS-343-08 - nombre de plan incorrecto
#--=====================================================================================--


archivo_configuracion="configuracion.conf"

. $archivo_configuracion
ruta=$ruta_resources
cd $ruta
usuario_base=$usuario_base
password_base=$password_base
sid_base=$sid_base
#nombre_archivo="consulta.sql";
export ORACLE_SID=$sid_base

cat>consulta.sql<<END
set pagesize 0
set colsep '|'
set linesize 2000
set termout off
set head off
set trimspool on
set feedback off
spool plan.txt
select /*+ruler+*/ c.des
from rateplan c , rateplan_hist d
where d.tmcode = c.tmcode
and d.co_id = $1;
spool off
exit;
END
#echo $ORACLE_SID
sqlplus -s $usuario_base/$password_base@$ORACLE_SID @consulta.sql 
#rm  -f consulta.sql
#rm -f plan.txt
cat plan.txt

