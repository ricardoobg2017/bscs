archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

sid_base=$sid_base
export ORACLE_SID=$sid_base

extraccion_timm(){
echo "0" > $ruta_logs"/extr_timm_doc1.ctl"
echo "Desea Continuar s/S [  ]\b\b\b\c"
read salir
if [ "$salir" = "s" ] || [ "$salir" = "S" ]
then
nohup ./llama_proceso.sh 1 &
nohup ./llama_proceso.sh 2 &
nohup ./llama_proceso.sh 3 &
nohup ./llama_proceso.sh 4 &
nohup ./llama_proceso.sh 5 &
nohup ./llama_proceso.sh 6 &
nohup ./llama_proceso.sh 7 &
nohup ./llama_proceso.sh 8 &
fi

}


depura_ambiente() {
	echo "Desea Continuar S/s [ ]\b\b\c"
    read continuar
	if [ "$continuar" = "s" ] || [ "$continuar" = "S" ]
	then
    	depura_ambiente.sh
	fi	
}

load_archivo(){
archivo_load="Final_Secuencias_Generadas.txt"
rm $archivo_load
cd $ruta_pce
cat *.txt > $archivo_load
echo "LOAD DATA
	INFILE '$archivo_load'
	BADFILE 'doc1_inter.bad'
	DISCARDFILE '$doc1_inter.dis'
	truncate
	INTO TABLE doc1_numero_fiscal
	FIELDS TERMINATED BY '|'
	TRAILING NULLCOLS
	(ruta,
	 ohrefnum,
	 orden,
	 custcode
	 ) " > $$doc1.ctl
	sqlldr $usuario_base/$password_base control=$$doc1.ctl log=$$doc1.log errors=6000 >/dev/null 2>&1
	codigo_error=$?
	export codigo_error
	echo "Termino carga Codigo $codigo_error "
	sleep 1
	rm $$doc1.ctl
    mv *.txt procesado
	mv *.log log
}

actualiza_bscs_2(){
cat > actualiza_bscs.sql << eof
execute doc1_update_bscs.doc1_update_orderhdr_all('$lfecha');
exit;
eof

sqlplus $usuario_base/$password_base  @actualiza_bscs.sql

echo "Proceso ejecutado exitosamente"
sleep 1
rm actualiza_bscs.sql

}

actualiza_bscs(){
while [ 1 -eq 1 ]
do
	echo "\n\t\t Periodo Facturacion [DD/MM/YYYY]\b\b\b\b\b\b\b\b\b\b\b\c"
	read lfecha
	if [ "$lfecha" = "" ]
	then
		echo "Ingrese la fecha Correcta"
	else
		echo "Fecha Ingresada "$lfecha
		sleep 1
		export lfecha
		echo "Desea Continuar S/s [ ]\b\b\c"
		read continuar
		if [ "$continuar" = "s" ] || [ "$continuar" = "S" ]
		then
			actualiza_bscs_2
			return;
		else
			return;
		fi	
	fi 
done
}

actualiza_numero_fiscal(){
cat > actualiza_numero_fiscal.sql << eof
execute doc1_update_bscs.doc1_update_aut_seq;
exit;
eof

sqlplus $usuario_base/$password_base  @actualiza_numero_fiscal.sql

echo "Proceso ejecutado exitosamente"
sleep 1
rm actualiza_numero_fiscal.sql
}

menu_actualiza_bscs(){
	while [ 1 -eq 1 ]
	do
		clear
		echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $fecha"
		echo "\t\t\t                              Usuario: $LOGNAME"
		echo "\n\t\t   =========================================="
		echo "\t\t                 MENU DE DOC1"
		echo "\t\t   ==========================================="
		echo "\n\t\t\t[1].- Load archivo Secuencias"
		echo "\n\t\t\t[2].- Actualizar orderhdr_all"
		echo "\n\t\t\t[3].- Actualizar aut_seq"
		echo "\n\t\t\t[S].- Salir"
		echo "\n\t\t\t\t Opcion [ ]\b\b\c"
		read opc_m
		if [ $opc_m = "s" ] || [ $opc_m = "S" ]
		then
			return;
		fi
		case $opc_m in
			1)  load_archivo;;
			2)  actualiza_bscs;;
			3)  actualiza_numero_fiscal;;
			*) echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_m ;;
		esac
	done
}

princ_manual() {
	while [ 1 -eq 1 ]
	do
		clear
		echo "\n\n\t\t\t  C.  O.  N.  E.  C.  E.  L.  Fecha  : $fecha"
		echo "\t\t\t                              Usuario: $LOGNAME"
		echo "\n\t\t   =========================================="
		echo "\t\t                 MENU DE DOC1"
		echo "\t\t   ==========================================="
		echo "\n\t\t\t[1].- Obtiene Información"
		echo "\n\t\t\t[2].- Extracción TIMM/DOC1"
		echo "\n\t\t\t[3].- Procesa Puntos + Bulk"
#		echo "\n\t\t\t[4].- Procesa Información"
#		echo "\n\t\t\t[5].- Menu Actualizacion"
		echo "\n\t\t\t[0].- Depura Ambiente"
		echo "\n\t\t\t[S].- Salir"
		echo "\n\t\t\t\t Opcion [ ]\b\b\c"
		read opc_m
		if [ "$opc_m" = "s" ] || [ "$opc_m" = "S" ]
		then
			break;
		fi
		case $opc_m in
			1)  obtiene_informacion.sh;;
			2)  extraccion_timm;;
			3)  adicionales/une_facturas.pl;;
#			4)  ejecuta_timm;;
#			5)  menu_actualiza_bscs;;
			0)  depura_ambiente;;
			*) echo " \n\n\t\t\tOpcion incorrecta!!" ; read opc_m ;;
		esac
	done
}

fecha=`date +%Y-%m-%d`
princ_manual
