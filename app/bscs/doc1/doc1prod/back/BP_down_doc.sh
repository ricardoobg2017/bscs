#--======================================================================================--
#-- Version: 2.0.0
#-- Descripcion: Realiza spool a archivo de datos de la tabla document_all
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 03/Abril/2006
#-- Proyecto: Group 1
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 13/Nov/2006
#-- Motivo: Incluir la Etiqueta de descripción de Cada Cuenta
#--         Tomar la tabla document_all desde el archivo de configuracion.conf
#-- Proyecto: Group 1
#--=====================================================================================--
#--=====================================================================================--
#-- Desarrollado por:  CLS Rolando Herrera B.
#-- Fecha de modificación: 26/Sep/2008
#-- Motivo: Incluir campo de direccion (ccadr1,ccadr2)en cada Cuenta
#--         Tomada de la tabla ccontact_all 
#-- Proyecto: [3512] Automatizacion de Facturas y Rezagos
#--=====================================================================================--
archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

ruta_local=$ruta_principal
usuario_base=$usuario_base
password_base=$password_base
sid_base=$sid_base
# VARIABLES DE FECHA
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_corte=$mes"/"$anio
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
ORACLE_SID=$sid_base
export ORACLE_SID

numero_despachador=$1
billcycle=$2
tabla=$3

ruta_log=$ruta_local"/log/despachador"$numero_despachador"/"
ruta_dat=$ruta_local"/dat/CICLO_"$billcycle"/"

if [ $# -eq 0 ]
then
	echo "\n\t\tBP_down_docV2.sh  # Indique el Numero Despachador y la Tabal  \n"
	exit;
fi


nombre_archivo1=$ruta_log"DOC1_script_"$billcycle"_"$numero_despachador".sql"
nombre_archivo2=$ruta_log"DOC1_script_"$billcycle"_"$numero_despachador
nombre_archivo_log=$ruta_log"DOC1_down_"$billcycle"_"$numero_despachador".log" 
archivo_cuentas="DOC1_cuentas_"$billcycle"_"$numero_despachador".dat"
nombre_cuentas=$ruta_log$archivo_cuentas
archivo_control="DOC1_control_"$billcycle"_"$numero_despachador".ctl"
archivo_control_down="/doc1/doc1prod/log/extr_timm_doc1.ctl"
nombre_archivo_control=$ruta_log$archivo_control
contador=0


if [ -s $nombre_archivo_control ]
then
	echo "\n    ACTUALMENTE UNOS DE LOS PROCESOS DE DOC1 SE ENCUENTRA EN EJECUCION"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1

echo "0" > $nombre_archivo_control


if [ $tabla = "C" ]
then
  tabla_document_all=$table_document_all
else
  tabla_document_all=$table_document_all_cg
fi

rm -f $nombre_archivo

echo "\t I N I C I O   D E L   P R O C E S O " >$nombre_archivo_log
fecha=`date`
echo $fecha  >>$nombre_archivo_log
echo "Despachador: $numero_despachador  ">>$nombre_archivo_log
echo "BillCycle: $billcycle  ">>$nombre_archivo_log
echo "Tabla: $tabla  ">>$nombre_archivo_log
echo "Logs: $nombre_archivo_log ">>$nombre_archivo_log
echo "Scripts: $nombre_archivo ">>$nombre_archivo_log
echo "Cuentas a Procesar: $nombre_cuentas ">>$nombre_archivo_log
echo "Control Down: $nombre_archivo_control ">>$nombre_archivo_log
# CLS RHE proyecto [3512] se agrega campo A.ADDRESS en select -- linea 109
cat>$nombre_archivo1<<eof
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 1000
spool $nombre_cuentas
select a.prgcode||' '||a.costcenter_id||' '||a.seq_id||' '||a.custcode||' '||a.ohrefnum||' '||a.customer_id ||' '||a.document_id ||' '||nvl(b.customer_id,'0') ||' '||nvl(b.document_id,'0') ||' '||replace(a.address,' ','"') from doc1_cuentas a, doc1_cuentas b where b.customer_id_high(+) = a.customer_id and a.customer_id_high is null and a.billcycle = $billcycle and a.estado ='A' and b.estado(+) ='A' and a.procesador = $numero_despachador and b.billcycle(+) =$billcycle;
spool off
/
exit
eof

sqlplus -s $usuario_base/$password_base @$nombre_archivo1
rm -f $nombre_archivo1

cd $ruta_log

nombre_archivo=$nombre_archivo2".sql"

#echo "alter session set cursor_sharing=EXACT;" >> $nombre_archivo #AAP
echo "set long 9000000" >> $nombre_archivo
echo "set pagesize 0" >> $nombre_archivo
echo "set termout off" >> $nombre_archivo
echo "set head off" >> $nombre_archivo
echo "set trimspool on" >> $nombre_archivo
echo "set feedback off" >> $nombre_archivo
echo "set serveroutput off" >> $nombre_archivo
echo "set linesize 1000">> $nombre_archivo
echo "column DOCUMENT format a1000 " >> $nombre_archivo

contador=0

while read i
do
set -A cus_doc $i

l_prgcode=${cus_doc[0]}
l_costcenter_id=${cus_doc[1]}
l_seq_id=${cus_doc[2]}
l_custcode=${cus_doc[3]}
l_ohrefnum=${cus_doc[4]}
l_customer_id_1=${cus_doc[5]}
l_document_id_1=${cus_doc[6]}
l_customer_id_2=${cus_doc[7]}
l_document_id_2=${cus_doc[8]}
l_address=${cus_doc[9]} # CLS RHE proyecto [3512] nueva variable
l_address=`echo $l_address | sed 's/"/ /g'`
cd $ruta_log
if [ ! -s $archivo_control_down  ]
then
  echo "Fin de Proceso manualmente BP_down_doc.sh $archivo_control_down"
  rm -f $nombre_archivo_control
  return
fi
nombre_datos=$anio$mes$dia"_"$l_customer_id_1"_"$billcycle"_"$numero_despachador".dat"
# CLS RHE proyecto [3512] se agrega campo $l_address en linea 154 CUSTODE
echo "spool $nombre_datos  " >> $nombre_archivo
#echo "select 'CUSTCODE|'||$l_custcode||'|'||$l_prgcode||'|'||$l_costcenter_id||'|'||$l_seq_id||'|'||lpad($billcycle,2,0)||'|'||'''$l_ohrefnum'''||'|'||'$l_address' from dual;" >> $nombre_archivo
echo "select 'CUSTCODE|'||'$l_custcode'||'|'||$l_prgcode||'|'||$l_costcenter_id||'|'||$l_seq_id||'|'||lpad($billcycle,2,0)||'|'||'''$l_ohrefnum'''||'|'||'$l_address' from dual;" >> $nombre_archivo
if [ "$l_customer_id_2" -eq "0" ]
then
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_1 and type_id = 6;" >> $nombre_archivo
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_1 and type_id = 2;" >> $nombre_archivo
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_1 and type_id = 1;" >> $nombre_archivo
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_1 and type_id = 0;" >> $nombre_archivo
echo "spool off" >> $nombre_archivo
echo "update doc1_cuentas set estado = 'P', fecha =sysdate where customer_id = $l_customer_id_1 and document_id = $l_document_id_1 ;" >> $nombre_archivo
else
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_2 and type_id = 6;" >> $nombre_archivo
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_1 and type_id = 2;" >> $nombre_archivo
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_2 and type_id = 2;" >> $nombre_archivo
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_1 and type_id = 1;" >> $nombre_archivo
echo "select x.document from $tabla_document_all x where document_id = $l_document_id_1 and type_id = 0;" >> $nombre_archivo
echo "spool off" >> $nombre_archivo
echo "update doc1_cuentas set estado = 'P', fecha =sysdate where customer_id = $l_customer_id_1 and document_id = $l_document_id_1;" >> $nombre_archivo
echo "update doc1_cuentas set estado = 'P', fecha =sysdate where customer_id = $l_customer_id_2 and document_id = $l_document_id_2;" >> $nombre_archivo
fi


contador=`expr $contador + 1`
if [ $contador -eq 1000 ]
then
  echo "/" >> $nombre_archivo
  echo "exit" >> $nombre_archivo
  nombre_archivo=$nombre_archivo2"_${l_customer_id_1}.sql"
  echo "set long 9000000" >> $nombre_archivo
  echo "set pagesize 0" >> $nombre_archivo
  echo "set termout off" >> $nombre_archivo
  echo "set head off" >> $nombre_archivo
  echo "set trimspool on" >> $nombre_archivo
  echo "set feedback off" >> $nombre_archivo
  echo "set serveroutput off" >> $nombre_archivo
  echo "set linesize 1000">> $nombre_archivo
  echo "column DOCUMENT format a1000 " >> $nombre_archivo
  contador=0
fi


done <  $archivo_cuentas 

echo "/" >> $nombre_archivo
echo "exit" >> $nombre_archivo
cd $ruta_log
if [ ! -s $ruta_dat ]
then
 mkdir $ruta_dat
fi
for i in `ls DOC1*.sql`
do
if [ ! -s $archivo_control_down  ]
then
  echo "Fin de Proceso manualmente BP_down_doc.sh $archivo_control_down"
  rm DOC1*.sql
  mv 2*.dat $ruta_dat
  rm -f $nombre_archivo_control
  return
fi
sqlplus -s $usuario_base/$password_base @$i
rm -f $i
done
if [ ! -s $ruta_dat ]
then
 mkdir $ruta_dat
fi
mv 2*.dat $ruta_dat

#-------------------------------------------------------------------
fecha=`date`
echo $fecha  >>$nombre_archivo_log
echo "\n \t F I N   D E L   P R O C E S O \n">>$nombre_archivo_log
rm -f $nombre_archivo_control
