#--======================================================================================--
#-- Version: 1.0.1
#-- Descripcion: Realiza spool de tablas Maestras
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 17/Feb/2005
#-- Proyecto: DOC1 - Group 1
#--=====================================================================================--
#-- Modificado por  : Paola Carvajal
#-- Modificación    : 05/09/2007
#-- Proyecto		: [2766] - Ajuste para cambio en proceso Perl
#-- Motivo          : Agregar el Spool de la tabla info_contr_text
#--                 : Donde co_id= # de Contrato co_id, Text01= # de la Cédula o RUC, Text02= Nombre, Text03= #Ciclo
#--=====================================================================================--
#-- Modificado por  : Jorge Heredia
#-- Modificación    : 15/05/2008
#-- Proyecto	    : [3408] - Facturacion IVA 0%
#--=====================================================================================--

#VARIABLES
#-------------------------------------------------------------------
archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
ruta=$ruta_resources
cd $ruta
usuario_base=$usuario_base
password_base=$password_base
sid_base=$sid_base
nombre_archivo="spoolBscs.sql";
export ORACLE_SID=$sid_base
date
cat>$nombre_archivo<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool mpusntab.dat
select sncode ||'|'|| des ||'|'|| shdes ||'|'|| snind ||'|'|| rec_version ||'|'||billed_surcharge_ind ||'|'|| pde_implicit_ind from sysadm.mpusntab;
spool off
spool mpuzntab.dat
select zncode||'|'||des ||'|'|| shdes from sysadm.mpuzntab;
spool off
spool mpuzotab.dat
select zocode||'|'||des||'|'||cgi||'|'||sccode||'|'||rec_version||'|'||origin_npcode from sysadm.mpuzotab;
spool off
spool paymenttype_all.dat
select pay.payment_id ||'|'||pay.paymentcode||'|'||pay.paymentname datos from sysadm.paymenttype_all pay;
spool off
spool rateplan.dat
select tmcode||'|'|| des||'|'|| shdes from sysadm.rateplan;
spool off
spool bgh_tariff_plan_doc1.dat
select tmcode||'|'||shdes||'|'||firstname||'|'||secondname||'|'||porder ||'|'||plan_type||'|'||tax_code_iva ||'|'||tax_code_ice from bgh_tariff_plan_doc1 order by tmcode,shdes, plan_type, porder;
spool off
spool tax_category_doc1.dat
select i.taxcat_id||'|'||i.taxcat_name||'|'|| i.taxrate from tax_category_doc1 i;
spool off
spool call_grouping_doc1.dat
select label_id||'|'||nvl(servshdes,1)||'|'||nvl(tzcode,1)||'|'||nvl(ttcode,1)||'|'||leg_id||'|'|| calltype||'|'||tipo from call_grouping_doc1;
spool off
spool service_label_doc1.dat
select label_id||'|'||des||'|'||taxinf||'|'||shdes from service_label_doc1;
spool off
spool bgh_tariff_plan_doc1_EBC.dat
select tmcode || '|' || shdes || '|' || firstname || '|' || secondname || '|' || porder || '|' || plan_type || '|' || tax_code_iva || '|' ||tax_code_ice from bgh_tariff_plan_doc1  where tmcode in(612,614,35) order by tmcode, shdes, plan_type, porder;
spool off
spool mpulktmb_EBC.dat
select tmcode || '|' || to_char(vsdate, 'yyyymmdd') || '|' || spcode || '|' || sncode || '|' || accserv_catcode from sysadm.MPULKTMb a where vsdate in (select max(b.vsdate) from sysadm.MPULKTMb b where b.tmcode = a.tmcode and b.sncode = a.sncode and b.tmcode in (612,614,35)) and a.tmcode in (612,614,35);
spool off
spool info_contr_text.dat
select co_id||'|'||text01||'|'||text02||'|'||text03 from info_contr_text where text01 is not null union select '9999|9999|9999|9999' from dual;
spool off
spool excento_iva.dat
select y.custcode from customer_all y, customer_tax_exemption x where y.customer_id = x.customer_id and x.exempt_status = 'A';
spool off
exit
eof

echo "Obtiene información " $ORACLE_SID
sqlplus -s $usuario_base/$password_base @$nombre_archivo
errorcode=$?
echo "Codigo "$errorcode
rm -f $nombre_archivo
date

