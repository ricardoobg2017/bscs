echo "CUANDO TABLA = 'S' SE TRUNCARA LA INFORMACION"
echo "CUANDO NO SE PONGA CICLO A PROCESAR GENERARA "
echo " PARA TODOS LOS CICLOS ENCONTRADOS EN LA CARPETA"

RUTA=$1
CICLO=$2
#!/usr/bin/sh
cd /bscs/bscsprod
. ./.setENV

#SOLO SE PROCESA UN SOLO CICLO
cd $RUTA
uncompress *.txt.Z

##-------------------------------------------------------------------------------------------------------------------------
	#SE ELIMINA LOS ARCHIVOS ANTERIORES
cd /bscs/bscsprod/work/BGH/PRINT/SUBIR_TXT
rm LOADTXT.log
LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Inicio Carga"$LOG_DATE > LOADTXT$CICLO.log
rm *$CICLO.txt.sin_det

	echo "Procesando Carpeta: "$RUTA
	LOG_DATE=`date +"%Y%m%d%H%M%S"`
	echo "Inicio:-Separa Detalle"$LOG_DATE
	echo "Inicio:-Separa Detalle"$LOG_DATE >> LOADTXT$CICLO.log
##-------------------------------------------------------------------------------------------------------------------------
##-------------------------------------------------------------------------------------------------------------------------
	#SE PROCESA LOS ARCHIVOS DE LA CARPETA
cd $RUTA

carga_txt.pl

compress *.txt

##-------------------------------------------------------------------------------------------------------------------------
##-------------------------------------------------------------------------------------------------------------------------
	#SE CONCATENA LOS NUEVOS ARCHIVOS GENERADOS ".txt.sin_det"
	cd /bscs/bscsprod/work/BGH/PRINT/SUBIR_TXT
	#CREO EL ARCHIVO VACIO PARA LUEGO ADICIONAR LA INFORMACION
	CONCATENA="cat"
			echo 'Inicio concatena'
			for ind in `ls -1 *$CICLO.txt.sin_det`
			do
				CONCATENA=$CONCATENA" "$ind
			done

		echo $CONCATENA" > Datos"$CICLO".txt" > Concatena$CICLO.sh
		# SE EJECUTA EL SHELL QUE REALIZA LA CONCATENACION
		sh Concatena$CICLO.sh
##-------------------------------------------------------------------------------------------------------------------------
Arch='Datos'$CICLO'.txt'
mv Datos$CICLO.txt /bscs/bscsprod/work/BGH/PRINT/SUBIR_TXT/SUBIR
rm Concatena$CICLO.sh

		# SE EJECUTA EL SHELL QUE REALIZA LA CARGA A LA TABLA DE TMP
########		sh /bscs/bscsprod/work/BGH/PRINT/PROCESO_QC/CargaDatos.sh 

##-------------------------------------------------------------------------------------------------------------------------
#cat >InsertFacturas$CICLO.sql<<FIN
#insert into qc_proc_facturas
#select distinct cuenta, CAMPO_11, CAMPO_13,CAMPO_18,CAMPO_22,CAMPO_24,CAMPO_23, 0,0 from facturas_cargadas_tmp_resp where codigo = 10000 and CAMPO_24 = $CICLO;
#analyze table FACTURAS_CARGADAS_TMP estimate statistics;
#analyze table qc_proc_facturas estimate statistics;
#/
#quit
#FIN

#sqlplus sysadm/prta12jul @InsertFacturas$CICLO.sql

LOG_DATE=`date +"%Y%m%d%H%M%S"`
echo "Fin Total:"$LOG_DATE
echo "Fin Total:"$LOG_DATE >> LOADTXT$CICLO.log

cd /bscs/bscsprod/work/BGH/PRINT/SUBIR_TXT/SUBIR
echo "Iniciando Carga ..."
cat > CargaDatos$CICLO.ctl << eof_ctl
load data
infile $Arch
badfile CargaDatos$CICLO.bad
discardfile CargaDatos$CICLO.dis
append
into table FACTURAS_CICLO_$CICLO
fields terminated by '|'
TRAILING NULLCOLS
(
CUENTA,
TELEFONO,
CODIGO,
CAMPO_2,
CAMPO_3,
CAMPO_4,
CAMPO_5,
CAMPO_6,
CAMPO_7,
CAMPO_8,
CAMPO_9,
CAMPO_10,
CAMPO_11,
CAMPO_12,
CAMPO_13,
CAMPO_14,
CAMPO_15,
CAMPO_16,
CAMPO_17,
CAMPO_18,
CAMPO_19,
CAMPO_20,
CAMPO_21,
CAMPO_22,
CAMPO_23,
CAMPO_24
)
eof_ctl

sqlldr sysadm/prta12jul control=CargaDatos$CICLO.ctl log=CargaDatos$CICLO.log rows=10000 direct=true
mv $Arch $Arch.procesado
compress $Arch.procesado
