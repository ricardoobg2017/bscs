archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion


fecha=`date '+%Y-%m-%d'`
echo "Inicio "$fecha
echo "" > billcycles.dat

cd $ruta_logs
if [ ! -s $fecha ]
then
mkdir $fecha 
fi

mv *.log $fecha
mv despachador1/*.dat $fecha
mv despachador1/*.log $fecha
mv despachador1/*.ctl $fecha
mv despachador2/*.dat $fecha
mv despachador2/*.log $fecha
mv despachador2/*.ctl $fecha
mv despachador3/*.dat $fecha
mv despachador3/*.log $fecha
mv despachador3/*.ctl $fecha
mv despachador4/*.dat $fecha
mv despachador4/*.log $fecha
mv despachador4/*.ctl $fecha
mv despachador5/*.dat $fecha
mv despachador5/*.log $fecha
mv despachador5/*.ctl $fecha
mv despachador6/*.dat $fecha
mv despachador6/*.log $fecha
mv despachador6/*.ctl $fecha
mv despachador7/*.dat $fecha
mv despachador7/*.log $fecha
mv despachador7/*.ctl $fecha
mv despachador8/*.dat $fecha
mv despachador8/*.log $fecha
mv despachador8/*.ctl $fecha

echo "0" > timm_doc1.ctl

cd $ruta_datos
if [ ! -s $fecha ]
then
mkdir $fecha 
fi
mv CICLO* $fecha

cd $ruta_adicionales
if [ ! -s $fecha ]
then
mkdir $fecha 
fi
mv *.dat $fecha
mv *.ctl $fecha

cd $ruta_procesados
if [ ! -s $fecha ]
then
mkdir $fecha 
fi
mv CICLO* $fecha

cd $ruta_resources
if [ ! -s $fecha ]
then
mkdir $fecha 
fi
mv *.dat $fecha
echo "Fin del Proceso"