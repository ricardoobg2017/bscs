#!/usr/bin/perl

#---------------------------------------------------------------------
# Los cambios de este programa debe ser Autorizados por Patricio Chonillo
#---------------------------------------------------------------------
# Autor   : Miguel Garc�a
# Fecha   : 20/01/09
# Motivo  : Corregir nombre de plan incorrecto
#---------------------------------------------------------------------
#--=====================================================================================--
# Modificado por    : SUD Vanessa Gutierrez G.
# Proyecto			: [10179] Nuevos ciclos de Facturaci�n DTH
# Lider Conecel     : SIS Paola Carvajal
# Objetivo			: Cambio de rutas de archivos .DAT y .TXT
#--=====================================================================================--

#10179
#cambio de parametros y rutas
#my @archivo1 = `egrep "40000|Plan" /doc1/doc1prod/procesados/@ARGV[1]/@ARGV[0]`;
my $rutalocal = "/doc1/doc1prod";
my $rutalocal_arch = @ARGV[1];
my @archivo1 = `egrep "40000|Plan" $rutalocal_arch/@ARGV[0]`;
#10179
#my $ruta_error ="/doc1/doc1prod/procesados/".@ARGV[1].;

#10179
#$nombre_archivo = $rutalocal."/procesados/".@ARGV[1]."/".@ARGV[0];
$nombre_archivo = $rutalocal_arch."/".@ARGV[0];
#10179

open(FINAL,">$rutalocal/Final.txt");
foreach $linea (@archivo1) {
	$c=$c+1;
	if (($c%2)!= 0){
     	chop ($linea);
	}
	print FINAL $c."|".$linea;
}
close(FINAL);

my @cambiar = `cat Final.txt | grep "Special Number Rate Plan"`;


$cont = 0; 
foreach $cambio (@cambiar) {
	$cont = $cont + 1;
	#$coid = `awk  -F \| '{print \$4}' $cambio`;
    #$indice = `awk -F \| '{print \$5}' $cambio`;
	@parametros=split(/\|/,$cambio);
	$coid = @parametros[3];
	$indice = @parametros[4];
	system ("echo $indice");
	$indice = $indice / 2; 
	$new_plan = `Consulta_plan.sh $coid`;
    chop($new_plan); 
		
    	@archivo_origen = `cat $nombre_archivo `;
	  if ($cont == 1) {
		#10179
		#system("mkdir $rutalocal/procesados/@ARGV[1]/respaldo_out_error");
		system("mkdir $rutalocal_arch/respaldo_out_error");
		#10179
      }	
		
		#Moviendo el archivo a la Ruta de error siempre que exista algun cambio en los .out 
		#10179
		#system("cat $nombre_archivo > $rutalocal/procesados/@ARGV[1]/respaldo_out_error/@ARGV[0]");
		#open(FINAL2,">$rutalocal/procesados/@ARGV[1]/@ARGV[0]");
		system("cat $nombre_archivo > $rutalocal_arch/respaldo_out_error/@ARGV[0]");
		open(FINAL2,">$rutalocal_arch/@ARGV[0]");
		#10179
	  
    $z = 0;
	
    foreach $linea_origen (@archivo_origen) {
		$y = 0;
		@x = split(/\|/,$linea_origen);
		$plan = @x[1];
		#system("echo $plan");
		if ($plan eq "Plan:") {
			    $z = $z + 1;
				#system("echo Ingresando.. $z $indice");
				if ($z == $indice) {
				   #system("echo haciendo cambio....");
                   #system("echo nombre archivo @ARGV[0]");
				  
              	   #system("echo archivo anterior @archivo_anterior");
				   
				   @new_line = split(/\|/,$linea_origen);
				   $campo1=@new_line[0];
				   $campo2=@new_line[1];
				   $campo4=@new_line[3];
				   print FINAL2 $campo1."|".$campo2."|".$new_plan."|".$campo4."|\n";
				}
                else{
				  print FINAL2 $linea_origen; 
				}
		}else{
			print FINAL2 $linea_origen;               
		}

    }

	close FINAL2;
   
}
