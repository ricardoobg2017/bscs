#--=====================================================================================--
# Modificado por    : SUD Vanessa Gutierrez G.
# Proyecto			: [10179] Nuevos ciclos de Facturaci�n DTH
# Lider Conecel     : SIS Paola Carvajal
# Objetivo			: Cambio de rutas de archivos .DAT y .TXT
#--=====================================================================================--
archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
ciclo=$1

if [ $# -eq 0 ]
then
	echo "\n\t\tune_archivos_doc.sh  # Indique el Ciclo a Procesar  \n"
	exit;
fi

echo "\nUne Archivos en Procesados CICLO_"$ciclo 
nomarchivo="salida_output_doc1_"$ciclo".txt"
nomarchivo_e="salida_output_doc1_"$ciclo"_e.txt" #5328 factura electronica nombre del archivo de facturaci�n electronica
ruta_archivo=$ruta_principal"/procesados/CICLO_"$ciclo
ruta_archivo_dat=$ruta_principal"/dat/CICLO_"$ciclo
#10179
ruta_archivo_proc_cel=$ruta_procesados_movil"/CICLO_"$ciclo
ruta_archivo_dat_cel=$ruta_datos_movil"/CICLO_"$ciclo
ruta_archivo_proc_dth=$ruta_procesados_dth"/CICLO_"$ciclo
ruta_archivo_dat_dth=$ruta_datos_dth"/CICLO_"$ciclo
ln_contador=0
rm -f $ruta_principal"/listado_ejecucion.txt"
#10179

#10179
#cambio de rutas y procesos
if [ -d $ruta_archivo_proc_cel ]
then
   echo "Ruta VOZ creada"
   ln_contador=`expr $ln_contador + 1`
   echo "VOZ">> $ruta_principal"/listado_ejecucion.txt"
fi
if [ -d $ruta_archivo_proc_dth ]
then
   echo "Ruta DTH creada"
   ln_contador=`expr $ln_contador + 1` 
   echo "DTH" >> $ruta_principal"/listado_ejecucion.txt"
fi
#10179

#10179
#cambio de rutas y procesos
if [ $ln_contador -gt 0 ]
then
while read line
do 
   producto=$line
   if [ $producto = "VOZ" ]
   then
		echo "Ejecucion para producto VOZ"
		ruta_archivo=$ruta_archivo_proc_cel
		ruta_archivo_dat=$ruta_archivo_dat_cel
   else
		echo "Ejecucion para producto DTH"
		ruta_archivo=$ruta_archivo_proc_dth
		ruta_archivo_dat=$ruta_archivo_dat_dth
   fi
   echo "Ruta procesados "$ruta_archivo
   echo "Ruta dat "$ruta_archivo_dat
#10179

#10179
#perl Ejecuta_out.pl CICLO_$ciclo
perl Ejecuta_out.pl $ruta_archivo
#10179

cd $ruta_archivo

mkdir archivos
mkdir archivos_e #5328 crear repositorio para grabar archivo de fact. electronica
rm $nomarchivo
rm $nomarchivo_e #5328

val=`grep "10000|000-000-0000000|" *.out | wc -l`
if [ $val -eq 0 ]
then
val=`grep "001-0" *.out | wc -l`
fi
echo "\nExisten $val archivos .out\n"

if [ $val -ne 0 ]
then
for i in `ls out*.out`
do
echo "Desea Unir el Archivo $i S/s [ ]\b\b\c"
read tecla
if [ "$tecla" = "s" ] || [ "$tecla" = "S" ]
then
cat $i >> $nomarchivo_e
mv $i archivos
else
mv $i $i."nopro"  
fi

done
fi

echo "\n"
for i in `ls out*.txt |grep -v "e"`  #5328 |grep -v "e" para recoger archivos de fact. fisica
do
val=`grep "10000|000-000-0000000|" $i | wc -l`
if [ $val -eq 0 ]
then
val=`grep "001-0" $i | wc -l`
fi


cat $i >> $nomarchivo
echo "Archivo $i |"$val
mv $i archivos_e
done
val=`grep "10000|000-000-0000000|" $nomarchivo | wc -l`
if [ $val -eq 0 ]
then
val=`grep "001-01" $nomarchivo | wc -l`
fi
echo "Total $nomarchivo |"$val

#ini 5328 - facturacion electronica
#Une solo los archivos de facturaci�n electronica
echo "\n"
for i in `ls out*e.txt`
do
val=`grep "10000|000-000-0000000|" $i | wc -l`
if [ $val -eq 0 ]
then
val=`grep "001-0" $i | wc -l`
fi

cat $i >> $nomarchivo_e
echo "Archivo $i |"$val
mv $i archivos_e
done
val=`grep "10000|000-000-0000000|" $nomarchivo_e | wc -l`
if [ $val -eq 0 ]
then
val=`grep "001-01" $nomarchivo_e | wc -l`
fi
echo "Total $nomarchivo_e |"$val
#fin 5328
cd $ruta_archivo_dat
archivos_z=`ls *.Z | wc -l`
archivos_dat=`ls *.dat | wc -l`

echo "\nInformaci�n en DAT CICLO_"$ciclo
echo "Archivos .Z |"$archivos_z
echo "Archivos .dat |"$archivos_dat
cd $ruta_archivo

if [ -f "$nomarchivo" ]
then
separa_ciudad.pl $nomarchivo
rm $nomarchivo
#Temporal para la generacion de archivos s�lo GYE
#nombGYE=`ls -1 GYE_salida_output_doc*.txt` #5328
#Recoge el archivos de facturaci�n f�sica
nombGYE=`ls -1 GYE_salida_output_doc*.txt|grep -v "e"`
#cat *.txt  >$nombGYE"_1"	#5328
#mv *.txt archivos/			#5328
#Recoge los archivos de facturaci�n f�sica
for i in `ls *.txt | grep -v "e"`
do
cat $i >>$nombGYE"_1"
mv $i archivos
done
mv $nombGYE"_1" $nombGYE
else
echo "NO EXISTE ARCHIVO DE FACTURACI�N F�SICA PARA EL CICLO CICLO_$ciclo"
fi

#ini 5328
if [ -f "$nomarchivo_e" ]
then
#recoge y procesa el archivo de facturaci�n electr�nica
separa_ciudad.pl $nomarchivo_e
rm $nomarchivo_e
#Temporal para la generacion de archivos s�lo GYE
nombGYE_e=`ls -1 GYE_salida_output_doc*_e.txt`
cat *_e.txt>$nombGYE_e"_1"
mv *_e.txt archivos_e/
mv $nombGYE_e"_1" $nombGYE_e
else
echo "NO EXISTE ARCHIVO DE FACTURACI�N ELECTR�NICA PARA EL CICLO CICLO_$ciclo"
fi
#fin 5328

#10179
done < $ruta_principal"/listado_ejecucion.txt"

fi
#10179