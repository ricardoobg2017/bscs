#---======================================================================================--
#-- Version: 1.0.1
#-- Descripcion: Realiza spool de tablas Maestras de abonados
#-- Modificado por  : SUD Laura Peña
#-- Modificación    : 04/07/2015
#-- Proyecto	    : [10203] - ADICIONALES A LA FACTURA
#-- Líder Claro     : SIS Julia Rodas
#-- Líder PDS       : SUD Arturo Gamboa
#-- Objetivo        : Permite la generacion de archivos .dat para conciliaciones por cambios de billcycle  
#--=====================================================================================--
#VARIABLES
#-------------------------------------------------------------------
. /home/gsioper/.profile #PRODUCCION
cd /doc1/doc1prod
archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. /doc1/doc1prod/$archivo_configuracion

#[9321] - SUD EGO
billcycle=$1
if [ $# -eq 0 ]; then 
   echo "Debe ingresar un billcycle para continuar....."
   exit
fi
ruta=$ruta_resources
cd $ruta
usuario_base=$usuario_base
#password_base=$password_base
password_base=`/home/gsioper/key/pass $usuario_base`
#sid_base=$sid_base  --no funciono el que esta configurado
#sid_base="BSCSPROD"
nombre_archivo="spoolBscsBillcycle.sql";
nombre_archivo_log="spoolBscsBillcycle.log";
#export ORACLE_SID=$sid_base
date
echo "Obtiene información por Billcycle " $ORACLE_SID

cat>$nombre_archivo<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool abonados_adem_$billcycle.dat
select CUENTA_BSCS||'|'||TELEFONO||'|'||CAMPO1||'|'||CAMPO2||'|'||CAMPO3||'|'||CAMPO4||'|'||CAMPO5 FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_ADEN' AND BILLCYCLE_FINAL='$billcycle';
spool off
spool abonados_fechaSVA_$billcycle.dat
select CUENTA_BSCS||'|'||TELEFONO||'|'||CAMPO2||'|'||FEATURE_DESCRIPCION FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_FEAT' AND BILLCYCLE_FINAL='$billcycle';
spool off
spool abonados_CtasPlan_$billcycle.dat
select CUENTA_BSCS||'|'||TELEFONO||'|'||CAMPO2 FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_PLAN' AND BILLCYCLE_FINAL='$billcycle';
spool off
spool abonados_FeatCta_$billcycle.dat
select CUENTA_BSCS||'|'||TELEFONO||'|'||CAMPO2||'|'||FEATURE_DESCRIPCION FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_FEAT_CTA' AND BILLCYCLE_FINAL='$billcycle';
spool off
spool abonados_admin_$billcycle.dat
select CUENTA_BSCS||'|'|| SERVICIO_ADMINISTRADOR FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_ADM' AND BILLCYCLE_FINAL='$billcycle';
spool off

exit
eof
#sqlplus -s $usuario_base/$password_base @$nombre_archivo
echo $password_base | sqlplus -s $usuario_base @$nombre_archivo > $nombre_archivo_log
errorcode=$?

if [ $errorcode -gt 0 ]; then 
	cat abonados_*.dat |grep "ORA" >> $nombre_archivo_log 
    echo "Existen en error al ejecutar la consulta por favor verificar los files .dat generados y el log " $ruta/$nombre_archivo_log
else
echo "Codigo "$errorcode
echo "Ejuccion realizada con exito "$errorcode > Reprocesa_obtiene_informacion.log
echo "Ejuccion realizada con exito "
echo "Fin de proceso "
rm -f $nombre_archivo
fi
