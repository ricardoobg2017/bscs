#---======================================================================================--
#-- Version: 1.0.1
#-- Descripcion: Realiza spool de tablas Maestras
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 17/Feb/2005
#-- Proyecto: DOC1 - Group 1
#--=====================================================================================--
#-- Modificado por  : Paola Carvajal
#-- Modificación    : 05/09/2007
#-- Proyecto		: [2766] - Ajuste para cambio en proceso Perl
#-- Motivo          : Agregar el Spool de la tabla info_contr_text
#--                 : Donde co_id= # de Contrato co_id, Text01= # de la Cédula o RUC, Text02= Nombre, Text03= #Ciclo
#--=====================================================================================--
#-- Modificado por  : Jorge Heredia
#-- Modificación    : 15/05/2008
#-- Proyecto	    : [3408] - Facturacion IVA 0%
#--=====================================================================================--
#-- Modificado por  : CLS Julio Cedeño
#-- Modificación    : 22/10/2009
#-- Proyecto	    : [4697] - Cambio en Presentación Balance de Factura 
#              y Unificación de procesos generadores de facturas - Cambios en DOC1
#-- Líder     : Paola Carvajal
#--=====================================================================================--
#-- Modificado por  : SUD Cristhian Acosta Chambers 
#-- Modificación    : 28/11/2012
#-- Proyecto	    : [8504] - Facturación Electrónica nueva versión emitida por el SRI
#-- Líder           : SIS Paola Carvajal
#-- Motivo          : Se agrega codigo_sri al spool de la service_label_doc1;
#--=====================================================================================--
#-- Modificado por  : SUD Norman Castro
#-- Modificación    : 08/07/2013
#-- Proyecto	    : [8693] - DTH Venta DTH Postpago
#-- Líder Claro     : SIS Paola Carvajal
#-- Líder PDS       : SUD Cristhian Acosta Chambers
#-- Motivo          : Se agrega extraccion de datos para agrupacion de servicios DTH.
#--=====================================================================================--
#-- Modificado por  : SUD Arturo Gamboa
#-- Modificación    : 01/03/2014
#-- Proyecto	    : [9321] - Incluir Adendum en La factura fisica
#-- Líder Claro     : SIS Julia Rodas
#-- Líder PDS       : SUD Arturo Gamboa
#-- Motivo          : Se agrega extraccion de datos de Adendum y fecha de contratacion SVA con costo.
#--=====================================================================================--
#--=====================================================================================--
#-- Modificado por  : SUD Richard Rivera
#-- Modificación    : 31/07/2014
#-- Proyecto	    : [9587] - Facturacion Electronica DTH
#-- Líder Claro     : SIS Xavier Trivinio
#-- Líder PDS       : SUD Cristhian Acosta
#-- Motivo          : Se agrega dato para agrupacion de servicios DTH servicio 1057
#--=====================================================================================--
#--=====================================================================================--
#-- Modificado por  : SUD Evelyn Gonzalez
#-- Modificación    : 29/08/2014
#-- Proyecto	    : [9321] - INCLUSIÓN DE ADENDUM EN LA FACTURA
#-- Líder Claro     : SIS Julia Rodas
#-- Líder PDS       : SUD Arturo Gamboa
#-- Motivo          : Se agrega ingreso del Ciclo para generacion de archivos .dat para F.E.
#--=====================================================================================--
#-- Modificado por  : SUD ARTURO GAMBOA
#-- Modificacion    : 29/08/2014
#-- Proyecto	    : [10203] - ADICIONALES A LA FACTURA
#-- Lider Clro     : SIS Julia Rodas
#-- Lider PDS       : SUD Arturo Gamboa
#-- Motivo          : Se agrega Generacion de archivos .dat con informacion de DTH.
#--=====================================================================================--
#--=====================================================================================--
#-- Modificado por  : CLS JENNIFER CEVALLOS - CLS GEOVANNY BARRERA
#-- Modificacion    : 21/07/2016
#-- Proyecto	    : [A57] - MEJORAS AL OBTIENE DE INFORMACION DE SOCIEDADES
#-- Lider Clro      : SIS Hilda Mora - SIS Paola Carvajal
#-- Lider PDS       : CLS May Mite
#-- Motivo          : Se agregan nuevos campos al archivo mpusntab.dat para identificar los cargos y creditos
#                     Se crea archivo billcycles.dat donde identifica el billcycle con su tipo (NORMAL-SOCIEDADES-DTH). Se adiciona 11 y 98.
#--=====================================================================================--
#VARIABLES
#-------------------------------------------------------------------
#--=====================================================================================--
#-- Modificado por  : SUD Evelyn Gonzalez
#-- Modificación    : 22/04/2016
#-- Proyecto	    : [10417] - Facturación DTH Multipunto
#-- Líder Claro     : SIS Oscar Apolinario
#-- Líder PDS       : SUD Cristhian Acosta
#-- Motivo          : Se agrega ingreso del servicio para Cientes DTH Multipunto
#--					  para generacion de archivos .dat para F.E.
#--=====================================================================================--


. /home/gsioper/.profile #PRODUCCION

archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion
#[9321] - SUD EGO
ciclo=$1
if [ $# -eq 0 ]; then 
   echo "Debe ingresar un ciclo para continuar....."
   exit
fi



ruta=$ruta_resources
cd $ruta
usuario_base=$usuario_base
#password_base=$password_base
password_base=`/home/gsioper/key/pass $usuario_base`
#sid_base=$sid_base
nombre_archivo="spoolBscs.sql";
nombre_archivolog="spoolBscs.log";
text_sql="updateExceptos.sql";
text_log="updateExceptos.log";
#export ORACLE_SID=$sid_base
date
#actualiza_data Excentos
cat>$text_sql<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000

####aqui va el primer codigo MAY 25/11/2009
insert into doc1.servicios_balance
select sncode,des,shdes from mpusntab where sncode in (
select distinct sncode from mpulktm1 where sncode in (
select sncode from mpusntab where sncode > (select max(sncode) from doc1.servicios_balance))
and accserv_catcode='EXENTO');
commit;

exit
eof

echo "Actualiza información " $ORACLE_SID
#sqlplus -s $usuario_base/$password_base @$text_sql
echo $password_base | sqlplus -s $usuario_base @$text_sql >> $text_log
errorcode=$?

if [ $errorcode -gt 0 ]; then 
	cat $text_sql |grep "ORA" >> $text_log 
    echo "Existen en error al ejecutar la consulta por favor verificar el file $text_sql generados y el log " $ruta/$text_log
else
rm -f $text_sql
rm -f $text_log
fi


#actualiza_data Excentos
cat>$text_sql<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool services_ext.log
delete from doc1.servicios_balance where sncode in (
select distinct sncode from mpulktm1 g where sncode in (
select sncode from doc1.servicios_balance )
   and accserv_catcode<>'EXENTO' and sncode not in (11,16,38,904,905));
commit;
spool off
exit
eof

echo $password_base | sqlplus -s $usuario_base @$text_sql >> $text_log
errorcode=$?

if [ $errorcode -gt 0 ]; then 
	cat $text_sql |grep "ORA" >> $text_log 
    echo "Existen en error al ejecutar la consulta por favor verificar el file $text_sql generados y el log " $ruta/$text_log
else
rm -f $text_sql
rm -f $text_log
fi




cat>$nombre_archivo<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool legend_doc1.dat
select leg_id||'|'||sh_name||'|'||name from legend_doc1;
spool off
spool mpulktmb.dat
select tmcode||'|'||  to_char(vsdate,'yyyymmdd')||'|'|| spcode||'|'||  sncode||'|'|| accserv_catcode from sysadm.MPULKTMb a where  vscode in(select max(b.vscode) from sysadm.MPULKTMb b where b.tmcode=a.tmcode);
spool off
spool billcycles.dat
select a.billcycle||'|'||case when upper(a.description) like '%DTH%' then 'DTH' when upper(a.description) like '%SOCIEDAD%' then 'SOCIEDADES' when upper(a.description) not like '%DTH%' then 'NORMAL' when upper(a.description) not like '%SOCIEDAD%' then 'NORMAL' end TIPO_BILLCYCLE from sysadm.billcycles a where UPPER(a.description) not like '%PRUEBA%' UNION select '11|NORMAL' from dual UNION select '98|SOCIEDADES' from dual;
spool off
spool mpusntab.dat
select a.sncode ||'|'|| a.des ||'|'|| a.shdes ||'|'|| a.snind ||'|'|| a.rec_version ||'|'||a.billed_surcharge_ind ||'|'|| a.pde_implicit_ind||'|'|| b.tipo_cliente ||'|'||c.shdes from sysadm.mpusntab a, sysadm.gsi_configura_cargos_creditos b, sysadm.mpusntab c where a.sncode=b.occ_credito(+) and b.cargo_occ=c.sncode(+);
spool off
spool mpuzntab.dat
select zncode||'|'||des ||'|'|| shdes from sysadm.mpuzntab;
spool off
spool mpuzotab.dat
select zocode||'|'||des||'|'||cgi||'|'||sccode||'|'||rec_version||'|'||origin_npcode from sysadm.mpuzotab;
spool off
spool paymenttype_all.dat
select pay.payment_id ||'|'||pay.paymentcode||'|'||pay.paymentname datos from sysadm.paymenttype_all pay;
spool off
spool rateplan.dat
select tmcode||'|'|| des||'|'|| shdes from sysadm.rateplan;
spool off
spool bgh_tariff_plan_doc1.dat
select tmcode||'|'||shdes||'|'||firstname||'|'||secondname||'|'||porder ||'|'||plan_type||'|'||tax_code_iva ||'|'||tax_code_ice from bgh_tariff_plan_doc1 order by tmcode,shdes, plan_type, porder;
spool off
spool tax_category_doc1.dat
select i.taxcat_id||'|'||i.taxcat_name||'|'|| i.taxrate from tax_category_doc1 i;
spool off
spool call_grouping_doc1.dat
select label_id||'|'||nvl(servshdes,1)||'|'||nvl(tzcode,1)||'|'||nvl(ttcode,1)||'|'||leg_id||'|'|| calltype||'|'||tipo from call_grouping_doc1;
spool off
spool service_label_doc1.dat
select label_id||'|'||des||'|'||taxinf||'|'||shdes||'|'||ctaps||'|'||codigo_sri from service_label_doc1;
spool off
spool bgh_tariff_plan_doc1_EBC.dat
select tmcode || '|' || shdes || '|' || firstname || '|' || secondname || '|' || porder || '|' || plan_type || '|' || tax_code_iva || '|' ||tax_code_ice from bgh_tariff_plan_doc1  where tmcode in(612,614,35) order by tmcode, shdes, plan_type, porder;
spool off
spool mpulktmb_EBC.dat
select tmcode || '|' || to_char(vsdate, 'yyyymmdd') || '|' || spcode || '|' || sncode || '|' || accserv_catcode   from sysadm.MPULKTMb a where a.tmcode in (612, 614, 35) and vsdate in (select max(b.vsdate) from sysadm.MPULKTMb b where b.tmcode = a.tmcode and b.sncode = a.sncode);
spool off
spool info_contr_text.dat
select co_id||'|'||text01||'|'||text02||'|'||text03 from info_contr_text where text01 is not null union select '9999|9999|9999|9999' from dual;
spool off
spool excento_iva.dat
select y.custcode from customer_all y, customer_tax_exemption x where y.customer_id = x.customer_id and x.exempt_status = 'A';
spool off
spool services_ext.dat
select t.SNCODE ||'|'|| t.DES ||'|'|| t.SHDES from doc1.servicios_balance t where t.sncode not in (select x.sncode from doc1.doc1_excentos_no_balance x);
spool off
spool grupo_dth.dat
SELECT * FROM (select a.servicio||'|'||b.des||'|'||b.shdes||'|'||a.balance||'|' servicio from cob_servicios a, mpusntab b where a.servicio = b.sncode and a.tipo like '%DTH' UNION ALL SELECT a.servicio || '|' || b.des || '|' || b.shdes || '|' || decode(a.servicio, 1057, 'C', a.balance) || '|' servicio FROM cob_servicios a, mpusntab b WHERE a.servicio = b.sncode AND a.servicio = 1057 AND ROWNUM < 2) g ORDER by g.servicio;
spool off
#CONFIGURACION CODIGO SRI SUD MLU - JYU
spool codigo_sri.dat
select distinct campo from(select (a.bankname || '|' || nvl(c.id_fp_sri,20)  || '|') campo from sysadm.bank_all a, sysadm.bs_homologacion_fpsri c where a.bank_id = c.id_forma_pago(+) and a.insert_by_user is null order by c.id_fp_sri, a.bankname);
spool off
#CONFIGURACION CODIGO SRI SUD MLU - JYU
exit
eof

echo $password_base | sqlplus -s $usuario_base @$nombre_archivo > $nombre_archivolog
errorcode=$?

if [ $errorcode -gt 0 ]; then 
	cat $nombre_archivo |grep "ORA" >> $nombre_archivolog 
    echo "Existen en error al ejecutar la consulta por favor verificar los files .dat generados y el log " $ruta/$nombre_archivolog
else
rm -f $nombre_archivo
fi

echo "0">excento_iva.dat


nombre_archivo="spoolBscsSubCICLOS.sql";
nombre_archivolog="spoolBscsSubCICLOS.log";

 echo "ciclos-->$ciclo--"
cat>$nombre_archivo<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool FA_CICLOS_AXIS_BSCS.dat
select /*+index (C IDX_SUBCICLOS,IDX_CICLOS)+*/ C.ID_CICLO_ADMIN  from sysadm.FA_CICLOS_AXIS_BSCS C where  C.id_ciclo=$ciclo order by c.ID_CICLO_ADMIN;
spool off
exit
eof

echo $password_base | sqlplus -s $usuario_base @$nombre_archivo > $nombre_archivolog
errorcode=$?

if [ $errorcode -gt 0 ]; then 
	cat FA_CICLOS_AXIS_BSCS.dat |grep "ORA" >> $nombre_archivolog 
    echo "Existen en error al ejecutar la consulta por favor verificar los files .dat generados y el log " $ruta/$nombre_archivolog
else
rm -f $nombre_archivo
fi



if [ -s FA_CICLOS_AXIS_BSCS.dat ]; then
   while read line1
   do 
   identifica="$line1"
cat>$nombre_archivo<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool abonados_adem_$identifica.dat
select CUENTA_BSCS||'|'||TELEFONO||'|'||CAMPO1||'|'||CAMPO2||'|'||CAMPO3||'|'||CAMPO4||'|'||CAMPO5 FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_ADEN' AND BILLCYCLE_FINAL='$line1';
spool off
spool abonados_fechaSVA_$identifica.dat
select CUENTA_BSCS||'|'||TELEFONO||'|'||CAMPO2||'|'||FEATURE_DESCRIPCION FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_FEAT'  AND BILLCYCLE_FINAL='$line1';
spool off
spool abonados_CtasPlan_$identifica.dat
select CUENTA_BSCS||'|'||TELEFONO||'|'||CAMPO2 FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_PLAN' AND BILLCYCLE_FINAL='$line1';
spool off
spool abonados_FeatCta_$identifica.dat
select CUENTA_BSCS||'|'||TELEFONO||'|'||CAMPO2 FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_FEAT_CTA' AND BILLCYCLE_FINAL='$line1';
spool off
spool abonados_admin_$identifica.dat
select CUENTA_BSCS||'|'|| SERVICIO_ADMINISTRADOR FROM sysadm.BL_CUENTAS_FACTURAR WHERE TIPO_PROCESO='CTA_ADM' AND BILLCYCLE_FINAL='$line1';
spool off

exit
eof

echo $password_base | sqlplus -s $usuario_base @$nombre_archivo > $nombre_archivolog
errorcode=$?

if [ $errorcode -gt 0 ]; then 
	cat FA_CICLOS_AXIS_BSCS.dat |grep "ORA" >> $nombre_archivolog 
    echo "Existen en error al ejecutar la consulta por favor verificar los files .dat generados y el log " $ruta/$nombre_archivolog
else
rm -f $nombre_archivo
fi

#if ! [ -f abonados_adem_$identifica.dat ]; then
#   rm -f abonados_adem_$identifica.dat
#fi
#if ! [ -f abonados_fechaSVA_$identifica.dat ]; then
#   rm -f abonados_fechaSVA_$identifica.dat
#fi
#if ! [ -f abonados_CtasPlan_$identifica.dat ]; then
#   rm -f abonados_CtasPlan_$identifica.dat
#fi
#if ! [ -f abonados_FeatCta_$identifica.dat ]; then
#   rm -f abonados_FeatCta_$identifica.dat
#fi

#rm -f $nombre_archivo
   done < FA_CICLOS_AXIS_BSCS.dat
fi
rm -f FA_CICLOS_AXIS_BSCS.dat

############ INICIO DE DTH #############

nombre_archivo="spoolBscsSubCICLOS_DTH.sql";
nombre_archivolog="spoolBscsSubCICLOS_DTH.log";


 echo "ciclos-->$ciclo--"
cat>$nombre_archivo<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
spool FA_CICLOS_AXIS_BSCS_DTH.dat
select /*+index (C IDX_SUBCICLOS,IDX_CICLOS)+*/ C.ID_CICLO_ADMIN  from sysadm.FA_CICLOS_AXIS_BSCS C where  C.id_ciclo=$ciclo and ciclo_default='D' order by c.ID_CICLO_ADMIN;
spool off
exit
eof

echo $password_base | sqlplus -s $usuario_base @$nombre_archivo > $nombre_archivolog
sleep 5

errorcode=$?

if [ $errorcode -gt 0 ]; then 
	cat FA_CICLOS_AXIS_BSCS_DTH.dat |grep "ORA" >> $nombre_archivolog 
    echo "Existen en error al ejecutar la consulta por favor verificar los files .dat generados y el log " $ruta/$nombre_archivolog
else
rm -f $nombre_archivo
fi

if [ -s FA_CICLOS_AXIS_BSCS_DTH.dat ]; then
   while read line1
   do 
   identifica="$line1"
 

 cat>$nombre_archivo<<eof
whenever sqlerror exit failure
whenever oserror exit failure
set long 900000
set pagesize 0
set termout off
set head off
set trimspool on
set feedback off
set serveroutput off
set linesize 10000
--10203 LPE Se añade extraccion de informacion para DTH para visualizarla en la factura
spool abonados_DTH_fechaSVA_$identifica.dat
select CUENTA_BSCS||'|'||CAMPO2||'|'||CAMPO3 FROM sysadm.BL_CUENTAS_FACTURAR_DTH WHERE TIPO_PROCESO='CTA_FEAT'  AND CICLO='$ciclo' and BILLCYCLE_FINAL='$line1';
spool off
spool abonados_DTH_CtasPlan_$identifica.dat
select CUENTA_BSCS||'|'||CAMPO2||'|'||CAMPO3 FROM sysadm.BL_CUENTAS_FACTURAR_DTH WHERE TIPO_PROCESO='CTA_PLAN' AND CICLO='$ciclo' and BILLCYCLE_FINAL='$line1';
spool off
spool abonados_DTH_FeatCta_$identifica.dat
select CUENTA_BSCS||'|'||CAMPO2||'|'||CAMPO3 FROM sysadm.BL_CUENTAS_FACTURAR_DTH WHERE TIPO_PROCESO='CTA_FEAT_CTA' AND BILLCYCLE_FINAL='$line1';
spool off
# 10417 DTH Servicios Multipunto
spool servicios_multipuntoDTH_$identifica.dat
select CUENTA_BSCS||'|'|| CAMPO2||'|' FROM sysadm.BL_CUENTAS_FACTURAR_DTH WHERE TIPO_PROCESO='CTA_SERV_COID' AND CICLO='$ciclo'  AND BILLCYCLE_FINAL='$line1';
spool off
spool mpulktmb_dth.dat
select tmcode || '|' || to_char(vsdate, 'yyyymmdd') || '|' || spcode || '|' ||
       sncode || '|' || accserv_catcode
  from sysadm.MPULKTMb a
 where vscode in (select max(b.vscode)
                    from sysadm.MPULKTMb b
                   where b.tmcode = a.tmcode)and tmcode in (select tmcode from rateplan where shdes like 'D%');
spool off 
exit
eof
   
   echo $password_base | sqlplus -s $usuario_base @$nombre_archivo > $nombre_archivolog
   errorcode=$?

if [ $errorcode -gt 0 ]; then 
    echo "Existen en error al ejecutar la consulta por favor verificar los files .dat generados y el log " $ruta/$nombre_archivolog
else
rm -f $nombre_archivo
   
fi

   done < FA_CICLOS_AXIS_BSCS_DTH.dat
fi
rm -f FA_CICLOS_AXIS_BSCS_DTH.dat

############ FIN DE DTH #############

date
echo "Comienza FTP a Servidor 130.2.130.15"
sh FTP_dat.sh
date

echo "Fin FTP a Servidor 130.2.130.15"

date
echo "Comienza FTP a Servidor 130.2.17.106"
sh FTP_dat_106.sh
date

echo "Fin FTP a Servidor 130.2.17.106"
