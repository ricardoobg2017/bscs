#--======================================================================================--
#-- Version: 2.0.0
#-- Descripcion: Realiza las llamadas de los proceso de Extraccion y Proceso de Informaci�n
#--              de Salida DOC1
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 03/Abril/2006
#-- Proyecto: Group 1
#--=====================================================================================--
#-- Desarrollado por:		Paola Carvajal
#-- Motivo :				N�mero de Secuencias
#-- Fecha de modificacion:  13/Nov/2006
#--=====================================================================================--
#-- Desarrollado por:  
#-- Fecha de modificacion: 
#--=====================================================================================--
#-- Modificado por                   : John Cardozo Villon 
#-- Lider CLS                        : Ing. Sheyla Ramirez
#-- Lider Claro SIS                  : Jackeline Gomez
#-- Fecha de Modificacion            : 26/Mayo/2014
#-- Proyecto  [9487] BCH Automaticos : Modificacion de cada uno de los despachadores al momento de realizar la extraccion
#--                                    con sus validaciones respectivas para completar con exito la extraccion.
#--=====================================================================================-- 
# Modificado por    : SUD Vanessa Gutierrez G.
# Proyecto	    : [10179] Nuevos ciclos de Facturaci�n DTH
# Lider Conecel     : SIS Paola Carvajal
# Objetivo	    : Cambio de rutas de archivos .DAT y .TXT
#--=====================================================================================--

# --========== Creacion de la variable para el archivo de configuracion ===-------
RUTA_P=/doc1/doc1prod
cd $RUTA_P 

#$archivo_configuracion="configuracion.conf"
archivo_configuracion=$RUTA_P"/configuracion_ultimo.conf" #CLS JCA [9487] 07/07/2014 Se crea una variable para abrir el archivo de configuracion

# --========= Verificar si el archivo de configuracion existe =====-----
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

# --===== Abrir el archivo de configuracion ==--
. $archivo_configuracion

#--- ==== Se recibe como parametro el numero del despachador y el billcycle ===---
numero_despachador=$1
numero_billcycle=$2 

archivo_ejecucion=$ruta_principal"/llama_proceso_"$numero_billcycle"_"$numero_despachador".lck" #CLS JCA [9487] 07/07/2014 Variable para controlar que no se levante de nuevo el despachador

# --===== Modificado CLS JCA [9487] 07/07/2014 Inicio ==--
# --===== Validacion para controlar que no se levante de nuevo el despachador ===---
if [ -s $archivo_ejecucion ]
then
echo "Proceso en ejecucion..." 
exit;
fi
echo $$ > $archivo_ejecucion

# --====== Fin Validacion ===---

# --====== Se crea el archivo ===---


# -----==== Archivos de conexion ===---
ruta=$ruta_principal
usuario_base=$usuario_base
password_base=$password_base
export ORACLE_SID=$sid_base
# ---=================================---

archivo_ctl=$ruta"/extr_timm_doc1_pro_"$numero_billcycle"_"$numero_despachador".ctl"

# ---====== Archivos LOG ======-----
ruta_log=$ruta_logs

NOMBRE_FILE_LOG=$ruta_log"/llama_proceso_"$numero_billcycle"_"$numero_despachador".log" 
NOMBRE_ERROR_LOG=$ruta_log"/llama_proceso_error_"$numero_billcycle"_"$numero_despachador".log" 
NOMBRE_ERROR_LOG_2=$ruta_log"/proceso_error_ejecuta_"$numero_billcycle"_"$numero_despachador".log" 
# --===========================-----

# ---===== Archivos SQL ====------
nombre_archivo=$ruta_sql"/spoolTIMM_in_"$numero_billcycle"_"$numero_despachador".sql";
nombre_archivo_sql=$ruta_sql"/actualiza_ciclo_"$numero_billcycle"_"$numero_despachador".sql";
NOMBRE_FILE_SQL=$ruta_sql"/verificar_estado_"$numero_billcycle"_"$numero_despachador".sql";
NOMBRE_FILE_SQL_2=$ruta_sql"/validacion_extraccion_"$numero_billcycle"_"$numero_despachador".sql";
NOMBRE_FILE_SQL_3=$ruta_sql"/actualizar_estado_"$numero_billcycle"_"$numero_despachador".sql";
# ----====================------

####============= VARIABLES DAT ====-----
OBTENER_INFORMACION=$ruta"/billcycles_extraer_in_"$numero_billcycle"_"$numero_despachador".txt"
VERIFICAR_ESTADO=$ruta"/verificar_estado_det_factura_"$numero_billcycle"_"$numero_despachador".txt"
VALIDACION_CUSTOMER_ID=$ruta"/validacion_customer_id_"$numero_billcycle"_"$numero_despachador".txt"
VALIDACION_CANTIDAD=$ruta"/validacion_cantidad_"$numero_billcycle"_"$numero_despachador".txt"

### ---================================----
#### -----===== Fin modificacion de variables ====------

echo "Se crearon los nuevos archivos" >> $NOMBRE_FILE_LOG

# --===== Modificado CLS JCA [9487] 07/07/2014 Inicio ==--
# --===== Funciones para controlar el proceso de extraccion de cada uno de los despachadores ===---
# ---===== funcion para extraer la informacion para realizacion la extraccion ====-----
obtiene_informacion(){
cat>$nombre_archivo<<eof
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off
set serveroutput on;
spool $OBTENER_INFORMACION
select *
  from (select f.billcycle || '|' || f.tipo || '|' || f.ejecucion || '|' ||
               f.salida_doc1 || '|' || f.estado || '|' ||
               to_char(f.periodo, 'YYYYMMDD') || '|' || f.secuencia || '|' ||
               c.ciclo_default
          from doc1.DOC1_DET_PARA_FACTURACION f, fa_ciclos_axis_bscs c
         where f.billcycle = '$numero_billcycle'
           and f.procesar = 'S'
           and f.estado = 'A'
           and f.despachador = $numero_despachador
           and c.id_ciclo_admin = f.billcycle
         order by to_number(f.secuencia))
 where rownum = 1;
spool off
exit;
eof
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$nombre_archivo > $NOMBRE_ERROR_LOG #CLS JCA [9487] 07/07/2014 
echo $password_base | sqlplus -s $usuario_base @$nombre_archivo > $NOMBRE_ERROR_LOG #10179

#-------Modificado [9487] John_cardozo INI ----
#----- 26/05/2014
#---- Verificar si hay error.
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
echo "Error al momento de obtener la informacion del CICLO: por despachado: $numero_despachador " >> $NOMBRE_FILE_LOG
fi
ciclo=`cat $OBTENER_INFORMACION | awk -F\| '{print $1}'`
tipo=`cat $OBTENER_INFORMACION | awk -F\| '{print $2}'`
ejecucion=`cat $OBTENER_INFORMACION | awk -F\| '{print $3}'`
salida_doc1=`cat $OBTENER_INFORMACION | awk -F\| '{print $4}'`
estado=`cat $OBTENER_INFORMACION | awk -F\| '{print $5}'`
periodo=`cat $OBTENER_INFORMACION | awk -F\| '{print $6}'`
secuencia=`cat $OBTENER_INFORMACION | awk -F\| '{print $7}'`
#10179
tipo_prod=`cat $OBTENER_INFORMACION | awk -F\| '{print $8}'`
#10179
#echo "Datos Despachador $numero_despachador "$ciclo $tipo $ejecucion $salida_doc1 $estado $periodo $secuencia > $NOMBRE_FILE_LOG
echo "Datos Despachador $numero_despachador "$ciclo $tipo $ejecucion $salida_doc1 $estado $periodo $secuencia $tipo_prod > $NOMBRE_FILE_LOG
#10179
#------- [9487] FIN ------
}

actualiza_ciclo(){
echo "Actualiza Ciclo " $periodo $ciclo $secuencia $numero_despachador $estado_ciclo $estado_ciclo $fecha $fecha_fin
cat >$nombre_archivo_sql<< eof_sql
SET SERVEROUTPUT ON
begin      
 doc1.doc1_actualizar(pv_periodo => '$periodo',
                       pv_ciclo => '$ciclo',
                       pn_secuencia => '$secuencia',
                       pv_despachador => $numero_despachador,
                       pv_estado => '$estado_ciclo',
                       pv_procesar => '$estado_ciclo',
                       pv_fecha_inicio => '$fecha',
                       pv_fecha_fin => '$fecha_fin');
end;
/
exit;
eof_sql
#sqlplus  $usuario_base/$password_base@$ORACLE_SID  @$nombre_archivo_sql > $NOMBRE_ERROR_LOG_2 #CLS JCA [9487] 07/07/2014 
echo $password_base | sqlplus -s $usuario_base @$nombre_archivo_sql > $NOMBRE_ERROR_LOG_2 #10179
 
#-------Modificado [9487] John_cardozo INI ----
#----- 26/05/2014
#---- Verificar si hay error.
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG_2|wc -l`
if [ $ERROR -ge 1 ];
then
echo "Error al momento de obtener la informacion del Query " >> $NOMBRE_FILE_LOG
fi
rm -f $nombre_archivo_sql
#------- [9487] FIN ------  
echo "$periodo $ciclo $secuencia $numero_despachador $estado_ciclo $estado_ciclo $fecha $fecha_fin" >> $NOMBRE_FILE_LOG
}
verificar_estado(){
cat>$NOMBRE_FILE_SQL<<END 
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off
spool $VERIFICAR_ESTADO
select count(*) from doc1.DOC1_DET_PARA_FACTURACION where billcycle ='$numero_billcycle' and despachador = $numero_despachador and estado = 'F';
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL > $NOMBRE_ERROR_LOG #10179
####===== Verificar si el query ejecuto correctamente ================= 
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
echo "Error al momento de obtener la cantidad de despachadores.... " >> $NOMBRE_FILE_LOG
fi
rm -f $NOMBRE_FILE_SQL

###====================================================================
}
actuaizar_estado(){
estado_doc1=$1
cat>$NOMBRE_FILE_SQL_3<<END 
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off
update doc1.DOC1_DET_PARA_FACTURACION set estado = '$estado_doc1' where billcycle ='$numero_billcycle' and despachador = $numero_despachador;
commit;
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_3 > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_3 > $NOMBRE_ERROR_LOG #10179
####===== Verificar si el query ejecuto correctamente ================= 
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
echo "Error al momento de obtener la cantidad de despachadores.... " >> $NOMBRE_FILE_LOG
fi
rm -f $NOMBRE_FILE_SQL_3
###====================================================================
}

# --===== Modificado CLS JCA [9487] 07/07/2014 Inicio ==--
# --===== Inicia proceso para realizar la extracion se cicla hasta que todos terminen correctamente ===---

intentos=0
## ---====== llama a la funcion para obtener la informacion ====-----
obtiene_informacion

# --==== inicia validacion si el archivo en donde se guarda la informacion se creo ===----
if [ -s $OBTENER_INFORMACION ]
then

# ---===== Variables de fechas ===------
fecha=`date '+%Y%m%d %H%M%S'`
fecha_fin=`date '+%Y%m%d %H%M%S'`
estado_ciclo="P"

# ---==== Verifica si el ciclo existe ===----
if [ "$ciclo" = "" ]
then
    rm $nombre_archivo
   return;
fi

export estado_ciclo
export fecha
export fecha_fin

# ---===== llama funcion para actualizar el estado del despachador que realiza la extraccion ====-----
actualiza_ciclo

echo "Empieza la Extraccion de los archivos DAT" >> $NOMBRE_FILE_LOG
if [ "$ejecucion" = "S" ]
then
	  
      echo "Ejecuta Despachador $numero_despachador $ciclo $tipo $ejecucion $salida_doc1 $estado"
    
echo "Ingresa extraccion.....$numero_despachador $ciclo" >> $NOMBRE_FILE_LOG 
sh BP_down_doc_ultimo.sh $numero_despachador $ciclo $tipo 
# ---======= Validacion para verificar qu el proceso haya terminado con exito ====-----
PidsEjecuta=`ps -eaf | grep BP_down_doc_ultimo.sh |grep -v grep | wc -l`
while [ $PidsEjecuta -ne 0 ] 
do
PidsEjecuta=`ps -eaf | grep BP_down_doc_ultimo.sh |grep -v grep | wc -l`
sleep 5
done
fi 
fecha_fin=`date '+%Y%m%d %H%M%S'`
estado_ciclo="F"
export fecha_fin
export estado_ciclo
actualiza_ciclo

#======= verificar estado ===


#-----================== VALIDACION PARA VERIFICAR QUE SE ESTEN INGRESANDO CORRECTO LOS DATOS DE LA EXTRACCION POR CADA DESPACHADOR =======-----
cat>$NOMBRE_FILE_SQL_2<<END 
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off
spool $VALIDACION_CUSTOMER_ID
SELECT to_char(fecha,'yyyymmdd')||'_'||customer_id||'_'||billcycle||'_'||procesador FROM DOC1.DOC1_CUENTAS  WHERE PROCESADOR IN('$numero_despachador')AND BILLCYCLE = '$numero_billcycle';
spool off
spool $VALIDACION_CANTIDAD
SELECT COUNT(*)FROM DOC1.DOC1_CUENTAS WHERE PROCESADOR IN('$numero_despachador')and customer_id_high is null AND BILLCYCLE = '$numero_billcycle';
spool off
exit;
END

#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_2 > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_2 > $NOMBRE_ERROR_LOG #10179
####===== Verificar si el query ejecuto correctamente ================= 
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
echo "Error al momento de obtener la cantidad de despachadores.... " >> $NOMBRE_FILE_LOG
fi
rm -f $NOMBRE_FILE_SQL_2
###====================================================================

if [ -s $VALIDACION_CUSTOMER_ID ]
then
#10179
if [ "$tipo_prod" = "D" ]
	cd $ruta_datos_dth"/CICLO_"$numero_billcycle
else
	#cd $ruta"/dat/CICLO_"$numero_billcycle
	cd $ruta_datos_movil"/CICLO_"$numero_billcycle
fi
#10179
contador_registros=0
for line in `cat $VALIDACION_CUSTOMER_ID`
do

 if [ "$line" != "" -a "$line" != " " ]
 then
 pwd
 
existe=`ls $line".dat" |wc -l`

if [ $existe -eq 1 ]
then

contador_registros=`expr $contador_registros + 1`
  
 fi
  
 fi
done

resultado_2=`cat $VALIDACION_CANTIDAD`

####==============================================================================================================-------
verificar_estado
v_estado=`cat $VERIFICAR_ESTADO`

if [ $v_estado -eq 1 -a $contador_registros -eq $resultado_2 ] 
then
echo "Se actualizo correctamente....el ciclo: $numero_billcycle del despachador: $numero_despachador " >> $NOMBRE_FILE_LOG
rm -f $archivo_ejecucion
rm -f $archivo_ctl
rm -f $nombre_archivo
rm -f $OBTENER_INFORMACION
  
else
echo "Error al momento de verificar la actualizacion....." > $NOMBRE_ERROR_LOG
  
rm -f $VERIFICAR_ESTADO
rm -f $archivo_ejecucion
rm -f $VALIDACION_CUSTOMER_ID
rm -f $VALIDACION_CANTIDAD
rm -f $OBTENER_INFORMACION
rm -f $nombre_archivo
 
actuaizar_estado 'E'
		
echo "Error al momento de realizar la extraccion del Billcycle: $numero_billcycle con numero de despachador: $numero_despachador" >> $NOMBRE_FILE_LOG
rm -f $archivo_ctl
rm -f $OBTENER_INFORMACION
		

fi
fi
#============================	
    #done #[9487] CLS JCAR 27/05/2014
    #ingreso_proceso=1 #[9487] CLS JCAR 27/05/2014
fi

# --================= Fin de modificacion que realiza la extraccion ====-------- 
echo "fin de la extraccion de ciclo : $ciclo" >> $NOMBRE_FILE_LOG

echo "Comenzara la eliminacion de los archivos creados......" >> $NOMBRE_FILE_LOG
rm -f $NOMBRE_ERROR_LOG
rm -f $NOMBRE_ERROR_LOG_2
#rm -f $RESULTADO_VALIDACION
rm -f $OBTENER_INFORMACION
rm -f $VERIFICAR_ESTADO
rm -f $VALIDACION_CANTIDAD
#rm -f $VALIDACION_RESULTADO
rm -f $VALIDACION_CUSTOMER_ID
rm -f $nombre_archivo_sql
rm -f $archivo_ejecucion





