#!/opt/perl_64/bin/perl
#!/usr/bin/perl
#!/usr/local/bin/perl
#---------------------------------------------------------------------
# Los cambios de este programa debe ser Autorizados por Patricio Chonillo
#---------------------------------------------------------------------
# Autor   : Paola Carvajal    # Fecha   : 16/02/06
# Motivo  : Desglosar el Detalle de Llamadas
#---------------------------------------------------------------------
# Versi�n : 1.0.1
# Autor   : Paola Carvajal   # Fecha   : 30/06/06
# Motivo  : Ajuste de XFU conteo de unidades Libres puede existir 
#			mas de un paquete.
#---------------------------------------------------------------------
# Versi�n : 1.0.2
# Autor   : Paola Carvajal   # Fecha   : 06/07/06
# Motivo  : Incluir Consumos del Mes por N�mero Celular en Total
#			de Mes Minutos Inclu�dos
#---------------------------------------------------------------------
# Versi�n : 1.0.3
# Autor   : Paola Carvajal   # Fecha   : 10/07/06
# Motivo  : Ajuste al Calculo de Promociones puede existir m�s de una 
# promoci�n para un mismo Servicio
#---------------------------------------------------------------------
# Versi�n : 2.0.0
# Autor   : Paola Carvajal   # Fecha   : 13/11/06
# Motivo  : Numero de Secuencias
#---------------------------------------------------------------------
# Versi�n : 2.0.1
# Autor   : Romeo Cabrera   # Fecha   : 13/11/06
# Motivo  : Disminucion de variables en hash %MAPEODetalleLlamdas para usar menos memoria
#---------------------------------------------------------------------
# Versi�n : 2.0.2
# Autor   : Mario Pala     # Fecha   : 11/05/06
# Motivo  : Correcci�n de plan impreso incorrecto
#---------------------------------------------------------------------
# Versi�n : 2.0.3
# Autor   : Mario Pala    # Fecha   : 20/06/2007
# Motivo  : Correcci�n de plan impreso incorrecto
#---------------------------------------------------------------------
# Versi�n   : 2.0.4
# Autor     : Paola Carvajal    # Fecha     : 05/09/2007
# Proyecto  : [2766] - Ajuste para cambio en proceso Perl 
# Motivo    : Agregar C�dula/Nombre
#---------------------------------------------------------------------
# Versi�n   : 2.0.5
# Autor     : Francisco Miranda   # Fecha     : 12/09/2007
# Motivo    : Correcci�n de plan impreso incorrecto
#---------------------------------------------------------------------
# Versi�n   : 2.0.6
# Autor     : Paola Carvajal    # Fecha     : 18/10/2007
# Proyecto  : [2917] AJUSTE PROGRAMA PERL
# Motivo    : Nuevo Formato de Factura
#---------------------------------------------------------------------
# Versi�n   : 2.0.7
# Autor     : Paola Carvajal   # Fecha     : 03/01/2008
# Proyecto  : [3127] - Eliminaci�n del ICE proceso PERL
# Motivo    : Nueva Ley Tributaria - Eliminaci�n del ICE
#---------------------------------------------------------------------
# Versi�n   : 2.0.8
# Autor     : Edwin Berrones        # Fecha     : 07/01/2008
# Proyecto  : [2589] - Mejoras a planes ideal empresa 4
# Motivo    : Consideraci�n de planes ideal empresa 4 en el anexo de la factura
#---------------------------------------------------------------------
# Versi�n   : 2.0.9
# Autor     : Francisco Miranda     # Fecha     : 18/02/2008
# Motivo    : Correcci�n de plan no impreso.
#---------------------------------------------------------------------
# Versi�n   : 2.1.0
# Autor     : Paola Carvajal       # Fecha     : 17/03/2008
# Motivo    : [3311] MEJORAS AL PROCESOS DE DOC1
#---------------------------------------------------------------------
# Versi�n   : 2.1.1
# Autor     : Jorge Heredia        # Fecha     : 15/05/2008
# Motivo    : [3408] FACTURACION IVA 0%
#---------------------------------------------------------------------
#---------------------------------------------------------------------
# Versi�n   : 2.1.2
# Autor     : Miguel Garcia       # Fecha     : 17/07/2008
# Motivo    : [3589] FACTURA DETALLADA COBRADA EN EXCESO
#---------------------------------------------------------------------
# Versi�n   : 2.1.3
# Autor     : Miguel Garcia       # Fecha     : 28/07/2008
# Motivo    : [3636] Agrupacion de codigos de llamadas(GS034-GS032)
#---------------------------------------------------------------------
# Versi�n   : 2.1.4
# Autor     : Miguel Garcia       # Fecha     : 28/07/2008
# Motivo    : [3636] Agrupacion Servicio de Video Llamada (GS028) 
#---------------------------------------------------------------------
# Versi�n   : 2.1.4
# Autor     : Juan Ronquillo     # Fecha     : 01/09/2008
# Motivo    : [1396] Facturaci�n Pymmes
#             Se comprueba que no exista la etiqueta "Llamada Tarjeta" 
#             para que no agrupe las llamadas y ademas no se genere en el TXT
#---------------------------------------------------------------------
# Versi�n   : 2.1.4
# Autor     : Miguel Garcia       # Fecha     : 08/09/2008
# Motivo    : [3769] Cambio en etiquetas de video llamada  (GS028) 
#----------------------------------------------------------------------
# Version : 2.1.5
# Autor:   Carolina Chang   # Fecha	 : 15/09/2008
# Motivo:   [2427] DM Facturacion
#               Se debe anadir el codigo del plan en BSCS
#               para que luego se adicione en la tabla BS_FACT
#----------------------------------------------------------------------
# Version : 2.1.6
# Autor:   CLS Rolando Herrera   # Fecha	 : 26/09/2008
# Motivo:   [3512] Automatizaci�n de facturas y rezagos
#               Se debe anadir el campo direccion de BSCS (ccadr1, ccadr2)
#               para que luego se adicione en la tabla doc1_cuentas
#---------------------------------------------------------------------
# Versi�n   : 2.1.7
# Autor     : Paola Carvajal     # Fecha     : 15/12/2008
# Motivo    : [3925] Parametrizaci�n Etiqueta Voz
#             Se adiciona parametrizaci�n de las Etiquetas de Servicio de voz 
#             se agrega el archivo en resources etiqueta_voz.dat
#---------------------------------------------------------------------
# Versi�n   : 2.1.8
# Autor     : Paola Carvajal     # Fecha     : 07/05/2009
# Motivo    : [4376] Reporte Financiero
#             Se adiciona en el estado de cuenta 20200 el shdes
# L�der     : Wendy Burgos
#---------------------------------------------------------------------
# Versi�n   : 2.1.9
# Autor     : CLS Julio Cede�o     # Fecha     : 22/09/2009
# Motivo    : [4697] Cambio en Presentaci�n Balance de Factura 
#              y Unificaci�n de procesos generadores de facturas - Cambios en DOC1
# L�der     : Paola Carvajal
#---------------------------------------------------------------------
# Versi�n   : 2.2.0
# Autor     : CLS Miguel Garc�a     # Fecha     : 04/02/2010
# Motivo    : [5058] Ajustes a procesos Doc1
# L�der     : Paola Carvajal
#---------------------------------------------------------------------
# Versi�n   : 2.3.0
# Autor     : Paola Carvajal     # Fecha     : 19/05/2010
# Motivo    : 4505 - Circulo POrta
# L�der     : Paola Carvajal
#---------------------------------------------------------------------
# Versi�n   : 2.3.1
# Autor     : CLS Miguel Garc�a      # Fecha     : 17/11/2010
# Motivo    : [5413] - Correccion en plan no impreso
# L�der     : Jackelinne Gomez
#---------------------------------------------------------------------
# Versi�n   : 2.3.2
# Autor     : CIMA Eduardo Coronel - ECO_CIMA     # Fecha     : 20/12/2010
# Motivo    : 3804 - Mejoras Workflow y Solicitudes de Aclaraci�n
#             Agregar el c�digo del servicio a cada feature que aparece en la factura
#             y la cuenta PeopleSoft en los rubros de llamadas
# L�der CIMA: C�sar Balanz�tegui
# L�der Port: Gary Reyes
#---------------------------------------------------------------------
# Versi�n   : 2.3.3
# Autor     : SUD Cristhian Acosta Chambers      # Fecha     : 12/12/2011
# Motivo    : [5328] - Facturaci�n Electr�nica
# L�der     : Paola Carvajal
#---------------------------------------------------------------------
# Versi�n   : 2.3.4
# Autor     : SUD Cristhian Acosta Chambers      # Fecha     : 31/10/2012
# Motivo    : [8504] - Facturaci�n Electr�nica nueva versi�n emitida por el SRI
# L�der		: SIS Paola Carvajal
#---------------------------------------------------------------------
# Versi�n   : 2.3.5
# Autor     : SUD Norman Castro Jervis			  # Fecha     : 21/06/2013
# Motivo    : Validaciones para factura electronica cuando el nombre, direccion, 
#			  email tienen caracteres especiales y CI o RUC estan inconsistentes.
# L�der		: SIS Paola Carvajal
#---------------------------------------------------------------------
# Variables #
#--=====================================================================================--
#-- Modificado por  : SUD Arturo Gamboa
#-- Modificaci�n    : 01/03/2014
#-- Proyecto	    : [9321] - Incluir Adendum en La factura fisica
#-- L�der Claro     : SIS Julia Rodas
#-- L�der PDS       : SUD Arturo Gamboa
#-- Motivo          : Se agrega extraccion de datos de Adendum y fecha de contratacion SVA con costo.
#--=====================================================================================--
# INCLUIR A ROAMING CON IVA #
#--=====================================================================================--
#-- Modificado por  : UEES - Veronica Alay
#-- Modificaci�n    : 01/06/2015
#-- Proyecto	    : [10182] - Factuaracion 12% a productos Roaming y adendum
#-- L�der Claro     : SIS Julia Rodas
#-- L�der PDS       : UEES - Lucy Caregua
#-- Motivo          : Se agrega variable para que obtenga en valor de la tarifa (IVA) para productos roaming .
#--=====================================================================================--
#-- Modificado por  : SUD - Laura Pe�a
#-- Modificaci�n    : 22/06/2015
#-- Proyecto	    : [10203] - Adicionales a la factura
#-- L�der Claro     : SIS Julia Rodas
#-- L�der PDS       : SUD - Arturo Gamboa
#-- Motivo          : Se a�ade informacion para mostrar la fecha de emision, el numero administrador, y la fecha de activancion del feature/plan .
#--=====================================================================================--
# INCLUIR SUMATORIA DE TIEMPO PARA ROAMING LLAMADAS #
#--=====================================================================================--
#-- Modificado por  : UEES - Veronica Alay
#-- Modificaci�n    : 26/06/2015
#-- Proyecto	    : [10182] - Factuaracion 12% a productos Roaming y adendum
#-- L�der Claro     : SIS Julia Rodas
#-- L�der PDS       : UEES - Lucy Caregua
#-- Motivo          : Se agrega calculo para presentar la sumatoria de segundos en la factura 
#--=====================================================================================--
#-- Modificaci�n    : 29/06/2015
#-- Proyecto	    : [10203] - Adicionales a la factura
#-- L�der Claro     : SIS Julia Rodas
#-- L�der PDS       : SUD - Arturo Gamboa
#-- Motivo          : Se a�ade validacion para mostrar descripcion de resumen etiquetas.
#--=====================================================================================--
# Modificado por    : SUD Vanessa Gutierrez G.
# Proyecto			: [10179] Nuevos ciclos de Facturaci�n DTH
# Lider Conecel     : SIS Paola Carvajal
# Objetivo			: Cambio de rutas de archivos .DAT y .TXT
#--=====================================================================================--
#===============================================================================================#
#-- Proyecto		     :	 [10861] - Impuestos ICE
#-- Modificado por		 :   SUD Evelyn Gonz�lez
#-- Fecha de modificacion:	 22/04/2016
#-- Lider Claro		     :   Ing. Ma. Elizabeth Estev�z
#-- Lider PDS		     :	 SUD Cristhian Acosta
#-- Descripcion		     :   Se agrega el Impuesto ICE para Clientes VOZ en la Factura.
#===============================================================================================#
%MAPEOGeneral;
%MAPEOSuma;
%MAPEOSumaDetalles;
%MAPEOTotales;
%MAPEOImprime;
%MAPEOImprimeUno;
%MAPEODetalleLlamdas;
%ResumenCelular;
%ResumenCelularPicoNoPico;
%ConsumoCelular;
%EtiquetasResumenCelular;
%banderaUno;
%Graba_co_id;
%Resumen_Celular_Dos;
%Minutos;
%ResumenConsumos;
%VerificaNumber;

my %MAPEOmpuzotab;
my %MAPEOlegend;
my %MAPEOmpusntab;
my %MAPEOmpuzntab;
my %MAPEOcall_grouping;
my %MAPEOpaymenttype_all;
my %MAPEOservice_label;
my %MAPEOservice_label2;
#10182
my %MAPEOservice_labelR;
#10182
my %MAPEOmpulktmb;
my %MAPEOmpulktmbANT;
my %MAPEOrateoplan;
my %MAPEOServicesExt;
my %MAPEObgh_tariff_plan;
my %MAPEOImprimeDetLlamadas;
my %MAPEOImprimeConsumoCelular;
my %MAPEObgh_tariff_plan;
my %MAPEOtax_category;
my %MAPEOinfo_contr_text;
my %cuentas_largas;
my %MapeoSecuencias;
my %MapeoEtiquetasVoz; #3925
my %MapeoEtiquetasVozTipo; #3925
my %MAPEOCtasAdendum; #9321
my %MAPEOCtasSVAFecha; #9321
my %MAPEOCtasSVAPLAN; #9321
my %MAPEOCtasSVAFecha_CTA; #9321
my %MAPEOAbonadosAdmin; #10203

my $procesador=$ARGV[0];
my $ciclo=$ARGV[1];
my $ruta=$ARGV[2]."/";
my $tipo_ejecucion=$ARGV[3]; # Control [G]ruop - [C]ommit
my $cuenta_out_llam=$ARGV[4];
if( $#ARGV < 3 ) {
	  print "NO hay argumentos \n";
	  die "error $!";
}
$filempuzotab=$ruta."resources/mpuzotab.dat";
$filelegend=$ruta."resources/legend_doc1.dat";
$filempusntab=$ruta."resources/mpusntab.dat";
$filempuzntab=$ruta."resources/mpuzntab.dat";
$filecall_grouping=$ruta."resources/call_grouping_doc1.dat";
$filepaymenttype_all=$ruta."resources/paymenttype_all.dat";
$fileservice_label=$ruta."resources/service_label_doc1.dat";
$filempulktmb=$ruta."resources/mpulktmb.dat";
$filempulktmbANT=$ruta."resources/mpulktmbANT.dat";
$filerateplan=$ruta."resources/rateplan.dat";
$filebgh_tariff_plan=$ruta."resources/bgh_tariff_plan_doc1.dat";
$filetax_category=$ruta."resources/tax_category_doc1.dat";
$fileinfo_contr_text=$ruta."resources/info_contr_text.dat";
#CLS JCE [4697]
$fileServices_ext=$ruta."resources/services_ext.dat";
#FIN CLS JCE [4697]
#$filerubros=$ruta."resources/rubros.dat"; [3636] MGA Eliminacion de Rubro
$ruta_adicional=$ruta."adicionales";
$filename_pos_cuenta=$ruta_adicional."/PUNTOS_DOC1.ctl";
$filename_pos_cuenta_bulk=$ruta_adicional."/BULK_DOC1.ctl";
$archivo_sec=$ruta."PCE/";
$file_pto=$ruta_adicional."/DOC1_PUNTOS.dat";
$file_bulk=$ruta_adicional."/DOC1_BULK.dat";
$fecha=`date '+%Y-%m-%d-%H-%M-%S'`;
$fecha=&enceraEspacios($fecha);
$archivo_logs=$ruta."log/DOC1_".$procesador."_".$ciclo."_".$tipo_ejecucion."-".$fecha.".log";
$file_name_cuentas_largas=$ruta."cuentas_largas.dat";
$fileetiquetas_voz=$ruta."resources/etiquetas_voz.dat"; #3925
#JH
$filexcento_iva=$ruta."resources/excento_iva.dat";
#CLS_MGA
$fileno_etiquetas=$ruta."resources/etiqueta_no_impresa.dat";
$file_nuevorubro=$ruta."resources/nuevos_rubros.dat";
$file_adendum=$ruta."resources/abonados_adem_".$ciclo.".dat";#9321
$file_fechaSVA=$ruta."resources/abonados_fechaSVA_".$ciclo.".dat";#9321
$file_CtasPlan=$ruta."resources/abonados_CtasPlan_".$ciclo.".dat";#9321
$file_FeatCta=$ruta."resources/abonados_FeatCta_".$ciclo.".dat";#9321
$file_administrador=$ruta."resources/abonados_admin_".$ciclo.".dat";#10203

#SIS FMI-03-DIC-2007
$indexHashUnico = 0;

#Llamadas
$cont_calculo_mayor=0;
#10179
&valida_arch_dat;
#&grabo_informacion;
#&llama_tim;
#10179

#10179
sub valida_arch_dat{
	$ruta_dat_cel=$ruta."VOZ/dat/CICLO_".$ciclo;
	$ruta_ls_cel=$ruta_dat_cel."/20*_".$procesador."_VOZ.dat";
	$ruta_dat_dth=$ruta."DTH/dat/CICLO_".$ciclo;
	$ruta_ls_dth=$ruta_dat_dth."/20*_".$procesador."_DTH.dat";
	$procesarDTH = "";
	$procesarVOZ = "";

	if (! -d $ruta_dat_dth) {
		print "No existe ruta de archivos .DAT ".$ruta_dat_dth." para el producto DTH \n";
		$procesarDTH = "N";
    }
	else {
		print "Existe ruta de archivos .DAT ".$ruta_dat_dth." para el producto DTH \n";
		$procesarDTH = "S";
	}
	if (! -d $ruta_dat_cel) {
		print "No existe ruta de archivos .DAT ".$ruta_dat_cel." para el producto VOZ \n";
		$procesarVOZ = "N";
	}
	else{
		print "Existe ruta de archivos .DAT ".$ruta_dat_cel." para el producto VOZ \n";
		$procesarVOZ = "S";
	}

	if ($procesarVOZ eq "N" && $procesarDTH eq "N") {
		print "Sale del proceso por no existir rutas de archivos .DAT para ambos productos VOZ y DTH \n";
		die;
	}

	if ($procesarVOZ eq "S") {
		print "Se procesaran archivos .dat para el producto VOZ de la ruta ".$ruta_dat_cel." \n";
		&grabo_informacion;
		&llama_tim;
	}

	if ($procesarDTH eq "S") {
		print "Se procesaran archivos .dat para el producto DTH de la ruta ".$ruta_dat_dth." \n";
		system("perl obtiene_detalles_dth.pl @ARGV[0] @ARGV[1] @ARGV[2] @ARGV[3]");
	}

}
#10179

sub grabo_informacion{
	$line="";
    open(FILE_READ_TMP, $filempuzotab);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$MAPEOmpuzotab{$arreglo_lin[2]} =[$arreglo_lin[0],$arreglo_lin[1] ,$arreglo_lin[3]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempuzotab." \n";
		die;
	}
#           0                  1          2                          3              4            5            6
#select label_id||'|'|| servshdes||'|'||nvl(tzcode,1)||'|'||nvl(ttcode,1)||'|'||leg_id||'|'|| calltype||'|'||tipo from call_grouping_doc1
	$line="";
    open(FILE_READ_TMP, $filecall_grouping);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[6]);
#		print $arreglo_lin[1]." ".$arreglo_lin[5]." ".$arreglo_lin[2]." ".$arreglo_lin[3]." ".$val." ".$arreglo_lin[0]." ".$arreglo_lin[4]."\n";
		$MAPEOcall_grouping {$arreglo_lin[1],$arreglo_lin[5],$arreglo_lin[2], $arreglo_lin[3], $val} =[$arreglo_lin[0],$arreglo_lin[4]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filecall_grouping." \n";
		die;
	}

	$line="";
#select leg_id||'|'||sh_name||'|'||name from legend_doc1;
    open(FILE_READ_TMP, $filelegend);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOlegend{$arreglo_lin[0]} =[$arreglo_lin[1],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filelegend." \n";
		die;
	}

	$line="";
#select sncode ||'|'|| des ||'|'|| shdes ||'|'|| snind ||'|'|| rec_version ||'|'||billed_surcharge_ind ||'|'|| pde_implicit_ind from sysadm.mpusntab;
    open(FILE_READ_TMP, $filempusntab);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$MAPEOmpusntab{$arreglo_lin[2]} =[$arreglo_lin[0],$arreglo_lin[1] ];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempusntab." \n";
		die;
	}

	$line="";
#select zncode||'|'||des ||'|'|| shdes from mpuzntab;
    open(FILE_READ_TMP, $filempuzntab);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[2]);
#		print $val." ".$arreglo_lin[0]." ".$arreglo_lin[1]."\n";
		$MAPEOmpuzntab{$val}=[$arreglo_lin[0],$arreglo_lin[1]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempuzntab." \n";
		die;
	}

	$line="";
#select pay.payment_id ||'|'||pay.paymentcode||'|'||pay.paymentname datos from paymenttype_all pay
    open(FILE_READ_TMP, $filepaymenttype_all);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
#		print $arreglo_lin[0]." ".$arreglo_lin[1]." ".$val."\n";
		$MAPEOpaymenttype_all{$arreglo_lin[1]}=[$arreglo_lin[0],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filepaymenttype_all." \n";
		die;
	}

	$line="";
    #select label_id||'|'||des||'|'||taxinf||'|'||shdes||'|'||ctaps||'|'||codigo_sri from service_label_doc1
    open(FILE_READ_TMP, $fileservice_label);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
		$val=&enceraEspacios($arreglo_lin[4]); #ECO_CIMA
		#print "Service Label ".$arreglo_lin[0]." ".$arreglo_lin[1]." ".$val." ".$arreglo_lin[2]."\n";
		$MAPEOservice_label{$arreglo_lin[0]}=[$arreglo_lin[1],$arreglo_lin[3],$arreglo_lin[2],$arreglo_lin[5]]; #ECO_CIMA #8504 - Se agrega $arreglo_lin[5] C�digo del Servicio Rubro Llamadas para F.E.
		$MAPEOservice_label2{$arreglo_lin[1]}=[$val]; #ECO_CIMA
	    $MAPEOservice_labelR{$arreglo_lin[3]}=[$arreglo_lin[0]]; #10182

    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$fileservice_label." \n";
		die;
	}


	$line="";
    #select tmcode||'|'||  to_char(vsdate,'yyyymmdd')||'|'|| spcode||'|'||  sncode||'|'|| accserv_catcode from MPULKTMb a where vsdate in(select max(b.vsdate) from MPULKTMb b where b.tmcode=a.tmcode and b.sncode = a.sncode)
    open(FILE_READ_TMP, $filempulktmb);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[4]);
#		print $arreglo_lin[0]." ".$arreglo_lin[3]." ".$arreglo_lin[1]." ".$arreglo_lin[2]." ".$val."\n";
		$MAPEOmpulktmb{$arreglo_lin[0]." ".$arreglo_lin[3]}=[$arreglo_lin[1],$arreglo_lin[2],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempulktmb." \n";
		die;
	}

    #3097
	$line="";
    #select tmcode||'|'||  to_char(vsdate,'yyyymmdd')||'|'|| spcode||'|'||  sncode||'|'|| accserv_catcode from MPULKTMb a where vsdate in(select max(b.vsdate) from MPULKTMb b where b.tmcode=a.tmcode and b.sncode = a.sncode)
    open(FILE_READ_TMP, $filempulktmbANT);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[4]);
		$MAPEOmpulktmbANT{$arreglo_lin[0]." ".$arreglo_lin[3]}=[$arreglo_lin[1],$arreglo_lin[2],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filempulktmbANT." \n";
		die;
	}

	$line="";
	#select tmcode||'|'|| des||'|'|| shdes from rateplan
    open(FILE_READ_TMP, $filerateplan);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOrateplan{$val}=[$arreglo_lin[0],$arreglo_lin[1]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filerateplan." \n";
		die;
	}

	$line="";
	#select tmcode||'|'||shdes||'|'||firstname||'|'||secondname||'|'||porder ||'|'||plan_type||'|'||tax_code_iva ||'|'||tax_code_ice from bgh_tariff_plan_doc1 order by tmcode,shdes, plan_type, porder
    open(FILE_READ_TMP, $filebgh_tariff_plan);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[7]);
#		print $arreglo_lin[1]." ".$arreglo_lin[4]." ".$arreglo_lin[0]." ".$val." ".$arreglo_lin[2]." ".$arreglo_lin[3]."\n";
		$MAPEObgh_tariff_plan{$arreglo_lin[1],$arreglo_lin[4]}=[$arreglo_lin[0],$arreglo_lin[5],$arreglo_lin[2],$arreglo_lin[3],$arreglo_lin[6],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filebgh_tariff_plan." \n";
		die;
	}

	$line="";
	#select i.taxcat_id||'|'||i.taxcat_name||'|'|| i.taxrate from tax_category i
	open(FILE_READ_TMP, $filetax_category);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
		$MAPEOtax_category{$arreglo_lin[0]}=[$arreglo_lin[1],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado ".$filetax_category." \n";
		die;
	}

	$line="";
	#select co_id||'|'||text01||'|'||text02||'|'||text03 from info_contr_text;
    # co_id= # de Contrato co_id, Text01= # de la C�dula o RUC, Text02= Nombre, Text03= #Ciclo
	open(FILE_READ_TMP, $fileinfo_contr_text);
	while (<FILE_READ_TMP>) {
		$line=$_;
	    @arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[3]);
		#[5099] MGA Se quita el ciclo por solicitud de Billing
		#if ($val eq $ciclo) {
        	$MAPEOinfo_contr_text{$arreglo_lin[0]}=[$arreglo_lin[1],$arreglo_lin[2]];
		#}
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$fileinfo_contr_text." \n";
		die;
	}
	
    #JH
	$line="";
	#Clientes excentos de IVA 
	#select y.custcode from customer_all y, customer_tax_exemption x where y.customer_id = x.customer_id and x.exempt_status = 'A';
	open(FILE_READ_TMP, $filexcento_iva);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$Cuentas_Excentas{$arreglo_lin[0]}="S";
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$filexcento_iva." \n";
		die;
	}

    #CLS_MGA
	$line="";
	#Etiquetas no presentadas
	open(FILE_READ_TMP, $fileno_etiquetas);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$No_etiquetas{$arreglo_lin[0]}=[$arreglo_lin[0]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Ciclo ".$ciclo." el Archivo ".$fileno_etiquetas." \n";
		die;
	}

    $line="";
	#Nuevos Rubros
	open(FILE_READ_TMP, $file_nuevorubro);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		chomp($arreglo_lin[0]);
	    $val=&enceraEspacios($arreglo_lin[0]);
       	$Nuevo_rubro{$arreglo_lin[0]}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$arreglo_lin[3]];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Ciclo ".$ciclo." el Archivo ".$file_nuevorubro." \n";
		die;
	}

   $line="";


    $line="";
	# Se encuentra informaci�n sobre la etiqueta de Voz Clave la Descripcion [3925]
	# Llamadas Porta a Porta|00000002|PP|1
	open(FILE_READ_TMP, $fileetiquetas_voz);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[3]);
       	$MapeoEtiquetasVoz{$arreglo_lin[0]}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$val];
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$filerubros." \n";
		die;
	}

    $line="";
	# Se encuentra informaci�n sobre la etiqueta de Voz con Clave el Tipo [3925]
	# Llamadas Porta a Porta|00000002|PP|1
	open(FILE_READ_TMP, $fileetiquetas_voz);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[3]);
       	$MapeoEtiquetasVozTipo{$arreglo_lin[2]}=[$arreglo_lin[2],$arreglo_lin[0],$val];
		$cont_calculo_mayor=$cont_calculo_mayor+1;
    }
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$filerubros." \n";
		die;
	}
	# [4697] CLS JCE se adiciona Archivo de Config de Servicios Externos.
	open(FILE_READ_TMP, $fileServices_ext);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
	    $val=&enceraEspacios($arreglo_lin[2]);
       	$MAPEOServicesExt{$val}=[$arreglo_lin[0],$arreglo_lin[1],$val];
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$fileServices_ext." \n";
		die;
	}
$line="";
$valor="";
    #FILE_ADENDUM - 9321
open(FILE_READ_TMP, $file_adendum);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		#$index_adendum=&enceraEspacios($arreglo_lin[1]);
       	#$MAPEOCtasAdendum{$index_adendum}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$arreglo_lin[3],$arreglo_lin[4],$arreglo_lin[5],&enceraEspacios($arreglo_lin[6])];
	    $index_adendum=$arreglo_lin[1];
		$valor=&enceraEspacios($arreglo_lin[6]);
		$MAPEOCtasAdendum{$index_adendum}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$arreglo_lin[3],$arreglo_lin[4],$arreglo_lin[5],$valor];
	}
	close FILE_READ_TMP;

$line="";
$index_FechaSVA1="";
$index_FechaSVA2="";
#FILE_ADENDUM - 9321
$feature_num="";
open(FILE_READ_TMP, $file_fechaSVA);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		#$index_FechaSVA1=&enceraEspacios($arreglo_lin[1]);
        #$index_FechaSVA2=&enceraEspacios($arreglo_lin[3]);
       	$index_FechaSVA1=&enceraEspacios($arreglo_lin[1]);  #TELEFONO
        $index_FechaSVA2=&enceraEspacios($arreglo_lin[3]);  #SHDES
		$feature_num=&enceraEspacios($arreglo_lin[2]."|".$arreglo_lin[4]);
#        $MAPEOCtasSVAFecha{$index_FechaSVA1,$index_FechaSVA2}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$arreglo_lin[3]];
		$MAPEOCtasSVAFecha{$index_FechaSVA1,$index_FechaSVA2}=[$feature_num]; #LPE
	}
	close FILE_READ_TMP;
$line="";
$index_Telefono="";
$index_Plan="";
#FILE_CtasPlan - 9321
open(FILE_READ_TMP, $file_CtasPlan);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		#$index_Telefono=&enceraEspacios($arreglo_lin[1]);
		#$index_Plan=&enceraEspacios($arreglo_lin[3]);
         $index_Telefono=&enceraEspacios($arreglo_lin[1]);
		 $index_Plan=&enceraEspacios($arreglo_lin[3]);       	
         $MAPEOCtasSVAPLAN{$index_Telefono,$index_Plan}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$arreglo_lin[3]];
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_CtasPlan." \n";
	
	}

$line="";
$index_CtaBSCS="";
$index_Plan="";
$feature_num="";
  #file_FeatCta - 9321
open(FILE_READ_TMP, $file_FeatCta);
	while (<FILE_READ_TMP>) {
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
		#$index_CtaBSCS=&enceraEspacios($arreglo_lin[0]);
		#$index_Plan=&enceraEspacios($arreglo_lin[3]);
        $index_CtaBSCS=&enceraEspacios($arreglo_lin[0]);
		$index_Plan=&enceraEspacios($arreglo_lin[3]);
#		$feature_num=&enceraEspacios($arreglo_lin[2]."|".$arreglo_lin[3]."|".$arreglo_lin[4]);
       	$MAPEOCtasSVAFecha_CTA{$index_CtaBSCS,$index_Plan}=[$arreglo_lin[0],$arreglo_lin[1],$arreglo_lin[2],$arreglo_lin[3]];
#       	$MAPEOCtasSVAFecha_CTA{$index_CtaBSCS,$index_Plan}=[$feature_num]; #LPE
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_FeatCta." \n";
	
	}

$line="";
$index_CtaBSCS="";
$index_Plan="";
  #file_administrador - 10203
open(FILE_READ_TMP, $file_administrador);
	while (<FILE_READ_TMP>) {	
		$line=$_;
		@arreglo_lin=split(/\|/,$line);
        $index_CtaBSCS=&enceraEspacios($arreglo_lin[0]);		
       	$MAPEOAbonadosAdmin{$index_CtaBSCS}=[$index_CtaBSCS,$arreglo_lin[1]];
        $index_CtaBSCS="";		
	}
	close FILE_READ_TMP;
	if ($line eq "") {
		print "Error no se encuentra configurado para el Cilo ".$ciclo." el Archivo ".$file_administrador." \n";
	}
}

sub llama_tim{ 
#10179 cambio de rutas en carpetas procesados
#$carpeta_ciclo="procesados/CICLO_".$ciclo;
$carpeta_ciclo="VOZ/procesados/CICLO_".$ciclo;
#10179 cambio de rutas en carpetas procesados
$file_out=$ruta.$carpeta_ciclo."/output_doc1_".$procesador."_".$ciclo.".txt";
$file_out_e=$ruta.$carpeta_ciclo."/output_doc1_".$procesador."_".$ciclo."_e.txt"; #5328 nombre del archivo de fact. Electr�nica con la ruta
$file_out_2=$ruta.$carpeta_ciclo."/output_doc1_".$procesador."_".$ciclo.".out";
$file_out_3=$ruta.$carpeta_ciclo."/output_doc1_".$procesador."_".$ciclo.".det";

#10179 cambio de rutas en carpetas de archivos dat
#$ruta_dat=$ruta."dat/CICLO_".$ciclo;
$ruta_dat=$ruta."VOZ/dat/CICLO_".$ciclo;
#10179 cambio de rutas en carpetas de archivos dat
#10179 cambio en nombre de archivo .DAT
#$ruta_ls=$ruta_dat."/20*_".$procesador.".dat";
$ruta_ls=$ruta_dat."/20*_".$procesador."_VOZ.dat";
#10179 cambio en nombre de archivo .DAT
# Inicio NCA 10/07/2013 creacion de carpeta de inconsistencias

$carpeta_inc=$carpeta_ciclo."/inconsistencias";
$crea_carpeta="mkdir $carpeta_inc";
if (! -d $carpeta_inc) {
	my $resultado= `$crea_carpeta`; 	
}
$file_out_4=$carpeta_inc."/output_doc1_inconsistencia_".$procesador."_".$ciclo.".det";	# NCA 21/06/2013 archivo de inconsistencias en datos nombre, direccion, CI
# Fin NCA 10/07/2013 creacion de carpeta de inconsistencias

# --- Cuentas Largas --- #
open(FILE_READ_TMP, $file_name_cuentas_largas);
while (<FILE_READ_TMP>) {
	$line=&enceraEspacios($_);
	$cuentas_largas{$line} =[$line];
}
close FILE_READ_TMP;
# -----------------------#

&leerArchivoPuntos;
&leerArchivoBulk;
#ini 5328
#Se comento para controlar que no se creen archivos vacios
#La creaci�n de los archivos txt se maneja en modo escritura - adici�n
#tanto para fact. Electr�nica como F�sica
#open(FILE_WRITE,"> $file_out");#archivo destino  #5328
#open(FILE_WRITE_E,"> $file_out_e");#5328 archivo destino factura electronica  #5328
#fin 5328
open(FILE_WRITE_2,"> $file_out_2");#archivo destino Cuentas no Procesado por Inconsistencia
open(FILE_WRITE_DET,"> $file_out_3");#Detalle de las Cuentas no Procesado por Inconsistencia
open(FILE_INCONSISTENCIA,"> $file_out_4");# Archivo de inconsistencias en nombres, direccion, CI
open(FILE_WRITE_LOGS,"> $archivo_logs");#archivo logs
$fecha=&enceraEspacios(`date '+%Y-%m-%d-%H-%M-%S'`);
$archivo_sec=$archivo_sec.$procesador."_".$ciclo."-".$tipo_ejecucion."-".$fecha.".dat";
open(FILE_SECUENCIAS, "> $archivo_sec");#Archivo Secuencias

$fecha=`date '+%Y/%m/%d %H:%M:%S'`;
print $fecha."\n";
print  FILE_WRITE_LOGS $fecha."\n";
open LS, "ls -1 $ruta_ls|" or die "Error llama_tim No se encuentra archivos ls -1 $ruta_ls  $!";
while (<LS>) {
$file_ls=&enceraEspacios($_);
$file_ls=~ /(.*)\/([^\/]*)$/;
$path=$1;
$archivo=$2;

$archivo_ctl=$ruta."log/timm_doc1.ctl";
$comando="cat $archivo_ctl";	
my $res_control= `$comando`;
$res_control=&enceraEspacios($res_control);

if ($res_control eq "1") {
	print "Proceso Finalizado Manualmente ".$procesador."\n";
	print FILE_WRITE_LOGS "Proceso Finalizado Manualmente ".$procesador."\n";
	exit;	
}

undef(%MAPEOGeneral);
undef(%MAPEOSuma);
undef(%MAPEOTotales);
undef(%MAPEOImprime);
undef(%MAPEOImprimeUno);
undef(%MAPEODetalleLlamdas);
undef(%ResumenCelular);
undef(%ResumenCelularPicoNoPico);
undef(%ConsumoCelular);
undef(%EtiquetasResumenCelular);
undef(%banderaUno);
undef(%MAPEOSumaDetalles);
undef(%Graba_co_id);
undef(%Resumen_Celular_Dos);
undef(%Minutos);
undef(%ResumenConsumos);
undef(%VerificaNumber);
$des_plan_tele="";
$plan_tele1="";
$banderaUno="0";
$cont_calculo=5;
$ban_timm1_completo="0";
$ban_cedula_no_impresa="0";
$band_no_imprime_cuenta="0";
$band_numero_no_existe="0";
$ejecuta_cuentas_largas="0";
$bandera_type_uno="0";
$bandera_type_cero="0";
$band_electronica="0"; #5328 para controlar archivos facturas electronicas 
$band_correo="0";#5328 validar si existe correo electronico
@arreglo=split(/\_/,$archivo);
$customer_id_cuentas_largas=$arreglo[1];
if ($cuentas_largas{$customer_id_cuentas_largas}[0] == $customer_id_cuentas_largas ) {
	print "Cuenta Especial"."\n";
	$ejecuta_cuentas_largas="1";
}
&tim_type_seis($file_ls);

$comando="compress $file";	
my $result= `$comando`; 

$/="\n";

}	
close FILE_WRITE; # .txt
close FILE_WRITE_E; # _E.txt 5328
close FILE_INCONSISTENCIA; # .det de caracteres especiales
close FILE_WRITE_2; # .out
close FILE_WRITE_DET; # .det
close FILE_SECUENCIAS; # Archivo de Secuencias
$fecha=`date '+%Y/%m/%d %H:%M:%S'`;
print $fecha."\n";
print FILE_WRITE_LOGS $fecha."\n";
close FILE_WRITE_LOGS;
print "Fin del Proceso ".$procesador."\n";
}

sub graba_datos($){
my $fileHandle=$_[0];
foreach (sort(keys %MAPEOImprimeUno)) {	
	print $fileHandle $MAPEOImprimeUno{$_}[0]."\n";
	#print  $MAPEOImprimeUno{$_}[0]."\n";
}
undef(%MAPEOImprimeUno);
if ($banderaUno eq "1") {

	foreach  (sort(keys %MAPEOImprimeConsumoCelular)) {
		print $fileHandle $MAPEOImprimeConsumoCelular{$_}[0]."\n";
	}
	undef(%MAPEOImprimeConsumoCelular);

 if ($ejecuta_cuentas_largas eq "1") {

	$/="\n";
	open(FILE_WRITE_3,"> temporal_cuentas.tmp");#archivo destino Cuentas no Procesado por Inconsistencia
	foreach  (keys %MAPEOImprime) {
		print FILE_WRITE_3 $_.",".$MAPEOImprime{$_}[0]."\n";
	}
	undef(%MAPEOImprime);

    open(INFILE_TMP, "< temporal_cuentas_larga.tmp") or print "Can't open temporal_cuentas_larga.tmp for temporal_cuentas_larga.tmp: $!\n";
	while (<INFILE_TMP>) {		
	@arreglo_num=split(/\,/,$_);
	$val=&enceraEspacios($arreglo_num[1]);
	@arreglo=split(/ /,$arreglo_num[0]);
	$co_id=$arreglo[0];
	$tipo=$Graba_co_id{$co_id}[1];
		if ($tipo eq "2") {
			print FILE_WRITE_3 $arreglo_num[0].",".$val."\n";
		}
	}
	close INFILE_TMP;	
	close FILE_WRITE_3;

	$comando="sort -t\\, -k1n,1n temporal_cuentas.tmp > sorter.tmp";
	my $var=`$comando`;

    open(INFILE_TMP, "< sorter.tmp") or print "Can't open temporal_cuentas_larga.tmp for temporal_cuentas_larga.tmp: $!\n";
	while (<INFILE_TMP>) {		
	@arreglo_num=split(/\,/,$_);
	$val=&enceraEspacios($arreglo_num[1]);
	print $fileHandle $val."\n";

	}
	close INFILE_TMP;	

 } else { # else de Cuentas Especiales
	foreach  (sort(keys %MAPEOImprimeDetLlamadas)) {
		@arreglo=split(/ /,$_);
		$co_id=$arreglo[0];
		$tipo=$Graba_co_id{$co_id}[1];
		if ($tipo eq "2") {
			$MAPEOImprime{$_}=[$MAPEOImprimeDetLlamadas{$_}[0]];
		}
	}
	undef(%MAPEOImprimeDetLlamadas);

	foreach  (sort(keys %MAPEOImprime)) {
		print $fileHandle $MAPEOImprime{$_}[0]."\n";
	}
	undef(%MAPEOImprime);
 } # Fin de Cuentas Especiales

#Puntos Nuevo Formato de Factura 4505
#if ($cod_insert eq "*I*") {
#  print $fileHandle &obtieneInformacionPuntos($account_no);
#}

if ($cod_bulk eq "1") {
  print $fileHandle &obtieneInformacionBulk($account_no)."\n";
}

}

}


sub tim_type_seis{	
$key_llamadas="43100";
$file=$_[0];
$file1=$_[0];
if ($cuenta_out_llam eq "1") {
   $file1=$ruta_dat."/OUT.llam";
   $file1=$file1;
}
print $file1." ".$cuenta_out_llam." ".$ruta_dat."\n";
open(FILE_READ,$file1);
$/="XCD";
$cont=0;
$call_timeR="";
$feha_horaR="";
$des_cgiR="";
$destinoR="";
$num_llamadoR="";
$tipo_llamR="";
$tipo_des_llamR="";
$timeR="";
$valor_originalR="";
$valor_originalAireR="";
$valueR=0;
$valueInterR=0;
$valueInterROriginal=0;
$servshdesR="";
$zonaR="";
$tariff_timeR="";
$tariff_modelR="";
$datos="";
$bandera_xcd="0";
$co_id="";
$co_idR="";
if ($ejecuta_cuentas_largas eq "1") {
	open(FILE_WRITE_3,"> temporal_cuentas_larga.tmp");#archivo destino Cuentas no Procesado por Inconsistencia
}

while(<FILE_READ>){ 
 $line=$_;
 $xfu=0;
 $seg_xfu=0;
 $seg_xfu_2=0;
if ($. == 1) {
 if (  /IMD\+\+8\+CO\:\:\:\:([^\:\']*)\'\n\n/ ) { #if #1
  $co_id=$1;
 }
}

if ($. != 1) {
    $xfu=0;
    #Obtengo unidades libres #
	while  (/XFU[^\n]+\+([^\:]+)\:([^\:]+)\:.+\+([^\:]+)\:USD[^\n]+\n/g)  {
		$seg_xfu+=$1;
		$seg_xfu_2+=$2;
		$xfu += $3; # Valor Unidades Libres	
	}

	@arreglo=split(/\+/,$line);

	# SIS RCA, Febrero 8,2007
	# Consideraci�n: Para Family and friends, un campo adicional.
	if ( $arreglo[38] eq "FA?"  ) { 
		splice(@arreglo, 40,1);
		}
		
    $main_number=$arreglo[1];
	$sub_number=$arreglo[2];
	$customer_id=$arreglo[3];
	$call_time=$arreglo[5];
	$value=&enceraEspacios($arreglo[16]); # Valor de la Llamada
	$tariff_model=&enceraEspacios($arreglo[22]);
	$servshdes=&enceraEspacios($arreglo[23]);
	$services=&enceraEspacios($arreglo[24]);
	$tariff_time=&enceraEspacios($arreglo[25]);
	$tipo_base_ic=&enceraEspacios($arreglo[33]);
	$zona=&enceraEspacios($arreglo[26]);

	#if ($zona eq "ZPG29" || $zona eq "ZPG30" || $zona eq "") { print "\n\n$line\n";
	#if ($. <= 7) { print "\n\n$line\n";
	#}

	#if ( $zona eq "ZDUMM" ) {
	#	next;
	#}

	$chargetype=&enceraEspacios($arreglo[7]);

	$number=&enceraEspacios($arreglo[42]);
    @arreglo_num=split(/\:/,$arreglo[52]);
	$num_llamado=&enceraEspacios($arreglo_num[0]);
	$destino=&enceraEspacios($arreglo_num[2]);

	#$cgi=&enceraEspacios($arreglo_num[3]);
	# SIS RCA, Febrero 8,2007
	# Ahora tomamos el CGI del CUA.
    $line =~ /CUA\+(\d+)/;
	$cgi = $1;

	($feha_hora1,$feha_hora)=&transforma_fecha($call_time);
	$des_cgi=&busca_cgi($cgi);
	$roaming_normal=&enceraEspacios($arreglo[46]);
	$des_roaming_normal=&enceraEspacios($arreglo[47]); #ECUPG

  	$time=&enceraEspacios($arreglo[9]); # Los Segundo Depende si la Llamada es Roaming o No
	if ($chargetype eq "I") {
    ($descripcion_ser,$tipo_llam,$sh_des)=&busca_descripcion_etiqueta($services,$zona,'BASE',$tariff_time,$tariff_model);

	} else {
	  if ($chargetype eq "R") {
		$time=&enceraEspacios($arreglo[12]);
		$tipo_llam="RI";
	  } else {
		$tipo_llam="EG";
	    @arreglo_num=split(/\:/,$arreglo[53]);
		$num_llamado=&enceraEspacios($arreglo_num[0]);
		
		#SIS RCA 22/mar/2007
		#Etiquetado para llamadas entrantes.
		$des_cgi = "";
		$destino = "CLARO";
	  }
	}

	if ($roaming_normal eq "V") { # Si es Roaming Clasifica la llamada 
		$time=&enceraEspacios($arreglo[12]);
		$tipo_llam="RI";
	}

	if ($tipo_base_ic eq "IC" && $sub_number eq "0") {
		$sub_number="1";
	}

#	print $sub_number." ".$co_idR." ".$call_time." ".$call_timeR." ".$feha_hora1." ".$feha_hora."\n";
    if ($sub_number eq "0") {
    	$tipo_des_llam=&busca_tipo_llamada($zona);
		$bandera_xcd="1";		
		if ( $main_number != 1 || $co_idR ne "" ) { 
			if ($datos ne "") {
				$call_type='BASE';
				if ($valueInterR gt 0) {
					$call_type='IC';
				}

				($descripcion_ser,$des,$sh_des,$taxinf)=&busca_descripcion_etiqueta($servicesR,$zonaR,$call_type,$tariff_timeR,$tariff_modelR,$roaming_normalR);


				if ($seg_xfuR ne $seg_xfuRInCompara) {
				    $duracionCobrada=($valor_originalAireR+$valueInterROriginal)-($valor_originalAireXFUR+$valueOriginalInterXfu);
					$costoLlamada=($valor_originalAireR+$valueInterROriginal)/$timeR;
					$segCobrados=$duracionCobrada/$costoLlamada;
					$segCobrados = sprintf("%d", $segCobrados);
					$seg_xfuR=$timeR-$segCobrados;
				}

	           #SIS FMI-03-DIC-2007
    	       #$MAPEODetalleLlamdas{$call_timeR." ".$num_llamadoR}=[$feha_horaR,$des_cgiR,$destinoR,$num_llamadoR,$tipo_llamR,$timeR,$valor_originalR,$valueInterR,$sh_des,$numberR,$co_idR,$taxinf,$seg_xfuR];	
			   $key_det=++$indexHashUnico;
               $MAPEODetalleLlamdas{$key_det}=[$feha_horaR,$des_cgiR,$destinoR,$num_llamadoR,$tipo_llamR,$timeR,$valor_originalR,$valueInterR,$sh_des,$numberR,$co_idR,$taxinf,$seg_xfuR,$call_timeR,($valor_originalAireXFUR+$valueOriginalInterXfu)];			   
			   &generar_detalles($key_det);
			   $timeRIn=0;

			}
		}
		
        $valor_originalAire=$value;
        $valor_originalAireXFU=$xfu;
        $valor_original=$value - $xfu;
    	$datos=$feha_hora."|".$des_cgi."|".$destino."|".$num_llamado."|".$tipo_llam."|".$tipo_des_llam."|".$time."|".$valor_original."|".$value1;

		$call_timeR=$call_time;
		$feha_horaR=$feha_hora;
		$des_cgiR=$des_cgi;
		$destinoR=$destino;
		$num_llamadoR=$num_llamado;
		$tipo_llamR=$tipo_llam;
		$tipo_des_llamR=$tipo_des_llam;
		$timeR=$time;
		$valor_originalR=$valor_original;
        $valor_originalAireR=$valor_originalAire;
		$valor_originalAireXFUR=$valor_originalAireXFU;
		$valueR=0;
		$valueInterR=0;
		$valueInterROriginal=0;
		$valueOriginalInterXfu=0;
		$valor_originalAireXFU=0;
		$servshdesR=$servshdes;
		$zonaR=$zona;
		$tariff_timeR=$tariff_time;
		$tariff_modelR=$tariff_model;
		$servicesR=$services;
		$numberR=$number;
		$co_idR=$co_id;
		$roaming_normalR=$roaming_normal;
		$seg_xfuRInCompara=$seg_xfu;
		$seg_xfuR=$seg_xfu;
    } else {
		$valueOriginalInterXfu=$xfu;
		$valueInterROriginal=$value;
		$valueInterR=$value - $xfu;
		$timeRIn=$time;
		$seg_xfuRInCompara=$seg_xfu;
	}

	 if (  /IMD\+\+8\+CO\:\:\:\:([^\:\']*)\'\n\n/ ) { #if #1
	  $co_id=$1;
	 }
	 
#INICIO 10182 VA
	 $producto_roam=&enceraEspacios($arreglo[22]);
	 if ($producto_roam eq "ROTIN")
	{
	$timeRoa=&enceraEspacios($arreglo[12]);	
	$valor_total_roam=$valor_total_roam+$timeRoa;
	}	
	 
}
}
#print $datos."\n";
$tiempo_final_roaming=&formatea_seg_minutos($valor_total_roam);
#print "TIEMPO ROAMING en segundos".$valor_total_roam."\n";
$valor_total_roam=0;
#FIN 10182 VA

$call_type='BASE';
if ($valueInterR gt 0) {
	$call_type='IC';
}
if ($bandera_xcd != "0") {
if ($sub_number eq "1") {
    $seg_xfu=$seg_xfuR;
}
($descripcion_ser,$des,$sh_des,$taxinf)=&busca_descripcion_etiqueta($services,$zona,$call_type,$tariff_time,$tariff_model,$roaming_normal);

#print "\nInput: $services,$zona,$call_type,$tariff_time,$tariff_model,$roaming_normal \n";
#print "Output: $descripcion_ser,$des,$sh_des,$taxinf \n";
#print "\n"."Ultimo ". $seg_xfu." ".$seg_xfuRInCompara."\n";
if ($seg_xfu ne $seg_xfuRInCompara) {
	$duracionCobrada=($valor_originalAireR+$valueInterROriginal)-($valor_originalAireXFUR+$valueOriginalInterXfu);
	$costoLlamada=($valor_originalAireR+$valueInterROriginal)/$timeR;
	$segCobrados=$duracionCobrada/$costoLlamada;
	$segCobrados = sprintf("%d", $segCobrados);
	$seg_xfuR=$time-$segCobrados;
}

#SIS FMI-03-DIC-2007
#$MAPEODetalleLlamdas{$call_time." ".$num_llamado}=[$feha_hora,$des_cgi,$destino,$num_llamado,$tipo_llam,$time,$valor_original,$valueInterR,$sh_des,$number,$co_id,$taxinf,$seg_xfu];
$key_det=$index++;
$MAPEODetalleLlamdas{$key_det}=[$feha_hora,$des_cgi,$destino,$num_llamado,$tipo_llam,$time,$valor_original,$valueInterR,$sh_des,$number,$co_id,$taxinf,$seg_xfuR,$call_time,($valor_originalAireXFUR+$valueOriginalInterXfu)];
&generar_detalles($key_det);
}

close FILE_READ;
$verifica_number="";
$verifica_co_id="";

if ($bandera_xcd != "0") {

	foreach (keys %MAPEOSumaDetalles) {
	    if ($ejecuta_cuentas_largas eq "1") {
			if ($_ eq "676493" || $_ eq "676494" || $_ eq "1106627" ) { #EBC
				print FILE_WRITE_3 $_." 43000 1111111111,"."43000|".$MAPEOSumaDetalles{$_}[4]."|".$MAPEOSumaDetalles{$_}[5]."|1|0|\n";
			} else {
				print FILE_WRITE_3 $_." 43000 1111111111,"."43000|".$MAPEOSumaDetalles{$_}[4]."|".$MAPEOSumaDetalles{$_}[5]."|0|0|\n";
			}
			print FILE_WRITE_3 $_." 43200 9999999999,"."43200|".$MAPEOSumaDetalles{$_}[0]."|".&formatea_numeros_detalles($MAPEOSumaDetalles{$_}[1])."|".&formatea_numeros_detalles($MAPEOSumaDetalles{$_}[2])."|".&formatea_numeros_detalles($MAPEOSumaDetalles{$_}[3])."|\n";
	        close FILE_WRITE_3;
		} else {
			$MAPEOImprimeDetLlamadas{$_." 43000 1111111111"}=["43000|".$MAPEOSumaDetalles{$_}[4]."|".$MAPEOSumaDetalles{$_}[5]."|0|0|"];
			$MAPEOImprimeDetLlamadas{$_." 43200 9999999999"}=["43200|".$MAPEOSumaDetalles{$_}[0]."|".&formatea_numeros_detalles($MAPEOSumaDetalles{$_}[1])."|".&formatea_numeros_detalles($MAPEOSumaDetalles{$_}[2])."|".&formatea_numeros_detalles($MAPEOSumaDetalles{$_}[3])."|"];
		}
	}
	undef(%MAPEOSumaDetalles); 


}
#  Informaci�n del Tim II #
undef(%MAPEOImprime);
&tim_type_dos;
if ($band_correo eq "0" && $band_no_imprime_cuenta eq "0" && $ban_timm1_completo eq "1" && $ban_cedula_no_impresa eq "1" && $band_numero_no_existe eq "0" && $bandera_type_uno eq "0" && $num_secuencia ne "000-000-0000000" && $bandera_type_cero ne "1" && $band_ci eq "0") {# 5328 aumenta $ban_correo

	
	#ini 5328 
	#Maneja archivos de fact. electr�nica y f�sica por separado
	if ($band_electronica eq "4")
	{ 
      open(FILE_WRITE_E,">> $file_out_e");#5328 archivo destino factura electronica  modo escritura-adicion
	  $fileGlob = \*FILE_WRITE_E;	 
	}
	else
	{
	  open(FILE_WRITE,">> $file_out"); #5328 archivo destino factura fisica modo escritura-adicion
      $fileGlob = \*FILE_WRITE;
	}
	#fin 5328
	&graba_datos($fileGlob);
} else {
	if ($banderaUno eq "1") {
		if ($band_no_imprime_cuenta ne "0") {
		 print FILE_WRITE_DET "001|Plan No Encontrado|".$account_no."|".$file_ls."\n";
		}
		if ($ban_timm1_completo ne "1") {
		 print FILE_WRITE_DET "002|TIMM 1 No es Correcto|".$account_no."|".$file_ls."\n";
		}

		if ($ban_cedula_no_impresa ne "1") {
		 print FILE_WRITE_DET "003|C�dula No Impresa|".$account_no."|".$file_ls."\n";
		}

		if ($band_numero_no_existe ne "0") {
		 print FILE_WRITE_DET "004|Numero no encontrado 59399999999|".$account_no."|".$file_ls."\n";
		}

		if ($bandera_type_uno ne "0") {
		 print FILE_WRITE_DET "005|Descripcion en Blanco|".$account_no."|".$file_ls."\n";
		}

		if ($num_secuencia eq "000-000-0000000") {
		 print FILE_WRITE_DET "006|Numero de Secuencia|".$account_no."|".$file_ls."\n";
		}

		if ($bandera_type_cero eq "1") {
		 print FILE_WRITE_DET "007|Saldos de Factura no Cuadran |".$account_no."|".$file_ls."\n";
		}
		#ini 5328
		if ($band_correo eq "1") {
		 print FILE_WRITE_DET "008|Correo No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		if ($band_correo eq "2") {
		 print FILE_WRITE_DET "009|Telefono del Cliente No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		#fin 5328
		#ini - 8504 - SUD CAC - control de errores
		if ($band_correo eq "3") {
		 print FILE_WRITE_DET "010|Tipo de Identificaci�n del Cliente No encontrado para Factura Electronica|".$account_no."|".$file_ls."\n";
		}
		#fin - 8504 - SUD CAC
		# Inicio NCA 21/06/2013 validacion longitud de la identificacion
		if ($band_ci ne "0") {
		 print FILE_WRITE_DET "011|La longitud de la identificacion no es correcta para el tipo de identificacion|".$account_no."|".$file_ls."\n";
		}
		# Fin NCA 21/06/2013 		
	}
	my $fileGlob = \*FILE_WRITE_2;
	&graba_datos($fileGlob);
}

}

sub generar_detalles{
	  $clave_det=$_[0];
	  $verifica_number=$MAPEODetalleLlamdas{$clave_det}[9];
	  $verifica_co_id=$MAPEODetalleLlamdas{$clave_det}[10];
	  &graba_co_id($verifica_co_id,$verifica_number,"6");
	  $clave=$verifica_co_id;

	  $total=$MAPEODetalleLlamdas{$clave_det}[6]+$MAPEODetalleLlamdas{$clave_det}[7];
	  $total_segundos=$MAPEOSumaDetalles{$clave}[0]+$MAPEODetalleLlamdas{$clave_det}[5];
	  #$total_aire=$MAPEOSumaDetalles{$clave}[1]+$MAPEODetalleLlamdas{$clave_det}[6]; [3311]
	  #$total_interconexion=$MAPEOSumaDetalles{$clave}[2]+$MAPEODetalleLlamdas{$clave_det}[7]; [3311]
      # Cambio del Formato del Detalle de Llamadas
	  $total_incluido=$MAPEOSumaDetalles{$clave}[1]+$MAPEODetalleLlamdas{$clave_det}[14]; #[3311]
	  $total_excedente=$MAPEOSumaDetalles{$clave}[2]+$total; #[3311]


	  #$total_general=$MAPEOSumaDetalles{$clave}[3]+$total;#[3311]
	  $total_general=$MAPEOSumaDetalles{$clave}[3]+$total+$MAPEODetalleLlamdas{$clave_det}[14];#[3311]

	  #print $MAPEODetalleLlamdas{$clave_det}[5]."|"; #debug
	  $seg_LLamadas=$MAPEODetalleLlamdas{$clave_det}[5];
	  $seg_LLamadasG=$MAPEODetalleLlamdas{$clave_det}[12];

#	  $MAPEOSumaDetalles{$clave}=[$total_segundos,$total_aire,$total_interconexion,$total_general,$verifica_number,$verifica_co_id];#[3311]
	  $MAPEOSumaDetalles{$clave}=[$total_segundos,$total_incluido,$total_excedente,$total_general,$verifica_number,$verifica_co_id];

	  $totalResumen=$total; #SIS PCA 3097

	  $total_exedente_incluido=&formatea_numeros_detalles($MAPEODetalleLlamdas{$clave_det}[14]+$total);
	  $total=&formatea_numeros_detalles($total);
	  $valor_incluido=&formatea_numeros_detalles($MAPEODetalleLlamdas{$clave_det}[14]);
	  $valor=&formatea_numeros_detalles($MAPEODetalleLlamdas{$clave_det}[6]);
	  $valorInter=&formatea_numeros_detalles($MAPEODetalleLlamdas{$clave_det}[7]);

	  #$total_aire=&formatea_numeros_detalles($total_aire); #[3311]
	  #$total_interconexion=&formatea_numeros_detalles($total_interconexion);  #[3311]
	  #$total_general=&formatea_numeros_detalles($total_general); #[3311]
	  $valor_segundos=$MAPEODetalleLlamdas{$clave_det}[5];
	  if ($MAPEODetalleLlamdas{$clave_det}[5] eq "0") {
		  $valor_segundos="";
	  }


      #SIS PCA -- 3097      # Eliminaci�n de ICE  #3097
      $clave_ResumenCelularPicoNoPico=$verifica_co_id."|".$MAPEODetalleLlamdas{$clave_det}[4];
      $valor_Resumen_IVA=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[4];
      $valor_Resumen_IVAICE=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[5];
      $segundos_Resumen_IVA=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[6];
      $segundosG_Resumen_IVA=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[7];
      $segundos_Resumen_IVAICE=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[8];
      $segundosG_Resumen_IVAICE=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[9];

  	  $call_time_llamada=$MAPEODetalleLlamdas{$clave_det}[13];
      $tipo_impuesto="IVA-ICE";
	  $val_IVAICE=0;
	  $val_IVA=0;
	  $val_segIVA=0;
  	  $val_segIVAICE=0;
	  $val_segGIVA=0;
  	  $val_segGIVAICE=0;
	  if ($call_time_llamada ge "20080101000000") {
    	$tipo_impuesto="IVA";
        $valor_Resumen_IVA=$valor_Resumen_IVA+$totalResumen;
        $segundos_Resumen_IVA=$segundos_Resumen_IVA+$seg_LLamadas;
        $segundosG_Resumen_IVA=$segundosG_Resumen_IVA+$seg_LLamadasG;
   	    $val_IVA=$totalResumen;
    	$val_segIVA=$seg_LLamadas;
	    $val_segGIVA=$seg_LLamadasG;
	  } else {
        $valor_Resumen_IVAICE=$valor_Resumen_IVAICE+$totalResumen;		
        $segundos_Resumen_IVAICE=$segundos_Resumen_IVAICE+$seg_LLamadas;
	    $segundosG_Resumen_IVAICE=$segundosG_Resumen_IVAICE+$seg_LLamadasG;
   	    $val_IVAICE=$totalResumen;
   	    $val_segIVAICE=$seg_LLamadas;
  	    $val_segGIVAICE=$seg_LLamadasG;
	  }

  	  #SIS RCA -- 22 marzo 2007.
	  if ($MAPEODetalleLlamdas{$clave_det}[4] eq "IN1" || $MAPEODetalleLlamdas{$clave_det}[4] eq "IN2") {
			$ValorParaMostrarTipo = "IN";
	  } else { $ValorParaMostrarTipo = $MAPEODetalleLlamdas{$clave_det}[4]  }

  
	  if ($ejecuta_cuentas_largas eq "1") {	  
        #SIS FMI-03-DIC-2007
 		#print FILE_WRITE_3 $verifica_co_id." ".$key_llamadas." ".$_.",".$key_llamadas."|".$MAPEODetalleLlamdas{$_}[0]."|".$MAPEODetalleLlamdas{$_}[1]."|".$MAPEODetalleLlamdas{$_}[2]."|".$MAPEODetalleLlamdas{$_}[3]."|".$MAPEODetalleLlamdas{$_}[4]."|".$valor_segundos."|".$valor."|".$valorInter."|".$total."|".$MAPEODetalleLlamdas{$_}[12]."|"."\n";
# 		 print FILE_WRITE_3 $verifica_co_id." ".$key_llamadas." ".$MAPEODetalleLlamdas{$clave_det}[13].$_.",".$key_llamadas."|".$MAPEODetalleLlamdas{$clave_det}[0]."|".$MAPEODetalleLlamdas{$clave_det}[1]."|".$MAPEODetalleLlamdas{$clave_det}[2]."|".$MAPEODetalleLlamdas{$clave_det}[3]."|".$MAPEODetalleLlamdas{$clave_det}[4]."|".$valor_segundos."|".$valor."|".$valorInter."|".$total."|".$MAPEODetalleLlamdas{$clave_det}[12]."|".$val_IVA."|".$val_IVAICE."|"."\n"; 		
#         print FILE_WRITE_3 $verifica_co_id." ".$key_llamadas." ".$MAPEODetalleLlamdas{$clave_det}[13].$clave_det.",".$key_llamadas."|".$MAPEODetalleLlamdas{$clave_det}[0]."|".$MAPEODetalleLlamdas{$clave_det}[1]."|".$MAPEODetalleLlamdas{$clave_det}[2]."|".$MAPEODetalleLlamdas{$clave_det}[3]."|".$MAPEODetalleLlamdas{$clave_det}[4]."|".$valor_segundos."|".$valor."|".$valorInter."|".$total."|".$MAPEODetalleLlamdas{$clave_det}[12]."|".$val_IVA."|".$val_IVAICE."|"."\n"; 		

         print FILE_WRITE_3 $verifica_co_id." ".$key_llamadas." ".$MAPEODetalleLlamdas{$clave_det}[13].$clave_det.",".$key_llamadas."|".$MAPEODetalleLlamdas{$clave_det}[0]."|".$MAPEODetalleLlamdas{$clave_det}[1]."|".$MAPEODetalleLlamdas{$clave_det}[2]."|".$MAPEODetalleLlamdas{$clave_det}[3]."|".$MAPEODetalleLlamdas{$clave_det}[4]."|".$valor_segundos."|".$valor_incluido."|".$total."|".$total_exedente_incluido."|\n"; #3311
	  } else {
         #SIS FMI-03-DIC-2007
         #$MAPEOImprimeDetLlamadas{$verifica_co_id." ".$key_llamadas." ".$_}=[$key_llamadas."|".$MAPEODetalleLlamdas{$_}[0]."|".$MAPEODetalleLlamdas{$_}[1]."|".$MAPEODetalleLlamdas{$_}[2]."|".$MAPEODetalleLlamdas{$_}[3]."|".$ValorParaMostrarTipo."|".$valor_segundos."|".$valor."|".$valorInter."|".$total."|".$MAPEODetalleLlamdas{$_}[12]."|"];
         #$MAPEOImprimeDetLlamadas{$verifica_co_id." ".$key_llamadas." ".$MAPEODetalleLlamdas{$clave_det}[13].$_}=[$key_llamadas."|".$MAPEODetalleLlamdas{$clave_det}[0]."|".$MAPEODetalleLlamdas{$clave_det}[1]."|".$MAPEODetalleLlamdas{$clave_det}[2]."|".$MAPEODetalleLlamdas{$clave_det}[3]."|".$ValorParaMostrarTipo."|".$valor_segundos."|".$valor."|".$valorInter."|".$total."|".$MAPEODetalleLlamdas{$clave_det}[12]."|".$val_IVA."|".$val_IVAICE."|".$val_segIVA."|".$val_segGIVA."|".$val_segIVAICE."|".$val_segGIVAICE."|"];
#          $MAPEOImprimeDetLlamadas{$verifica_co_id." ".$key_llamadas." ".$MAPEODetalleLlamdas{$clave_det}[13].$clave_det}=[$key_llamadas."|".$MAPEODetalleLlamdas{$clave_det}[0]."|".$MAPEODetalleLlamdas{$clave_det}[1]."|".$MAPEODetalleLlamdas{$clave_det}[2]."|".$MAPEODetalleLlamdas{$clave_det}[3]."|".$ValorParaMostrarTipo."|".$valor_segundos."|".$valor."|".$valorInter."|".$total."|".$MAPEODetalleLlamdas{$clave_det}[12]."|".$val_IVA."|".$val_IVAICE."|".$val_segIVA."|".$val_segGIVA."|".$val_segIVAICE."|".$val_segGIVAICE."|"];
         $MAPEOImprimeDetLlamadas{$verifica_co_id." ".$key_llamadas." ".$MAPEODetalleLlamdas{$clave_det}[13].$clave_det}=[$key_llamadas."|".$MAPEODetalleLlamdas{$clave_det}[0]."|".$MAPEODetalleLlamdas{$clave_det}[1]."|".$MAPEODetalleLlamdas{$clave_det}[2]."|".$MAPEODetalleLlamdas{$clave_det}[3]."|".$ValorParaMostrarTipo."|".$valor_segundos."|".$valor_incluido."|".$total."|".$total_exedente_incluido."|"]; #3311
	  }

	  if ($MAPEODetalleLlamdas{$clave_det}[4] eq "RI") {
		  $valor1=$MAPEODetalleLlamdas{$clave_det}[6];
	  } else {
		  $valor1=$MAPEODetalleLlamdas{$clave_det}[7];
	  }

	  $valor_RC=$ResumenCelular{$verifica_co_id."|".$MAPEODetalleLlamdas{$clave_det}[8]}[0] + $valor1;
	  $segundos_RC=$ResumenCelular{$verifica_co_id."|".$MAPEODetalleLlamdas{$clave_det}[8]}[1] + $MAPEODetalleLlamdas{$clave_det}[5];
	 
	   $ResumenCelular{$verifica_co_id."|".$MAPEODetalleLlamdas{$clave_det}[8]}=[$valor_RC,$segundos_RC,$MAPEODetalleLlamdas{$clave_det}[11]];


	  $valor=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[0] + $total;  #Uno AIRE + INTERCONECCION;
	  $segundos=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[1] + $seg_LLamadas;
	  $segundos_gratis=$ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}[3] + $seg_LLamadasG;
	  #print $clave_ResumenCelularPicoNoPico."[".$valor."!!".$segundos."!!".$tipo_impuesto."!!".$segundos_gratis."!!".$valor_Resumen_IVA."!!".$valor_Resumen_IVAICE."!!".$segundos_Resumen_IVA."!!".$segundosG_Resumen_IVA."!!".$segundos_Resumen_IVAICE."!!".$segundosG_Resumen_IVAICE."] \n";#debug
	  $ResumenCelularPicoNoPico{$clave_ResumenCelularPicoNoPico}=[$valor,$segundos,$tipo_impuesto,$segundos_gratis,$valor_Resumen_IVA,$valor_Resumen_IVAICE,$segundos_Resumen_IVA,$segundosG_Resumen_IVA,$segundos_Resumen_IVAICE,$segundosG_Resumen_IVAICE];
      undef(%MAPEODetalleLlamdas);
}


sub graba_co_id {
my $co_id=$_[0];
my $number=$_[1];
my $tipo=$_[2];
my $plan_co_id=$_[3];
my $des_co_id=$_[4];
$tip=$Graba_co_id{$co_id}[1];
$plan_g=$Graba_co_id{$co_id}[2];
$des_plan_g=$Graba_co_id{$co_id}[3];
if ($plan_co_id eq "") {
	$plan_co_id=$plan_g;
}
if ($des_co_id eq "") {
	$des_co_id=$des_plan_g;
}
$tip=$Graba_co_id{$co_id}[1];
if ($tipo eq "0" && $tip eq "6") {
	$tipo=$tip;
}
  if ($number ne "") {
#	  $Graba_co_id{$co_id}=[$number,$tipo];
      $Graba_co_id{$co_id}=[$number,$tipo,$plan_co_id,$des_co_id];
  }
}

sub cambia_etiqueta {
$descripcion_ser=$_[0];
if ($descripcion_ser =~ /Cargo a Paquete Fac/) {
	$descripcion_ser=~ s/Cargo a Paquete Fac/Cargo a Paquete/;
}
if ($descripcion_ser =~ /Cargo a Paquete Cto/) {
	$descripcion_ser=~ s/Cargo a Paquete Cto/Cargo a Paquete/;
}
if ($descripcion_ser =~ /Detalle de llamadas \- \no cobro/) {
	$descripcion_ser=~ s/Detalle de llamadas \- \no cobro/Factura Detallada Plus/;
}
return($descripcion_ser);
}

sub tim_type_dos{ 

open(FILE_READ,$file);

$/="LIN\+";
$key_resumen_celular1="40000";
$key_resumen_celular="42000";
$total_iva=0;
$total_ice=0;
$suma_total=0;
$valor_contratado=0;
$ban="0";
$ban_dos="0";
$valor_descuento=0;
$total_consumo_mes_primera=0;
$total_servicios_Ext=0;
$account_no="";
$ci_ruc=""; 
$cod_insert="";
$customer_name="";
$address3="";
$address1="";
$address2="";
$zone_no="";
$zone_name="";
$type_pay="";
$type_pay_description="";
$city="";
$billing_date="";
$total_due="000";
$bandera_plan = "0";
$bandera_plan1 = "0";
$quiebre_co_id = "-999";
####  Arreglo FMI plan no impreso 18/feb2008
$co_id_alter1 = "-999";
$co_id_alter2 = "-999";
$account_no_resumen="0"; #4505
#############################################

while(<FILE_READ>){ 
	 $interconexion=0;
	 $aire=0;
	 $line=$_;
	 if ($. == 1) {
		 $lcustcode="";
 	     $lprgcode="";
 	     $lcostcenter_id="";
 	     $lseq_id="";
 	     $lbillcycle="";
		 $lohrefnum="";
		 $ladrs="";#-- CLS RHE proyecto[3512]

		if (/CUSTCODE\|([^\n]+)\n/) {
		 #1.10306366|1|2|2|28 
	     @arreglo_num=split(/\|/,$1);
 	     $lcustcode=$arreglo_num[0];
 	     $lprgcode=$arreglo_num[1];
 	     $lcostcenter_id=$arreglo_num[2];
 	     $lseq_id=$arreglo_num[3];
 	     $lbillcycle=$arreglo_num[4];
		 $lohrefnum=$arreglo_num[5];
		 $ltipofactura=$arreglo_num[6]; #5328 tipo de factura
		 $lmail=$arreglo_num[7]; #5328 mail en caso de fact. electr�nica
		 $ltelefono=$arreglo_num[8]; #5328 telefono cliente en caso de fact. electr�nica
		 $tipo_identificacion=$arreglo_num[9]; #8504 - tipo de identificaci�n del cliente para Factura Electronica
         $ladrs=$arreglo_num[10]; #--- CLS RHE proyecto[3512]
		 $lohrefnum=~ tr/'.//d;
		}
	 }

	##ini - 8504 - SUD CAC - bandera de control de F.E.
	 if ($ltipofactura eq "4"){$band_electronica="4";}
	##fin - 8504 - SUD CAC 

 	 if (/PAT\+5/) {
	    $banderaUno="1";

		
		#JH
		if ($Cuentas_Excentas{$lcustcode} eq "S") {
			&valores_iva_ice($plan_tele1,$des_plan_tele1,"N","N");
		}

		else {
			&valores_iva_ice($plan_tele1,$des_plan_tele1,"S","S");
		}

        # Obtengo la Cuenta #
		if (/RFF\+IT\:(\d\.\d+)[^\d]/) {  $account_no_resumen=$1; }#4505
		&obtiene_cedula_nombre;
		&tim2_calcula;
		&calculo_consumo_mes_celular;
		$clave="10000";
		$num_invoice="000-000-0000000";
		$emission_due_date="06032007";
		$total_due="000";
		$type_key="00000";
		$total_iva=0;
		$total_ice=0;
		$valor_absoluto=0;		
		$impuesto_iva=0;
		$impuesto_ice=0;
		$valor_descuento=0;
		$des_descuento="";
		$ban_iva="";
		$ban_ice="";
     }

 	 if ($banderaUno eq "0") {
		  if ($ban_dos eq "0") {
		    &timDosBuscaValores;
		  } else {
			&buscaValoresResumeCelular;
		  }
		  if (/UNB\+UNOC/) {
		  $ban_dos="0";
		  $ban_acceso="0";
		  $account_no_resumen="0"; #4505
		  }
	 } else { 		
	   	&tim_type_uno;
	   	&tim_type_cero;
	 }
	} #while

   if ($banderaUno eq "1") {
	   $total_iva=sprintf('%.2f',$total_iva);
	   $total_ice=sprintf('%.2f',$total_ice);
	   $type_key_iva="20900";
	   $MAPEOGeneral{$des_iva}=[$type_key_iva,$total_iva];
#	   $MAPEOGeneral{$des_ice}=[$type_key_iva,$total_ice]; # Cambio ICE 08/08/08

	   $MAPEOTotales{"21300"}=[$total_consumo_mes_primera];


	   # Copio Informaci�n #
		foreach (keys %MAPEOGeneral) {
			$dolar=sprintf('%.2f',$MAPEOGeneral{$_}[1]);
			$dolar=~ tr/\.//d;
			$descCambiar=$_;
			@arreglo_num=split(/\|/,$descCambiar);
			$descCambiar=$arreglo_num[0];

			#ini - 8504 - SUD CAC - se aumenta codigo auxiliar y descuento para las  keys 20200, 21100 para Factura Electr�nica
			if ($ltipofactura eq "4" &&($MAPEOGeneral{$_}[0] eq "20200" || $MAPEOGeneral{$_}[0] eq "21100")){
				$valor_desc_rubro=sprintf('%.2f',$MAPEOGeneral{$_}[5]);
				$valor_desc_rubro=~ tr/\.//d;
                
				#INICIO 10182
		 if ($descCambiar eq "Roaming llamadas") 
		 {
		 $val_resumen= $tiempo_final_roaming;
		 $tiempo_final_roaming="";
		 }
		 else
		 {
		 $val_resumen=$MAPEOGeneral{$_}[2];
		 }
		 #print "TIEMPO ROAMING".$tiempo_final_roaming."\n";
		 #FIN 10182

				
				
				#9321 FECHA_CTA
           
#               $MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|".$MAPEOGeneral{$_}[4]."|".$valor_desc_rubro."|".$MAPEOCtasSVAFecha_CTA{$lcustcode , $MAPEOGeneral{$_}[3] }[2]."|"]; #9321
                $MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$val_resumen."|".$MAPEOGeneral{$_}[3]."|".$MAPEOGeneral{$_}[4]."|".$valor_desc_rubro."|".$MAPEOCtasSVAFecha_CTA{$lcustcode , $MAPEOGeneral{$_}[3] }[0]."|"]; #10203 LPE		      		      
				# $MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|".$MAPEOGeneral{$_}[4]."|".$valor_desc_rubro."|"]; 
				#$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$codigo_principal."|".$codigo_auxiliar."|".$valor_desc_rubro."|"]; 
			
			}
			else
    {
			#fin - 8504 - SUD CAC
			#$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"]; #4376
	        #9321 FECHA_CTA
          

				 $MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|".$MAPEOCtasSVAFecha_CTA{$lcustcode , $MAPEOGeneral{$_}[3] }[2]."|"];#9321
#				 $MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|".$MAPEOCtasSVAFecha_CTA{$lcustcode , $MAPEOGeneral{$_}[3] }[0]."|"]; #10203 LPE
		  		 #$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"]; #4376
			     
                    
					#$MAPEOImprimeUno{$MAPEOGeneral{$_}[0]."|".$descCambiar}=[$MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|"]; #4376
			
			}#8504 - SUD CAC - fin if
			#print $_ ." : ".$MAPEOGeneral{$_}[0]."|".$descCambiar . "--" .  $MAPEOGeneral{$_}[0]."|".$descCambiar."|".$dolar."|".$MAPEOGeneral{$_}[2]."|".$MAPEOGeneral{$_}[3]."|\n\n";

			$dolar=$MAPEOSuma{$MAPEOGeneral{$_}[0]}[0] + $MAPEOGeneral{$_}[1];
			$MAPEOSuma{$MAPEOGeneral{$_}[0]}=[$dolar,$_];
		}


		foreach (keys %MAPEOSuma) {
			$key_suma="";
			if ($_ eq "20000" ) { $key_suma="20100"; }
			if ($_ eq "20200")  { $key_suma="20300"; }
			if ($_ eq "20400")  { $key_suma="20600"; }
			if ($_ eq "20900")  { $key_suma="21000"; }
			if ($_ eq "21100")  { $key_suma="21200"; }
			# CLS JCE [4697] -no aparezca total de group
			if ($_ eq "21650")  { $key_suma="NING"; }

		   $dolar=sprintf('%.2f',$MAPEOSuma{$_}[0]);
		   $dolar=~ tr/\.//d;
		   # CLS JCE [4697] -no aparezca total de group
		   if ( $key_suma ne "NING" ) {
			   $MAPEOImprimeUno{$key_suma}=[$key_suma."|".$dolar."|"];
		   }
		   
		}
		
	 
		$dolar=$MAPEOSuma{"20000"}[0] + $MAPEOSuma{"20200"}[0] + $MAPEOSuma{"20400"}[0];
		$dolar=sprintf('%.2f',$dolar);
		$dolar=~ tr/\.//d;
		$MAPEOImprimeUno{"20800"}=["20800|".$dolar."|"];

		foreach (keys %MAPEOTotales) {
			$MAPEOTotales{$_}[0]=~ tr/\.//d;
			$MAPEOImprimeUno{$_}=[$_."|".$MAPEOTotales{$_}[0]."|"];
		}

		$total_due=~ tr/\.//d;
		# Verifico Bulk #
		$cod_bulk=$MAPEO_BULK{$account_no}[2];

        # Ini-2589
		$cod_plan_bulk=$MAPEO_BULK{$account_no}[3];
		if ($cod_bulk eq "") {
			$cod_bulk="0";
			$cod_plan_bulk="0";
		}
	    # Fin-2589

		$num_invoice="000-000-0000000";
		if ($tipo_ejecucion eq "C" || $lbillcycle eq "34" ) {
			$num_invoice=$lohrefnum;
			$fecha_sec=`date '+%Y-%m-%d-%H-%M-%S'`;
			$fecha_sec=&enceraEspacios($fecha_sec);
			print FILE_SECUENCIAS $account_no."|".$num_invoice."|".$ciclo."|".$fecha_sec."\n";
		}

		#CLS_MGA [5058]
		#print "NUEVO_RUBRO:--".$lcustcode."\n"; 
          
        if ($lcustcode eq $Nuevo_rubro{$lcustcode}[0]){
            # print "Debud no etiqueta2".$lcustcode."---".$Nuevo_rubro{$lcustcode}[0]."\n";
            #null;
        	#$MAPEOImprime{$lcustcode." ".$key_resumen_celular." ".$cont_calculo}=[$MAPEOImprime{$clave_anterior}[0]];
            #MAPEOImprimeUno{"21675"}=["21675|(1)Valor no cobrado por la prestaci�n del servicio de Distribuci�n de Estado de Cuenta a raz�n de USD  $Nuevo_rubro{$lcustcode}[1]  mensual del  $Nuevo_rubro{$lcustcode}[2]  al  $Nuevo_rubro{$lcustcode}[3] |"];
			$arreglo_3= $Nuevo_rubro{$lcustcode}[3];
            chop($arreglo_3);
			#julio 20 de 2010 - RCA
			#$MAPEOImprimeUno{"21675"}=["21675|(1) Reclasificaci�n concepto Distribuci�n Estado de Cuenta (".$Nuevo_rubro{$lcustcode}[2]." - ".$arreglo_3.")|".$Nuevo_rubro{$lcustcode}[2]."|".$arreglo_3."|"];
			#$MAPEOImprimeUno{"21676"}=["21675|(2) Reclasificaci�n concepto Factura Detallada (".$Nuevo_rubro{$lcustcode}[2]." - ".$arreglo_3.")|".$Nuevo_rubro{$lcustcode}[2]."|".$arreglo_3."|"];
			$MAPEOImprimeUno{"21675"}=["21675|(1) N/C: Cr�dito por  intereses en servicio de \"Factura Detallada\" ". $Nuevo_rubro{$lcustcode}[2] ." a ". $arreglo_3 . ", concepto reclasificado en Mayo 2009" ."|".$Nuevo_rubro{$lcustcode}[2]."|".$arreglo_3."|"];
			$MAPEOImprimeUno{"21676"}=["21675|(2) N/D: D�bito por intereses en servicio \"Distribuci�n de Estado de Cuenta\" ". $Nuevo_rubro{$lcustcode}[2] ." a ". $arreglo_3 . ", concepto reclasificado en Mayo 2009" . "|".$Nuevo_rubro{$lcustcode}[2]."|".$arreglo_3."|"];
			

#(1) N/C: Cr�dito por  intereses en servicio de "Factura Detallada" Oct/2008 a Abr/2009, concepto reclasificado en Mayo 2009
#(2) N/D: D�bito por intereses en servicio �Distribuci�n de Estado de Cuenta� Oct/2008 a Abr/2009, concepto reclasificado en Mayo 2009

 


		  } 
		

		#$MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1."|".$address2."|".$address3."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."|"]; #2589
		#-- CLS RHE proyecto[3512] se concatenan los campos de direcciones 

		# Inicio NCA 21/06/2013 validacion de caracteres especiales.
		$band_ci="0";
		if ($ltipofactura eq "4") {		
			if (substr($customer_name,0,1) eq "." || substr($customer_name,0,1) eq "*" || substr($customer_name,0,1) eq "-" || substr($customer_name,0,1) eq "+")
			{	print FILE_INCONSISTENCIA "Cuenta con nombre inconsistente ".$account_no."|".$customer_name."\n";
				$customer_name=substr($customer_name,1); 
			}
			if (substr($address1,0,1) eq "." || substr($address1,0,1) eq "*" || substr($address1,0,1) eq "-" || substr($address1,0,1) eq "+")
			{	print FILE_INCONSISTENCIA "Cuenta con direccion inconsistente ".$account_no."|".$address1."\n";
				$address1=substr($address1,1); 
			}
			if (substr($lmail,0,1) eq "." || substr($lmail,0,1) eq "*" || substr($lmail,0,1) eq "-" || substr($lmail,0,1) eq "+")
			{	print FILE_INCONSISTENCIA "Cuenta con direccion de correo inconsistente ".$account_no."|".$lmail."\n";
				$lmail=substr($lmail,1);
			}
			if (substr($city,0,1) eq "." || substr($city,0,1) eq "*" || substr($city,0,1) eq "-" || substr($city,0,1) eq "+")
			{	print FILE_INCONSISTENCIA "Cuenta con ciudad inconsistente ".$account_no."|".$city."\n";
				$city=substr($city,1); 
			}
			if ($tipo_identificacion eq "04") 	#si es RUC
			{	if (length($ci_ruc) != 13 )
				{	print FILE_INCONSISTENCIA "Cuenta con RUC inconsistente ".$account_no."|".$ci_ruc."\n";
					$band_ci="1";

				}
			}
			else	# si no es RUC, valido cedula
			{	if ($tipo_identificacion eq "05")	# si es cedula
				{	if (length($ci_ruc) != 10 )
					{	print FILE_INCONSISTENCIA "Cuenta con Cedula inconsistente ".$account_no."|".$ci_ruc."\n";
						$band_ci="1";
					}
				}
			}
			# validacion del & en nombre y direccion
			$customer_name=~ s/[&]/&amp;/g;
			$address1=~ s/[&]/&amp;/g;
			# si el correo tiene espacios en blanco.
			if ($lmail=~/ /)
			{
				print FILE_INCONSISTENCIA "El correo electronico tiene espacios en blanco ".$account_no."|".$lmail."\n";
				$lmail=~ s/[ ]//g;	
			}
		}
		
        $fecha_actual=`date '+%d/%m/%Y`;
        $fecha_actual=&enceraEspacios($fecha_actual);
        $fecha_emite=&calcula_fecha($fecha_actual);	
		print "cuenta ".$account_no."\n";
		$num_admin="";   ## Inicializando 		
        $num_admin=$MAPEOAbonadosAdmin{$account_no}[1];  ###LPE	
        $num_admin=&enceraEspacios($num_admin);
      

		# Fin NCA 21/06/2013 validacion de caracteres especiales.
#   	    $MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1.$address2.$address3."|"."|"."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."|"]; #2589 
#   	    $MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1.$address2.$address3."|"."|"."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."||".$fecha_emite."|"]; #10203
   	    $MAPEOImprimeUno{$clave}=[$clave."|".$num_invoice."|".$emission_due_date."|".$customer_name."|".$address1.$address2.$address3."|"."|"."|".$zone_no."|".$zone_name."|".$cod_insert."|".$ci_ruc."|".$type_pay."|".$type_pay_description."|".$account_no."|".$city."|".$billing_date."|".$total_due."|".$due_date."|".$cod_bulk."||".$archivo."|".$lprgcode."|".$lcostcenter_id."|".$lbillcycle."|".$cod_plan_bulk."||".$fecha_emite."|".$num_admin."|"]; #10203
		
		#ini 5328 validad que la linea 10100 solo salga para facturacion electronica
		if ($ltipofactura eq "4") {		
			
			#ini - 8504 - SUD CAC - control de errores
			if ($ltipofactura eq "0"){$ltipofactura="";}
			if ($lmail eq "0"){$lmail="";}
			if ($ltelefono eq "0"){$ltelefono="";}
			if ($tipo_identificacion eq "0"){$tipo_identificacion="";}
			#fin - 8504 - SUD CAC
			
			#$MAPEOImprimeUno{"10100"}=["10100|".$ltipofactura."|".$lmail."|".$ltelefono."|"]; #8504 - SUD CAC - comentado 
			$MAPEOImprimeUno{"10100"}=["10100|".$ltipofactura."|".$lmail."|".$ltelefono."|".$tipo_identificacion."|"]; ##8504 - SUD CAC - se agrega tipo_identificacion
			$band_electronica="4";

			if ($lmail eq "") {
				$band_correo="1";
			}
			if ($ltelefono eq "") {
				$band_correo="2";
			}
			#ini - 8504 - SUD CAC - bandera de control de errores
			if ($tipo_identificacion eq "") {
				$band_correo="3";
			}
			#fin - 8504 - SUD CAC
		}
		#fin 5328

		#$MAPEOImprimeUno{"21700"}=["21700|Estimado cliente: si su forma de pago es contra factura pague su consumo celular en Servipagos, Banco del Pacifico o Banco del Pichincha, si su plan es personal; y en Servipagos si su plan es corporativo. Podr� presentar este desprendible en Servipagos.|"];
		$MAPEOImprimeUno{"21700"}=["21700| |"]; #SUD CAC 

	}
} # tim_type_dos

# Funcion utilizado para tener la CI y el Nombre del due�o de la cuenta
# en caso de no existir informaci�n en el archivo fileinfo_contr_text.dat
# imprimo el de la Cuenta
sub obtiene_cedula_nombre{
if ( /RFF\+SC\:([^\:]*)\'\n/ ) { $ci_ruc=$1; } # C�dula #
if (/NAD\+IV/) {
$_=~ s/\?\://g;
$_=~ s/\?\?/\?/g;
}

if (/NAD\+IV\+\+\+([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:\']*)/) {
$customer_name=$2;
$customer_name=~ tr/\|//d;
}

$MAPEOinfo_contr_text{"999999"}=[$ci_ruc,$customer_name];

}

sub tim_type_uno{ 
 $valor_absoluto=0;		
 $aire=0;		
 $interconexion=0;		
 $impuesto_iva=0;
 $impuesto_ice=0;
 $valor_descuento=0;
 $des_descuento="";
 $ban_iva="";
 $ban_ice="";
 $des_tiempo="";
 $valor_descuento_original=0;

# Datos Personales del Cliente #
# *I* ANGEL LLERENA HIDALGO CDLA NUEVA KENEDY CALLE B 108 Y LA CUART A ESTE  028 GUAYAQUIL

#NAD+IT+++*I*:JORGE LAGOS BALSECA:VIA A COLOMBIA 1020 Y JORGE A??AZCO  LOCA:L ?: "SUPER MINI  DE TODO " :LAGO AGRIO  *I*:++ECUADOR'
#NAD+IV+++*I*:JORGE LAGOS BALSECA:VIA A COLOMBIA 1020 Y JORGE A??AZCO  LOCA:L ?: "SUPER MINI  DE TODO " :LAGO AGRIO  *I*:++ECUADOR'
#print " linea ".$line."\n";
if (/NAD\+IV/) {
$_=~ s/\?\://g;
$_=~ s/\?\?/\?/g;
}

if (/NAD\+IV\+\+\+([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:]*)\:([^\:\']*)/) {
$cod_insert=$1;
$customer_name=$2;
$customer_name=~ tr/\|//d;
$customer_name=~ tr/\"//d;
$address1=&enceraEspacios($3);
$address2=&enceraEspacios($4);
$address3=$ladrs;#-- CLS RHE proyecto[3512]
$address1=~ tr/\"//d;
$address2=~ tr/\"//d;
$address1=~ tr/\|//d;
$address2=~ tr/\|//d;
#-- CLS RHE proyecto[3512] se agrega 
$address3=~ tr/\"//d;
$address3=~ tr/\|//d;
#-----------------------------------
if ($address1 eq "") {
	$address1=$address2;
    $address2="";
}
$zone_name=$5;

	#RCA.27/12/2006:Consideracion en cambio de TIMMs
	if (/\*I\*\:[^\:\']*/ && $cod_insert eq "") {
		$cod_insert = "*I*";
	}

#if (substr($zone_name,0,1) eq "0") {
# $zone_no=substr($zone_name,0,3);
# $zone_name=substr($zone_name,3);
#} else {
#  $zone_no="0"
#}
#SIS RCA:Correcci�n.
if (substr($zone_name,0,1) =~ /^\d/ ) {
# $zone_name =~ /^(\d+) (.*)/;
 $zone_name =~ /^([^ ]+) (.*)/;
 #$zone_no=substr($zone_name,0,3);
 #$zone_name=substr($zone_name,3);
 $zone_no= $1;
 $zone_name= " ".$2;

} else {
  $zone_no="0"
}

}

if ( /RFF\+SC\:([^\:]*)\'\n/ ) { $ci_ruc=$1; $ban_cedula_no_impresa="1";} # C�dula #
if ( /RFF\+PG\:([^\:]*)\'\n/) {  $type_pay=$1; } # Forma de Pago #
if ( /FII\+AO\+.*\:+\:([^\+\:\']*)/ ) { $type_pay_description=$1; }
if (/RFF\+IT\:(\d\.\d+)[^\d]/) {  $account_no=$1; }
if ( /FII\+RH\+9999\:([^\:]*)/) { $city=$1; } # Ciudad #
if ( /DTM\+168\:([^\:]*)/ ) {   $billing_date=$1; } # Fecha Fatura #

if (/DTM\+13\:([^\:]*)/) {
  $due_date=$1;
}

# Detalles #
if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n/ ) {
  $codigo_servicio=$1;
#		  print $codigo_servicio."\n";

#IMD++8+TM:::ROTIN:ROAMING TAPIN'
if ( /IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'\n/ ) {
  $tmshdes=$1;
}

if (/PRI\+CAL\:([^\:]*)\:INV\:USD\:959\'\n/) {
 $valor_absoluto=$1;		
}


while  (/ALC\+.+\+([^\+]*)\+\'\n/g)  {
  $des_descuento=$1;
  if ( /MOA\+53\:([^\:]*)\:USD.*\n/g ) {
	  $valor_descuento=$1;
	  $valor_descuento_original=$valor_descuento_original+$valor_descuento;
	  $valor_descuento=sprintf('%.2f',$valor_descuento);
	  $valor_descuento1=($valor_descuento* -1) + $MAPEOGeneral{$des_descuento}[1];
	  $MAPEOGeneral{$des_descuento}=["20400",($valor_descuento1)];
#	  print $des_descuento."|".$valor_descuento_original."|".$valor_descuento."\n";
   }
}

if ($des_descuento ne "") {
	  $valor_descuento=$valor_descuento_original;
}
# Otengo el Valor el 2do MOA #
#MOA+125:5.36:USD:959:9'
#MOA+125:3.75:USD:959:5'
/MOA\+125\:([^\n\:]*)\:USD.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/;
$dolar_original=$1;
$dolar=$2;


/IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/; #  IMD++8+SP    :  ::  SP51   :   GSM ? : Servicios Basicos '
/IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'\n/; #IMD++8+SP   : : :   SP06  :Servicios OCC'
$codigo_servicio_basico=$1;   # Es para obtener si es Servicio de Voz B�sico SP51 SP02


#3097
# IVA #
if ( /TAX\+1\+VAT\:\:I\.V\.A.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ){
 $impuesto_iva=$1;
 $total_iva=$total_iva+$impuesto_iva;
 $ban_iva=1;
}
# ICE #
if ( /TAX\+1\+VAT\:\:ICE.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ) {
 $impuesto_ice=$1;
 $total_ice=$total_ice+$impuesto_ice;
 $ban_ice=1;
}
#3097

#ini - 8504 - SUD CAC 
$codigo_aux_rubro="";
$aux_8504="";
#fin - 8504 - SUD CAC

if ( /PIA\+1\+*/ ) {    
  /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'\n/;
  $calltype=$1;
  $ttcode=$2;
  if ($calltype eq "IC" || $calltype eq "BASE") {
	  if ($calltype eq "IC") {
		 $ttcode=$ttcode;
	  }
	  $tzcode=$3;     

	 #ini - 8504 - SUD CAC - se extrae el codigo auxiliar de los rubros de llamadas
	  if ( $band_electronica eq "4"){
		($descripcion_ser,$des,$sh_des,$aux_8504,$codigo_aux_rubro)=&busca_descripcion_etiqueta($codigo_servicio,$tzcode,$calltype,$ttcode,$tmshdes,"");
		 $aux_8504="";
	  }else{
	 #fin - 8504 - SUD CAC

	  ($descripcion_ser,$des,$sh_des)=&busca_descripcion_etiqueta($codigo_servicio,$tzcode,$calltype,$ttcode,$tmshdes,"");

	 }#8504 - SUD CAC - fin IF
	  
	  
	  #3097
	  if ($ban_ice eq 1 && 0 eq 1) {	  
    	  $des_tiempo=&formatea_seg_minutos_primera_hoja($Minutos{$sh_des}[4],$Minutos{$sh_des}[5]);
	  } else {
    	  $des_tiempo=&formatea_seg_minutos_primera_hoja($Minutos{$sh_des}[2],$Minutos{$sh_des}[3]);
	  }



#	  $des_tiempo=&formatea_seg_minutos_primera_hoja($Minutos{$sh_des}[0],$Minutos{$sh_des}[1]);
	  #print ">> $sh_des \n";
	  
  } else {
	  $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
  }

} else {
  $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
}

if ($descripcion_ser eq "") {
	$bandera_type_uno="1";
}

#3097
## IVA #
#if ( /TAX\+1\+VAT\:\:I\.V\.A.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ){
# $impuesto_iva=$1;
# $total_iva=$total_iva+$impuesto_iva;
# $ban_iva=1;
#}
## ICE #
#if ( /TAX\+1\+VAT\:\:ICE.*\nMOA\+124\:([^\n\:]*)\:USD.*\n/ ) {
# $impuesto_ice=$1;
# $total_ice=$total_ice+$impuesto_ice;
# $ban_ice=1;
#}
#
$type_key=&verifico_tipo_agrupacion($ban_iva,$ban_ice);
#CLS JCE [4691] - Se Verifica type_key del codigo_servicio
if ( $type_key eq "21100" ) {
	$type_key=&verifica_servExterno($codigo_servicio,$type_key);
	if ( $type_key eq "21650") {
		$total_servicios_Ext=$total_servicios_Ext+$dolar;
	}
}
$dolar=$dolar+$valor_descuento;
$dolar=sprintf('%.2f',$dolar);
if ( $dolar != 0 ){
 $descripcion_ser=cambia_etiqueta($descripcion_ser)."|".$type_key;
 $dolar=$dolar + $MAPEOGeneral{$descripcion_ser}[1];

	#ini - 8504 - SUD CAC - Se agrege codigo de servicio y descuento a las keys 20200 - 21100
	if ( $band_electronica eq "4" && ($type_key eq "20200" || $type_key eq "21100")){
		if($codigo_aux_rubro eq ""){$codigo_aux_rubro="0";}
		$sumas_desc_rubro=($valor_descuento)+$MAPEOGeneral{$descripcion_ser}[5];
		$sumas_desc_rubro=sprintf('%.2f',$sumas_desc_rubro);
		$MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio,$codigo_aux_rubro,($sumas_desc_rubro)];
	}else{
	#fin - 8504 - SUD CAC
 $MAPEOGeneral{$descripcion_ser}=[$type_key,$dolar,$des_tiempo,$codigo_servicio]; #4376
	}#fin if - 8504
 #print $calltype."|".$ttcode."|".$codigo_servicio."|".$tzcode."|".$type_key."|".$descripcion_ser."|".$dolar."|".$des_tiempo." \n";
}

$codigo_aux_rubro=""; #8504 - SUD CAC 

}

# Total Consumo del Mes #
# CLS JCE [4697] - Total consume del Mes se le resta el valor de Otros servicios. 
if (/MOA.*\nMOA\+77\:([^\n\:]*)\:USD.*\n/) {
  #print "total_servicios_Ext=".$total_servicios_Ext." TotalConsumoMesPrimera=".$1;
  $total_consumo_mes_primera=sprintf('%.2f',&obtieneTotalMenosServExt($1)); # CLS JCE [4697]
  # $total_consumo_mes_primera=$1; 
  $ban_timm1_completo="1";
}


} # tim_type_uno

#sub tim_type_cero {
#
#if ( /MOA\+961\:([^\:]*)\:USD.*\n/ ) {  #Saldo Anterior 
#$saldo_anterior=sprintf('%.2f',$1);
#}
#
##RCA.23/12/2006:Suma de pagos recibidos.
#if ( /MOA\+962\:([^\:]*)\:USD.*\n/ ) {  
#$suma_pagos_rec=sprintf('%.2f',$1);
#}
#
##RCA.23/12/2006:Suma de sobrepagos.
#if ( /MOA\+971\:([^\:]*)\:USD.*\n/ ) {  
#$suma_sobrepagos=sprintf('%.2f',$1);
#}
#
#if ( /MOA\+967\:([^\:]*)\:USD.*\n/ ) { # Consumos del Mes
#$consumo_mes=sprintf('%.2f',$1);
#}
#
##RCA.23/12/2006:Saldo actual en la fecha de cierre
#if ( /MOA\+969\:([^\:]*)\:USD.*\n/ ) {  
#$suma_actual_cierre=sprintf('%.2f',$1);
#}
#
#if ( /MOA\+11\:([^\:]*)\:USD.*\n/ ) { # Pagos Recibidos
#$dolar=sprintf('%.2f',$1);
#$suma_cargos_anteriores=$dolar; #RCA
##$valor=$saldo_anterior."|".$dolar;  #RCA:As� estaba antes...
#$A_21400 = ($saldo_anterior+abs($suma_actual_cierre));
#$B_21400 = ($suma_pagos_rec+$suma_sobrepagos);
#$valor=$A_21400."|".$B_21400;  #RCA:As� estaba antes...
#$MAPEOTotales{"21400"}=[$valor];
#}
#
##RCA.23/12/2006:Ahora calculamos de otra manera!
##if ( /MOA\+968\:([^\:]*)\:USD.*\n/ ) { # Valor a Pagar
##$dolar=sprintf('%.2f',$1);
##$valor=$consumo_mes."|".$dolar;
##$MAPEOTotales{"21600"}=[$valor];
##$total_due=$dolar;
##}
#
##RCA.23/12/2006:Ahora calculamos de otra manera!
#if ( /MOA\+968\:([^\:]*)\:USD.*\n/ ) { # Valor a Pagar
#$dolar=$saldo_anterior+$suma_pagos_rec+$suma_sobrepagos+abs($suma_actual_cierre)+$consumo_mes;
#$dolar=sprintf('%.2f',$dolar);
#$valor=$dolar."|".($dolar+$A_21400+$B_21400);
#$MAPEOTotales{"21600"}=[$valor];
#$total_due=$dolar;
#}
#
#}

sub tim_type_cero {

## 4697 - CLS JCE se modifica forma de Obtener Saldo Anterior 24/09/2009
#if ( /MOA\+961\:([^\:]*)\:USD.*\n/ ) {  #Saldo Anterior 
#$saldo_anterior=sprintf('%.2f',$1);
#}
## CLS JCE Suma de pagos recibidos.
if ( /MOA\+962\:([^\:]*)\:USD.*\n/ ) {  
$suma_pagos_rec=$1;
}

if ( /MOA\+76\:([^\:]*)\:USD.*\n/ ) {  
$saldo_consumo=$1;
}

### CLS JCE.06/10/2009:Sumas de sobrepagos.
if ( /MOA\+971\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos=$1;
}

if ( /MOA\+978\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos2=$1;
}
####COAOCC
if ( /MOA\+981\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos3=$1;
}
##OVPAY
if ( /MOA\+988\:([^\:]*)\:USD.*\n/ ) {
$suma_sobrepagos4=$1;
}

$saldo_tmp=$saldo_consumo - $suma_pagos_rec;
$saldo_tmp_tot=$saldo_tmp - $suma_sobrepagos - $suma_sobrepagos2 - $suma_sobrepagos3 - $suma_sobrepagos4;
$saldo_anterior=sprintf('%.2f',$saldo_tmp_tot);

## FIN SALDO ANTERIOR [4697] CLS JCE

if ( /MOA\+967\:([^\:]*)\:USD.*\n/ ) { # Consumos del Mes
$consumo_mes=sprintf('%.2f',&obtieneTotalMenosServExt($1));
$totalservExt=$total_servicios_Ext;
# $consumo_mes=sprintf('%.2f',$1); # CLS [4697] 
}


if ( /MOA\+968\:([^\:]*)\:USD.*\n/ ) { # Valor a Pagar
$dolar=sprintf('%.2f',$1);
$valor=$consumo_mes."|".$dolar;
$MAPEOTotales{"21600"}=[$valor];
$total_due=$dolar;
}

if ( /MOA\+11\:([^\:]*)\:USD.*\n/ ) { # Pagos Recibidos
$dolar=sprintf('%.2f',$1);
#Verifica si cuadra el total de la Factura.
$sumacomprobacion=sprintf('%.2f',$saldo_anterior + $dolar + $consumo_mes + $totalservExt);
	if ( $sumacomprobacion != $total_due) {
	    $saldo_anterior_tmp = $total_due - $dolar - $consumo_mes - $totalservExt;
#		$saldo_anterior_tmp=abs($saldo_anterior_tmp);
		$saldo_anterior=sprintf('%.2f',$saldo_anterior_tmp);
		# Verifica si cuadra el total de la Factura II. 
		$sumacomprobacion=sprintf('%.2f',$saldo_anterior + $dolar + $consumo_mes + $totalservExt);
		if ( $sumacomprobacion != $total_due) {
			$bandera_type_cero="1";
		}else{
			$bandera_type_cero="0";
		}
	}else{
		$bandera_type_cero="0";
	}

$valor=$saldo_anterior."|".$dolar;
$MAPEOTotales{"21400"}=[$valor];
}

}
#######MGA###########
$lb_control_plan="0";
$lb_control_sum=0;

sub timDosBuscaValores {
    $val_resumen=0;
    if ( /(IMD\+\+8\+CT\:\:\:\:U\')\n/ )  {
        $ban_dos="1";
        $ban_acceso="0";
        return;
    }
    
    if ( /(IMD\+\+8\+CT\:\:\:\:A\')\n/ )  {
        $ban_acceso="1";
    }
    
    if (/0\+\+\:SA\:\:\+\+02\'\n/) {
        $co_id="";
        $fono="00000000000";
    }
    if (  /IMD\+\+8\+CO\:\:\:\:([^\:\']*)\'\n/ ) { #if #1
        $co_id=$1;
    }
    
    
    if ( /(0\+\+\:SA\:\:\+\+02\'\n)/) {
        if (/IMD\+\+8\+MRKT\:\:\:([^\:]*)\:/) {
            $marca_fono=$1;
            if ($marca_fono eq "ISD") {
                $key_resumen_celular="31000";
                $key_tot_cargos_cta="31100";
                &graba_co_id($co_id,$fono,"0");
                } else {
                $key_resumen_celular="42000";
                $fono="59399999999";
            }
        }
        #Obtengo el n�mero de Tel�fono#
        if (/IMD\+\+8\+DNNUM\:M\:\:([^\:]*)\:([^\:\']*)\'\n/) {
            $fono=$2;
        }
        if ($fono eq "59399999999" ) {
            $band_numero_no_existe="1";
        }
        
        
        
        } else {
        
        ####Mejora FMI para plan no impreso  18/feb/2008
        if( $co_id_alter1 ne $co_id){
            &graba_co_id($co_id,$fono,"0",$plan_tele,$des_plan_tele);
            $co_id_alter1=$co_id;
            #$co_id_alter2=$co_id;
        }
        ###############################################   18/feb/2008
        
        if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n[^\n]/ ) {
            $codigo_servicio=$1;
            $co_id_det=$Graba_co_id{$co_id}[1];
            
            if ($ejecuta_cuentas_largas ne "1") { #EBC
                if (($co_id_det eq "6") && ($codigo_servicio eq "PV013" || $codigo_servicio eq "PV037")) {
                    $datos_detalles_llamada=$MAPEOImprimeDetLlamadas{$co_id." 43000 1111111111"}[0];
                    @arreglo_num=split(/\|/,$datos_detalles_llamada);
                    $val1=$arreglo_num[0];
                    $val2=$arreglo_num[1];
                    $val3=$arreglo_num[2];
                    $MAPEOImprimeDetLlamadas{$co_id." 43000 1111111111"}=[$val1."|".$val2."|".$val3."|1|0|"];
                }
            }
            
            if (/IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'/) {
                if ($1 eq "ROTIN" || $1 eq "ROTOP" || $1 eq "ROTIT"|| $1 eq "RITIM" || $1 eq "RITIA"  || $1 eq "RITIN" || $1 eq "ROTIN" || $1 eq "TPESN" || $1 eq "POCC") {
                    $plan_tele=$1;
                    $des_plan_tele=$2;
                    &graba_co_id($co_id,$fono,"0","","");
                    } else {
                    $plan_tele=$1;
                    $des_plan_tele=$2;
                    $plan_tele1=$plan_tele;
                    $des_plan_tele1=$des_plan_tele;
                    
                    # SIS EXT SUD MPA, Mayo 11,2007
                    # Para tomar el primer registro de Tarifa B�sica
                    

                  if( $codigo_servicio eq "PV002") {
                        
                        # SIS EXT CLS FMI, SEP 12,2007
                        # Esta pregunta descrimina los bloques que no llegan en buen estado
                        if( $line=~ /PIA|DAY/ ){
                            if( $quiebre_co_id eq $co_id){
                                # print "no presenta FONO ".$fono." Codigo Servicio ".$codigo_servicio." Co_id ".$co_id_det." Descripcion plan1: ".$des_plan_tele."T |\n" ;
                                $bandera_plan1="0";#para que no tome en cuenta el siguiente PV002
                                
                                }else{
                                #  print "Entrada 1 FONO ".$fono." Codigo Servicio ".$codigo_servicio." Co_id ".$co_id_det." Descripcion plan1: ".$des_plan_tele."T |\n" ;
                                &graba_co_id($co_id,$fono,"0",$plan_tele1,$des_plan_tele1);
                                $quiebre_co_id=$co_id;
                                
                                $co_id_alter1=$co_id;
                                $co_id_alter2=$co_id;
                            }
                        } #FIN DE PREGUNTA DE VALIDACION DE BLOQUE
                        ###
                        }else{
                        #print "Entrada 2 FONO ".$fono." Codigo Servicio ".$codigo_servicio." Co_id ".$co_id_det." Descripcion plan1: ".$des_plan_tele."T |\n" ;
                        &graba_co_id($co_id,$fono,"0",$plan_tele1,$des_plan_tele1);
                        
                        $co_id_alter1=$co_id;
                        $co_id_alter2=$co_id;
                    }
                    
                }
            }
            
            
            # Otengo el Valor el 2do MOA #
            if (/MOA.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/) {
                $dolar=$1;
            }
            
            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:]*)\:([^\:\']*)\'\n/; #  IMD++8+SP    :  ::  SP51   :   GSM ? : Servicios Basicos '
            /IMD\+\+8\+SP\:\:\:([^\:]*)\:([^\:\']*)\'\n/; #IMD++8+SP   : : :   SP06  :Servicios OCC'
            $codigo_servicio_basico=$1;   # Es para obtener si es Servicio de Voz B�sico SP51 SP02
            $descripcion_ser=&busca_descripcion_servicio($codigo_servicio);
            $descripcion_ser=cambia_etiqueta($descripcion_ser);
            
            $desglosa_periodos = "0";
            
            if (lc($descripcion_ser) ne "servicio de voz"  ) {
                if ((lc($descripcion_ser) eq "tarifa basica " || lc($descripcion_ser) eq "tarifa b�sica " || lc($descripcion_ser) eq "cargo a paquete" ) && $dolar == 0) {
                    $no_imprime="1";
                    $clave_anterior="";
                    #$plan_anterior=""; #SIS RCA 04/01/2008
                    #$plan_actual=""; #SIS RCA 04/01/2008
                    $dolar_anterior="";
                    $codigo_servicio_a="";
                    $plan_tele_a="";
                    $co_id_a="";
                    
                    } else {
                    $no_imprime="0";
                }
                
                #             if (lc($descripcion_ser) eq "tarifa basica " || lc($descripcion_ser) eq "tarifa b�sica " || lc($descripcion_ser) eq "cargo a paquete" ) {                  #$desglosa_periodos = "1";
                    
                    #/IMD\+\+8\+TM\:\:\:([^\']+)/; #INICIO SIS RCA 04/01/2008
                    #if ($plan_actual ne "") {     $plan_anterior = $plan_actual }
                    #$plan_actual=$1;
                    #                 print "\nPLAN ANTERIOR... $plan_anterior   PLAN ACTUAL... $plan_actual\n"; #FIN SIS RCA 04/01/2008
                    
                    #if ($plan_actual ne $plan_anterior) {
                        #     $desglosa_periodos = "1";
                    # }
                    #                 $desglosa_periodos = "1";
                #           }
                
                
               # $info_desglosa_periodos=$MapeoRubros{$codigo_servicio}[0]; --[3636] MGA Eliminacion de Rubros
                #if ($info_desglosa_periodos eq "" && $info_desglosa_periodos eq "1") {
                    
                    #$desglosa_periodos = "1";
                    #} else {
                    #if ($info_desglosa_periodos eq "2") {
                     #   $desglosa_periodos = "0";
                    #}
                    
                    #            print $codigo_servicio." ".$desglosa_periodos." ".$info_desglosa_periodos."\n";
                #}
                
                
                if ($no_imprime eq "0") {
                    
                    # 3097
                    $total_iva=$MAPEOTotales{$co_id}[1];
                    $total_ice=$MAPEOTotales{$co_id}[2];
                    
                    $dolar_anterior=$dolar;
                    $codigo_servicio_a=$codigo_servicio;
                    $plan_tele_a=$plan_tele;
                    $co_id_a=$co_id;
                    #             IMD++8+FE::::Proceso Autom�tico - Transacci�n N�. 1218 Fact.07/01/2008 carga INTEROPER (01/01/2008 - 05/01/2008) AAPOLOZ-20071231MAYCART'
                    #             IMD++8+FE:::: TRANSFERENCIA_TRX_299225637-20071231MAYCART'
                    #             /IMD\+\+8\+FE[^\n]+\-([^\n]+)\n/;
                    #             /IMD\+\+8\+FE[^\n]+\-(\d+)MAYCART\n/;
                    
                    if ($desglosa_periodos eq "0") {
                        
                        $tiempo_fin_no_imprime="20080101";
                        if (/IMD\+\+8\+FE/) {
                            #                    print "\n\n VALOR*:$1 \n\n";
                            if (/IMD\+\+8\+FE[^\n]+\-(\d+MAYCART)\'\n/) {
                                $tiempo_fin_no_imprime=substr($1,0,8);
                            }
                            &impuesto_calcular($dolar,$codigo_servicio,$plan_tele,$tiempo_fin_no_imprime);
                            $suma_total=$MAPEOTotales{$co_id}[0]+$dolar;
                            #                 print $dolar." ".$codigo_servicio." ".$MAPEOTotales{$co_id}[0]." IVA ".$total_iva." ICE ".$total_ice." ".$tiempo_fin_no_imprime."\n";
                            $MAPEOTotales{$co_id}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
                        }
                        
                    }
                    
                    
                    $cont_calculo=&obtiene_contador($descripcion_ser,$desglosa_periodos);
                    
                    #SIS RCA DEBUG
                    #print "Descripcion Ser: $descripcion_ser ---- Cont_calculo: $cont_calculo\n";
                    
                    ($val,$tmp_inicio,$tmp_fin)=&obtiene_valor_resumen($co_id,$key_resumen_celular,$cont_calculo);
                    
                    $dolar=&formatea_numeros($dolar);
                    $dolar=~ tr/\.//d;
                   
	                # SIS RCA [2573]
					#if (lc($descripcion_ser) ne "roaming blackberry diario") {

						$val_resumen=$dolar + $val;
#	                   print "Vlor del Resumen : $val_resumen ---- Cont_calculo: $dolar\n";
#					   print "Vlor del Resumen2 : $val_resumen ---- Cont_calculo: $val";
					#}

							#9321 fecha_contratacion
#						   	 $MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$key_resumen_celular."|".$descripcion_ser."||".$val_resumen."|".$val_resumen."|".$tmp_inicio."|".$tmp_fin."|".$codigo_servicio."|".$MAPEOCtasSVAFecha{$Graba_co_id{$co_id}[0], $codigo_servicio }[2]."|"]; #ECO_CIMA1
						   	 $MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$key_resumen_celular."|".$descripcion_ser."||".$val_resumen."|".$val_resumen."|".$tmp_inicio."|".$tmp_fin."|".$codigo_servicio."|".$MAPEOCtasSVAFecha{$Graba_co_id{$co_id}[0], $codigo_servicio }[0]."|"]; #ECO_CIMA1
							 #$MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$key_resumen_celular."|".$descripcion_ser."||".$val_resumen."|".$val_resumen."|".$tmp_inicio."|".$tmp_fin."|".$codigo_servicio."|"."|"]; #ECO_CIMA1
								
							

                    ##SIS RCA DEBUG
#                    print $co_id." ".$key_resumen_celular." ".$cont_calculo . "===" . $key_resumen_celular."|".$descripcion_ser."||".$val_resumen."|".$val_resumen."|".$tmp_inicio."|".$tmp_fin."|\n";
                    
                    $clave_anterior=$co_id." ".$key_resumen_celular." ".$cont_calculo;
                }
                } else {$clave_anterior="";    $no_imprime="1";
                #                 $plan_anterior=""; #SIS RCA 04/01/2008
                #                 $plan_actual=""; #SIS RCA 04/01/2008
                $dolar_anterior="";
                $codigo_servicio_a="";
                $plan_tele_a="";
                $co_id_a="";

				#ClS MGA [5058]
                $band_esp_diario = "0";

            }   
              # $band_esp_diario = "0";
			  #[5099] MGA 
			  $band_borrar_total_bb_diario_3 = "0";
            } else {
            # Para Servicio de Acceso el tiempo de la Activaci�n
            if ($clave_anterior ne "" && $ban_acceso eq "1" && $no_imprime eq "0") {

				#SIS RCA
#				print "DEBUG: \$clave_anterior = ".$clave_anterior."\n";
#				print "DEBUG: \$MAPEOImprime{\$clave_anterior}[0] = ".$MAPEOImprime{$clave_anterior}[0]."\n";
#				print "DEBUG: \$dolar , \$dolar_anterior , \$dolar_new = "."$dolar , $dolar_anterior , $dolar_new"."\n";


				##SIS RCA
				## CLS MGA [5058]
                $band_borrar_total_bb_diario = "0"; 
				

				if ( (lc($descripcion_ser) eq "roaming blackberry diario") && ($band_esp_diario eq "1")) {
						$cont_calculo=&obtiene_contador($descripcion_ser,"1");
						$MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$MAPEOImprime{$clave_anterior}[0]];

						if ( ($band_borrar_total_bb_diario ne "1") and (lc($descripcion_ser) eq "roaming blackberry diario")  ) {
							delete($MAPEOImprime{$clave_anterior});
#							print "BORRADO: $clave_anterior; \$MAPEOImprime{\$clave_anterior} $MAPEOImprime{$clave_anterior} \n\n";
							$band_borrar_total_bb_diario = "1";
						}

						#$band_borrar_total_bb_diario = "0";   
						$clave_anterior = $co_id." ".$key_resumen_celular." ".$cont_calculo;
                     
					


					#print "HELLO\n\n";
#					print "DEBUG2: \$clave_anterior = ".$clave_anterior."\n";
#					print "DEBUG2: \$MAPEOImprime{\$clave_anterior}[0] = ".$MAPEOImprime{$clave_anterior}[0]."\n";


				} else
				{
					$band_esp_diario = "1";
				}

                $tiempo_inicio="";
                $tiempo_fin="";
                if (/DTM\+901\:([^\:]*)\:102\'/) {
                    $tiempo_inicio=&enceraEspacios($1);
                }
                if (/DTM\+902\:([^\:]*)\:102\'/) {
                    $tiempo_fin=&enceraEspacios($1);
                }
				#MOA+60:25.00000:USD:959:9'
                # SIS RCA [2573]
				#SIS CLS MGA 
				if (/MOA\+60\:([^\:]*)\:USD\:959\:9\'/) {
                    $dolar_new=&enceraEspacios($1);
                } else { $dolar_new = $0; }

				#SIS RCA DEBUG
				#print "*\n$_ \n*\nDOLAR_NEW : $dolar_new\n";

                #if ( ($tiempo_inicio ne "") && ($band_borrar_total_bb_diario = "0")) {
                
				

				if ( ($tiempo_inicio ne "") ) {

                 #[5099] MGA
                 $band_borrar_total_bb_diario_2 = "0";  

				#CLS JCE 4697
				#if ( ($tiempo_inicio ne "") && ( lc($descripcion_ser) ne "roaming blackberry diario") ) {
                    @arreglo_num=split(/\|/,$MAPEOImprime{$clave_anterior}[0]);

                    $clave=$arreglo_num[0];
                    $descripcion=$arreglo_num[1];
                    $dolar=$arreglo_num[3];
                    #3097
                    $t_inicio=&enceraEspacios($arreglo_num[5]);
                    $t_fin=&enceraEspacios($arreglo_num[6]);
                    

                    
                    &impuesto_calcular($dolar_anterior,$codigo_servicio_a,$plan_tele_a,$tiempo_fin);
                    #$suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
                    
					
			        #print "*\n$_ \n*\nDOLAR_NEW_M : $co_id\n";
					#print "*\n$_ \n*\nDOLAR_NEW_C2 : $dolar_anterior\n";
					
					
                      if (lc($descripcion_ser) ne "roaming blackberry diario") {
                   
				          #Validacion Momentania por Facturacion
                           $suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
					  	    #print "*\n$_ \n*\n Decripcion : $descripcion_ser\n";
							#print "*\n$_ \n*\n Suma1 : $suma_total\n";
				       } 
					  
					  				  
                      
                        if ( ($band_borrar_total_bb_diario_2 ne "1") and (lc($descripcion_ser) eq "roaming blackberry diario")  ) {
							#print "BORRADO_M: $suma_total; \$suma_total\n\n";
							$suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
							
							if ($band_borrar_total_bb_diario_3 ne "2")
							{
							 $MAPEOTotales{$co_id_a}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
							}
							
							$band_borrar_total_bb_diario_2 = "1";
							$band_borrar_total_bb_diario_3 = "2";
							#print "*\n$_ \n*\n Suma2 : $suma_total\n";
						}else
                        {
					      $suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
				     	  #print "*\n$_ \n*\n Suma_total : $suma_total\n";
						  $MAPEOTotales{$co_id_a}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];
                          #print $codigo_servicio_a." ".$dolar_anterior." IVA ".$total_iva." ICE ".$total_ice." ".$marca_fono."\n";
                    
					   }
                          
                        

					if ($t_inicio eq "") {
                        $t_inicio=$tiempo_inicio;
                        $t_fin=$tiempo_fin;
                    }
                    
                    if ($tiempo_inicio gt $t_inicio) {
                        $tiempo_inicio=$t_inicio;
                    }
                    
                    if ($tiempo_fin lt $t_fin) {
                        $tiempo_fin=$t_fin;
                    }
                    #3097
                    $des_rango_fecha=&rango_fecha($tiempo_inicio,$tiempo_fin);
                    
                    #9321 fecha_contratacion de feature 
					 
                     
						      
#							  $MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar."|".$dolar."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio."|".$MAPEOCtasSVAFecha{$Graba_co_id{$co_id}[0], $codigo_servicio }[2]."|"]; #ECO_CIMA2
							  $MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar."|".$dolar."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio."|".$MAPEOCtasSVAFecha{$Graba_co_id{$co_id}[0], $codigo_servicio }[0]."|"]; #10203
							   #$MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar."|".$dolar."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio."|"."|"]; #ECO_CIMA2
                    # fin 9321 se comenta esta linea


                    #SIS RCA DEBUG
#					print "G ".$MAPEOImprime{$clave_anterior}[0]." ".$t_inicio." ".$t_fin." ".$tiempo_inicio." ".$tiempo_fin."\n";
                    #                   print $clave_anterior." Valores ".$MAPEOImprime{$clave_anterior}[0]."\n";
                    
                    #SIS RCA DEBUG
#                    print "G:".$clave_anterior . "===" . $clave."|".$descripcion."|".$des_rango_fecha."|".$dolar."|".$dolar."|".$tiempo_inicio."|".$tiempo_fin."|\n\n\n";
                } else
				{
                    
					@arreglo_num=split(/\|/,$MAPEOImprime{$clave_anterior}[0]);

                    $clave=$arreglo_num[0];
                    $descripcion=$arreglo_num[1];
                    $dolar_anterior=$dolar_new;


                    #$val_resumen=$dolar_anterior + $val;
                    
                    &impuesto_calcular($dolar_anterior,$codigo_servicio_a,$plan_tele_a,$tiempo_fin);
                    $suma_total=$MAPEOTotales{$co_id}[0]+$dolar_anterior;
                    $MAPEOTotales{$co_id_a}=[$suma_total, $total_iva, $total_ice,0,$marca_fono];

					#3097
                    $des_rango_fecha=&rango_fecha($tiempo_inicio,$tiempo_fin);

                    $dolar_anterior=&formatea_numeros($dolar_anterior);
                    $dolar_anterior=~ tr/\.//d;
                    

                    #9321 fecha_contratacion de feature


#							  $MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar_anterior."|".$dolar_anterior."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio."|".$MAPEOCtasSVAFecha{$Graba_co_id{$co_id}[0], $codigo_servicio }[2]."|"]; #ECO_CIMA
							  $MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar_anterior."|".$dolar_anterior."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio."|".$MAPEOCtasSVAFecha{$Graba_co_id{$co_id}[0], $codigo_servicio }[0]."|"]; #10203
							  #$MAPEOImprime{$clave_anterior}=[$clave."|".$descripcion."|".$des_rango_fecha."|".$dolar_anterior."|".$dolar_anterior."|".$tiempo_inicio."|".$tiempo_fin."|".$codigo_servicio."|"."|"]; #ECO_CIMA
                            
								##SIS RCA DEBUG
								#  print "F:".$clave_anterior . "===" . $clave."|".$descripcion."|".$des_rango_fecha."|".$dolar_anterior."|".$dolar_anterior."|".$tiempo_inicio."|".$tiempo_fin."|\n\n\n";

				}

            }
            

			if (lc($descripcion_ser) ne "roaming blackberry diario") {
				$clave_anterior="";
			}
            #          $plan_anterior=""; #SIS RCA 04/01/2008
            #        $plan_actual=""; #SIS RCA 04/01/2008
            
        }
    }
}

sub buscaValoresResumeCelular{
#LIN+12++TP088.4.SP02.AM001.U:SA::++04'
#PIA+1+BASE.F.I.4.3.NOP.ZP13.1.2++'
#IMD++8+PRO::::'
#IMD++8+TM:::TP088:Especial 6'
#IMD++8+SP:::SP02:AMPS ?: Servicios Basicos '
#IMD++8+SN:::AM001:Servicio de Voz'
#QTY+107:83:UNI'
#MOA+125:4.66:USD:959:9'
#MOA+125:4.66:USD:959:19'

#PIA+1+BASE.F.I.4.3.                  NOP.ZP13.1.2++'

if ( /PIA\+1\+*/ ) {    
#  /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'\n/;
  /PIA\+1\+([^\.]*)\..+\.([^\.]*)\.([^\.]*)\.\d\.\d+\+\+\'\n/;
  $calltype=$1;
  $ttcode=$2;
  $tzcode=$3;
  if ($calltype eq "IC" || $calltype eq "BASE") {
	if ( /IMD\+\+8\+SN\:\:\:([^\:]*)\:([^\:\']*)\'\n[^\n]/ ) {
		  $codigo_servicio=$1;
		  #print "codigo servicio" .$codigo_servicio."|\n" # para saber que codigo de servicio esta enviando.
		 if (/IMD\+\+8\+TM\:\:\:([^\:]*)\:([^\:\']*)\'/) {

		   if ($1 eq "ROTIN" || $1 eq "ROTOP" || $1 eq "ROTIT"|| $1 eq "RITIM" || $1 eq "RITIA"  || $1 eq "RITIN" || $1 eq "ROTIN" || $1 eq "TPESN" || $1 eq "POCC") {
#			  $plan_tele1=$plan_tele;
#			  $des_plan_tele1=$des_plan_tele;
			  $plan_tele=$1;
			  $des_plan_tele=$2;
			  
			  ####Mejora FMI para plan no impreso  18/feb/2008
                            if( $co_id_alter2 ne $co_id){
		   		&graba_co_id($co_id,$fono,"0",$plan_tele,$des_plan_tele);
		   		$co_id_alter2=$co_id;
	                       # $co_id_alter1=$co_id;
	       	             }
	                   ###############################################18/feb/2008
			  
			 } else {
  			  $plan_tele=$1;
			  $des_plan_tele=$2;
			  $plan_tele1=$plan_tele;
			  $des_plan_tele1=$des_plan_tele;
			  
			  ####Mejora FMI para plan no impreso   18/feb/2008
                            if( $co_id_alter2 ne $co_id){
		   		&graba_co_id($co_id,$fono,"0",$plan_tele,$des_plan_tele);
		   		$co_id_alter2=$co_id;
	                        #$co_id_alter1=$co_id;
	       	             }
	                   ###############################################18/feb/2008
			  
        	    # SIS EXT SUD MPA, Mayo 11,2007
              # Para tomar el primer registro de Tarifa B�sica
         
			if ($codigo_servicio eq "PV002") {
                if ($bandera_plan eq "0") {
		      #  print "Entrada 3 FONO ".$fono." Codigo Servicio ".$codigo_servicio." Co_id ".$co_id_det." Descripcion plan1: ".$des_plan_tele."R |\n" ;	
				&graba_co_id($co_id,$fono,"0",$plan_tele1,$des_plan_tele1);
				$bandera_plan = "1";
				
				####Mejora FMI para plan no impreso   18/feb/2008
			         $co_id_alter1=$co_id;
			         $co_id_alter2=$co_id;
                }

			}        	 			  

			 }
		 }

		 # Otengo el Valor el 2do MOA #
		 if (/MOA.*\nMOA\+125\:([^\n\:]*)\:USD.*\n/) {
			 $dolar=$1;
		 }	
		 ($descripcion_ser,$des,$sh_des)=&busca_descripcion_etiqueta($codigo_servicio,$tzcode,$calltype,$ttcode,$plan_tele,"");
		 if ($des eq "") {
			 $des=$sh_des;
		 }
		 $val_dol=$ResumenConsumos{$co_id,$des}[0];
		 $val_dol=$val_dol + $dolar;
		 $ResumenConsumos{$co_id,$des}=[$val_dol];
	} # fi SN
   } # fi calltype
} # fi PIA

} # End buscaValoresResumeCelular

#CLS_MGA
sub tim2_calcula {

	foreach  (keys %ResumenCelular ) {
   	  @arreglo_num=split(/\|/,$_);
 	  $co_id=$arreglo_num[0];
	  $tipo=$arreglo_num[1];
	  $clave=$co_id;
	  $cta_ps="";
	  #3925
#	  if ($tipo eq "Llamadas Porta a Porta Pico" || $tipo eq "Llamadas Porta a Porta No Pico" || $tipo eq "Llamada Entrante Gratis" || $tipo eq "" || $tipo eq "Llamadas Porta a Porta" || $tipo eq "Llamadas Porta a Otecel" || $tipo eq "Llamada a Red Inteligente" || $tipo eq "Llamadas Porta a Telecsa" || $tipo eq "Llamadas Porta a Fijas" || $tipo eq "Llamadas Internacionales" || $tipo eq "Llamadas Porta a Otecel Pico" || $tipo eq "Llamadas Porta a Otecel No Pico" || $tipo eq "Llamadas Porta a Telecsa Pico" || $tipo eq "Llamadas Porta a Telecsa No Pico" || $tipo eq "Llamadas Porta a Fijas Pico" || $tipo eq "Llamadas Porta a Fijas No Pico" || $tipo eq "Llamadas Internacionales Pico" || $tipo eq "Llamadas Internacionales No Pico" || $tipo eq "Llamada Porta a Porta in Pool"|| $tipo eq "Consumo VC Tiempo Aire Pico"|| $tipo eq "Consumo VC Tiempo Aire No Pico"|| $tipo eq "Min Videollamada Adic."|| $tipo eq "Consumo VC Tiempo Porta Otros") {
   	  if ($tipo eq $MapeoEtiquetasVoz{$tipo}[0]) {
		   null;
	  } else {		

 	  	  $total_iva=$MAPEOTotales{$clave}[1];
		  $total_ice=$MAPEOTotales{$clave}[2];
		  $valor_dolares=$ResumenConsumos{$co_id,$tipo}[0];
		  $valor_dolares=&formatea_numeros($valor_dolares);

		  $val_resumen_cel=$ResumenCelular{$_}[0];
  	      $val_resumen_cel=&formatea_numeros($valor_dolares);
		  $val_resumen_cel=~ tr/\\\.//d;

		  ($descripcion,$etiqueta,$total_iva,$total_ice)=&obtiene_impuesto($tipo,$ResumenCelular{$_}[1],$ResumenCelular{$_}[2],$total_iva,$total_ice,$valor_dolares,"0");
		  $cont_calculo=&obtiene_contador($etiqueta,"0");
		  $suma_total=$MAPEOTotales{$clave}[0]+$valor_dolares;
		  $MAPEOTotales{$clave}=[$suma_total,$total_iva,$total_ice,$MAPEOTotales{$clave}[3]];

		  ($valor_r,$tmp_inicio,$tmp_fin)=&obtiene_valor_resumen($co_id,$key_resumen_celular,$cont_calculo); #3097
		  $val_resumen=$valor_dolares + $valor_r;

   	      $val_resumen=&formatea_numeros($valor_dolares);
		  $val_resumen=~ tr/\\\.//d;
          $cta_ps=$MAPEOservice_label2{$etiqueta}[0]; #ECO_CIMA
		  #print "Output. $cta_ps \n";
  		  $MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$key_resumen_celular."|".$etiqueta."|".$descripcion."|".$val_resumen."|".$val_resumen_cel."|".$tmp_inicio."|".$tmp_fin."|".$cta_ps."|"]; #ECO_CIMA
           
		  #SIS RCA DEBUG
		  #print "*" . $co_id." ".$key_resumen_celular." ".$cont_calculo . "===" . $key_resumen_celular."|".$etiqueta."|".$descripcion."|".$val_resumen."|".$val_resumen_cel."|\n"
	  }
	}

	foreach  (keys %ResumenCelularPicoNoPico ) {

   	  
	  @arreglo_num=split(/\|/,$_);
 	  $co_id=$arreglo_num[0];
	  $tipo=$arreglo_num[1];
	  $clave=$co_id;
	  $cta_ps="";
      
	  #CLS_MGA
	  #SIS RCA agregar "IN"
	  #print "Tipo: ".$tipo." MapeoVoz: ".$MapeoEtiquetasVozTipo{$tipo}[0]."\n";#debug
	  if ($tipo eq $MapeoEtiquetasVozTipo{$tipo}[0] ) { #3925
#	  if ($tipo eq $MapeoEtiquetasVozTipo{$tipo}[0] && $tipo ne "") { #3925
#	  if ($tipo eq "NPP" || $tipo eq "P.P" || $tipo eq "PP" || $tipo eq "PO"|| $tipo eq "IN1" || $tipo eq "IN2" || $tipo eq "PT" || $tipo eq "PF" || $tipo eq "PI" || $tipo eq "P.PO" ||  $tipo eq "NPPO" || $tipo eq "P.PT" || $tipo eq "NPPT" || $tipo eq "P.PF" || $tipo eq "NPPF" || $tipo eq "P.IP" || $tipo eq "NPPI" || $tipo eq "VI"|| $tipo eq "VN1"|| $tipo eq "VP1"|| $tipo eq "VL"|| $tipo eq "VO") {		 
		 #print "Resumen normal ".$_."|".$ResumenCelularPicoNoPico{$_}[1]."\n";  #Debug.
		 $total_iva=$MAPEOTotales{$clave}[1];
		 $total_ice=$MAPEOTotales{$clave}[2];

		 $valor_dolares=$ResumenConsumos{$co_id,$tipo}[0];
		 $valor_dolares=&formatea_numeros($valor_dolares);

	     $val_resumen_cel=$ResumenCelular{$_}[0];
  	     $val_resumen_cel=&formatea_numeros($valor_dolares);
		 $val_resumen_cel=~ tr/\\\.//d;

		 #$suma_seg_fono=$MAPEOTotales{$clave}[3] + $ResumenCelularPicoNoPico{$_}[1];
		 #$suma_seg_fono_gratis=$MAPEOTotales{$clave}[4] + $ResumenCelularPicoNoPico{$_}[3];

         $seg_ResumenCelularPicoNoPico=$ResumenCelularPicoNoPico{$_}[1];
         $segGratis_ResumenCelularPicoNoPico=$ResumenCelularPicoNoPico{$_}[3];

         $valor_Resumen_IVA=$ResumenCelularPicoNoPico{$_}[4];
 		 $valor_Resumen_IVA=&formatea_numeros($valor_Resumen_IVA);

         $valor_Resumen_IVAICE=$ResumenCelularPicoNoPico{$_}[5];
		 $valor_Resumen_IVAICE=&formatea_numeros($valor_Resumen_IVAICE);

         $segundos_Resumen_IVA=$ResumenCelularPicoNoPico{$_}[6];
         $segundosG_Resumen_IVA=$ResumenCelularPicoNoPico{$_}[7];
         $segundos_Resumen_IVAICE=$ResumenCelularPicoNoPico{$_}[8];
         $segundosG_Resumen_IVAICE=$ResumenCelularPicoNoPico{$_}[9];

		 $suma_seg_fono=$MAPEOTotales{$clave}[3] + $seg_ResumenCelularPicoNoPico;
		 $suma_seg_fono_gratis=$MAPEOTotales{$clave}[4] + $segGratis_ResumenCelularPicoNoPico;

#         print $segundos_Resumen_IVA." ".$segundosG_Resumen_IVA." ".$segundos_Resumen_IVAICE." ".$segundosG_Resumen_IVAICE."\n";

		 ($descripcion,$etiqueta,$total_iva,$total_ice)=&obtiene_impuesto_IVAICE($tipo,$seg_ResumenCelularPicoNoPico,$ResumenCelularPicoNoPico{$_}[2],$total_iva,$total_ice,$valor_dolares,$segGratis_ResumenCelularPicoNoPico,$valor_Resumen_IVA,$valor_Resumen_IVAICE,$segundos_Resumen_IVA,$segundosG_Resumen_IVA,$segundos_Resumen_IVAICE,$segundosG_Resumen_IVAICE);

		 $cont_calculo=&obtiene_contador($etiqueta,"0");

		 $suma_total=$MAPEOTotales{$clave}[0]+$valor_dolares;
		 #print $clave."[".$suma_total."!!".$total_iva."!!".$total_ice."!!".$suma_seg_fono."!!".$suma_seg_fono_gratis."] \n"; #Debug.
		 $MAPEOTotales{$clave}=[$suma_total,$total_iva,$total_ice,$suma_seg_fono,$suma_seg_fono_gratis];
		 ($valor_r,$tmp_inicio,$tmp_fin)=&obtiene_valor_resumen($co_id,$key_resumen_celular,$cont_calculo); #3097
		 $val_resumen=$valor_dolares + $valor_r;
	     $val_resumen=&formatea_numeros($valor_dolares);
		 $val_resumen=~ tr/\\\.//d;

	     $valor_Resumen_IVA=&formatea_numeros($valor_Resumen_IVA);
	     $valor_Resumen_IVAICE=&formatea_numeros($valor_Resumen_IVAICE);
		 if ($MapeoEtiquetasVozTipo{$tipo}[2] eq "1") {#3925
			$cta_ps=$MAPEOservice_label2{$etiqueta}[0]; #ECO_CIMA
			#print "Output. $cta_ps \n";
		    $MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$key_resumen_celular."|".$etiqueta."|".$descripcion."|".$val_resumen."|".$val_resumen_cel."|".$valor_Resumen_IVA."|".$valor_Resumen_IVAICE."|".$cta_ps."|"];	#ECO_CIMA	 
		 }

	  }

	}

	foreach  (keys %MAPEOTotales ) {
		#IVA ICE  #
		@arreglo_num=split(/\|/,$_);
 		$co_id=$arreglo_num[0];	
		$marca=$MAPEOTotales{$_}[4];
		$key_resumen_celular="42000";
		$key_resumen_total_celular="42200";
		$cta_ps="";
		if ($marca eq "ISD") {
			$key_resumen_celular="31000";
			$key_resumen_total_celular="31100";
		} else {
#			&etiquetas_resumen_celular($plan_tele1,$des_plan_tele1);
			&etiquetas_resumen_celular($Graba_co_id{$co_id}[2],$Graba_co_id{$co_id}[3]);
		}

		$IVAv=$MAPEOTotales{$_}[1]*$por_iva;
		$ICEv=$MAPEOTotales{$_}[2]*$por_ice;
		$ICEv1=$MAPEOTotales{$_}[2]*$por_ice;
		$IVAv=&formatea_numeros($IVAv);
		$ICEv=&formatea_numeros($ICEv);

		$suma_total=$MAPEOTotales{$_}[0]+$IVAv+$ICEv;
		$suma_total=&formatea_numeros($suma_total);
		$suma_total=~ tr/\\\.//d;

		$cont_calculo=&obtiene_contador("IVA","0");
		$IVAv=~ tr/\\\.//d;
		$MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$key_resumen_celular."|".$des_iva."||".$IVAv."|".$IVAv."|"];
		$cont_calculo=&obtiene_contador("ICE","0");;

		$ICEv=~ tr/\\\.//d;
#		$MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$key_resumen_celular."|".$des_ice."||".$ICEv."|".$ICEv."|"]; #08/08/08
		$cont_calculo=&obtiene_contador("SUMA","0");
		$MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}=[$key_resumen_total_celular."|".$suma_total."|"];
#		print $_."!!".$suma_total."!!".$MAPEOTotales{$_}[3]."!!".$MAPEOTotales{$_}[4]."\n"; #Debug

		#Nuevo Formato de Puntos PCA 4505 INICIO#
        if ($Graba_co_id{$co_id}[0] != "00000000000") {
			$key_resumen_puntos="42300";
			$informacion_ptos=&obtieneInformacionPuntos($account_no_resumen.$Graba_co_id{$co_id}[0]);
			if ($informacion_ptos ne "") {
#				print "Puntos ".$account_no_resumen.$Graba_co_id{$co_id}[0] ." ".$informacion_ptos."\n";
				$clave=$key_resumen_puntos.'\|'.'\*'	;
				@arreglo_num_ptos=split($clave,$informacion_ptos);
				$des=&enceraEspacios($arreglo_num_ptos[1]);
	#			$des="42300|* Puntos Equipos: 138 Puntos Servicios: 125 Cantidad de puntos por vencer: 0 Fecha Prox. de Vencimiento: 13/05/2011.|";
#	            print " 0 ".&enceraEspacios($arreglo_num_ptos[0])."\n";
#	            print " 1 ".&enceraEspacios($arreglo_num_ptos[1])."\n";
#	            print " 2 ".&enceraEspacios($arreglo_num_ptos[2])."\n";
#	            print " 3 ".&enceraEspacios($arreglo_num_ptos[3])."\n";
				$MAPEOImprime{$co_id." ".$key_resumen_puntos." 00000001"}=[$key_resumen_puntos."|* ".$des]; 
#				print $key_resumen_puntos." Resument ".$des."\n";
				#$des="42300|* Mas informacion sobre el detalle de puntos de Circulo PORTA en Tu Cuenta Porta, ingresando en www.porta.net o tambien llamando al *611|";
				$des=&enceraEspacios($arreglo_num_ptos[2]);
#				print " DES ".$des."\n";
				$MAPEOImprime{$co_id." ".$key_resumen_puntos." 00000002"}=[$key_resumen_puntos."|* ".$des]; 
			}
        }
		# PCA FIN

		$ConsumoCelular{$_}=[$suma_total,$MAPEOTotales{$_}[3], $MAPEOTotales{$_}[4]];
		if ($Graba_co_id{$co_id}[0] != "00000000000") {
			$MAPEOImprime{$co_id." 40000"}=["40000|".$Graba_co_id{$co_id}[0]."|".$co_id."|"];
		}
	  $fono1=$Graba_co_id{$co_id}[0];
      &graba_co_id($co_id,$fono1,"2");
	}
	undef(%MAPEOTotales);
}



sub calculo_consumo_mes_celular {
$key_consumo_mes="30100";
$total_consumo_mes=0;
$total_seg_mes=0;
$total_seg_mes_gratis=0;
	foreach  (keys %ConsumoCelular ) {
 		$co_id=$_;
 		$fono=$Graba_co_id{$co_id}[0];
		$des_con_mes=&formatea_seg_minutos($ConsumoCelular{$_}[1]);
		if ($fono eq "00000000000") {
			#Cambio PCA#
			$MAPEOImprimeConsumoCelular{"0 ".$co_id." 30000 ".$fono}=["30000|".$ConsumoCelular{$_}[0]."|"];
		} else {

            #9321  Adiccion informacion de Adendum
                          

						      $MAPEOImprimeConsumoCelular{$co_id." ".$key_consumo_mes." ".$fono}=[$key_consumo_mes."|".$Graba_co_id{$co_id}[0]."||".$des_con_mes."|".$ConsumoCelular{$_}[0]."|".$MAPEOCtasAdendum{$Graba_co_id{$co_id}[0]}[2]."|".$MAPEOCtasAdendum{$Graba_co_id{$co_id}[0]}[3]."|".$MAPEOCtasAdendum{$Graba_co_id{$co_id}[0]}[4]."|".$MAPEOCtasAdendum{$Graba_co_id{$co_id}[0]}[5]."|".$MAPEOCtasAdendum{$Graba_co_id{$co_id}[0]}[6]."|"];
					         #$MAPEOImprimeConsumoCelular{$co_id." ".$key_consumo_mes." ".$fono}=[$key_consumo_mes."|".$Graba_co_id{$co_id}[0]."||".$des_con_mes."|".$ConsumoCelular{$_}[0]."|"."|"."|"."|"."|"."|"];
					        
     		#fin 9321
	
		}

		$total_consumo_mes=$total_consumo_mes + $ConsumoCelular{$_}[0]; # Consumos
		$total_seg_mes=$total_seg_mes+$ConsumoCelular{$_}[1]; #Segundos
		if ($ConsumoCelular{$_}[2] ne "") {
		  $total_seg_mes_gratis=$total_seg_mes_gratis+$ConsumoCelular{$_}[2];
		}

	}
	$des_con_mes=&formatea_seg_minutos($total_seg_mes);
	if ($total_seg_mes_gratis != 0) {
      $des_con_mes_gratis=&formatea_seg_minutos($total_seg_mes_gratis);
      $des_con_mes=$des_con_mes."/ ".$des_con_mes_gratis ." Incluidos";
	}
	$MAPEOImprimeConsumoCelular{"9999999 30200 99999999999"}=["30200|".$des_con_mes."|"];
	$MAPEOImprimeConsumoCelular{"9999999 30300 99999999999"}=["30300|".$total_consumo_mes."|"];
}

sub obtiene_valor_resumen {
my $co_id=$_[0];
my $key_resumen_celular=$_[1];
my $cont_calculo=$_[2];
my $val_resumen;
my $val_dolares=0;

$val_resumen=$MAPEOImprime{$co_id." ".$key_resumen_celular." ".$cont_calculo}[0];
@arreglo_num=split(/\|/,$val_resumen);
$val_dolares=$arreglo_num[3];
$tp_inicio=&enceraEspacios($arreglo_num[5]);
$tp_fin=&enceraEspacios($arreglo_num[6]);
return($val_dolares,$tp_inicio,$tp_fin);
}

sub formatea_seg_minutos {
$vseg=$_[0];
$vseg1=$vseg;
$vseg=$vseg/60;
$var=sprintf('%.2f',$vseg);
($a,$b)=split(/\./,$var);
$b=$vseg1-$a*60;
if ($b == 00) {
	$b="00";
} else { 
if ($b <= 9 ) {
	$b="0".$b;
}
}
if ($a == 0) {
   $des_vseg="(".$b." Seg)";
} else {
   $des_vseg="(".$a." Min ".$b." Seg)";
}

return $des_vseg;
}



sub formatea_seg_minutos_primera_hoja {
$vseg=$_[0];
$vseg_gratis=$_[1];
$vseg1=$vseg - $vseg_gratis;
$vseg2=$vseg1/60;
$var=sprintf('%.2f',$vseg2);
($a,$b)=split(/\./,$var);
$b=$vseg1-$a*60;

if ($b == 00) {
	$b="00";
} else { 
if ($b <= 9 ) {
	$b="0".$b;
}
}

if ($a == 0) {
   $des_vseg="(".$b." Seg)";
} else {
   $des_vseg="(".$a." Min ".$b." Seg)";
}

return $des_vseg;
}


sub obtiene_contador {
my $des=$_[0];
my $desglosa_periodos = $_[1];
my $val="";

#SIS RCA... 
#ClS MGA [5058]
#if ($des eq "Zona Tarjeta") {

#print "Debud no etiqueta".$des."---".$No_etiquetas."\n";
if ($des eq $No_etiquetas{$des}[0]){
  print "Debud no etiqueta2".$des."---".$No_etiquetas{$des}[0]."\n";
  next;
}else
{
   if ($desglosa_periodos eq "0") {
	  $val=$Resumen_Celular_Dos{$des}[0];
    }

}
#CLS_MGA  

if ($val ne "") {
  $valor_obtiene_contador=$val;
} else {
  if ($des eq "Tarifa Basica ") {
	$valor_obtiene_contador="00000001";
} elsif ($des eq $MapeoEtiquetasVoz{$des}[0] ){ #3925
	$valor_obtiene_contador=$MapeoEtiquetasVoz{$des}[1];
#  if ($des eq "Tarifa Basica ") {
#	$valor_obtiene_contador="00000001";
#  } elsif ($des eq "Llamadas Porta a Porta" ){
#	$valor_obtiene_contador="00000002";
#  } elsif ($des eq "Llamadas Porta a Telecsa" ){
#	$valor_obtiene_contador="00000003";
#  } elsif ($des eq "Llamadas Porta a Otecel" ){
#	$valor_obtiene_contador="00000004";
#  } elsif ($des eq "Llamada Porta a Fijas" ){
#	$valor_obtiene_contador="00000005";
#  } elsif ($des eq "Llamadas Internacionales" ){
#	$valor_obtiene_contador="00000006";
#   } elsif ($des eq "Llamadas Porta a Porta Pico" ){
#	$valor_obtiene_contador="00000007";
#  } elsif ($des eq "Llamadas Porta a Porta No Pico" ){
#	$valor_obtiene_contador="00000008";
#  } elsif ($des eq "Llamadas 1700 a otra red" ){
#	$valor_obtiene_contador="00000009";
#  } elsif ($des eq "Llamadas 1800 a otra red" ){
#	$valor_obtiene_contador="00000010";
#  } elsif ($des eq "Llamadas Porta a Otecel Pico" ){
#	$valor_obtiene_contador="00000011";
#  } elsif ($des eq "Llamadas Porta a Otecel No Pico" ){
#	$valor_obtiene_contador="00000012";
#  } elsif ($des eq "Llamadas Porta a Telecsa Pico" ){
#	$valor_obtiene_contador="00000013";
#  } elsif ($des eq "Llamadas Porta a Telecsa No Pico" ){
#	$valor_obtiene_contador="00000014";
#  } elsif ($des eq "Llamadas Porta a Fijas Pico" ){
#	$valor_obtiene_contador="00000015";
#  } elsif ($des eq "Llamadas Porta a Fijas No Pico" ){
#	$valor_obtiene_contador="00000016";
#  } elsif ($des eq "Llamadas Internacionales Pico" ){
#	$valor_obtiene_contador="00000017";
#  } elsif ($des eq "Llamadas Internacionales No Pico" ){
#	$valor_obtiene_contador="00000018";
#  } elsif ($des eq "Llamada Porta a Porta in Pool" ){
#	$valor_obtiene_contador="00000019";
#  }elsif ($des eq "Consumo VC Tiempo Aire Pico" ){
#	$valor_obtiene_contador="00000020";
#  } elsif ($des eq "Consumo VC Tiempo Aire No Pico" ){
#	$valor_obtiene_contador="00000021";
#  } elsif ($des eq "Consumo VC Tiempo Porta Porta" ){
#	$valor_obtiene_contador="00000022";
#  } elsif ($des eq "Consumo VC Tiempo Porta Otros" ){
#	$valor_obtiene_contador="00000023";
  }else	{
	   $cont_calculo=$cont_calculo_mayor;
	   $cont_calculo=$cont_calculo+1;
		if ($cont_calculo > 9) { $valor_obtiene_contador="000000".$cont_calculo;
		} else { $valor_obtiene_contador="0000000".$cont_calculo;}
 	   $cont_calculo_mayor=$cont_calculo_mayor+1;
   }
}

$Resumen_Celular_Dos{$des}=[$valor_obtiene_contador];
return($valor_obtiene_contador);
}


sub formatea_numeros_detalles {
    my $var=$_[0];
	my $valor_nuevo;
	$valor_nuevo=sprintf('%.5f',$var);
	$valor_nuevo=~ tr/\\\.//d;
	return $valor_nuevo;
}


sub formatea_numeros_detalles_iva_ice {
    my $var=$_[0];
	my $valor_nuevo;
	$valor_nuevo=sprintf('%.2f',$var);
	$valor_nuevo=formatea_numeros($valor_nuevo);
	return $valor_nuevo;
}

sub formatea_numeros {
    my $var=$_[0];
    my $var11=$_[0];
	my $valor_nuevo;
	($a,$b)=split(/\./,$var);
	if (length($b) == 3 && substr($b,2,1) == 5) {
	  if (substr($b,1,1) == 9) {
  	    if (substr($b,0,1) == 9) {
  	     $var=($a+1).".00";
		} else {
  	     $var=$a.".".(substr($b,0,1)+1).".0";
		}
	  } else {
  	    $var=$a.".".substr($b,0,1).(substr($b,1,1)+1);
	  }
	} 
    $valor_nuevo=sprintf('%.2f',$var);
#    print $var11." ".sprintf('%.2f',$var11)." ".$valor_nuevo."\n";
	return $valor_nuevo;
}


sub impuesto_calcular {
$valor_impuesto_calcula=$_[0];
$servicio_c=$_[1];
$plan_c=$_[2];
$tmp_final=$_[3];
$tm_code=$MAPEOrateplan{$plan_c}[0];
$sn_code=$MAPEOmpusntab{$servicio_c}[0];
$impuesto=$MAPEOmpulktmb{$tm_code." ".$sn_code}[2];
if ($tmp_final le "20071231") {
	$impuesto=$MAPEOmpulktmbANT{$tm_code." ".$sn_code}[2];
#	print "SI"."\n";
}
#print $servicio_c." ".$tm_code." ".$sn_code." ".$impuesto." ".$tmp_final."\n";
#print $tmp_final." ".$impuesto."\n";

if ($impuesto eq "IVA-ICE") {
	 $total_iva=$MAPEOTotales{$co_id}[1]+$valor_impuesto_calcula;
	 $total_ice=$MAPEOTotales{$co_id}[2]+$valor_impuesto_calcula;
} 
if ($impuesto eq "IVA" || "3IVA-ICE") { #cambio por el ICE
	 $total_iva=$MAPEOTotales{$co_id}[1]+$valor_impuesto_calcula;
} 
if ($impuesto eq "ICE") {
	 $total_ice=$MAPEOTotales{$co_id}[2]+$valor_impuesto_calcula;
} 

}

sub obtiene_impuesto {
$des=$_[0];
$seg=$_[1];
$tipo_impuesto=uc($_[2]);
$tot_iva=$_[3];
$tot_ice=$_[4];
$valor_dol=$_[5];
$seg_gratis=$_[6];

$descripcionV=&formatea_seg_minutos($seg);

$total_ivaV=$tot_iva;
$total_iceV=$tot_ice;

if ($tipo_impuesto eq "ICE" || $tipo_impuesto eq "IVA-ICE" || $tipo_impuesto eq "3IVA-ICE" ) {#cambios por ICE
	$total_iceV=$total_iceV+$valor_dol;
} 

if ($tipo_impuesto eq "IVA" || $tipo_impuesto eq "IVA-ICE" || $tipo_impuesto eq "3IVA-ICE" ) {#cambios por ICE
	$total_ivaV=$total_ivaV+$valor_dol;
} 

$etiquetaV=$des;

#3925
if ($etiquetaV eq $MapeoEtiquetasVozTipo{$etiquetaV}[0] ) {
$etiquetaV=$MapeoEtiquetasVozTipo{$etiquetaV}[1];
if ($seg_gratis > 0 ) {
  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
  $descripcionVSeg=~ tr/\)//d;
  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
}
}
#
#if ($etiquetaV eq "NPP") {
#$etiquetaV="Llamadas Porta a Porta No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.P") {
#$etiquetaV="Llamadas Porta a Porta Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/" .$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PP") {
#$etiquetaV="Llamadas Porta a Porta";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PO") {
#$etiquetaV="Llamadas Porta a Otecel";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#
##SIS RCA, 22 marzo 2007.
#if ($etiquetaV eq "IN1") {
#$etiquetaV="Llamadas 1800 a otra red";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
##SIS RCA, 22 marzo 2007.
#if ($etiquetaV eq "IN2") {
#$etiquetaV="Llamadas 1700 a otra red";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PT") {
#$etiquetaV="Llamadas Porta a Telecsa";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PF") {
#$etiquetaV="Llamadas Porta a Fijas";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PI") {
#$etiquetaV="Llamadas Internacionales";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.PO") {
#$etiquetaV="Llamadas Porta a Otecel Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "NPPO") {
#$etiquetaV="Llamadas Porta a Otecel No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.PT") {
#$etiquetaV="Llamadas Porta a Telecsa Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "NPPT") {
#$etiquetaV="Llamadas Porta a Telecsa No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.PF") {
#$etiquetaV="Llamadas Porta a Fijas Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "NPPF") {
#$etiquetaV="Llamadas Porta a Fijas No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.IP") {
#$etiquetaV="Llamadas Internacionales Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "NPPI") {
#$etiquetaV="Llamadas Internacionales No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
##CLS_MGA
#if ($etiquetaV eq "VI") {
#$etiquetaV="Llamada Porta a Porta in Pool";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
##CLS_MGA
#
#if ($etiquetaV eq "VP1") {
#$etiquetaV="Consumo VC Tiempo Aire No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "VN1") {
#$etiquetaV="Consumo VC Tiempo Aire Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "VL") {
#$etiquetaV="Min Videollamada Adic.";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "VO"){
#$etiquetaV="Consumo VC Tiempo Porta Otros";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
##CLS_MGA
#

$val=$Minutos{$etiquetaV}[0];
$gratis=$Minutos{$etiquetaV}[1];
$val=$val+$seg;
$gratis=$gratis+$seg_gratis;
#print "// $etiquetaV : $val  // $gratis \n";
$Minutos{$etiquetaV}=[$val,$gratis];


#print $descripcionV."|".$etiquetaV."|".$total_ivaV."|".$total_iceV."\n";
return($descripcionV,$etiquetaV,$total_ivaV,$total_iceV);
}


sub obtiene_impuesto_IVAICE {
$des=$_[0];
$seg=$_[1];
$tipo_impuesto=uc($_[2]);
$tot_iva=$_[3];
$tot_ice=$_[4];
$valor_dol=$_[5];
$seg_gratis=$_[6];
$valor_dol_iva=$_[7];
$valor_dol_ivaice=$_[8];
$seg_iva=$_[9];
$segG_iva=$_[10];
$seg_ivaice=$_[11];
$segG_ivaice=$_[12];


$descripcionV=&formatea_seg_minutos($seg);

$total_ivaV=$tot_iva;
$total_iceV=$tot_ice;

#if ($tipo_impuesto eq "ICE" || $tipo_impuesto eq "IVA-ICE" ) {
#	$total_iceV=$total_iceV+$valor_dol;
#} 
#
#if ($tipo_impuesto eq "IVA" || $tipo_impuesto eq "IVA-ICE") {
#	$total_ivaV=$total_ivaV+$valor_dol;
#}

$total_ivaV=$total_ivaV+$valor_dol;
#$total_ivaV=$total_ivaV+$valor_dol_iva;
$total_iceV=$total_iceV+$valor_dol_ivaice;
#$total_ivaV=$total_ivaV+$valor_dol_ivaice;

$etiquetaV=$des;

#3925
if ($etiquetaV eq $MapeoEtiquetasVozTipo{$etiquetaV}[0] ) {
$etiquetaV=$MapeoEtiquetasVozTipo{$etiquetaV}[1];
if ($seg_gratis > 0 ) {
  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
  $descripcionVSeg=~ tr/\)//d;
  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
}
}
#if ($etiquetaV eq "NPP") {
#$etiquetaV="Llamadas Porta a Porta No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.P") {
#$etiquetaV="Llamadas Porta a Porta Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/" .$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PP") {
#$etiquetaV="Llamadas Porta a Porta";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PO") {
#$etiquetaV="Llamadas Porta a Otecel";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#
##SIS RCA, 22 marzo 2007.
#if ($etiquetaV eq "IN1") {
#$etiquetaV="Llamadas 1800 a otra red";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
##SIS RCA, 22 marzo 2007.
#if ($etiquetaV eq "IN2") {
#$etiquetaV="Llamadas 1700 a otra red";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PT") {
#$etiquetaV="Llamadas Porta a Telecsa";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PF") {
#$etiquetaV="Llamadas Porta a Fijas";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "PI") {
#$etiquetaV="Llamadas Internacionales";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.PO") {
#$etiquetaV="Llamadas Porta a Otecel Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "NPPO") {
#$etiquetaV="Llamadas Porta a Otecel No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.PT") {
#$etiquetaV="Llamadas Porta a Telecsa Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "NPPT") {
#$etiquetaV="Llamadas Porta a Telecsa No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.PF") {
#$etiquetaV="Llamadas Porta a Fijas Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "NPPF") {
#$etiquetaV="Llamadas Porta a Fijas No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "P.IP") {
#$etiquetaV="Llamadas Internacionales Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "NPPI") {
#$etiquetaV="Llamadas Internacionales No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
##CLS_MGA
#if ($etiquetaV eq "VI") {
#$etiquetaV="Llamada Porta a Porta in Pool";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
##CLS_MGA
#if ($etiquetaV eq "VP1") {
#$etiquetaV="Consumo VC Tiempo Aire No Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "VN1"){
#$etiquetaV="Consumo VC Tiempo Aire Pico";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "VL") {
# $etiquetaV="Min Videollamada Adic.";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
#
#if ($etiquetaV eq "VO") {
#$etiquetaV="Consumo VC Tiempo Porta Otros";
#if ($seg_gratis > 0 ) {
#  $descripcionVSeg=&formatea_seg_minutos($seg_gratis);
#  $descripcionVSeg=~ tr/\)//d;
#  $descripcionV=$descripcionV."/".$descripcionVSeg." Incluidos)";
#}
#}
##CLS_MGA

$val=$Minutos{$etiquetaV}[0];
$gratis=$Minutos{$etiquetaV}[1];
$val=$val+$seg;
$gratis=$gratis+$seg_gratis;

$valseg_iva=$Minutos{$etiquetaV}[2];
$gratisseg_iva=$Minutos{$etiquetaV}[3];
$valseg_ivaice=$Minutos{$etiquetaV}[4];
$gratisseg_ivaice=$Minutos{$etiquetaV}[5];

$valseg_iva=$valseg_iva+$seg_iva;
$valseg_ivaice=$valseg_ivaice+$seg_ivaice;

$gratisseg_iva=$gratisseg_iva+$segG_iva;
$gratisseg_ivaice=$gratisseg_ivaice+$segG_ivaice;


#print "// $etiquetaV : $val  // $gratis \n";
$Minutos{$etiquetaV}=[$val,$gratis,$valseg_iva,$gratisseg_iva,$valseg_ivaice,$gratisseg_ivaice];


#print $descripcionV."|".$etiquetaV."|".$total_ivaV."|".$total_iceV."\n";
return($descripcionV,$etiquetaV,$total_ivaV,$total_iceV);
}


sub busca_descripcion_etiqueta{

#print "\nInput. $_[0] - $_[1] - $_[2] - $_[3] - $_[4] - $_[5] \n";
#$services,$zona,'BASE',$tariff_time,$tariff_model

$servshdes=$_[0];
$tzcode=$_[1];
$calltype=$_[2];
$ttcode=$_[3];
$tmshdes=$_[4];
$clase_llam=$_[5];
my $des;
my $sh_name;
my $sh_des;
my $taxinf;
my $label_id="";
my $legend_id="";
my $tipo="";

$codigo_servicio_cl="";##8504

$tipo=$MAPEObgh_tariff_plan{$tmshdes,1}[1];
if ($tipo eq "") {
	$tipo ="D";
}
if ($clase_llam eq "V" || $tmshdes eq "ROTIN" || $tmshdes eq "ROTOP" || $tmshdes eq "ROTIT"|| $tmshdes eq "RITIM" || $tmshdes eq "RITIA"  || $tmshdes eq "RITIN" || $tmshdes eq "ROTIN" ) {
$des="Roaming llamadas";
$sh_name="Roaming Internacional";
$sh_des="Roaming Internacional";
#$taxinf="Excento";

#INI 10182 - UEES 
$label_id=$MAPEOservice_labelR{$sh_des}[0]; #10182
$taxinf=$MAPEOservice_label{$label_id}[2]; #10182
  if ($taxinf eq "") {
    $taxinf="IVA"; 
  }
#FIN 10182 - UEES 

	#8504 - ini - SUD CAC - Envia el codigo de Servicio para Roming
	if ( $band_electronica eq "4"){$codigo_servicio_cl="CL015";}
	#8504 -fin - SUD CAC 

} else {

#print $servshdes,$calltype,$tzcode,$ttcode,$tipo."\n";
##{1,BASE,ZPG55,1,D}[60,67];
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,$tzcode,$ttcode,$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,$tzcode,$ttcode,$tipo}[1]);

	if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,$tzcode,"1",$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,$tzcode,"1",$tipo}[1]);
	}

	if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,"1",$ttcode,$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,"1",$ttcode,$tipo}[1]);
	}

	if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{$servshdes,$calltype,"1","1",$tipo}[0],$MAPEOcall_grouping{$servshdes,$calltype,"1","1",$tipo}[1]);
	}

	# 4697 CLS JCEDENO
	if ($label_id eq "") {
	($label_id,$legend_id)=($MAPEOcall_grouping{"1",$calltype,$tzcode,"1",$tipo}[0],$MAPEOcall_grouping{"1",$calltype,$tzcode,"1",$tipo}[1]);
	}


$sh_name=$MAPEOlegend{$legend_id}[0];
$sh_name=&enceraEspacios($sh_name);
$des=$MAPEOservice_label{$label_id}[0];
$sh_des=$MAPEOservice_label{$label_id}[1];
$taxinf=$MAPEOservice_label{$label_id}[2];
$sh_des=&enceraEspacios($sh_des);
$des=&enceraEspacios($des);
$codigo_servicio_cl=$MAPEOservice_label{$label_id}[3];##8504 - SUD CAC 
$codigo_servicio_cl=&enceraEspacios($codigo_servicio_cl);##8504 - SUD CAC

}

#print "\nInput. $_[0] - $_[1] - $_[2] - $_[3] - $_[4] - $_[5] \n";
#print "Output. $des , $sh_name , $sh_des , $taxinf \n";

#ini - 8504 - SUD CAC - se devuelve el codigo_servicio_cl (nuevo codigo del rubro)
#return($des,$sh_name,$sh_des,$taxinf);
return($des,$sh_name,$sh_des,$taxinf,$codigo_servicio_cl);#8504 se aunmenta $codigo_servicio_cl
#fin - 8504 - SUD CAC 

}


sub busca_tipo_llamada{
$zcode=$_[0];
$dato_zona=$MAPEOmpuzntab{$zcode}[1];
return($dato_zona);
}


sub busca_descripcion_servicio{
$zcode=$_[0];
$descripcion_mpusntab=$MAPEOmpusntab{$zcode}[1];
return($descripcion_mpusntab);
}


sub busca_cgi{
$cgi1=$_[0];
$val1=$cgi1;
while ($val1 ne "") {
#	print $val1."\n";
  $valor=$MAPEOmpuzotab{$val1}[1];
  $val1=substr($val1,0,-1);
  if ($valor ne "") {
      return($valor);
  } 
}
}

sub busca_payment_all{
$vpaymentcode=$_[0];
$vpaymentname=$MAPEOpaymenttype_all{$vpaymentcode}[1];
return($vpaymentname);
}

sub valores_iva_ice {
	my $shdes=$_[0];
	my $des_shdes=$_[1];
	#JH
	my $aplica_iva=$_[2];
	my $aplica_ice=$_[3];

	if ($aplica_iva eq "S") {
		$tipo_iva=$MAPEObgh_tariff_plan{$shdes,1}[4];
		$des_iva=$MAPEOtax_category{$tipo_iva}[0];
		$por_iva=$MAPEOtax_category{$tipo_iva}[1]/100;
		if ($des_iva eq "") {
			$des_iva="I.V.A. Por servicios (12%)";
			$por_iva=0.12;
		}
	} 
	else {
		$des_iva="I.V.A. Por servicios (0%)";
		$por_iva=0.0;
	}

	if ($aplica_ice eq "S") {
		$tipo_ice=$MAPEObgh_tariff_plan{$shdes,1}[5];
		$des_ice=$MAPEOtax_category{$tipo_ice}[0];
		$por_ice=$MAPEOtax_category{$tipo_ice}[1]/100;
		if ($des_ice eq "") {
			$des_ice="ICE en servicios de Telecomunicacion (15%)";
			$por_ice=0.15;
		}
	}
	else {
		$des_ice="ICE en servicios de Telecomunicacion (0%)";
		$por_ice=0.0;
	}
}

sub etiquetas_resumen_celular {
my $shdes=$_[0];
my $des_shdes=$_[1];
my $cont_R=1;
my $cedula_linea="";
my $nombre_linea="";
$key_etiquetas_resumen_cel="41000";
# Se Agrega informaci�n de C�dula y Ruc # 
$cedula_linea=$MAPEOinfo_contr_text{$co_id}[0];
$nombre_linea=$MAPEOinfo_contr_text{$co_id}[1];
if ($cedula_linea eq "") {
	$cedula_linea=$MAPEOinfo_contr_text{"999999"}[0];
	$nombre_linea=$MAPEOinfo_contr_text{"999999"}[1];
}
#
$cont_bgh_tar_plan=1;
#$MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$cont_R}=[$key_etiquetas_resumen_cel."|Nombre:|".$nombre_linea."||"];
$MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$cont_R}=[$key_etiquetas_resumen_cel."|Nombre:|".$nombre_linea."|||"];
$cont_R+=1;
#$MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$cont_R}=[$key_etiquetas_resumen_cel."|Ced/RUC:|".$cedula_linea."||"];
$MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$cont_R}=[$key_etiquetas_resumen_cel."|Ced/RUC:|".$cedula_linea."|||"];
$cont_R+=1;
#CCH   09/09/2008  Proyecto: 2427    Se a�ade el codigo del plan shdes.

#9321 SE A�ADE FECHA CONTRATACION DEL PLAN 

   
   $MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$cont_R}=[$key_etiquetas_resumen_cel."|Plan:|".$des_shdes."|".$shdes."|".$MAPEOCtasSVAPLAN{$Graba_co_id{$co_id}[0], $shdes }[2]."|"];
   #$MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$cont_R}=[$key_etiquetas_resumen_cel."|Plan:|".$des_shdes."|".$shdes."|"]; se adiciona fecha en el plan

#FIN 9321								

while ($cont_R ne 0) {
	$planType_R=$MAPEObgh_tariff_plan{$shdes,$cont_bgh_tar_plan}[1];
	if ($cont_R == 3 && $planType_R eq "" ) {
		print FILE_WRITE_LOGS "No existe Plan Rateplan ".$shdes."\n";
		$band_no_imprime_cuenta="1";
	}
	$firstNamme_R=$MAPEObgh_tariff_plan{$shdes,$cont_bgh_tar_plan}[2];
	$secondNamme_R=$MAPEObgh_tariff_plan{$shdes,$cont_bgh_tar_plan}[3];
	if ($firstNamme_R eq "") {
		$cont_R=0;
	} else {
		$cont_R+=1;
		$cont_bgh_tar_plan+=1;
	
		if ($cont_R >= 10 ){ 		

			$contador_key="999".$cont_R
		}else{
		    $contador_key=$cont_R;
		}
		#[10203] - lpe
		#$MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$cont_R}=[$key_etiquetas_resumen_cel."|".$firstNamme_R."|".$secondNamme_R."||"];
#		$MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$cont_R}=[$key_etiquetas_resumen_cel."|".$firstNamme_R."|".$secondNamme_R."|||"];
		$MAPEOImprime{$co_id." ".$key_etiquetas_resumen_cel." ".$contador_key}=[$key_etiquetas_resumen_cel."|".$firstNamme_R."|".$secondNamme_R."|||"]; #LPE				
	}
}
}

sub transforma_fecha{
#20060108022847
#28102006|12:00|
$fecha=$_[0];
$anio=substr($fecha,0,4);
$mes=substr($fecha,4,2);
$dia=substr($fecha,6,2);
$hora=substr($fecha,8,2);
$min=substr($fecha,10,2);
$seg=substr($fecha,12,2);
%arreglo= ('01','Ene','02','Feb','03','Mar','04','Abr','05','May','06','Jun','07','Jul','08','Ago','09','Sep','10','Oct','11','Nov','12','Dic');
$mes_letra=$arreglo{$mes};
$fecha_completa=$dia.$mes.$anio."|".$hora.":".$min.":".$seg;
$fecha_completa1=$dia." ".$mes_letra." ".$hora.":".$min;
return($fecha_completa1,$fecha_completa);
}

sub rango_fecha{
#DTM+901:20060308:102'
#DTM+902:20060407:102'
my $fecha=$_[0];
my $fecha1=$_[1];
$mes=substr($fecha,4,2);
$dia=substr($fecha,6,2);

$mes1=substr($fecha1,4,2);
$dia1=substr($fecha1,6,2);

%arreglo= ('01','Ene','02','Feb','03','Mar','04','Abr','05','May','06','Jun','07','Jul','08','Ago','09','Sep','10','Oct','11','Nov','12','Dic');
$mes_letra=$arreglo{$mes};
$mes_letra1=$arreglo{$mes1};

$fecha_completa=$mes_letra.". ".$dia." al ".$mes_letra1.". ".$dia1;
return($fecha_completa);
}
sub enceraEspacios{
	$valor=$_[0];
	$valor=~s/^\s+//;	
	$valor=~s/\s+$//;
	$valor=~s/\n+//;
	return($valor); 
}

sub verifico_tipo_agrupacion{
$iva=$_[0];
$ice=$_[1];
#Falta definir los Descuentos #

# Servicio Telecomunicacion #
if ($iva > 0 && $ice > 0) {
	#$tipo="20000";
	$tipo="20200";
}

# Otros Servicios #
if ($iva > 0 && $ice == 0) {
	$tipo="20200";
}

# Descuentos #
#$tipo="20400";

# Sin Impuestos #
if ($iva == 0 && $ice == 0) {
	$tipo="21100";
} 
return($tipo);

}

# CLS JCE [4697] Crea fucncion que Verifica si esta en el archivo de conf.
sub verifica_servExterno{
 $codigo_servicio=$_[0];
 $tipo=$_[1];
 $existe=exists $MAPEOServicesExt{$codigo_servicio};

 if ( $existe == 1 ) {
	 $tipo="21650";
 }
 return($tipo);
}

# CLS JCE [4697] Crea fucncion que obtiene nuevo Consumo del mes
sub obtieneTotalMenosServExt{
$Valor=$_[0];
$Valor_Total=$Valor-$total_servicios_Ext;
return $Valor_Total
}



sub obtieneInformacionPuntos{
	$cuenta_post=$_[0];
	# Archivos Postscript de Puntos Correspondiente #
	$data_file=$file_pto;
	open(READ_PTO, "< $data_file") or print "Can't open $data_file for leoArchivoFactura Puntos : $!\n";
	$posicion_ini=0;
	$posicion_fin=0;
	($posicion_ini, $posicion_fin) = &posicion_puntos($cuenta_post);
	$buf="";
#	print $data_file." ".$posicion_ini." ".$posicion_fin."\n";
	seek(READ_PTO, $posicion_ini, 0);
	read(READ_PTO, $buf, $posicion_fin-$posicion_ini);
	close READ_PTO;
	return($buf);
}

sub obtieneInformacionBulk{
	$cuenta_post=$_[0];
	# Archivos Postscript de Puntos Correspondiente #
	$data_file=$file_bulk;
	open(READ_BULK, "< $data_file") or print "Can't open $data_file for leoArchivoFactura Bulk : $!\n";
	$posicion_ini=0;
	$posicion_fin=0;
	($posicion_ini, $posicion_fin) = &posicion_bulk($cuenta_post);
	$buf="";
	seek(READ_BULK, $posicion_ini, 0);
	read(READ_BULK, $buf, $posicion_fin-$posicion_ini);
	close READ_BULK;
	return($buf);
}


sub leerArchivoPuntos{
$ruta_sql=$filename_pos_cuenta;
open LS, "ls -1 $ruta_sql|" or print "Error leerArchivoPuntos ls -1 $ruta_sql $!";

while (<LS>) {
open(INFILE, "< $_") or print "Can't open $_ for leerArchivoPuntos: $!\n";
while (<INFILE>) {
	$line = $_;			
	#1.10036393|1080|2134
	#4505
	#1.10321699|59386493191|0|332 
	@arreglo=split(/\|/,$line);
	$val=&enceraEspacios($arreglo[3]);
	$MAPEO_PTO{$arreglo[0].$arreglo[1]} = [$arreglo[2],$val];			
}
close INFILE;
}

close LS;
}

sub leerArchivoBulk{
$ruta_sql=$filename_pos_cuenta_bulk;
open LS, "ls -1 $ruta_sql|" or print "Error leerArchivoBulk ls -1 $ruta_sql $!";

while (<LS>) {
open(INFILE, "< $_") or print "Can't open $_ for leerArchivoBulk: $!\n";
while (<INFILE>) {
	$line = $_;			
	#1.10036393|1080|2134
	@arreglo=split(/\|/,$line);
	$val=&enceraEspacios($arreglo[2]);
	$planes_bulk=&enceraEspacios($arreglo[3]); #2589
	#$MAPEO_BULK{$arreglo[0]} = [$arreglo[1],$val,"1"];			
	$MAPEO_BULK{$arreglo[0]} = [$arreglo[1],$val,"1",$planes_bulk];	#2589		
}
close INFILE;
}
}

sub posicion_puntos{
#PARAMETRO ENTRADA: CUENTA
#PARAMETROS SALIDA: POS INI, POS FIN
my $cuenta=$_[0];
return($MAPEO_PTO{$cuenta}[0],$MAPEO_PTO{$cuenta}[1]);
}

sub posicion_bulk{
#PARAMETRO ENTRADA: CUENTA
#PARAMETROS SALIDA: POS INI, POS FIN
my $cuenta=$_[0];
return($MAPEO_BULK{$cuenta}[0],$MAPEO_BULK{$cuenta}[1]);
}

sub calcula_fecha{
my $fecha=$_[0];
my $anio=substr($fecha,6,4);
my $mes=substr($fecha,3,2);
my $dia=substr($fecha,0,2);

%arreglo= ('01','31','02','28','03','31','04','30','05','31','06','30','07','31','08','31','09','30','10','31','11','30','12','31');
$mes_dia=$arreglo{$mes};

if (($anio%4)==0 and ($anio%100)!=0 or ($anio%400)==0)
{
  $mes_dia="29";
}
if ($dia == $mes_dia) {
       $dia="01";
	   if ($mes != 12 ){
		  $mes=$mes+1;
		  if ($mes < 10){
		    $mes="0".$mes;
		  }
	   }else{
	       $mes="01";
		   $anio=$anio+1;
	   }
}else{	   
   $dia=$dia+1;    
   if ($dia < 10){
   $dia="0".$dia;
   }
}
$fecha_emision=$anio.$mes.$dia;
print "fecha-->".$fecha_emision."\n";

return($fecha_emision);
}
