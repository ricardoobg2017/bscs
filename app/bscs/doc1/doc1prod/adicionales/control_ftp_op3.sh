#
#--=====================================================================================--
#--Creado por:     		CLS Norma Manzaba M. NMA
#--Lider Claro:			SIS Jackelinne G�mez
#--Lider CLS:			CLS Miguel Garc�a
#-- Fecha creacion:		16/04/2012
#-- Motivo:			Simular   el  env�o de FTP  para el control de conexion 
#-- Proyecto:                   [8261]Puntos por Consumo Facturado de Equipos - Reingenieria al Menu de Puntos
#--=====================================================================================--
#

archivo_configuracion="../configuracion_ciclo.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

#--- Identifica la ruta actual ---
CurrentPath=`pwd`

#--- Cambia el nombre con la extension temporal antes de enviarlo --
cd $ruta_principal

#--- Arma script FTP y transfiere el archivo ---
ftp -in <<END
open $servidor_ftp
user rtxprod 123rtx
bin
cd $ruta_ftp
by
END
