#--======================================================================================--
#-- Version: 2.0.0
#-- Descripcion: Realiza las llamadas de los proceso de Extraccion y Proceso de Informaci�n
#--              de Salida DOC1
#--=====================================================================================--
#-- Desarrollado por:  Paola Carvajal
#-- Fecha de creacion: 03/Abril/2006
#-- Proyecto: Group 1
#--=====================================================================================--
#-- Desarrollado por:		Paola Carvajal
#-- Motivo :				N�mero de Secuencias
#-- Fecha de modificacion:  13/Nov/2006
#--=====================================================================================--
#-- Desarrollado por:  
#-- Fecha de modificacion: 
#--=====================================================================================--
#. /home/oracle/profile_BSCSD #desarrollo
# RECIBE COMO PARAMETRO EL NUMERO DEL DESPACHADOR

archivo_configuracion="configuracion.conf"
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

numero_despachador=$1
ruta=$ruta_principal
archivo_ctl=$ruta"/log/extr_timm_doc1.ctl"
usuario_base=$usuario_base
password_base=$password_base
sid_base=$sid_base
ruta_log=$ruta_logs
nombre_archivo=$ruta_log"/spoolTIMM_$numero_despachador.sql";
nombre_archivo_sql=$ruta_log"/actualiza_ciclo_$numero_despachador.sql";
export ORACLE_SID=$sid_base


if [ -s $nombre_archivo ]
then
	echo "\n    ACTUALMENTE UNOS DE LOS PROCESOS DE DOC1 SE ENCUENTRA EN EJECUCION"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1


obtiene_informacion(){
cat>$nombre_archivo<<eof
SET NEWPAGE 0;
SET SPACE 0;
SET LINESIZE 314;
SET PAGESIZE 0;
SET FEEDBACK OFF;
SET HEADING OFF;
SET VERIFY OFF;
SET ECHO OFF;
set serveroutput on;
spool billcycles_$numero_despachador.dat
select *
  from (select f.billcycle || '|' || f.tipo || '|' || f.ejecucion || '|' ||
               f.salida_doc1 || '|' || f.estado || '|' ||
               to_char(f.periodo, 'YYYYMMDD') || '|' || f.secuencia || '|' ||
               c.ciclo_default
          from DOC1_DET_PARA_FACTURACION f, fa_ciclos_axis_bscs c
         where f.procesar = 'S'
           and f.estado = 'A'
           and f.despachador = $numero_despachador
           and c.id_ciclo_admin = f.billcycle
         order by to_number(f.secuencia))
 where rownum = 1;
spool off
exit
eof
dato=`sqlplus -s $usuario_base/$password_base @$nombre_archivo` #produccion
#dato=`sqlplus -s $usuario_base/$password_base@$sid_base @$nombre_archivo` #desarrollo
ciclo=`echo $dato | awk -F\| '{print $1}'`
tipo=`echo $dato | awk -F\| '{print $2}'`
ejecucion=`echo $dato | awk -F\| '{print $3}'`
salida_doc1=`echo $dato | awk -F\| '{print $4}'`
estado=`echo $dato | awk -F\| '{print $5}'`
periodo=`echo $dato | awk -F\| '{print $6}'`
secuencia=`echo $dato | awk -F\| '{print $7}'`
#10179 07062016
tipo_prod=`echo $dato | awk -F\| '{print $8}'`
#10179 07062016
#echo "Datos Despachador $numero_despachador "$ciclo $tipo $ejecucion $salida_doc1 $estado $periodo $secuencia
echo "Datos Despachador $numero_despachador "$ciclo $tipo $ejecucion $salida_doc1 $estado $periodo $secuencia $tipo_prod

}


actualiza_ciclo(){
echo "Actualiza Ciclo " $periodo $ciclo $secuencia $numero_despachador $estado_ciclo $estado_ciclo $fecha $fecha_fin
cat >$nombre_archivo_sql<< eof
set serveroutput on;
execute DOC1_ACTUALIZAR('$periodo','$ciclo','$secuencia',$numero_despachador,'$estado_ciclo','$estado_ciclo','$fecha','$fecha_fin');                                                                                
commit;
exit;
eof
dato=`sqlplus  $usuario_base/$password_base  @$nombre_archivo_sql`
sleep 1  
rm $nombre_archivo_sql                                                                                                                                                                                  
}


while  [ 1 -eq 1 ]
do

	if [ ! -s $archivo_ctl  ]
	then
	rm $nombre_archivo
	echo "Fin de Proceso manualmente llama_procesho.sh  "$archivo_ctl $numero_despachador
	return
	fi
    
	obtiene_informacion

	fecha=`date '+%Y%m%d %H%M%S'`
	fecha_fin=`date '+%Y%m%d %H%M%S'`
	estado_ciclo="P"
    
	if [ "$ciclo" = "" ]
	then
	  rm $nombre_archivo
	  return;
	fi

	export estado_ciclo
	export fecha
	export fecha_fin
    actualiza_ciclo

	if [ "$ejecucion" = "S" ]
	then
	  
		echo "Ejecuta Despachador $numero_despachador $ciclo $tipo $ejecucion $salida_doc1 $estado"
		#10179
		#-----------------------------------------
		if [ "$tipo_prod" = "D" ]
		then
		   if [ ! -s $ruta_datos_dth"/CICLO_"$ciclo ]
		   then
			  mkdir $ruta_datos_dth"/CICLO_"$ciclo >/dev/null 2>&1
		   fi
		else
		   if [ ! -s $ruta_datos_movil"/CICLO_"$ciclo ]
		   then
			 mkdir $ruta_datos_movil"/CICLO_"$ciclo >/dev/null 2>&1
		   fi
		fi
		#-----------------------------------------
		#if [ ! -s $ruta"/dat/CICLO_"$ciclo ]
		#then
		# mkdir $ruta"/dat/CICLO_"$ciclo >/dev/null 2>&1
		#fi
		#if [ ! -s $ruta_datos_movil"/CICLO_"$ciclo ]
		#then
		# mkdir $ruta_datos_movil"/CICLO_"$ciclo >/dev/null 2>&1
		#fi
		#if [ ! -s $ruta_datos_dth"/CICLO_"$ciclo ]
		#then
		# mkdir $ruta_datos_dth"/CICLO_"$ciclo >/dev/null 2>&1
		#fi
		#10179
		BP_down_doc.sh $numero_despachador $ciclo $tipo
	fi


	if [ ! -s $archivo_ctl  ]
	then
	echo "Fin de Proceso manualmente llama_procesho.sh  "$archivo_ctl $numero_despachador
	return
	fi

	if [ "$salida_doc1" = "S" ]
	then
	 #10179
	 #-----------------------------------------
     if [ "$tipo_prod" = "D" ]
	 then
		if [ ! -s $ruta_procesados_dth"/CICLO_"$ciclo ]
		then
		  mkdir $ruta_procesados_dth"/CICLO_"$ciclo >/dev/null 2>&1
		fi
	 else
	    if [ ! -s $ruta_procesados_movil"/CICLO_"$ciclo ]
	    then
		  mkdir $ruta_procesados_movil"/CICLO_"$ciclo >/dev/null 2>&1
	    fi
	 fi
	 #-----------------------------------------
	 #if [ ! -s $ruta"/procesados/CICLO_"$ciclo ]
	 #then
	 #  mkdir $ruta"/procesados/CICLO_"$ciclo >/dev/null 2>&1
	 # fi
	 #if [ ! -s $ruta_procesados_movil"/CICLO_"$ciclo ]
	 #then
	 #  mkdir $ruta_procesados_movil"/CICLO_"$ciclo >/dev/null 2>&1
	 #fi
	 #if [ ! -s $ruta_procesados_dth"/CICLO_"$ciclo ]
	 #then
	 #  mkdir $ruta_procesados_dth"/CICLO_"$ciclo >/dev/null 2>&1
	 #fi
	 #10179
	  obtiene_detalle_llamada.pl $numero_despachador $ciclo $ruta $tipo
	  export ciclo
	fi


	fecha_fin=`date '+%Y%m%d %H%M%S'`
	estado_ciclo="F"
	export fecha_fin
	export estado_ciclo
    actualiza_ciclo

done
rm $nombre_archivo


