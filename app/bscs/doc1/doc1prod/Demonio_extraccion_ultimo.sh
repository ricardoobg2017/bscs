#--===============================================================================================
# ===================== Creado por       :  John Cardozo Villon					==		 
# ===================== Lider CLS        :  Ing. Sheyla Ramirez					==
# ===================== Lider Claro SIS  : Jackeline Gomez					==
# ===================== Fecha de creacion: 26/Mayo/2014						==
# Proyecto [9797] BCH AUTOMATICOS        : Verificacion de los Timm que van hacer procesados    ==
#                                          llevando el control hasta que finalize con su        ==
#			                   respectiva extraccion.				==
#--===============================================================================================
# Modificado por    : SUD Vanessa Gutierrez G.
# Proyecto	    : [10179] Nuevos ciclos de Facturación DTH
# Lider Conecel     : SIS Paola Carvajal
# Objetivo	    : Cambio de rutas de archivos .DAT y .TXT
#--=====================================================================================--
. /bscs/bscsprod/.setENV
#!/usr/bin/sh
RUTA=/doc1/doc1prod
cd $RUTA

#cantidad_ciclos_extraer=0
archivo_configuracion=$RUTA"/configuracion_ultimo.conf"

#==== Verificar si el archivo de configuracion existe =======
if [ ! -s $archivo_configuracion ]
then
   echo "No se encuentra el archivo de Configuracion $archivo_configuracion=\n"
   sleep 1
   exit;
fi

. $archivo_configuracion

#==== Variables extraidas del archivo de configuracion USUARIO, PASSWORD, RUTA_RPINCIPAL =========
ruta_local=$ruta_principal
usuario_base=$usuario_base
password_base=$password_base

#10179
#Nuevas rutas para archivos .DAT por producto
ruta_dat_cel=$ruta_datos_movil"/CICLO_"
ruta_dat_dth=$ruta_datos_dth"/CICLO_"
#10179

#====== VARIABLES DE FECHA =============
dia=`date +%d`
mes=`date +%m`
anio=`date +%Y`
hora=`date +%H`
minuto=`date +%M`
segundo=`date +%S`
fecha_corte=$mes"/"$anio
fecha=$dia"/"$mes"/"$anio" "$hora":"$minuto":"$segundo
ORACLE_SID=$sid_base
export ORACLE_SID
IP="18.14"

###======== Creacion de variables para controlar cada uno de las ejecuciones ================ 
nombre_archivo_control=$ruta_local"/Demonio_Extraccion.ctl"
NOMBRE_FILE_SQL2=$ruta_sql"/obtener_billcycle.sql"
NOMBRE_FILE_SQL3=$ruta_sql"/verificar_billcycle.sql"
NOMBRE_FILE_SQL4=$ruta_sql"/extraer_billcycle.sql"
NOMBRE_FILE_SQL5=$ruta_sql"/extraer_obtiene.sql"
NOMBRE_FILE_SQL_VALIDACION=$ruta_sql"/validacion_doc1_cuentas.sql"
NOMBRE_FILE_SQL_COMIENZO=$ruta_sql"/obtener_fecha_cantidad.sql"
NOMBRE_FILE_SQL_FUNCION_DAT=$ruta_sql"/funcion_extraccion_dat.sql"
NOMBRE_FILE_SQL_EXTRACCION_DAT=$ruta_sql"/validacion_extraccion_dat.sql"
NOMBRE_FILE_SQL_DIRECTORIO_DAT=$ruta_sql"/validacion_directorio_dat.sql"
NOMBRE_FILE_SQL_BAJAR_DAT=$ruta_sql"/validacion_BAJAR_dat.sql"
NOMBRE_FILE_LOG=$ruta_logs"/Demonio_Ejecuta_doc1_cuentas.log"
NOMBRE_ERROR_LOG=$ruta_logs"/Demonio_Ejecuta_doc1_error.log" 
fecha=`date`

### ---========= Creacion del archivo de control de Alarma ==========-----
Control_alarma=$ruta_local"/Demonio_Control_alarma.txt"
### ----======================================================-------
cd $ruta_local

echo "\n \t I N I C I O   D E L   P R O C E S O $fecha \n" >> $NOMBRE_FILE_LOG

# ================== Control para que no este dos procesos a la vez ========================= #
if [ -s $nombre_archivo_control ]
then
	echo "\n    ACTUALMENTE SE ENCUENTRA EN EJECUCION Demonio_Ejecuta_doc1_cuentas.sh"
	echo "    YA QUE EXISTE EL ARCH $nombre_archivo_control "
	echo "    SI ESTA SEGURO DE QUE EL PROCESO ESTA ABAJO ELIMINE ESTE"
	echo "    ARCHIVO Y VUELVA A INTENTARLO\n"
	exit;
fi
sleep 1
# =========================================================================================== #

echo $$ > $nombre_archivo_control

#cantidad_ciclo=$1
# -- VMA se controlara con la tabla de parametrizacion del proceso de Extraccion -- no deberia ir
#==========================================================================================

#-----=================== Se crea funcion para controlar el estado si la extraccion es sastifactorio o Error =================-------------
validacion_estados_dat() {
nuevo_estado_dat=$1;
billcycle_dat=$2;
cat>$NOMBRE_FILE_SQL_FUNCION_DAT<<END
set heading off;
UPDATE MONITOR_FIN_MES SET ESTADO_DAT = '$nuevo_estado_dat'  WHERE BILLCYCLE = $billcycle_dat;
commit;
exit;
END
#resultado6=`sqlplus -s $usuario_base/$password_base @$NOMBRE_FILE_SQL 2>/dev/null`
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_FUNCION_DAT > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_FUNCION_DAT > $NOMBRE_ERROR_LOG #10179
rm -f $NOMBRE_FILE_SQL_FUNCION_DAT
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de realizar la conexion a la base para actualizar el campo  que se verifica la extraccion .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para actualizar el campo  que se verifica la extraccion .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi
}

#========= OBTENER FECHA_CORTE Y CANTIDAD DE BILLCYCLES ===
cat > $NOMBRE_FILE_SQL_COMIENZO <<END
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off 
spool obtener_fecha_cantidad.txt
SELECT FECHA_CORTE||'|'||CANTIDAD_EXTRACCION||'|'||TIPO FROM sysadm.PARAMETRO_DAT_AUTOM WHERE INICIO_EXTRACCION = 'S';
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_COMIENZO > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_COMIENZO > $NOMBRE_ERROR_LOG #10179
rm -f $NOMBRE_FILE_SQL_COMIENZO
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "Error al momento de realizar la conexion a la base para obtener los parametros del inicio del proceso .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener los parametros del inicio del proceso .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi

if [ ! -s obtener_fecha_cantidad.txt ]
then
rm -f obtener_fecha_cantidad.txt
rm -f $nombre_archivo_control
rm -f $Control_alarma
exit 0;
fi

#-------===================== verifica  espacio en filesystem =====-------------------
espacio=`bdf .|awk ' {print $5}'|grep -v used|head -2`
longitud_sistema=`expr length $espacio`
longitud_nueva_sistema=`expr $longitud_sistema - 1`
longitud_nueva_obtenido=` expr substr $espacio 1 $longitud_nueva_sistema`
echo " ESPACIO EN LA RUTA /DOC1/DOC1PROD ES $longitud_nueva_obtenido" >> $NOMBRE_FILE_LOG
if [ $longitud_nueva_obtenido -gt 85 ]
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "ESPACIO EN EL SERVIDOR SOBRE PASA EL 85%, FAVOR VERIFICAR EL ESPACIO EN EL FILESYSTEM .....Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
#rm -f obtener_fecha_cantidad.txt
#rm -f $nombre_archivo_control
#rm -f $Control_alarma
#exit 0;
fi

obtener_fecha_corte_dat=`cat  obtener_fecha_cantidad.txt| awk -F "|" '{print $1}'`
cantidad_ciclos_dat=`cat  obtener_fecha_cantidad.txt| awk -F "|" '{print $2}'`
obtener_tipo=`cat  obtener_fecha_cantidad.txt| awk -F "|" '{print $3}'`

cantidad_inicio=0
cantidad_ciclo=0
cantidad_ciclo_proceso=0
while [ $cantidad_ciclo_proceso -eq 0 ] 
do

# ----=========================================-----

#===================== Se inicia el proceso de los BCH a procesar =========================
#== Se obtiene los billcycles que esten listos a procesar y listos para su extraccion =====
#== En el transcurso del proceso se revisa nuevamente verificando si hay billcycles =======
#== Para procesar nuevamente con su respectiva extraccion =================================
fecha=`date`
ciclo_act=0
echo "\n Busca Ciclo a Procesar $ciclo_act "$fecha >> $NOMBRE_FILE_LOG
echo "\n Busca Ciclo a Procesar $ciclo_act "$fecha 

#####====== Comienza el proceso de ingreso a las tablas de cada uno de los billcycles =====

cat > $NOMBRE_FILE_SQL2 <<END
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off 
spool obtener_billcycle.txt
SELECT BILLCYCLE FROM monitor_fin_mes WHERE ESTADO ='T' AND DOC1 ='P' and ROWNUM <= $cantidad_ciclos_dat and servidor_dat = $servidor_ejecucion order by prioridad;
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL2 > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL2 > $NOMBRE_ERROR_LOG #10179

####===== Verificar si el query ejecuto correctamente ================= 
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
#rm -f $nombre_archivo_control
rm -f obtener_billcycle.txt
echo "Error al momento de realizar la conexion a la base para obtener los billcycles .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener los billcycles .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi
###====================================================================

ciclo_act=`cat obtener_billcycle.txt |wc -l` 

###==== Se verifica si hay billcycles listos a procesar en caso de que no hay se revisara nuevamente =======
cantidad_ciclo=`expr $cantidad_ciclo + 1` 
if [ $ciclo_act -eq 0 ]
then

if [ $cantidad_ciclo -eq 1 ]
then
rm -f $NOMBRE_FILE_SQL_CORTE
rm -f $NOMBRE_FILE_SQL_CORTE_2
rm -f $NOMBRE_FILE_SQL_CORTE_3
rm -f $NOMBRE_FILE_SQL_CORTE_4
rm -f $NOMBRE_FILE_SQL_CORTE_5
rm -f validar_fecha_corte.txt
rm -f obtener_fecha_corte.txt
rm -f verificar_fechas.txt
rm -f $nombre_archivo_control
rm -f obtener_billcycle.txt
rm -f $NOMBRE_FILE_SQL2
rm -f $Control_alarma
exit 0;
elif [ $cantidad_ciclo -gt 1 ]
then
echo "No hay ciclos para procesar......" >> $NOMBRE_FILE_LOG
fi
cantidad_ciclo_proceso=1
sleep 2
else

if [ $cantidad_inicio -eq 0 ]
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "INICIO DEL PROCESO DEMONIO EXTRACCION DE TIMM .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
echo "$Control_alarma " >> $NOMBRE_FILE_LOG
echo "sh envia_alarma_ultimo.sh $Control_alarma" >> $NOMBRE_FILE_LOG
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
fi
cantidad_inicio=`expr  $cantidad_inicio + 1`
#-----------------------------------------------------------------------------#
#                 Procesa los hilos                                          -#
#-----------------------------------------------------------------------------#
fecha=`date`
echo "\n Ciclo a Procesar "$ciclo_act" "$fecha >> $NOMBRE_FILE_LOG

###----- Obtener el tipo C: Commit , CG: Control Group ------
pv_commit=$obtener_tipo 

ciclos_guardados=""
for line in `cat obtener_billcycle.txt`
do
ciclos_guardados=$ciclos_guardados"'"$line"',"
echo "Ejecuta_doc1_cuentas_ultimo $line $pv_commit" >> $NOMBRE_FILE_LOG 
sh Ejecuta_doc1_cuentas_ultimo.sh $line $pv_commit 
done
PidsEjecuta_1=`ps -eaf | grep Ejecuta_doc1_cuentas_ultimo.sh |grep -v grep | wc -l`
while [ $PidsEjecuta_1 -ne 0 ] 
do
PidsEjecuta_1=`ps -eaf | grep Ejecuta_doc1_cuentas_ultimo.sh |grep -v grep | wc -l`
sleep 3
done
echo "Fin de realizar la inserccion de cada uno de los billcycles..." >> $NOMBRE_FILE_LOG
longitud=`expr length $ciclos_guardados`
longitud_nueva=`expr $longitud - 1`
cilos_nuevos_obtenidos=` expr substr $ciclos_guardados 1 $longitud_nueva`

#-----===============VALIDACION PARA VERIFICAR QUE LA INSERCION A LA DOC1_CUENTAS SE REALIZO CON EXITO ===============---------------------
cat > $NOMBRE_FILE_SQL_EXTRACCION_DAT <<END
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off 
spool obtener_billcycle_doc1.txt
SELECT BILLCYCLE FROM MONITOR_FIN_MES WHERE DOC1 ='D' AND BILLCYCLE IN ($cilos_nuevos_obtenidos);
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_EXTRACCION_DAT > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_EXTRACCION_DAT > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
rm -f obtener_billcycle.txt
#rm -f $nombre_archivo_control
rm -f obtener_billcycle_doc1.txt
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de  billcycles que terminaron de insertar a la doc1_cuentas .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de  billcycles que terminaron de insertar a la doc1_cuentas .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi

if [ ! -s obtener_billcycle_doc1.txt ]
then
rm -f $nombre_archivo_control
echo "No se encuentran billcycles insertados en la doc1_cuentas para poder realizar el proceso de extraccion" >> $NOMBRE_FILE_LOG
exit 0;
fi


for line_2 in `cat obtener_billcycle_doc1.txt`
do
validacion_estados_dat "I" "$line_2"
done
rm -f obtener_billcycle_doc1.txt
rm -f obtener_billcycle.txt

#====================== Realizar la extraccion de los bicllycles ======================================
cat>$NOMBRE_FILE_SQL4<<END 
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off
spool extraccion_billcycle.txt
SELECT D.BILLCYCLE || '|' || F.CICLO_DEFAULT
FROM DOC1.DOC1_PARAMETROS_FACTURACION D, FA_CICLOS_AXIS_BSCS F
WHERE D.ESTADO ='A' 
AND D.PROCESAR ='S' 
AND D.BILLCYCLE IN ($cilos_nuevos_obtenidos)
AND F.ID_CICLO_ADMIN = D.BILLCYCLE;
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL4 >> $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL4 >> $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
rm -f obtener_billcycle.txt
#rm -f $nombre_archivo_control
rm -f obtener_billcycle_doc1.txt
rm -f extraccion_billcycle.txt
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de  billcycles que esten listos para realizar la extraccion .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de  billcycles que esten listos para realizar la extraccion .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi
#====== Proceso para ejecutar los 8 despachadores y que sean extraidos ====== #

#10179
for dato in `cat extraccion_billcycle.txt`
#10179

do
cd $ruta_local

#10179
line=`echo $dato | awk -F\| '{print $1}'`
tipo_prod=`echo $dato | awk -F\| '{print $2}'`
#10179

#10179
#if [ ! -d $ruta_local"/dat/CICLO_"$line ]
#then
#mkdir $ruta_local"/dat/CICLO_"$line
#fi
if [ "$tipo_prod" = "D" ]
then
	#DTH
	if [ ! -d $ruta_dat_dth$line ]
	then
		mkdir $ruta_dat_dth$line
	fi
else
	#VOZ
	if [ ! -d $ruta_dat_cel$line ]
	then
		mkdir $ruta_dat_cel$line
	fi
fi
#10179

validacion_estados_dat "E" "$line"

nohup sh llama_proceso_ultimo.sh 1 $line &
echo "nohup sh llama_proceso_ultimo.sh 1 $line & " >> $NOMBRE_FILE_LOG
nohup sh llama_proceso_ultimo.sh 2 $line &
echo "nohup sh llama_proceso_ultimo.sh 2 $line & " >> $NOMBRE_FILE_LOG
nohup sh llama_proceso_ultimo.sh 3 $line &
echo "nohup sh llama_proceso_ultimo.sh 3 $line & " >> $NOMBRE_FILE_LOG
nohup sh llama_proceso_ultimo.sh 4 $line &
echo "nohup sh llama_proceso_ultimo.sh 4 $line & " >> $NOMBRE_FILE_LOG
nohup sh llama_proceso_ultimo.sh 5 $line &
echo "nohup sh llama_proceso_ultimo.sh 5 $line & " >> $NOMBRE_FILE_LOG
nohup sh llama_proceso_ultimo.sh 6 $line &
echo "nohup sh llama_proceso_ultimo.sh 6 $line & " >> $NOMBRE_FILE_LOG
nohup sh llama_proceso_ultimo.sh 7 $line &
echo "nohup sh llama_proceso_ultimo.sh 7 $line & " >> $NOMBRE_FILE_LOG
nohup sh llama_proceso_ultimo.sh 8 $line &
echo "nohup sh llama_proceso_ultimo.sh 8 $line & " >> $NOMBRE_FILE_LOG

echo "$line"
PidsEjecuta=`ps -eaf | grep llama_proceso_ultimo.sh |grep -v grep | wc -l`
while [ $PidsEjecuta -ne 0 ] 
do
PidsEjecuta=`ps -eaf | grep llama_proceso_ultimo.sh |grep -v grep | wc -l`
sleep 3
####---===== alarmas filesystem ====----

espacio=`bdf .|awk ' {print $5}'|grep -v used|head -2`
longitud_sistema=`expr length $espacio`
longitud_nueva_sistema=`expr $longitud_sistema - 1`
longitud_nueva_obtenido=` expr substr $espacio 1 $longitud_nueva_sistema`
echo " ESPACIO EN LA RUTA /DOC1/DOC1PROD ES $longitud_nueva_obtenido" >> $NOMBRE_FILE_LOG
#echo "$longitud_nueva_obtenido"
if [ $longitud_nueva_obtenido -gt 85 ]
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "ESPACIO EN EL SERVIDOR SOBRE PASA EL 85%, FAVOR VERIFICAR EL ESPACIO EN EL FILESYSTEM .....Fecha : $fecha  Hora: $hora" > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma

fi
done
#----------- Validar que esten correctos los dat ingresados ----
cat>$NOMBRE_FILE_SQL_DIRECTORIO_DAT<<END 
set pagesize 0
set linesize 0
set termount off
set head off  
set trimspool on
set feedback off
spool obtener_cantidad_doc1.txt
select count(*) from doc1.doc1_cuentas where billcycle in ('$line') and customer_id_high is null  and estado ='P';
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_DIRECTORIO_DAT > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_DIRECTORIO_DAT > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
rm -f obtener_billcycle.txt
#rm -f $nombre_archivo_control
rm -f obtener_billcycle_doc1.txt
rm -f extraccion_billcycle.txt
rm -f obtener_cantidad_doc1.txt
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de cuentas que contiene el billcycle $line .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de cuentas que contiene el billcycle $line .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi

#10179
#cd $ruta_local"/dat/CICLO_"$line
cd $ruta_datos_movil"/CICLO_"$line
#10179

cantidad_dat_guardado=`ll -t |wc -l`
cantidad_dat_guardado_re=`expr $cantidad_dat_guardado - 1`
#10179
#DTH
cd $ruta_datos_dth"/CICLO_"$line
cantidad_dat_guardado_dth=`ll -t |wc -l`
cantidad_dat_guardado_re_dth=`expr $cantidad_dat_guardado_dth - 1`

cantidad_dat_guardado_re=`expr $cantidad_dat_guardado_re + $cantidad_dat_guardado_re_dth`
#10179
echo "$cantidad_dat_guardado_re" >>  $NOMBRE_FILE_LOG
cantidad_dat_doc1_cuentas=`cat $ruta_local"/obtener_cantidad_doc1.txt`

if [ $cantidad_dat_guardado_re -eq $cantidad_dat_doc1_cuentas ];
then
validacion_estados_dat "X" "$line"   
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "$line TERMINO SU EXTRACCION CON EXITO ... Fecha : $fecha Y Hora: $hora  " > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
else
validacion_estados_dat "F" "$line"
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "$line NO TERMINO SU EXTRACCION CON EXITO ... Fecha : $fecha Y Hora: $hora DE ERROR " > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
fi
echo "El proceso a terminado..." >> $NOMBRE_FILE_LOG

done 
rm -f extraccion_billcycle.txt

#=========== Verificar que los ciclos procesados hayan sido extraido con exito ======
cd $ruta_local
cat>$NOMBRE_FILE_SQL3<<END 
set heading off; 
spool ciclos_extraidos_final_.txt
select (select count(*) from doc1.doc1_parametros_facturacion)-(select count(*)from doc1.doc1_parametros_facturacion where estado = 'F' and procesar = 'F') from dual ;
spool off
exit;
END

#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL3 > $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL3 > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
rm -f obtener_billcycle.txt
#rm -f $nombre_archivo_control
rm -f obtener_billcycle_doc1.txt
rm -f extraccion_billcycle.txt
rm -f obtener_cantidad_doc1.txt
rm -f ciclos_extraidos_final_.txt
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de billcycle que terminaron su ejecucion correcta .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener la cantidad de billcycle que terminaron su ejecucion correcta .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi

ciclos_extraidos=`cat ciclos_extraidos_final_.txt`

#===== Verificar si todos los archivos fueron extraidos con exito ===========
if [ $ciclos_extraidos -eq 0 ];
then
echo "Se realizo la extraccion con exito ....." >> $NOMBRE_FILE_LOG
else
echo "Faltan $ciclos_extraidos para ser extraidos...." >> $NOMBRE_FILE_LOG
fecha=`date`
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "$ciclos_extraidos BILLCYCLES NO TERMINARON SU EXTRACCION CON EXITO... FECHA Y HORA DE ERROR: $fecha " > $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
fi
echo "fin de la extraccion"
cantidad_ciclo_proceso=0
fi

##---======VERIFICANDO SI EL USUARIO A DECIDIDO BAJAR EL PROCESO ====----------------
cat>$NOMBRE_FILE_SQL_BAJAR_DAT<<END 
set heading off; 
spool bajar_proceso.txt
SELECT COUNT(*) FROM sysadm.PARAMETRO_DAT_AUTOM WHERE INICIO_EXTRACCION = 'S';
spool off
exit;
END
#sqlplus -s $usuario_base/$password_base@$ORACLE_SID @$NOMBRE_FILE_SQL_BAJAR_DAT> $NOMBRE_ERROR_LOG
echo $password_base | sqlplus -s $usuario_base @$NOMBRE_FILE_SQL_BAJAR_DAT > $NOMBRE_ERROR_LOG #10179
ERROR=`grep "ORA-" $NOMBRE_ERROR_LOG|wc -l`
if [ $ERROR -ge 1 ];
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
rm -f obtener_billcycle.txt
#rm -f $nombre_archivo_control
rm -f obtener_billcycle_doc1.txt
rm -f extraccion_billcycle.txt
rm -f obtener_cantidad_doc1.txt
rm -f ciclos_extraidos_final_.txt
rm -f bajar_proceso.txt
echo "Error al momento de realizar la conexion a la base para obtener el estado si el usuario a decidico bajar el proceso manualmente .......... Fecha : $fecha  Hora: $hora" >> $NOMBRE_FILE_LOG
echo "Error al momento de realizar la conexion a la base para obtener el estado si el usuario a decidico bajar el proceso manualmente .......... Fecha : $fecha  Hora: $hora" >> $Control_alarma
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
exit 0;
fi
obtener_dato_bajar=`cat bajar_proceso.txt`
if [ $obtener_dato_bajar -eq 0 ]
then
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo " SE A DECIDIDO TERMINAR MANUALMENTE EL PROCESO DE EXTRACCION Fecha : $fecha  Hora: $hora ------" > $Control_alarma 
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
rm -f $nombre_archivo_control
rm -f $NOMBRE_ERROR_LOG 
rm -f billcycle_pendiente.txt 
rm -f billcycle_procesando.txt 
rm -f ciclos_extraidos_final_.txt
rm -f billcycles_extraer.txt
rm -f validar_fecha_corte.txt
rm -f obtener_fecha_corte.txt
rm -f verificar_fechas.txt
rm -f extraccion_billcycle.txt
rm -f total_cantidad_billcycles_extraer.txt
rm -f obtener_fecha_cantidad.txt
rm -f obtener_cantidad_doc1.txt
rm -f obtener_billcycle.txt
rm -f bajar_proceso.txt
rm -f $NOMBRE_FILE_SQL_DIRECTORIO_DAT
rm -f $NOMBRE_FILE_SQL2
rm -f $NOMBRE_FILE_SQL3
rm -f $NOMBRE_FILE_SQL4
rm -f $Control_alarma
rm -f $NOMBRE_FILE_SQL_BAJAR_DAT
exit 0;
fi

done
#================= Eliminar todos archivos creados ==========================

rm -f $nombre_archivo_control
rm -f $NOMBRE_ERROR_LOG 
rm -f billcycle_pendiente.txt 
rm -f billcycle_procesando.txt 
rm -f ciclos_extraidos_final_.txt
rm -f billcycles_extraer.txt
rm -f validar_fecha_corte.txt
rm -f obtener_fecha_corte.txt
rm -f verificar_fechas.txt
rm -f extraccion_billcycle.txt
rm -f  total_cantidad_billcycles_extraer.txt
rm -f obtener_fecha_cantidad.txt
rm -f obtener_cantidad_doc1.txt
rm -f obtener_billcycle.txt
rm -f bajar_proceso.txt
rm -f $NOMBRE_FILE_SQL_DIRECTORIO_DAT
rm -f $NOMBRE_FILE_SQL_EXTRACCION_DAT
rm -f $NOMBRE_FILE_SQL2
rm -f $NOMBRE_FILE_SQL3
rm -f $NOMBRE_FILE_SQL4
rm -f $NOMBRE_FILE_SQL_BAJAR_DAT
rm -f $Control_alarma
fecha=`date +%d/%m/%y`
hora=`date +"%T"`
echo "---------- FIN DEL PROCESO TOTAL DE LA EXTRACCION FECHA Y HORA DE FINALIZACION Fecha : $fecha  Hora: $hora ------" > $Control_alarma 
sh $ruta_local/envia_alarma_ultimo.sh $Control_alarma
rm -f $Control_alarma

