@echo off

set OLDPATH=%PATH%
set JDK_HOME=C:\jdk1.3.1
set CMSGATEWAY_HOME=C:\CMS_NEW_CLIENT

%JDK_HOME%\jre\bin\java.exe -cp %CMSGATEWAY_HOME%\lib\cmsgateway-core.jar;%CMSGATEWAY_HOME%\lib\cmsgateway.jar net.porta.cms.service.StopCMSGateway %CMSGATEWAY_HOME%\resources\middleware.properties
set PATH=%OLDPATH%
