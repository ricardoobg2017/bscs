Note: This is a sample RULETAB.STB (Sql/Windows String Table)
      It must be customized according to the customer-specific needs.

      All examples are marked with a $$$ string , so that no match is encountered
      and the rules remain inactive.

      If you want to activate the rules you have to proceed as follows:

      Step 1: remove the $$$ string
      Step 2: customize the rules according to the needs of the project site.



Note : the following fixed tag string serves to check if the 
       ruletab string table is loaded. 

Never remove this tag ...

{RULETAB} RULETAB



The following tags are relevant for installations having switches with HLR method 0.
An example are Ericsson R3 switches.
If you define HLR method 0 in MPDHLTAB then the HLR requires Telephony service
in any case. Use the tag HLR-METHOD-0-xxx (with xxx = SVLPREFIX from MPDSCTAB)
to specify by which SVLCODE the standard telephony service for this market is represented.

GSMFUNC.fncheckServiceconsistency then checks, if this service is part of the
contracted services.

Note: you do not need this type of tag if you do not use HLR method 0 in MPDHLTAB... 

The following tag specifies how telephony is represented for market GSM...

{HLR-METHOD-0-GSM} GSMT11B*****S***


The following tag specifies how telephony is represented for market NMT...

{HLR-METHOD-0-NMT} NMTT11B*****S***


The following tag specifies how telephony is represented for market CCD...

{HLR-METHOD-0-CCD} CCDT11B*****S***



The following tag specifies how telephony is represented for market ISD...

{HLR-METHOD-0-ISD} ISDT11B*****S***


The following tag specifies how a telephony is represented for market X25...

{HLR-METHOD-0-X25} X25T**B20***S***


The following tag specifies how telephony is represented for market LLI...

{HLR-METHOD-0-LLI} LLITF6B*****S***


Note: all following entries are marked inactive by a $$$-header string.
      Remove this $$$ if you want to activate the rules.


The following tags are used if you have a service (i.e. a SVLCODE) for which an consistency
rule (abbreviated as CON ) is formulated.
A consistency rule is an arbitrary boolean expression of SVLCODEs.
A consistency rule is TRUE if the boolean expression evaluates to TRUE.
To formulate the boolean expression use & for logical AND and | for logical OR.

Consistency rukes are appropriate if you have services, which need other services 
in order to operate in a meaningful way.

A prominent example is the use of voice or fax mail in GSM.
Assume you have Voice Mail then you normally want to have conditional Call Forwardings
to be present.

Another well-kown example is , that GSM multi-party (MPTY) requires call hold (CH).



These tags define that there exits consistency rules...

{GSMT**B*****SS2*} CON
{GSMT**B*****SS3*} CON
{GSMT**B*****S29*} CON

{ISDT**B*****SS0*} CON
{ISDT**B*****SS2*} CON



{$$$GSMT**B*****SF1*} CON

{$$$GSMT**B*****S50*} CON
{$$$GSMT**B*****S51*} CON


These tags define the actual consistency rules...

{CON-GSMT**B*****SS2*} GSMT61B*****S***  
{CON-GSMT**B*****SS3*} GSMT11B*****S*** & GSMT61B*****S***  
{CON-GSMT**B*****S29*} GSMT**B*****S21* & GSMT11B*****S*** 

{CON-ISDT**B*****SS2*} ISDT63B*****S***  
{CON-ISDT**B*****SS0*} ISDT11B*****S*** & ISDT63B*****S***  


{$$$CON-GSMT**B*****SF1*} GSMT**B*****S29* & GSMT**B*****S2A* & GSMT**B*****S2B* & GSMT11B*****S***

{$$$CON-GSMT**B*****S50*} GSMT**B*****S40* | GSMT**B*****S42*
{$$$CON-GSMT**B*****S51*} GSMT**B*****S40* | GSMT**B*****S42*





The following tags are used to setup error messages which popup in CA if the 
consistency rule is FALSE.
Use the lg-TXT prefix with lg = 2 character language code (e.g. EN, DE,etc) 
to build the error msg tag. Thus the general tag structure is lg-TXT-SVLCODE where
SVLCODE is the SVLCODE for which the consistency rule should exits.

If you have several languages, the system chooses the language which is selected in KV.

Note , that error messages are optional.
If no explicit error message is found, then the system uses a default general error message.
 
These tags define error messages in English....  


{EN-TXT-GSMT**B*****SS2*} Selling Siemens Handy (S2) requires service Fax
{EN-TXT-GSMT**B*****SS3*} Selling Nokia Handy (S3) requires service Telephony and Fax
{EN-TXT-GSMT**B*****S29*} Service CF on mobile subscriber busy (29) requires service Telephony and CF

{EN-TXT-ISDT**B*****SS2*} Service (S2) requires service Telefax
{EN-TXT-ISDT**B*****SS0*} Selling Goods  (S0) requires service Telephony and Telefax

{$$$EN-TXT-GSMT**B*****SF1*} Voice Mail (F1) requires all CFc and Telephony
{$$$EN-TXT-GSMT**B*****SF2*} Fax Mail (F2) requires Call Forwarding Unconditional and Fax 
{$$$EN-TXT-GSMT**B*****S50*} Multi Party (MTPY) requires Call Hold(CH)
{$$$EN-TXT-GSMT**B*****S51*} Multi Party (MTPY) requires Call Hold(CH)

These tags define error messages in German....  

{$$$DE-TXT-GSMT**B*****SF1*} VMS ben�tigt alle cond. CF's und Tel.
{$$$DE-TXT-GSMT**B*****SF2*} FMS ben�tigt CFU und Fax
{$$$DE-TXT-GSMT**B*****S50*} MTPY ben�tigt CH
{$$$DE-TXT-GSMT**B*****S51*} MTPY ben�tigt CH


