!*********************************************************************
!*
!* ABSTRACT :   SQLWindows String Table: CA_MEM.STB
!*                                                              
!* AUTHOR   :   Maciej Markiewicz, LHS
!*
!* CREATED  :   08-AUG-1997
!*
!* MODIFIED :   14-NOV-1997 Feat 31095 modify taxation logic, the CA is responsible to provide appropriate taxation data in ALLTAXES
!*              16-JAN-1998 Modify offsets for OTMERCH, OTMERCH_GROSS, OTGPRICE
!*              10-MAR-1998 Rename offset OTDISCOUNT -> OTGPRICE_DIS
!*
!* DESCRIPTION :
!*
!* This string table contains parameter to control of generation
!* credit memo bill EDIFACT message with function
!* 'fnEDIConverter'.     
!*      
!*
!*********************************************************************
!*
!* Version Control: 
!*   1.4 bscs_eur/eur/win32/exe/ca_mem.stb, bscs_eur_ca_eur, bscs_eur 3/10/98
!*
!*********************************************************************

!* Control Header

{INT-SEG}UNB,UNH,BGM,DTM,GROUP01,GROUP02A,GROUP02B,GROUP02C,GROUP07,TRBEG,GROUP22,TREND,UNS,GROUP45,GROUP47,UNT,UNZ
{UNB}UNB+UNOC:2+%s+%s+%s:%s+%s'
{UNH}UNH+%s+CUSDOC:100:921:UN:ETEB'
{BGM}BGM+380:::CA-CREDITMEMO++9'
{DTM}DTM+3:%s:102',DTM+126:%s:102'
{GROUP01}RFF+IV:%s',RFF+LY:%s',RFF+TM:%s',RFF+CT:%s'
{GROUP02A}NAD+IV++%s:%s+%s:%s:%s:%s:%s++%s',FII+AO+%s:%s:%s+%s::::::%s:%s'
{GROUP02B}NAD+II++%s:%s+%s:%s:%s:%s:%s++%s++%s',FII+RH+%s:%s:%s+%s::::::%s:%s' 
{GROUP02C}NAD+IT++%s:%s+%s:%s:%s:%s:%s++%s',GROUP03,GROUP04,GROUP05
{GROUP03}RFF+IT:%s',RFF+PM:%s',RFF+VA:%s',RFF+CRN:%s'
{GROUP04}DOC+380+:::%s'
{GROUP05}CTA+IC+1:%s',COM+%s:%s'
{GROUP07}CUX+1:%s+1:%s+%s',DTM+903:%s:102',CUX+1:%s+3:%s+%s',DTM+903:%s:102'
{GROUP22}LIN+%s++%s:SA',GROUP22IMD,QTY+107:%s:UNI',FTX+ADS+++%s:%s:%s',GROUP23,GROUP25,GROUP30
{GROUP22IMD}IMD++8+SP:::%s:%s',IMD++8+SN:::%s:%s',IMD++8+DNNUM::::%s',IMD++8+CHNN::::%s',IMD++8+CT::::%s',IMD++8+FEE::::%s',IMD++8+STATE::::%s'
{GROUP23}MOA+125:%s:%s::5',MOA+919:%s:%s::5',MOA+203:%s:%s::5'
{GROUP25}PRI+CAL:%s:INV'
{***GROUP30}TAX+1+VAT:::%s+++%s:::%s:%s::+%s',MOA+124:%s:%s::5'
{GROUP30}FTX+ATX+++%s'
{UNS}UNS+S'
{GROUP45}MOA+77:%s:%s::4',MOA+79:%s:%s::4',MOA+124:%s:%s::4'
{***GROUP47}TAX+3+VAT:::%s+++%s:::%s:%s::+%s',MOA+124:%s:%s::4'
{GROUP47}ALLTAXES
{UNT}UNT+%n+%s'
{UNZ}UNZ+1+%s'

!*
!* Data array offsets
!*
{UNB-IDX}H:1:2:3:4:5
{UNH-IDX}H:5
{BGM-IDX}C
{DTM-IDX}H:7,H:8
{GROUP01-IDX}H:12,H:13,H:14,H:15
{GROUP02A-IDX}H:16:17:18:19:20:21:22:23,H:24:25:26:27:28:29
{GROUP02B-IDX}H:30:31:1:33:34:35:36:37:38,H:78:1:80:81:82:83
{GROUP02C-IDX}H:39:40:41:42:43:44:45:46,GROUP03,GROUP04,GROUP05
{GROUP03-IDX}H:47,H:48,H:49,H:50
{GROUP04-IDX}H:51
{GROUP05-IDX}H:52,H:53:54
{GROUP07-IDX}H:55:55:57,H:58,H:55:59:61,H:62
{GROUP22-IDX}T:1:2,GROUP22IMD,T:12,T:13:14:15,GROUP23,GROUP25,GROUP30
{GROUP22IMD-IDX}T:3:4,T:5:6,T:7,T:8,T:9,T:10,T:11
{GROUP23-IDX}T:16:17,T:18:17,T:20:17
{GROUP25-IDX}T:22
{***GROUP30-IDX}T:23:24:25:26:27,T:28:17
{GROUP30-IDX}T:23
{UNS-IDX}C
{GROUP45-IDX}H:65:55,H:67:55,H:74:55
{***GROUP47-IDX}H:69:70:71:72:73,H:74:55
{GROUP47-IDX}C
{UNT-IDX}H:5
{UNZ-IDX}H:5

!*
!* This section contains offsets which will be used to correct
!* populate the edifact data structure. Used in class CBase_TBL_EDI
!*
!* Syntax:  <report item name>=<T (trailer), H: (header)>:<offset like above>
!* Sample:  OTMERCH=T:6 
!*          means the report item 'OTMERCH' has offset nr. 6 in trailer 
!*
!* Note: this section is used just as each windows *.INI file
!*

[OFFSETS]
;..UNB
NETWORKNAME=H:1
CSID=H:2
PREP_DATE=H:3
PREP_TIME=H:4
;..UNB & UNH
EDI_ICR=H:5
;..DTM
OHREFDATE=H:7
OHFULFILDATE=H:8
;..GROUP01
OHREFNUM=H:12
TMCODE=H:14
CONTRACTID=H:15
;..GROUP02A
CONLINE1=H:18
CONLINE2=H:19
CONLINE3=H:20
CONLINE4=H:21
CONLINE5=H:22
CONLINE6=H:23
;..FII
PAYACCNO=H:24
PAYBANKCODE=H:27
;..GROUP2B-NADII
;..NETWORKNAME=H:30
NETWORKADDR1=H:33
NETWORKADDR2=H:34
NETWORKADDR3=H:35
NETWORKCITY=H:37
NETWORKZIP=H:38
;..GROUP2B-RII
COMPBANKACCNO=H:78
COMPBANKCODE=H:81
COMPBANKNAME=H:82

;..NADIT
;..GROUP03
CONCUSTCODE=H:47
PAYMETH=H:48
COMPTAXNO=H:49

;..GROUP04
LANGUAGE=H:51
;..GROUP05
COMPPERSON=H:52
COMPCOM1=H:53
COMPTYPE1=H:54
;..GROUP07
HOMECURR=H:55
HOMECURRRATE=H:57
CONTCURR=H:59
CONTCURRRATE=H:61
CONTCURRDATE=H:62

;..GROUP22
ITEMNUMBER=T:1
ARTNAME=T:2
OTQORD=T:12
;..GROUP22IMD
SERVPSHDES=T:3
4
SERVICESHDES=T:5
6
DIRNUM=T:7
CHANNEL=T:8
FEETYPE=T:9

STATUS=T:11
VASPARAMS=T:13
;..GROUP23
OTMERCH=T:16
T_HOMECUR=T:17
OTGPRICE_DIS=T:18
OTMERCH_GROSS=T:20
;OTGPRICE=T:20

;..GROUP25
OTGPRICE=T:22
;..GROUP30
TAXCODES=T:23
; TAXNAME=T:23
; LEGALCODE=T:24
; TAXRATE=T:25
; OTEXTTAX=T:28
;..GROUP45
SUMTOTAL=H:65
SUMOTMERCH=H:67
SUMOTEXTTAX=H:74

;..GROUP47
;  SUMOTEXTTAX=H:74

[ENDOFFSETS]





