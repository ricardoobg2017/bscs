/**
 ** File        : Contract.brz
 **
 ** Copyright LHS Group Inc.
 ** 6 Concourse Parkway
 ** Suite 2700
 ** Atlanta, GA 30328
 ** USA
 **
 ** The copyright to the computer program(s) herein is the
 ** property of LHS Group Inc., USA
 ** The program(s) may be used and/or copied only with
 ** the written permission of LHS Group Inc. or
 ** in accordance with the terms and conditions stipulated
 ** in the agreement/contract under which the program(s) have
 ** been supplied.
 **
 ** File version: Targys/Product/Dev/Server/Common/Src/Resource/bscs_eur/contract.brz, , CMS_3.00_CON01, CMS_3.00_CON01_091217
 **/

/**
 **
 ** <pre>
 ** Revision History
 ** Date   		Rev		Author  		Description
 ** 99-08-20		1.0		Gregor Lorenz		Initial version
 ** 99-09-14		1.1		Ulrich Schaefer		Integrated in contract consistency check
 ** 99-09-24		1.2		Ulrich Schaefer		Initial AMP market integration
 ** 99-11-24		1.3		Ulrich Schaefer		Integration of ISDN, CCD, LLi, X25 markets
 ** 00-04-12		1.4		Ulrich Schaefer		Added new NMT and iDEN specific rules
 ** 00-06-15		1.5		Ulrich Schaefer		Improvements (performance, replace 'instanceof' by 'null' comparision)
 ** 00-08-25		1.6		Ulrich Schaefer		Defect TARG200003194, checking resource for external submarket
 ** 00-12-20		1.7		Ulrich Schaefer	  	Moved to bscs_eur because of Targys 2.0 and massive changes in resource handling (numbering plan)	
 ** 00-12-20		1.8		Ulrich Schaefer	  	Adapt to numbering plans
 ** 00-12-21		1.9		Renata Spencer		Remove check of rule ContractHasBasicService to GPRS only contracts
 ** 01-05-22		1.10		Ulrich Schaefer		Enabled check of rule ContractHasBasicService to GPRS only contracts, added check for contract status, onhold services are not considered
 ** 01-07-19		2.0		Ulrich Schaefer		Targys R3.0 initial version. First try of 201959	
 ** 01-07-27		2.1		Sergiu Burian		Add rule for prepaid contracts templates
 ** 01-07-28        	2.2   	Johnny Jauwerissa   	Added main directory number rules for MSISDN assignment method 4 (UMTS)
 ** 01-09-12       	2.3     	Johnny Jauwerissa   	Added CheckClassFunctionalities rule 
 ** 01-09-28        	2.4     	Johnny Jauwerissa   	Added ActiveContractHasActiveNetworkService rule
 ** 01-11-14        	2.5     	Renata Spencer   		Added CheckALSubscription rule
 ** </pre>
 **
 ** $Author: Ulrich Schaefer $
 ** $Revision: 1.7 $
 **
 ** 
 ** @see	com.lhsgroup.Contract.bscs_core.CustomerContract
 **
 */

import com.lhsgroup.Rulez.bscs_core.corba.*;
import com.lhsgroup.Rulez.bscs_core.*;
import com.lhsgroup.Product.bscs_core.corba.*;
import com.lhsgroup.framework.domain.corba.*;
import com.lhsgroup.framework.utilities.*;
import com.lhsgroup.Contract.bscs_core.corba.*;
import com.lhsgroup.Contract.bscs_core.ContractSettings;
import com.lhsgroup.Topology.bscs_core.corba.*;
import com.lhsgroup.Resource.bscs_eur.corba.PortI;
import com.lhsgroup.Resource.bscs_eur.corba.ResourceRegulationI;
import com.lhsgroup.Resource.bscs_core.corba.*;


/**
 * #TODO only for testing should be removed as soon as possible
*/ 
rule ContractPrecondition
{
	when {
		not TransactionContextI();
	}
	then {
		 System.err.println("No transaction context available!");
	}
}

/**
 * check whether a ContractedResourcesProperties instance is assigned to the contract
 */
rule ContractedResourcesPropertiesRequired
{
  when {
	?tc: TransactionContextI();
	?result: GenericResultTypeI();
//	?market: TechnologyI( getPrefixI().equals("GSM")); 
	not ContractedResourcesPropertiesI(); 
  } then {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),"Resource needed for contracted"));
  }
}

/**
 * check whether a ContractedResourcesProperties assigned to the contract
 * has a port.
 */
rule PortForContractedResourcesPropertiesRequired 
{
  when {
	?tc: TransactionContextI();
	?conResProp: ContractedResourcesPropertiesI( ?port: getPortI(?tc);?port==null );
	?result: GenericResultTypeI();

  } then {
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Port needed for Contract"));
  }
}

/**
 * check whether a network instance is assigned to the contract
 */
rule NetworkRequired
{
  when {  
	?tc: TransactionContextI();
	not NetworkI();
	?result: GenericResultTypeI();
  } then {
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"No network assigned."));
  }
}

/**
 * check whether exactly one network is assigned to the contract
 */
rule NetworkUniqueness
{
  when {

	?tc: TransactionContextI();
	?net1: NetworkI();
	?net2: NetworkI();

	?result: GenericResultTypeI();
	/*
 	 * should use comparision with equalsI because NetworkI might be 
	 * two different stubs pointing to same implemenation. As we have all
	 * packages in the server this should never happen, but the deployment
	 * might change in the future.
	 */
	evaluate ( !(?net1.equalsI(?net2)) );
	
  } then {
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Multiple networks assigned."));
  }
}

/**
 * check whether a HLR instance is assigned to the contract.
 * The HLR might be assigned directly to a ContractedResourcesPropertiesI or
 * indirectly through a resource. 
 */
rule HLRRequired
{
  when {
	?tc: TransactionContextI();
	not HLRI();
	?result: GenericResultTypeI();
  } then {
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"No HLR assigned."));
  }
}

/**
 * check whether exactly one HLR is assigned to the contract.
 * The HLR in different resources might differ or the directly assigned HLR
 * with HLR of a resource.
 */
rule HLRUniqueness
{
  when {

	?tc: TransactionContextI();
	?hlr1: HLRI();
	?hlr2: HLRI();

	?result: GenericResultTypeI();
	/*
 	 * should use comparision with equalsI because HLRI might be 
	 * two different stubs pointing to same implemenation. As we have all
	 * packages in the server this should never happen, but the deployment
	 * might change in the future.
	 */
	
	evaluate ( !(?hlr1.equalsI(?hlr2)) );
	
  } then {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Multiple HLRs assigned."));
  }
}



/**
 * check whether contract has a network service with directory number assigned!
 * precondition: ContractHasNetworkService
 * Note: removed the check for mandatory directory number because it became pretty senseless.
 * We can check that condition only if we have already a directory number because
 * otherwise we would not be able to get a virtual HLR which keeps the information
 * whether a directory number is mandatory or not.
 */
rule ContractHasBasicService
{
	when {
		?tc: TransactionContextI();
 		?contract: CustomerContractI( (?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc) !=null)) && ?this.getContractedCompositeProductI(?tc)!= null; ?this.getContractedCompositeProductI(?tc).isGPRSOnlyI(?tc)==false && ?this.getContractedCompositeProductI(?tc).isVASOnlyI(?tc)==false);
		?profile: ProfileI((?contract.getContractedCompositeProductI(?tc).existNonDummyAssignmentI() && ?this.getIDI()!=0) || (!?contract.getContractedCompositeProductI(?tc).existNonDummyAssignmentI());  ?profId: ?this.getIDI());
		
 		 /* we can not use the HLR to check for mandatory directory number any longer we need the virtual HLR
		 */
	// see note
	//	?dirNumber : DirectoryNumberI();
	//	?hlr: HLRI();


		/*
		 * at least one basic service must have a directory number if flag is set for HLR
		*/
		//not ContractedDNPropertiesI(getDirectoryNumberI(?tc)!=null;?service: NetworkServiceIHelper.narrow(getContractedProductElementI(?tc).getProductElementI(?tc).getProductElementTemplateI(?tc)); ?service!=null ; !?service.isSupplementaryI());		
		
		not ContractedDNPropertiesI(?this.getProfileI(?tc).getIDI() == ?profId || ?this.getProfileI(?tc).getIDI() == 0);		

		
		?result: GenericResultTypeI();
	// see note
	//	evaluate (?hlr.getVirtualHLRI(?tc,?dirNumber.getResourceRegulationI(?tc)).isDirectoryNumberMandatoryI(?tc));
	}
	then
	{
		?result.addI( RuleEngine.ruleViolation(?tc,?instance.getRule().getName(),"Contract must have at least one basic service! A directory number might be mandatory!"));	
	}
}

/**
 * check whether contract has an service at all
 */
rule ContractHasService
{
	when {
		?tc: TransactionContextI();
		?result: GenericResultTypeI();
		?contract: CustomerContractI((?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null)) && !(?this.getContractedCompositeProductI(?tc).isVASOnlyI(?tc)==true && ?this.getPendingStatusI(?tc)!=null && ?this.getPendingStatusI(?tc).getStateI()==ContractStatusI.Deactive) );



		
		not ProductElementI();
	}
	then 
	{
	 ?result.addI( RuleEngine.ruleViolation(?tc,?instance.getRule().getName(),"Contract must have at least one network service!"));	
	}
}
/**
 * check whether contract has a network service 
 */
rule ContractHasNetworkService
{
	when {
		?tc: TransactionContextI();
		?result: GenericResultTypeI();
		?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
		?profile: ProfileI((?contract.getContractedCompositeProductI(?tc).existNonDummyAssignmentI() && ?this.getIDI()!=0) || (!?contract.getContractedCompositeProductI(?tc).existNonDummyAssignmentI()); ?profId: ?this.getIDI());

		//PN211207: this rule is checking if a supplementary service is assigned, not only a network service.
		//not ContractedProductElementI(?service: NetworkServiceIHelper.narrow(?this.getProductElementI(?tc).getProductElementTemplateI(?tc));?service!=null;?service.isSupplementaryI();?profileId:?this.getProfileI(?tc).getIDI();(?profileId == 0 || ?profileId == ?profId));

		not ContractedProductElementI(?service: NetworkServiceIHelper.narrow(?this.getProductElementI(?tc).getProductElementTemplateI(?tc));?service!=null;?profileId:?this.getProfileI(?tc).getIDI();(?profileId == 0 || ?profileId == ?profId));

	}
	then 
	{
	 ?result.addI( RuleEngine.ruleViolation(?tc,?instance.getRule().getName(),"Contract must have at least one network service!"));	
	}
}

/**
 * check whether a active contract has at least one active network service 
 */
rule ActiveContractHasActiveNetworkService
{
	when {
		?tc: TransactionContextI();
		?result: GenericResultTypeI();
		?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Active && ?this.getPendingStatusI(?tc)==null && ?this.getContractedCompositeProductI(?tc).isVASOnlyI(?tc)==false);
		?profile: ProfileI((?contract.getContractedCompositeProductI(?tc).existNonDummyAssignmentI() && ?this.getIDI()!=0) || (!?contract.getContractedCompositeProductI(?tc).existNonDummyAssignmentI()); ?profId: ?this.getIDI());
		not ContractedProductElementI(?service: NetworkServiceIHelper.narrow(?this.getProductElementI(?tc).getProductElementTemplateI(?tc));?service!=null;!?service.isSupplementaryI();?this.getStateI(?tc)==2; ?this.getPendingStatusEntryI(?tc)==null);
	}
	then 
	{
	 ?result.addI( RuleEngine.ruleViolation(?tc,?instance.getRule().getName(),"A active contract must have at least one active network service!"));	
	}
}

/*
 * contract has basic service and service is tagged with HLR Method 0
 * in ruletab.
 * @precondition ContractHasStandardNetworkService
 */
rule ContractHasStandardNetworkService
{
	when {
		?tc: TransactionContextI();
		?result: GenericResultTypeI();
		?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));

		?dirNumber : DirectoryNumberI();
		?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==0);
		/*
		 * check whether a network service with HLR-Method0 tag in ruletab is in the contract
		*/	
		?profile: ProfileI((?contract.getContractedCompositeProductI(?tc).existNonDummyAssignmentI() && ?this.getIDI()!=0) || (!?contract.getContractedCompositeProductI(?tc).existNonDummyAssignmentI()); ?profId: ?this.getIDI());
		not ContractedProductElementI(?service: NetworkServiceIHelper.narrow(?this.getProductElementI(?tc).getProductElementTemplateI(?tc));?service!=null;?service.isStandardForHLRM0I(?tc);!?service.isSupplementaryI(); ?profileId:?this.getProfileI(?tc).getIDI();(?profileId == 0 || ?profileId == ?profId));
			
	}
	then 
	{
	 ?result.addI( RuleEngine.ruleViolation(?tc,?instance.getRule().getName(),"Contract must contain HLR-0 tagged, teleservice!"));	
	}
}

/**
 * check if MSISDN assignment method 0 is set and valid.
 * Only one MSISDN may be assigned to the contract.
 */ 
rule MSISDNAssignmentMethod_0
{
 when {
	?tc: TransactionContextI();
	?dirNumber : DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==0);
	?dir1 : DirectoryNumberI();
	?dir2 : DirectoryNumberI();
	?result: GenericResultTypeI();
	evaluate ( !(?dir1.equalsI(?dir2)) );
 }	
 then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"More than one directory number assigned to contract. MSISDN assignement method 0 violated!"));	
 }
}

/**
 * check if MSISDN assignment method 0 and at least one basic network service
 * must have a directory number,
 */ 
/*
NOTE: this rule doesn't make sense any longer with the new numbering plan 
handling, because to determine whether a directory number is mandatory or not
we have to get the directory number first (to access the numbering plan).
rule MSISDNAssignmentMethod_0_MandatoryMSISDN
{
 when 	{
	?tc: TransactionContextI();
	?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
	
	?dirNumber : DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==0;isDirectoryNumberMandatoryI(?tc,?dirNumber.getResourceRegulationI(?tc)));
	?result: GenericResultTypeI();
	not DirectoryNumberI();
 	 } 
 then
 	{
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),"No directory number assigned to contract althought HLR requires it. MSISDN assignement method 0 violated!"));	
 }	

}
*/

/**
 * check if MSISDN assignment method 1 is set and valid.
 * At least one basic Tele or Bearer service must be associated with a MSISDN. 
 */ 
rule MSISDNAssignmentMethod_1
{
 when {
	?tc: TransactionContextI();
	?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
	
	//#TODO improve this rule! It is not really meaningfull with the new
	// numbering plan feature
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==1);

	?result: GenericResultTypeI();
	?vProductElement: ProductElementI(?this.isResourceAssignableI(?tc,ResourceTypeI.directory_number);?lService: NetworkServiceIHelper.narrow(?this.getProductElementTemplateI(?tc));?lService!=null;!(?lService.isSupplementaryI()));

	not ContractedProductElementI(?this.getProductElementI(?tc).isResourceAssignableI(?tc,ResourceTypeI.directory_number);?lNetworkService: NetworkServiceIHelper.narrow(?this.getProductElementI(?tc).getProductElementTemplateI(?tc));?lNetworkService!=null;!(?lNetworkService.isSupplementaryI());?this.getResourcesByTypeI(?tc,ResourceTypeI.directory_number).sizeI()>0 );
 
 }	 
 then
 {
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"At least one basic Tele or Bearer service must be associated with a MSISDN. MSISDN assignement method 1 violated!"));
 }
}

/**
 * check if MSISDN assignment method 1 and all network service must have a mandatory directory number.
 */ 
rule MSISDNAssignmentMethod_1_MandatoryMSISDN
{
 when {
	?tc: TransactionContextI();
	
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==1;isDirectoryNumberMandatoryI(?tc,?dirNumber.getResourceRegulationI(?tc)));
	?result: GenericResultTypeI();

	ContractedProductElementI(?this.getProductElementI(?tc).isResourceAssignableI(?tc,ResourceTypeI.directory_number);?this.getResourcesByTypeI(?tc,ResourceTypeI.directory_number).sizeI()==0);
 }	
 then
 {
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"MSISDN is mandatory for each network service. MSISDN assignement method 1 violated!"));
 }
}

/**
 * check if MSISDN assignment method 1 and all network service must have a mandatory directory number, except such services which have ports and must
 * not have directory numbers assigned!
 * This method checks whether the ASSOCIATE_DN equals 'N' in BSCS 521 for 
 * a contracted service.
 */ 
rule MSISDNAssignmentMethod_1_MandatoryMSISDN_ISDN
{
 when {
	?tc: TransactionContextI();
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==1;isDirectoryNumberMandatoryI(?tc,?dirNumber.getResourceRegulationI(?tc)) );
	?result: GenericResultTypeI();

	ContractedProductElementI(?this.getProductElementI(?tc).isResourceAssignableI(?tc,ResourceTypeI.directory_number);?this.getResourcesByTypeI(?tc,ResourceTypeI.directory_number).sizeI()==0 );
 }	
 then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"MSISDN is mandatory for each network service. MSISDN assignement method 1 violated!"));
 }
}

/**
 * check if MSISDN assignment method 2 is set and valid.
 * One MSISDN must be marked as main MSISDN.
 * If no service is assigned this method will not fire.
 */ 
rule MSISDNAssignmentMethod_2
{
 when {
	?tc: TransactionContextI();
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==2);
	?result: GenericResultTypeI();
	
	not ContractedDNPropertiesI(isMainDirectoryNumberI());
 }	
 then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"One MSISDN must be marked as main!"));	
 }
}

/**
 * check if MSISDN assignment method 2 and all basic network service must have a mandatory directory number.
 */ 
rule MSISDNAssignmentMethod_2_MandatoryMSISDN
{
 when {
	?tc: TransactionContextI();
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==2;isDirectoryNumberMandatoryI(?tc,?dirNumber.getResourceRegulationI(?tc)));
	
	ContractedProductElementI(?this.getProductElementI(?tc).isResourceAssignableI(?tc,ResourceTypeI.directory_number);?this.getResourcesByTypeI(?tc,ResourceTypeI.directory_number).sizeI()==0);	
	
	?result: GenericResultTypeI();
 }	
 then
 {
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Mandatory MSISDN is missing!"));	
 }
}

/**
 * check if MSISDN assignment method 2 and all basic network service must have
 * a mandatory directory number, except such services which have ports and must
 * not have directory numbers assigned!
 * This method checks whether the ASSOCIATE_DN equals 'N' in BSCS 521 for 
 * a contracted service.
 */ 
rule MSISDNAssignmentMethod_2_MandatoryMSISDN_ISDN
{
 when {
	?tc: TransactionContextI();
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==2;isDirectoryNumberMandatoryI(?tc,?dirNumber.getResourceRegulationI(?tc)));
	?result: GenericResultTypeI();
	
	ContractedProductElementI(?this.getProductElementI(?tc).isResourceAssignableI(?tc,ResourceTypeI.directory_number);?this.getResourcesByTypeI(?tc,ResourceTypeI.directory_number).sizeI()==0);	

 }	
 then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Mandatory MSISDN is missing!"));	
 }
}


/**
 * check if MSISDN assignment method 4 and a non dummy profiles has maximal only one Main directory number
 */
rule MSISDNAssignmentMethod_4_MaxOneMainDirectoryNumberPerNonDummyProfile
{
 when 
  {
	?tc: TransactionContextI();
	?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==4);
	?profile: ProfileI(?this.getIDI()!= 0; ?profileID:?this.getIDI());
	
	?contrDNProperty1: ContractedDNPropertiesI(getProfileI(?tc).getIDI()==?profileID; isMainDirectoryNumberI());
	?contrDNProperty2: ContractedDNPropertiesI(getProfileI(?tc).getIDI()==?profileID; isMainDirectoryNumberI(); equalsI(?contrDNProperty1)==false);
	
	?result: GenericResultTypeI();
  }	
 then
 {

	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(), "At most one MSISDN must be marked as main directory number. MSISDN assignement method 4 violated!"));	
 }
}


/**
 * check if MSISDN assignment method 4 and a non dummy profiles must have at least one Main directory number.
 */

rule MSISDNAssignmentMethod_4_MinOneMainDirectoryNumberPerNonDummyProfile
{
 when 
  {
	?tc: TransactionContextI();
	?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==4);
	?profile: ProfileI(?this.getIDI()!= 0; ?profileID:?this.getIDI());
	
	not ContractedDNPropertiesI(getProfileI(?tc).getIDI()==?profileID; isMainDirectoryNumberI());
			  
	?result: GenericResultTypeI();
  }	
 then
 {

	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(), "At least one MSISDN must be marked as main directory number. MSISDN assignement method 4 violated!"));	
 }
}


/**
 * check if MSISDN assignment method 4 and a dummy profiles can have 0 or maximal 1 Main directory number
 */
rule MSISDNAssignmentMethod_4_MaxOneMainDirectoryNumberPerDummyProfile
{
 when 
  {
	?tc: TransactionContextI();
	?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==4);
	?profile: ProfileI(?this.getIDI() == 0; ?profileID:?this.getIDI());
	
	?contrDNProperty1: ContractedDNPropertiesI(getProfileI(?tc).getIDI()==?profileID; isMainDirectoryNumberI());
	?contrDNProperty2: ContractedDNPropertiesI(getProfileI(?tc).getIDI()==?profileID; isMainDirectoryNumberI(); ! ?this.equalsI(?contrDNProperty1));
	
	?result: GenericResultTypeI();
  }	
 then
 {

	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(), "At most one MSISDN must be marked as main directory number. MSISDN assignement method 4 violated!"));	
 }
}


/**
 * check if MSISDN assignment method 4 and wheteher all profiles have too much main directory number
 */
rule MSISDNAssignmentMethod_4_ToMuchMainDirectoryNumberInAllProfiles
{
 when 
  {
	?tc: TransactionContextI();
	?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==4);
	?profile: ProfileI(?this.getIDI() != 0; ?profileID:?this.getIDI());
	ContractedDNPropertiesI(getProfileI(?tc).getIDI()==0; isMainDirectoryNumberI());
	ContractedDNPropertiesI(getProfileI(?tc).getIDI()==?profileID);
	?result: GenericResultTypeI();
  }	
 then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(), "There are too much main directory numbers. MSISDN assignement method 4 violated!"));	
 }
}

/**
 * check if the level of each network service functionality is smaller or equal than the level of
 * storage medium functionality in a active contract or onhold contract with pending request.
 * 
 */
rule CheckClassFunctionalities
{
	when {
		?tc: TransactionContextI();
		?result: GenericResultTypeI();
		?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
		StorageMediumI(?smt: StorageMediumTemplateIHelper.narrow(?this.getResourceTemplateI(?tc)); ?classFunctionaltyList: ?smt.getClassFunctionalityParametersI(?tc)); 
		//StorageMediumTemplateI(?classFunctionaltyList: ?this.getClassFunctionalityParametersI(?tc));
		ContractedProductElementI(?service: NetworkServiceIHelper.narrow(?this.getProductElementI(?tc).getProductElementTemplateI(?tc));?service!=null;!?service.isSupplementaryI(); !?service.isSubsetOfSmcClassFunctionalitiesI(?tc,?classFunctionaltyList));
	}
	then 
	{
	 ?result.addI( RuleEngine.ruleViolation(?tc,?instance.getRule().getName(),"Storage medium class must support all class functionalities of contracted network services!"));	
	}
}


/**
 * check if basic network services configured to have a mandatory directory number has directory number
 */ 
rule MandatoryMSISDN
{
 when {
	?tc: TransactionContextI();
	
	ContractedProductElementI(?this.getProductElementI(?tc).isResourceAssignableI(?tc,ResourceTypeI.directory_number); getProductElementI(?tc).getProductElementTemplateI(?tc).isResourceMandatoryI(?tc,ResourceTypeI.directory_number) ;?this.getResourcesByTypeI(?tc,ResourceTypeI.directory_number).sizeI()==0);	
	?result: GenericResultTypeI();

 }	
 then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Mandatory MSISDN is missing!"));	
 }
}

/**
 * check if basic network services configured to have a mandatory port has port(s)
 */ 
rule MandatoryPort
{
 when {
	?tc: TransactionContextI();
	
	ContractedProductElementI(?this.getProductElementI(?tc).isResourceAssignableI(?tc,ResourceTypeI.port);getProductElementI(?tc).getProductElementTemplateI(?tc).isResourceMandatoryI(?tc,ResourceTypeI.port);?this.getResourcesByTypeI(?tc,ResourceTypeI.port).sizeI()==0 );	
	
	?result: GenericResultTypeI();

 }	
 then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Mandatory Port is missing!"));	
 }
}


/**
 * check if MSISDN assignment method 2 is set.
 * If so, check whether only one MSISDN must be marked as main!
 */ 
rule MainMSISDNUniqueness
{
 when {
	?tc: TransactionContextI();
	?dirNumber: DirectoryNumberI();
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==2);

	?prop1 : ContractedDNPropertiesI(isMainDirectoryNumberI());
	?prop2 : ContractedDNPropertiesI(isMainDirectoryNumberI());
	?result: GenericResultTypeI();
	/*
 	* comparision with '!=' instead of equalsI is ok, since 
	* never two reference in the server can point to the same implemenation
	*/	
	evaluate (?prop1 != ?prop2);
 }	
 then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Only one MSISDN could be marked as main!"));	
 }
}

/**
 * check count of port against limits
 */
rule PortLimits
{
 when {
	?tc : TransactionContextI();
	?product: CompositeProductI( getPortLimitI() != NullConversion.INT_NULL_VALUE);
	?result: GenericResultTypeI();
     }
	then {
	 System.out.println("Check port limit ..."); 
	}
} 

/**
 * check whether HLR supports MSIDSN method 0 otherwise rule failed.
 * This rule should be used to enforce the assignment of HLRs with 
 * assignment method 0. 
 */
rule HLRMSISDNMethod0Only
{
 when {

	?tc: TransactionContextI();
 	?dirNumber: DirectoryNumberI();		
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))!=0);
	
	?result: GenericResultTypeI();

  } then
 {
	 ?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"HLR does not support MSISDN assignment method 0"));
  }
}

/**
 * check whether HLR supports MSIDSN method 1 otherwise rule failed.
 * This rule should be used to enforce the assignment of HLRs with 
 * assignment method 1. 
 */
rule HLRMSISDNMethod1Only
{
 when {

	?tc: TransactionContextI();
 	?dirNumber: DirectoryNumberI();		
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))!=1);
	
	?result: GenericResultTypeI();

  } then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"HLR does not support MSISDN assignment method 0"));
  }
}

/**
 * check whether HLR supports MSIDSN method 2 otherwise rule failed.
 * This rule should be used to enforce the assignment of HLRs with 
 * assignment method 2. 
 */
rule HLRMSISDNMethod2Only
{
 when {

	?tc: TransactionContextI();
 	?dirNumber: DirectoryNumberI();		
	?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))!=2);
	
	?result: GenericResultTypeI();

  } then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"HLR does not support MSISDN assignment method 2"));
  }
}


/**
 * check whether link between port (on device level) and directory number exists.
 * This check is needed for the NMT market.
 * If contract is onhold and not activated the former checks for services (standard
 * and network service existence) will succeedd, although not assigned to contract.
 * For NMT this rule will fail if no service was assigned in onhold status.
 * @author Ulrich Schaefer
 */
rule CheckLinkPortDirectoryNumber
{
 when {

	?tc: TransactionContextI();
	?port: PortI();
	not DirectoryNumberI(?port.getDirectoryNumberI(?tc).equalsI(?this));
	 
	
	?result: GenericResultTypeI();

  } then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Directory number linked to port is not assigned to contract!"));
  }
}

/**
 * Check whether HLR and Network are compatible.
 * @author Ulrich Schaefer
 */
rule CheckHLRNetworkCompatibility
{
 when {

	?tc: TransactionContextI();
	?hlr: HLRI();
	not NetworkI(?hlr.getNetworkI(?tc).equalsI(?this));
	
	?result: GenericResultTypeI();

  } then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Illegal HLR assigned to contract!"));
  }
}

/*
 * each resource type is checked explicitly for 
 * consistency with the external issuer settings in the 
 * submarket. 
 * This could be done in one single rule, but would have 
 * worser performance. (check for resource if the submarket settings
 * of its type is external).
 */
/**
 * Check whether all MSISDNs are marked external,
 * if the assigned submarket has a external directory number issuer.
 * @author Ulrich Schaefer
 */
rule CheckMSISDNinExternalSubmarket
{
 when {

	?tc: TransactionContextI();
	?dirNumber: DirectoryNumberI();
	?submarket: TelecomTechnologyFlavourI(isExternalI(?tc,ResourceTypeI.directory_number,?dirNumber.getResourceRegulationI(?tc)));
	
	?result: GenericResultTypeI();
	evaluate( ! ?dirNumber.isExternalI(?tc));
  } then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Only external MSISDN allowed for contract's submarket."));
  }
}

/**
 * Check whether all Ports are marked as external,
 * if the assigned submarket has a external port issuer.
 * @author Ulrich Schaefer
 */
rule CheckPortinExternalSubmarket
{
 when {

	?tc: TransactionContextI();
	?port: com.lhsgroup.Resource.bscs_eur.corba.PortI();
	?submarket: TelecomTechnologyFlavourI(isExternalI(?tc,ResourceTypeI.port,?port.getResourceRegulationI(?tc)));

	?result: GenericResultTypeI();
	evaluate( ! ?port.isExternalI(?tc));
  } then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Only external Ports allowed for contract's submarket."));

  }
}

/**
 * Check whether all StorageMediums are marked as external,
 * if the assigned submarket has a external storage medium issuer.
 * @author Ulrich Schaefer
 */
rule CheckStorageMediuminExternalSubmarket
{
 when {

	?tc: TransactionContextI();
	?submarket: TelecomTechnologyFlavourI(isExternalI(?tc,ResourceTypeI.storage_medium,null));
	?storagemedium: StorageMediumI(!isExternalI(?tc));

	?result: GenericResultTypeI();

  } then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),
			"Only external StorageMediums allowed for contract's submarket."));

  }
}

/**
 * Check whether a port assigned to the contract is linked with 
 * any directory number.
 * This rule is checked for AMPS market in a pre MDN/MIN split scenario
 * @author Ulrich Schaefer
 */
rule CheckPortDNLinkExistence 
{
 when {

	?tc: TransactionContextI();
	HLRI(?this.getPortOwnerI()==HLRI.DirectoryNumberOwnsPort);
	?port: PortI(?this.getDirectoryNumberI(?tc)==null);
	
	?result: GenericResultTypeI();

  } then
 {
	?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(), "Port is not linked to a directory number."));
}
}

/**
 * Check whether a port assigned to the contract is linked with 
 * any directory number, that is assigned to the contract.
 * This rule is checked for AMPS market in a pre MDN/MIN split scenario
 * @author Ulrich Schaefer
 * @precondition CheckPortDNLinkExistence
 */
rule CheckPortDNLink 
{
	 when {

		?tc: TransactionContextI();
		HLRI(?this.getPortOwnerI()==HLRI.DirectoryNumberOwnsPort);
		?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
		?port: PortI();
		not DirectoryNumberI(?port.getDirectoryNumberI(?tc).equalsI(?this));
		
		?result: GenericResultTypeI();

	  } 
	  then
	  {
		?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(), "Directory number linked to the Port is not assigned to the contract."));
	  }
}

/**
 * Check whether the directory number marked as main is linked to the port, which is assigned 
 * to the current instance of ContractedResourcesProperties. 
 * @author Ulrich Schaefer
 * @precondition CheckPortDNLinkExistence
 */
rule CheckPortDNLinkForMSISDNAssignmentMethod_2 
{
	 when {

		?tc: TransactionContextI();
		HLRI(?this.getPortOwnerI()==HLRI.DirectoryNumberOwnsPort);
		?port: PortI();
		?dirNumber: DirectoryNumberI(?port.getDirectoryNumberI(?tc).equalsI(?this));
		?contractedDNProperties : ContractedDNPropertiesI(?this.getDirectoryNumberI(?tc).equalsI(?dirNumber));

		?hlr: HLRI(getMSISDNAssignmentMethodI(?tc,?dirNumber.getResourceRegulationI(?tc))==2);
		evaluate( ! ?contractedDNProperties.isMainDirectoryNumberI());

		?result: GenericResultTypeI();
		
	  } 
	  then
	  {
		?result.addI( RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(), "Directory number marked as main is not linked to the Port."));
	  }
}
/**
 * This rule is for prepaid contract templates. These special contracts
 * must have NO resources assigned.
 * US & SB : 13-Jul-01, 202820_3 Preactivated Prepaid cards
 */
rule PrepaidContractTemplateHasNoResources
{
 when {
 		?tc: 	TransactionContextI();
 		?result:	GenericResultTypeI();
 		?res:	ResourceI();
 	 } 	 
 then {
 		?result.addI(RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),"Preactivated contract templates must have no resources assigned"));
 	 }
}

/**
 * This rule is for administrative contracts in the INS market. These special contracts
 * must have a VPN subscriber service assigned.
 */
rule CheckVPNSubscriberService
{
 when {
 		?tc: 	TransactionContextI();
 		?result:	GenericResultTypeI();
 		?contract: CustomerContractI(?this.getCurrentStatusI(?tc).getStateI()!=ContractStatusI.Onhold || (?this.getCurrentStatusI(?tc).getStateI()==ContractStatusI.Onhold && ?this.getPendingStatusI(?tc)!=null));
		not ProductElementI(?service: NetworkServiceIHelper.narrow(?this.getProductElementTemplateI(?tc)); ?service!=null ; ?service.isVPNAdministratorI(?tc));
 	 } 	 
 then {
 		?result.addI(RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),"VPN subscriber contracts must have subscriber service assigned."));
 	 }
}

/**
 * This rule is for alternate line subscriptions.
 * The main contract in an AL subscription must have a telephony service.
 */
rule CheckALSubscription
{
 when {
 		?tc: 	TransactionContextI();
 		?result:	GenericResultTypeI();
 		?contract: CustomerContractI(?this.isMainContractI());
		not ProductElementI(?service: NetworkServiceIHelper.narrow(?this.getProductElementTemplateI(?tc)); ?service!=null ; ?service.isTeleI() ; ?service.getTeleCodeI().equals("T11"));
 	 } 	 
 then {
 		?result.addI(RuleEngine.ruleViolation(?tc, ?instance.getRule().getName(),"The main contract in an AL subscription must have Telephony service."));
 	 }
}


/** 
 * if a technology is not supported by the Targys server the following 'default' 
 * rule is used
 */
rule NOT_IMPLEMENTED {
when {
	?technology: TechnologyI();
} then {	

		System.out.println("Rules for "+ ?technology.getPrefixI() +" haven't been implemented yet");
        
}};


