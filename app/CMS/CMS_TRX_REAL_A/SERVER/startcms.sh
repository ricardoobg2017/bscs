#!/bin/sh
# ****************************************************
# * starter for CM Server
# *
# * change the first three lines below to suit your needs
# * (ORA_VER matches to your oracle version installed)
# *
# ****************************************************

JDKHOME=/usr/jdk1.6.0_31
#ORA_VER=817
ORA_VER=112
SERVER_NAME=your_targys_server

# ****************************************************
# * no changes below this line
# ****************************************************

MYPATH=lib/hotfixes:lib/vbjorb.jar:lib/migration.jar:lib/xml.jar:lib/vbsec.jar

if [ $ORA_VER = 817 ]
then
	MYPATH=$MYPATH:lib/Oracle-8.1.7-JDBC.jar
else
#	MYPATH=$MYPATH:lib/Oracle-9.i-JDBC.jar
	MYPATH=$MYPATH:lib/Oracle-11.2.0.1-JDBC-JDK1.6.jar
fi

MYPATH=$MYPATH:lib/TOPLink-3.5.jar:lib/etie.jar:lib/lm.jar:lib/ILOG.jar:lib/cms.jar:lib/cms_s.jar:lib/cms_cs.jar:lib/sfw.jar:lib/sfw_s.jar:lib/sfw_cs.jar:lib/fwcmn.jar:Resource:Resource/NLS:.

$JDKHOME/jre/bin/java  -Djava.util.logging.config.file=Resource\logging.properties  -Doracle.jdbc.V8Compatible=true -Xms100m -Xmx500m -cp $MYPATH com.lhsgroup.CustomerManagementServer.bscs_core.SLServer -dispenser $SERVER_NAME