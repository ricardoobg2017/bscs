rem ****************************************************
rem * starter for SL Client
rem *
rem * change the first three lines to suit your needs
rem *
rem ****************************************************

set JDK_HOME=C:\jdk1.3.1
set SERVER_NAME=SL_cms_Daniel

rem ****************************************************
rem * no changes below this line
rem ****************************************************

set OLD_CLASSPATH=%CLASSPATH%

set CLASSPATH=lib\hotfixes
set CLASSPATH=%CLASSPATH%;%JDK_HOME%\jre\lib\Dealerstation_deployment.jar
set CLASSPATH=%CLASSPATH%;lib/jhall.jar
set CLASSPATH=%CLASSPATH%;lib/xml.jar
set CLASSPATH=%CLASSPATH%;lib/sfw.jar
set CLASSPATH=%CLASSPATH%;lib/sfw_s.jar
set CLASSPATH=%CLASSPATH%;lib/sfw_cs.jar
set CLASSPATH=%CLASSPATH%;lib/jhall.jar
set CLASSPATH=%CLASSPATH%;lib/fwcmn.jar
set CLASSPATH=%CLASSPATH%;Resource
set CLASSPATH=%CLASSPATH%;Resource/NLS
set CLASSPATH=%CLASSPATH%;lib/vbjorb.jar;lib/migration.jar;.


%JDK_HOME%\jre\bin\java.exe -cp %CLASSPATH% com.semagroup.targys.client.test.servicelayer.SLClient %SERVER_NAME% WPP WPP TARGYS_SL_V_2_0

set CLASSPATH=%OLDCLASSPATH%
