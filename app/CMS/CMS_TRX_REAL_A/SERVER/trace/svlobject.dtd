<!-- This DTD describes the structure of command trace files.
 	 Traces contain a number of SL commands, that can be executed
 	 from a batch client using such a trace as input. 
 	 
 	 Traces can either be written by human beings or generated from
 	 a command tracing facility. The tracing facility writes the commands 
 	 received by the Service Layer to a specified trace file.  
-->


<!ELEMENT LIST (SVLOBJECT*)>
<!ATTLIST LIST
	name CDATA #REQUIRED
>

<!ELEMENT SVLOBJECT ((CHAR|STRING|INTEGER|LONG|DATE|DATETIME|MONEY|BOOLEAN|FLOAT|BINARY|LIST)*)>

<!ELEMENT CHAR EMPTY>
<!ATTLIST CHAR
	name CDATA #REQUIRED
	val	CDATA #REQUIRED
>

<!ELEMENT STRING EMPTY>
<!ATTLIST STRING
	name CDATA #REQUIRED
	val	CDATA #REQUIRED
>

<!ELEMENT INTEGER EMPTY>
<!ATTLIST INTEGER
	name CDATA #REQUIRED
	val	CDATA #REQUIRED
>

<!ELEMENT LONG EMPTY>
<!ATTLIST LONG
	name CDATA #REQUIRED
	val	CDATA #REQUIRED
>

<!-- see comment of TRACE element -->
<!ELEMENT DATE EMPTY>
<!ATTLIST DATE
	name CDATA #REQUIRED
	utc	CDATA #REQUIRED
	timezone CDATA #REQUIRED
>

<!-- see comment of TRACE element -->
<!ELEMENT DATETIME EMPTY>
<!ATTLIST DATETIME
	name CDATA #REQUIRED
	utc	CDATA #REQUIRED
	timezone CDATA #REQUIRED
>

<!ELEMENT MONEY EMPTY>
<!ATTLIST MONEY
	name CDATA #REQUIRED
	amount	CDATA #REQUIRED
	currency CDATA #REQUIRED
>

<!ELEMENT BOOLEAN EMPTY>
<!ATTLIST BOOLEAN
	name CDATA #REQUIRED
	val	(T|F) #IMPLIED
>

<!ELEMENT FLOAT EMPTY>
<!ATTLIST FLOAT
	name CDATA #REQUIRED
	val	CDATA #IMPLIED
>

<!ELEMENT BINARY (#PCDATA)>
<!ATTLIST BINARY
	name CDATA #REQUIRED
	val	CDATA #IMPLIED
>
