<?xml encoding="UTF-8"?>

<!-- 	
Defines the structure of the ErrorDictionary used within the Service Layer.
Comments immediately preceding an Element are associated with the element for 
documentation purposes. Comments immediately following such elements are used 
for internal documentation.
The verboseID contains a short identifier that can be used for 
 -->
<!ELEMENT ErrorDictionary (ErrorDefinition*, Folder*)>
<!ELEMENT ErrorDefinition (AltName, Arg*)>
<!ATTLIST ErrorDefinition
	name ID #REQUIRED
>

<!-- 
Specifies a human understandle name of an error. It is the first
element within an ErrorDefinition. Basically, there are two disjoint
namespaces for error codes. One contains abstract error codes used for 
referring to errors from within the Service Layer implentation 
(i.e. ErrorDefintiion@name), while AltName@name is additionally used 
for documentation purposes, online indices, search functions, etc.
This element is necessary, since XML does not allow an Element to have two
ID attributes, so this workaround is used, since ErrorDefinition@altName would
not work if also defined as an ID attribute. 
-->
<!ELEMENT AltName EMPTY>
<!ATTLIST AltName
	name ID #REQUIRED
>

<!--
Each argument of an error is represented by an Arg element. The name
attribute is only used in order to provide a way to refer to it, instead
of using semantically poor numbers. References to a particular argument 
for instance occur in the list of applicable errors within a Command 
Description File (CDF). Nonetheless, the ORDER of an ErrorDefinition's 
Arg children IS IMPORTANT, since the resource property files used for
internationalization refer to the ordinal number of an Arg w.r.t. the
Arg definiton. Ordinal numbers start with 1.
-->
<!ELEMENT Arg EMPTY>
<!ATTLIST Arg
	name CDATA #REQUIRED 
>

<!-- 
Folder elements are used for a hierarchical organization of errors. 
The Folder containing a particular ErrorDefinition is of no interest
to the users of the error. That is no sematics are associated with a 
Folder. It is only used for structuring the dictionary and for generating
navigation elements (trees) for HTML clients, respectively Table of contents
for print-documentation.
--> 
<!ELEMENT Folder ((Folder | ErrorDefinition)*)>
<!ATTLIST Folder
	name CDATA #REQUIRED
>