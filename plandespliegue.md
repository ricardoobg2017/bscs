Cuando se solicita un pase en CMS, considerar

1.- En producción, son 3 servidores y en cada uno hay 3 CMS, detallados a continuación:

-------------------------------------------------------
| IP                              | Rutas             |
|---------------------------------|-------------------|
|                                 | C:\CMS_TRX_REAL_A |
| 130.2.120.10 (Server Windows)   | C:\CMS_TRX_REAL_B |
|                                 | C:\CMS_TRX_REAL_C |
|---------------------------------|-------------------|
|                                 | C:\CMS_TRX_REAL_D |
| 130.2.120.11 (Server Windows)   | C:\CMS_TRX_REAL_E |
|                                 | C:\CMS_TRX_REAL_F |
|---------------------------------|-------------------|
|                                 | C:\CMS_TRX_REAL_X |
| 130.2.120.12 (Server Windows)   | C:\CMS_TRX_REAL_Y |
|                                 | C:\CMS_TRX_REAL_Z |
-------------------------------------------------------